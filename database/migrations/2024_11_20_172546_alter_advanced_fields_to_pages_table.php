<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        try {
            Schema::table('pages', function (Blueprint $table) {
                $table->boolean('fl_hide_sitemap')->default(false);
                $table->boolean('fl_no_indexable')->default(false);
                $table->boolean('fl_lock_page')->default(false);
            });
        } catch (\Exception $e) {
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn(['fl_hide_sitemap', 'fl_no_indexable', 'fl_lock_page']);
        });
    }
};
