window.modalBlockBuilder = function (event, id, parentBlockId = null) {
    event.preventDefault();
    event.stopPropagation();

    const iframe = document.querySelector('.modal-block-builder iframe');

    iframe.removeAttribute('src');
    iframe.setAttribute(
        'src',
        id
            ? iframe.dataset.editSrc.replace('@ID@', id)
            : iframe.dataset.createSrc.replace('@PARENT_BLOCK_ID@', parentBlockId)
    );

    document.querySelector('.modal-block-builder').classList.remove('hidden');
};

window.modalBlockBuilderPage = function (event, id) {
    event.preventDefault();
    event.stopPropagation();

    const iframe = document.querySelector('.modal-block-builder iframe');

    iframe.removeAttribute('src');
    iframe.setAttribute('src', iframe.dataset.pageSrc.replace('@ID@', id));

    document.querySelector('.modal-block-builder').classList.remove('hidden');
};

window.modalBlockBuilderClose = function (reload = false) {
    if (reload && window['Livewire']) {
        Livewire.emit('listChildrenRefresh'); 
        
        // Admin
        document.querySelector('.modal-block-builder').classList.add('hidden');
        document.querySelector('.modal-block-builder iframe').removeAttribute('src');
    } else if (reload) {
        window.location.reload();
    } else {
        // Admin
        document.querySelector('.modal-block-builder').classList.add('hidden');
        document.querySelector('.modal-block-builder iframe').removeAttribute('src');
    }
};

window.toggleBlockBuilderEdit = function () {
    document.querySelectorAll('.blockbuilder-edit').forEach(function (elem) {
        elem.classList.toggle('hidden');
    });
    document.querySelector('.btn-blockbuilder-show-hidden').classList.toggle('desactive');
};
