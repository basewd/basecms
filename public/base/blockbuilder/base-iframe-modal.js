function autosizeIframeModal(strElem) {
    var elem = document.querySelector(strElem);

    if (
        elem.contentWindow &&
        elem.contentWindow.document &&
        elem.contentWindow.document.body
    ) {
        var newheight =
            elem.contentWindow.document.body.scrollHeight;

        var offset = 30; // Offset 28;

        var heightElem = parseInt(elem.style.height);

        if (
            (heightElem + offset > newheight &&
                heightElem < newheight + offset) ||
            window.innerHeight == window.innerHeight - offset
        ) {
            return;
        }

        if (window.innerHeight < newheight) {
            newheight = window.innerHeight - offset;
        }

        elem.style.height = newheight + 'px';
    }
}