var forms = document.getElementsByClassName('needs-validation');

Array.prototype.filter.call(forms, function (form) {
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopPropagation();

        var self = this;

        if (self.blocked) {
            return;
        }

        //workaround validation input
        $("input[type='file'").attr('touched', 'true');

        updateErrorLabels();

        //Prevent submit if exist error
        var aireElement = window['$aire' + $(form).data('aire-id')];
        if (aireElement) aireElement.run();

        if (self.timeoutAire) {
            clearTimeout(self.timeoutAire);
        }

        //Alguns elementos não estavam sendo validados (aireElement.data não estava com os valores do form).
        //Na classe interna do aire tem um debouce O.O
        self.timeoutAire = setTimeout(function () {
            if (aireElement && !aireElement.valid) {
                toastr.error(window.Validator.getMessages(Validator.getDefaultLang()).required_fields);

                //Workaround p/ forçar erro nos inputs
                $('form :input').trigger('focus');
                $('form :input').trigger('blur');

                aireElement.run();

                var inputErrors = Object.keys(aireElement.validator.errors.errors);

                console.warn(inputErrors);

                $('html, body').animate(
                    {
                        scrollTop: $('[name=' + inputErrors[0] + ']').offset().top - 150,
                    },
                    500
                );

                return;
            }

            self.submit();
        }, 300);

        //Bloqueio de multiplos cliques
        if (self.timeoutBlocked) {
            clearTimeout(self.timeoutBlocked);
        }

        self.timeoutBlocked = setTimeout(function () {
            self.blocked = false;
        }, 2000);

        self.blocked = true;
    });
});

function updateErrorLabels() {
    if (window['Validator']) {
        //Replace :attribute pelo texto do Label (base-aire-locale.js), reference: https://github.com/skaterdav85/validatorjs
        Validator.setAttributeFormatter(function (attribute) {
            var id = $(":input[name='" + attribute + "']").attr('id');
            if (!id) {
                id = attribute;
            }

            var label = $("label[for='" + id + "']");

            if (label.length > 0) {
                return label.text().toLowerCase();
            } else {
                return attribute;
            }
        });
    }
}

function fixReadonlyCheckbox() {
    $("input[type='checkbox'][readonly]").each(function () {
        if ($(this).attr('id')) {
            $("label[for='" + $(this).attr('id') + "']").addClass('readonly');
        }
    });
}

$(document).ready(function () {
    updateErrorLabels();

    fixReadonlyCheckbox();
});
