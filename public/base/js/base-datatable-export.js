function datatableExport(tableId) {
    $('.base-datatable-export-' + tableId)
        .find('.btn')
        .click(function (e) {
            var selectorTable = '#' + tableId;

            e.preventDefault();
            e.stopPropagation();

            var datatable = $(selectorTable).DataTable();

            //Extract data do DataTable p/ enviar na chamada
            var data = datatable.settings()[0].oApi._fnAjaxParameters(datatable.settings()[0]);

            setDatatableFilterData(selectorTable, data, false);

            var url = $(this).data('href') + ($(this).data('href').indexOf('?') == -1 ? '?' : '&') + $.param(data);

            window.open(url);
        });
}
