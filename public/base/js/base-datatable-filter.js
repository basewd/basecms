/**
 * Implementação do BASE CUSTOM FILTER (baseCustomFilter do Datatable / Laravel)
 *
 * TODO: Converter em classe p/ isolar as funções.
 */

function setupDatatableFilter(selectorTable, hash) {
    var container = getParentDatatable(selectorTable);

    $(selectorTable).data('hash', hash);

    var fields = container.find('.fields');
    var filterContent = container.find('.filter-content');

    //Change select, new filter block
    fields.on('change', function () {
        filterContent.addClass('d-none');
        filterContent.find('.filter-type').addClass('d-none');

        if ($(this).val()) {
            filterContent.removeClass('d-none');

            var field = fields.find('option:selected');

            //Enum recebe tratamento especial
            if (filterContent.find('.filter-enum-' + field.val()).length > 0) {
                filterContent.find('.filter-enum-' + field.val()).removeClass('d-none');

                //Esconde o input qdo for enum
                filterContent.find('.filter-text input').addClass('d-none');
            } else {
                filterContent.find('.filter-text input').removeClass('d-none');
            }

            filterContent.find('.filter-' + field.data('type')).removeClass('d-none');
        }
    });

    filterContent.find('.filter-date select').change(function () {
        var inputFilterTypeDate = $(this).parent().find('input');

        if ($(this).val() == 'exist' || $(this).val() == 'not_exist') {
            inputFilterTypeDate.addClass('d-none');
        } else {
            inputFilterTypeDate.removeClass('d-none');
        }
    });

    filterContent.find('.filter-int select').change(function () {
        if ($(this).val() == 'in') {
            filterContent.find('.filter-int small').removeClass('d-none');
        } else {
            filterContent.find('.filter-int small').addClass('d-none');
        }
    });

    filterContent.find('input').on('keyup', function (event) {
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if (keyCode == '13') {
            btnAddClick(selectorTable, container);
        }
    });

    filterContent.find('.btn-add').on('click', function () {
        btnAddClick(selectorTable, container);
    });
}

function btnAddClick(selectorTable, container) {
    var fields = container.find('.fields');
    var filterData = container.find('.filter-data');
    var filterContent = container.find('.filter-content');
    var form = container.find('.datatable-filter form');

    var field = fields.find('option:selected');

    var filterTypeOption = filterContent.find('.filter-' + fields.find('option:selected').data('type'));

    var inputText = filterTypeOption.find('input').val();
    if (filterContent.find('.filter-enum-' + field.val()).length > 0) {
        inputText = filterContent
            .find('.filter-enum-' + field.val() + ' option:selected')
            .val()
            .toLowerCase();
    }

    if (
        !inputText &&
        field.data('type') !== 'boolean' &&
        filterTypeOption.find('select').val() != 'exist' && //Date
        filterTypeOption.find('select').val() != 'not_exist' //Date
    ) {
        alert('Digite alguma palavra para prosseguir com a busca.');
        return;
    }

    var btn = addFilterBtn(
        selectorTable,
        field.val(),
        field.text(),
        inputText,
        field.data('type'),
        filterTypeOption.find('select').val(),
        filterTypeOption.find('option:selected').text().toLowerCase()
    );

    filterData.append(btn);

    //Search Datatable (dispatch server-side)
    var datatable = $(selectorTable).DataTable();
    datatable.search('').draw();

    //Reset form
    form[0].reset();

    filterContent.addClass('d-none');
    filterContent.find('.filter-type').addClass('d-none');
}

function addFilterBtn(selectorTable, field, fieldText, value, type, filterType, filterTypeText) {
    //Monta o badge p/ o usuário poder remover o filtro
    var div = $('<div/>')
        .addClass('btn btn-small btn-primary mr-2')
        .data('field', field)
        .data('field-text', fieldText)
        .data('value', value)
        .data('type', type)
        .data('filter-type', filterType)
        .data('filter-type-text', filterTypeText);

    //Texto no Badge
    var valueReadable = div.data('value');
    if (type == 'date' && valueReadable) {
        valueReadable = new Date(valueReadable).toLocaleDateString(undefined, {
            timeZone: 'UTC',
        });
    }

    div.text(fieldText + ': ' + filterTypeText + (valueReadable ? ': ' + valueReadable : ''));

    //Botão X no badge
    div.html(div.text() + "<span class='badge badge-danger ml-2'>X</span>");

    //Event remove "tag"
    div.on('click', function () {
        $(this).remove();

        var datatable = $(selectorTable).DataTable();
        datatable.search('').draw();
    });

    return div;
}

function setDatatableFilterData(selectorTable, d, saveState) {
    var customFilter = [];

    var filterData = getParentDatatable(selectorTable).find('.filter-data');

    filterData.find('.btn').each(function () {
        var self = $(this);

        var ob = {
            field: self.data('field'),
            fieldText: self.data('field-text'),
            value: self.data('value'),
            type: self.data('type'),
            filterType: self.data('filter-type'),
            filterTypeText: self.data('filter-type-text'),
        };

        customFilter.push(ob);
    });

    if (saveState) {
        saveDatatableFilterState($(selectorTable).data('hash'), customFilter);
    }

    var localeCustomFilter = getLocaleCustomFilter(selectorTable);
    if (localeCustomFilter) {
        customFilter.push(localeCustomFilter);
    }

    d.customFilter = customFilter;
}

function saveDatatableFilterState(hash, data) {
    const expiration = new Date().getTime() + 60 * 60 * 1000 * 0.5;
    localStorage.setItem('datatable_' + hash, JSON.stringify({ data, expiration }));
}

function getParentDatatable(selectorTable) {
    var selector = selectorTable.replace('#', '');
    return $('.base-datatable-filter-' + selector).parent();
}

function loadDatatableFilterState(selectorTable, hash) {
    var saveFilterCustom = localStorage.getItem('datatable_' + hash);

    if (saveFilterCustom) {
        var filterData = getParentDatatable(selectorTable).find('.filter-data');

        var data;
        var savedData = JSON.parse(saveFilterCustom);

        //Expire
        if (savedData && savedData.expiration > new Date().getTime()) {
            data = savedData.data;
        } else {
            localStorage.removeItem('datatable_' + hash);
            return;
        }

        for (var i = 0; i < data.length; i++) {
            var v = data[i];

            var btn = addFilterBtn(
                selectorTable,
                v.field,
                v.fieldText,
                v.value,
                v.type,
                v.filterType,
                v.filterTypeText
            );

            filterData.append(btn);
        }
    }
}

function setupRefreshDatatable(selectorTable) {
    var container = getParentDatatable(selectorTable);
    container.find('.btn-refresh').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();

        refreshDatatable(selectorTable);
    });
}

function datatableFilter(tableId, hash) {
    var v = '#' + tableId;
    loadDatatableFilterQuerystring();
    loadDatatableFilterState(v, hash);
    setupDatatableFilter(v, hash);
    setupDatatableLocale(v);
    setupRefreshDatatable(v);
}

function addDatatableFilter(tableId, columnName, filterType, value, resetState) {
    var hash = 'datatable_' + tableId;

    var contentFilter = $('.base-datatable-filter-' + tableId);

    if (contentFilter.length == 0) {
        console.warn('addDatatableFilter: invalid tableId');
        return;
    }

    var column = contentFilter.find("option[value='" + columnName + "']").first();
    if (column.length == 0) {
        console.warn('addDatatableFilter: invalid columnName');
        return;
    }

    var columnType = column.data('type');
    var columnText = column.text();

    var filterTextElement = contentFilter.find(".filter-content option[value='" + filterType + "']").first();
    var filterText = filterTextElement.text().toLowerCase();

    if (filterTextElement.length == 0) {
        console.warn('addDatatableFilter: invalid filterType');
        return;
    }

    var obSearch = {
        field: columnName,
        fieldText: columnText,
        filterType: filterType,
        filterTypeText: filterText,
        type: columnType,
        value: value,
    };

    try {
        var datatableSearch = resetState == 'true' || resetState == true ? [] : JSON.parse(localStorage.getItem(hash));
    } catch (e) {
        datatableSearch = [];
    }

    datatableSearch.push(obSearch);
    localStorage.setItem(hash, JSON.stringify(datatableSearch));
}

/*
      http://localhost:8000/admin/users?tableId[]=users&columnName[]=name&filterType[]=like&value[]=rafael&resetState[]=true
  
      http://localhost:8000/admin/users?tableId[]=users&columnName[]=name&filterType[]=like&value[]=rafael&resetState[]=true&tableId[]=users&columnName[]=id&filterType[]==&value[]=1&resetState[]=false
  
      addDatatableFilter('users', 'name', 'like', 'rafa', true);
  */
function loadDatatableFilterQuerystring() {
    var urlParams = new URLSearchParams(window.location.search);
    var applyFilters = urlParams.getAll('tableId[]');

    for (var i = 0; i < applyFilters.length; i++) {
        var tableId = urlParams.getAll('tableId[]')[i];
        var columnName = urlParams.getAll('columnName[]')[i];
        var filterType = urlParams.getAll('filterType[]')[i];
        var value = urlParams.getAll('value[]')[i];
        var resetStatus = urlParams.getAll('resetState[]')[i];
        addDatatableFilter(tableId, columnName, filterType, value, resetStatus);
    }
}

function refreshDatatable(selectorTable) {
    var datatable = $(selectorTable).DataTable();
    datatable.search('').draw();
}

function setupDatatableLocale(selectorTable) {
    var parent = getParentDatatable(selectorTable);
    parent.find("input[name='locale']").on('change', function () {
        refreshDatatable(selectorTable);
    });
}

function getLocaleCustomFilter(selectorTable) {
    var locale = getParentDatatable(selectorTable).find("input[name='locale']:checked").val();

    if (locale) {
        return {
            field: 'locale',
            fieldText: 'Locale',
            value: locale,
            type: '=',
            filterType: 'locale',
            filterTypeText: 'locale',
        };
    }

    return null;
}
