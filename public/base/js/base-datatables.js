/**
 * Datatables Error - retirado o alerta e colocado no console.log
 */
$.fn.dataTable.ext.errMode = 'none';
$(document).ready(function () {
    setTimeout(function () {
        $('table.dataTable').on('error.dt', function (e, settings, techNote, message) {
            if (is_production) {
                console.error('An error has been reported by DataTables: ', message);
            } else {
                alert(message);
            }
        });
    });
});

/**
 * Helper p/ criar um DataTable simples
 *
 * @param string tableSelector
 * @param object customParams
 */
function simpleDataTable(tableSelector, customParams) {
    var datatable = $(tableSelector).DataTable({
        dom: '<"top table-top"f>rt<"row table-bottom"<"col-sm-12 col-md-5"il><"col-sm-12 col-md-7"p>><"clear">',
        lengthChange: false,
        autoWidth: false,
        scrollX: true,
        pageLenght: 20,
        stateSave: true,
        stateDuration: 60 * 60 * 0.5,
        language: {
            url: asset_url + 'base/js/i18n/datatables/' + (current_locale ? current_locale : 'pt_BR') + '.json',
        },
        ...customParams,
    });

    var resizeObserver = new ResizeObserver(function () {
        try {
            datatable.columns.adjust();
        } catch (e) {}
    });
    resizeObserver.observe($(tableSelector)[0]);

    return datatable;
}

/**
 * Helper para criar o DataTable utilizado nas chamadas do index() do Controller
 *
 * A grande diferença é o tratamento para o customFilter e o exportCsv
 */
function baseDataTable(
    tableSelector = '#table',
    ajaxUrl,
    columns,
    globalSearch = false, //Se true = habilita base search
    customParams = {},
    dom = '<"top table-top"f>rt<"row table-bottom"<"col-sm-12 col-md-5"il><"col-sm-12 col-md-7"p>><"clear">' //B
) {
    columns.push({
        data: 'actions',
        searchable: false,
        orderable: false,
        className: 'column-actions',
        render: function (data) {
            return data;
        },
    });

    return makeBaseDataTable(tableSelector, ajaxUrl, columns, globalSearch, customParams, dom);
}

/**
 * Método base p/ criação do Datatable
 */
function makeBaseDataTable(
    tableSelector = '#table',
    ajaxUrl,
    columns,
    globalSearch = false, //Se true = habilita base search
    customParams = {},
    dom = '<"top table-top"f>rt<"row table-bottom"<"col-sm-12 col-md-5"il><"col-sm-12 col-md-7"p>><"clear">' //B
) {
    if (globalSearch) {
        $(tableSelector).parent().find('.base-datatable-filter').addClass('d-none');
    } else {
        dom = dom.replace('f', ''); //desabilita o filtro global
    }

    //sorting pelo campo order, caso exista
    if (!customParams.order && columns.filter((v) => ['order', 'ordem'].indexOf(v.data) > -1).length > 0) {
        var positionOrder = 0;

        columns.map((v, k) => {
            if (v.data == 'order' || v.data == 'ordem') {
                positionOrder = k;
            }
        });

        customParams.order = [[positionOrder, 'asc']];
    }

    var datatable = $(tableSelector)
        .DataTable({
            dom: dom,
            processing: true,
            serverSide: true,
            lengthChange: false,
            autoWidth: false,
            scrollX: true,
            pageLenght: 20,
            stateSave: true,
            stateDuration: 60 * 60 * 0.5,
            ajax: {
                url: ajaxUrl,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content'),
                },
                data: function (d) {
                    setDatatableFilterData(tableSelector, d, true);
                },
            },
            language: {
                url: asset_url + 'base/js/i18n/datatables/' + (current_locale ? current_locale : 'pt_BR') + '.json',
            },
            columns: columns,
            infoCallback: function (settings, start, end, max, total, pre) {
                if (start == 1 && end == 1 && max == 1) {
                    return '';
                }

                return pre;
            },
            ...customParams,
        })
        .on('draw.dt', function () {
            var fakeContainer = $(tableSelector)
                .closest('.table-responsive')
                .parent()
                .find('.table-responsive-scroll-top');

            if (fakeContainer.length > 0) {
                var table = $(tableSelector);
                var tableContainer = $(tableSelector).parent(); //dataTables_scrollBody

                var fakeDiv = fakeContainer.find('div');

                fakeDiv.width(table.width());

                if (table.width() > tableContainer.width()) {
                    fakeContainer.removeClass('d-none');
                } else {
                    fakeContainer.addClass('d-none');
                }

                // fakeContainer.off("scroll");
                fakeContainer.scroll(function () {
                    tableContainer.scrollLeft(fakeContainer.scrollLeft());
                });

                // tableContainer.off("scroll");
                tableContainer.scroll(function () {
                    fakeContainer.scrollLeft(tableContainer.scrollLeft());
                });
            }
        });

    var resizeObserver = new ResizeObserver(function () {
        try {
            datatable.columns.adjust();
        } catch (e) {}
    });
    resizeObserver.observe($(tableSelector)[0]);

    return datatable;
}
