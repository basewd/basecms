/**
 * FIX File input
 */
if (!window.__) {
    window.__ = function (key) {
        return key;
    };
}

$(function () {
    function isValidHttpUrl(string) {
        var url;

        try {
            url = new URL(string);
        } catch (_) {
            return false;
        }

        return url.protocol === 'http:' || url.protocol === 'https:';
    }

    $('.custom-file-input').on('change', function () {
        var fileName = $(this).val().split('\\').pop();

        $(this).parent().find('.custom-file-label').html(fileName);
    });

    $('.custom-file').each(function (v) {
        var self = $(this);

        var value = $(this).find('input').attr('value');

        var path = $(this).find('input').data('path');

        if (!path || path == undefined) {
            path = '';
        } else {
            path = path + '/';
        }

        var labelText = $(this).find('label').text();

        var label = $('<div/>').html(labelText).addClass('base-label');

        $(this).append(label);

        $(this)
            .find('.custom-file-label')
            .data('label', labelText)
            .html(value ? value : '');

        if (value) {
            var filepath = isValidHttpUrl(value) ? value : storage_url + path + value;

            var filepathTest = filepath.toLowerCase();

            var isImage =
                filepathTest.indexOf('.jpg') > -1 ||
                filepathTest.indexOf('.png') > -1 ||
                filepathTest.indexOf('.jpeg') > -1 ||
                filepathTest.indexOf('.gif') > -1 ||
                filepathTest.indexOf('.webp') > -1;

            var labelBtnInfo = isImage ? window.__('Visualizar') : window.__('Download');

            $(this).append(
                "<div class='btn-view'><a class='btn btn-info' href='" +
                    filepath +
                    "' target='_blank'>" +
                    labelBtnInfo +
                    '</a>'
            );

            if (isImage) {
                $(this)
                    .find('.btn-view')
                    .popover({
                        html: true,
                        trigger: 'hover',
                        content: function () {
                            return (
                                '<div class="content-popover-image"><img class="popover-image-100" src="' +
                                filepath +
                                '" /></div>'
                            );
                        },
                    });
            }

            //Remove
            var btnRemove = $("<div class='btn btn-danger btn-remove'>" + window.__('Remover') + '</div>');
            btnRemove.on('click', function () {
                self.find('input').val(null);
                self.find('input').change();
            });
            $(this).append(btnRemove);
        }
    });
});
