/**
 * Button Submit outsite Form
 */
document
    .querySelector('[type="submit"]')
    .addEventListener("click", function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        var formId = this.getAttribute("form");
        document.querySelector('form[id="' + formId + '"]').submit();

        // Fix submit inside Javascript (Aire) //Comentado porque Aire não funciona no IE11 por hora //Analisar

        // var event = document.createEvent("Event");
        // event.initEvent("submit", true, true);

        // document
        //     .querySelector('form[id="' + formId + '"]')
        //     .dispatchEvent(event);
    });

/**
 * CustomEvent fix - IE11
 */
(function() {
    if (typeof window.CustomEvent === "function") return false; //If not IE

    function CustomEvent(event, params) {
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
        };
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(
            event,
            params.bubbles,
            params.cancelable,
            params.detail
        );
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

/**
 * WeakMap fix - IE11
 */
(function(e) {
    "use strict";
    if (e.WeakMap) {
        return;
    }
    var r = Object.prototype.hasOwnProperty;
    var n = function(e, t, r) {
        if (Object.defineProperty) {
            Object.defineProperty(e, t, {
                configurable: true,
                writable: true,
                value: r
            });
        } else {
            e[t] = r;
        }
    };
    e.WeakMap = (function() {
        function WeakMap() {
            if (this === void 0) {
                throw new TypeError("Constructor WeakMap requires 'new'");
            }
            n(this, "_id", genId("_WeakMap"));
            if (arguments.length > 0) {
                throw new TypeError("WeakMap iterable is not supported");
            }
        }
        n(WeakMap.prototype, "delete", function(e) {
            checkInstance(this, "delete");
            if (!isObject(e)) {
                return false;
            }
            var t = e[this._id];
            if (t && t[0] === e) {
                delete e[this._id];
                return true;
            }
            return false;
        });
        n(WeakMap.prototype, "get", function(e) {
            checkInstance(this, "get");
            if (!isObject(e)) {
                return void 0;
            }
            var t = e[this._id];
            if (t && t[0] === e) {
                return t[1];
            }
            return void 0;
        });
        n(WeakMap.prototype, "has", function(e) {
            checkInstance(this, "has");
            if (!isObject(e)) {
                return false;
            }
            var t = e[this._id];
            if (t && t[0] === e) {
                return true;
            }
            return false;
        });
        n(WeakMap.prototype, "set", function(e, t) {
            checkInstance(this, "set");
            if (!isObject(e)) {
                throw new TypeError("Invalid value used as weak map key");
            }
            var r = e[this._id];
            if (r && r[0] === e) {
                r[1] = t;
                return this;
            }
            n(e, this._id, [e, t]);
            return this;
        });
        function checkInstance(e, t) {
            if (!isObject(e) || !r.call(e, "_id")) {
                throw new TypeError(
                    t + " method called on incompatible receiver " + typeof e
                );
            }
        }
        function genId(e) {
            return e + "_" + rand() + "." + rand();
        }
        function rand() {
            return Math.random()
                .toString()
                .substring(2);
        }
        n(WeakMap, "_polyfill", true);
        return WeakMap;
    })();
    function isObject(e) {
        return Object(e) === e;
    }
})(
    typeof self !== "undefined"
        ? self
        : typeof window !== "undefined"
        ? window
        : typeof global !== "undefined"
        ? global
        : this
);

/**
 * Array.prototype.slice fix
 */
if (!Array.from) {
    Array.from = function(object) {
        "use strict";
        return [].slice.call(object);
    };
}

/**
 * Element.matches fix - IE10
 */
if (!Element.prototype.matches) {
    Element.prototype.matches =
        Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
}

/**
 * Element.closest fix - IE10
 */
if (!Element.prototype.closest) {
    Element.prototype.closest = function(s) {
        var el = this;

        do {
            if (Element.prototype.matches.call(el, s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
    };
}

/**
 * Element.dataset fix - IE10
 */
(function datasetModule(global, definition) {
    // non-exporting module magic dance
    "use strict";

    var amd = "amd",
        exports = "exports"; // keeps the method names for CommonJS / AMD from being compiled to single character variable

    if (typeof define === "function" && define[amd]) {
        define(function definer() {
            return definition(global);
        });
    } else if (typeof module === "function" && module[exports]) {
        module[exports] = definition(global);
    } else {
        definition(global);
    }
})(this, function datasetPolyfill(global) {
    "use strict";

    var attribute,
        attributes,
        counter,
        dash,
        dataRegEx,
        document = global.document,
        hasEventListener,
        length,
        match,
        mutationSupport,
        test = document.createElement("_"),
        DOMAttrModified = "DOMAttrModified";

    function clearDataset(event) {
        delete event.target._datasetCache;
    }

    function toCamelCase(string) {
        return string.replace(dash, function(m, letter) {
            return letter.toUpperCase();
        });
    }

    function getDataset() {
        var dataset = {};

        attributes = this.attributes;
        for (
            counter = 0, length = attributes.length;
            counter < length;
            counter += 1
        ) {
            attribute = attributes[counter];
            match = attribute.name.match(dataRegEx);
            if (match) {
                dataset[toCamelCase(match[1])] = attribute.value;
            }
        }

        return dataset;
    }

    function mutation() {
        if (hasEventListener) {
            test.removeEventListener(DOMAttrModified, mutation, false);
        } else {
            test.detachEvent("on" + DOMAttrModified, mutation);
        }

        mutationSupport = true;
    }

    if (test.dataset !== undefined) {
        return;
    }

    dash = /\-([a-z])/gi;
    dataRegEx = /^data\-(.+)/;
    hasEventListener = !!document.addEventListener;
    mutationSupport = false;

    if (hasEventListener) {
        test.addEventListener(DOMAttrModified, mutation, false);
    } else {
        test.attachEvent("on" + DOMAttrModified, mutation);
    }

    // trigger event (if supported)
    test.setAttribute("foo", "bar");

    Object.defineProperty(global.Element.prototype, "dataset", {
        get: mutationSupport
            ? function get() {
                  if (!this._datasetCache) {
                      this._datasetCache = getDataset.call(this);
                  }

                  return this._datasetCache;
              }
            : getDataset
    });

    if (mutationSupport && hasEventListener) {
        // < IE9 supports neither
        document.addEventListener(DOMAttrModified, clearDataset, false);
    }
});
