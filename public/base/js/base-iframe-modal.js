function autosizeIframeModal(elem) {
    var iframe = document.querySelector(elem);

    if (element.offsetWidth == 0 && element.offsetHeight == 0) {
        iframe.style.height = '0px';
        return;
    }

    if (!iframe) {
        console.error('Element not found: ' + elem);
        return;
    }

    var newHeight = iframe.contentWindow.document.body.scrollHeight;
    var offset = 30;

    if (Math.abs(newHeight - iframe.clientHeight) < offset) {
        return;
    }

    if (window.innerHeight < newHeight) {
        newHeight = window.innerHeight - offset;
    }

    iframe.style.height = newHeight + 'px';
}
