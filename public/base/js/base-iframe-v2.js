function autosizeIframe(strElem) {
    var elem = $(strElem);

    if (elem[0].contentWindow && elem[0].contentWindow.document && elem[0].contentWindow.document.body) {
        var newHeight = elem[0].contentWindow.document.body.scrollHeight;

        var offset = 30; //Offset 28;

        var height = elem[0].getBoundingClientRect().height;

        if (
            (height + offset > newHeight * 1 && height < newHeight + offset) ||
            window.outerHeight == window.outerHeight - offset
        ) {
            return;
        }

        if (window.outerHeight < newHeight) {
            newHeight = window.outerHeight - offset;
        }

        elem[0].height = newHeight + 'px';
    }
}
