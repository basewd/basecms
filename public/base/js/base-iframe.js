function autosizeIframe(strElem) {
    var elem = $(strElem);

    var contents = elem.contents();

    if (contents.length > 0) {
        var firstChildren = contents
            .find("body")
            .children()
            .not(".pace")
            .eq(0);

        if (firstChildren.length > 0) {
            contents.find("body").css("overflow", "hidden");

            var h = firstChildren.outerHeight();
            if (h > 5500 || !h || h === 0) {
                return;
            }

            const newH = h + 100;
            if (elem.height() >= h && elem.height() <= newH) {
                contents.find("body").css("overflow", "auto");
                return;
            }

            if (h == elem.data("height")) {
                return;
            }

            elem.data("height", h);
            elem.height(h);
            contents.find("body").css("overflow", "auto");
        }
    }
}
