/**
 * Classe p/ converter um JSON em uma Tabela a partir de uma definição.
 *
 * A definição define o "campo" (id), título (label) e o tipo de render do valor (type)
 *  - type: 'text', 'number', 'textarea', 'bool', 'select'
 *
 * Exemplo utilizando o BaseDefinition.php:
 *
 * BaseField::newObj('data', 'Data', InputTableJson::class, BaseColumnConst::Text)
 *               ->setInputMethods([
 *                   ['setOptions', [
 *                      'orderable' => false, 
 *                      'removable' => false, 
 *                      'addable' => false, 
 *                      'labelAdd' => 'Foo-bar'
 *                   ]],
 *                   ['setDefinition', [
 *                       ["id" => "id", "label" => "Código", "description" => "Lorem ipsun", "type" => "text", "class"=>"class-foo-bar", "attr" => [['property' => 'readonly', 'value' => 'false']]],
 *                       ["id" => "text", "label" => "Texto", "description" => "Lorem ipsun", "type" => "text"],
 *                       ["id" => "feedback", "label" => "Feedback", "description" => "Lorem ipsun", "type" => "text"],
 *                       ["id" => "flCorrect", "label" => "Correto?", "description" => "Lorem ipsun", "type" => "bool"],
 *                       ["id" => "select", "label" => "Lista", "description" => "Lorem ipsun", "type" => "select", "values"=>[['value'=>1, 'name'=>'Nome']]],
 *                   ]]
 *               ]),

 * @param string id
 * @param array definition [["id" => "id", "label" => "label", "description" => "text", "type" => "text"], ["id" => "id", "label" => "label", "description" => "text", "type" => "text"]]
 * @param string dataValue Convertido em JSON
 */
var defaultInputTableJsonOptions = {
    orderable: true,
    removable: true,
    addable: true,
    labelAdd: 'Adicionar nova linha',
};

function InputTableJson(id, definition, dataValue, options) {
    options = Object.assign(defaultInputTableJsonOptions, options);

    var container = $('.input-table-json-' + id);
    var containerTable = container.find('.content-table-json');
    var containerFooter = container.find('.card-footer');

    try {
        dataValue = JSON.parse(dataValue);
        dataValue = dataValue.list; //Wrap
    } catch (e) {
        dataValue = [];
    }

    //Fix array
    if (!Array.isArray(dataValue)) {
        dataValue = [];
    }

    function addRow() {
        var ob = {};

        definition.map(function (v) {
            ob[v.id] = '';
        });

        dataValue.push(ob);

        render();
    }

    function createTdActions(row, position) {
        var td = $('<td/>');
        td.addClass('td-actions');

        if (options.orderable) {
            var btnUp = $('<div/>')
                .addClass('btn btn-primary mr-1 btn-up')
                .html("<em class='fa fa-chevron-up'></em>")
                .on('click', function () {
                    if (position > 0) {
                        dataValue = move(dataValue, position, position - 1);
                        render();
                    }
                });
            td.append(btnUp);

            var btnDown = $('<div/>')
                .addClass('btn btn-primary mr-1 btn-down')
                .html("<em class='fa fa-chevron-down'></em>")
                .on('click', function () {
                    if (position + 1 < dataValue.length) {
                        dataValue = move(dataValue, position, position + 1);
                        render();
                    }
                });
            td.append(btnDown);
        }

        if (options.removable) {
            var btnDelete = $('<div/>')
                .addClass('btn btn-danger btn-trash')
                .html("<em class='fa fa-trash'></em>")
                .on('click', function () {
                    dataValue.splice(position, 1);
                    render();
                });
            td.append(btnDelete);
        }

        return td;
    }

    function move(arr, old_index, new_index) {
        while (old_index < 0) {
            old_index += arr.length;
        }
        while (new_index < 0) {
            new_index += arr.length;
        }
        if (new_index >= arr.length) {
            var k = new_index - arr.length;
            while (k-- + 1) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr;
    }

    function createTable() {
        var table = $('<table/>');
        table.addClass('table table-striped');
        return table;
    }

    function createTableHeader() {
        //header
        var trHeader = $('<tr/>');

        definition.map(function (v) {
            var td = $('<th/>');
            td.html(v.label + "<small class='muted d-block'>" + (v.description ? v.description : '') + '</small>');
            trHeader.append(td);
        });

        var tdAction = $('<th/>');
        tdAction.html('Ações');
        trHeader.append(tdAction);

        return trHeader;
    }

    function createInput(defColumn, value, position, key) {
        var inputRender;

        if (defColumn.type == 'text' || defColumn.type == 'number') {
            inputRender = $('<input/>');
            inputRender.attr('type', defColumn.type);
            inputRender.addClass('form-control');
            inputRender.val(value);

            inputRender.on('change keyup', function (event) {
                dataValue[position][key] = $(this).val();
                updateJson();
            });
        } else if (defColumn.type == 'textarea') {
            inputRender = $('<textarea/>');
            inputRender.addClass('form-control');
            inputRender.val(value);

            inputRender.on('change keyup', function (event) {
                dataValue[position][key] = $(this).val();
                updateJson();
            });
        } else if (defColumn.type == 'bool') {
            inputRender = $('<input/>').attr('type', 'checkbox');
            inputRender.addClass('form-check-input');
            inputRender.attr('checked', value == true);

            inputRender.on('change keyup', function (event) {
                dataValue[position][key] = $(this).is(':checked') ? true : false;
                updateJson();
            });
        } else if (defColumn.type == 'select') {
            inputRender = $('<select/>');
            inputRender.addClass('form-control');

            inputRender.append("<option value=''>Selecione</option>");
            $.each(defColumn.values, function (i, item) {
                inputRender.append('<option value="' + item.value + '">' + item.name + '</option>');
            });

            inputRender.val(value);

            inputRender.on('change', function (event) {
                dataValue[position][key] = $(this).val();
                updateJson();
            });
        }

        if (defColumn.class) {
            inputRender.addClass(defColumn.class);
        }

        if (defColumn.attr) {
            defColumn.attr.map(function (v) {
                if (v.property) {
                    inputRender.attr(v.property, v.value);
                } else {
                    console.warn('base-input-table-json: invalid value: ', v);
                }
            });
        }

        return inputRender;
    }

    function createTableValues(row, position) {
        var trValue = $('<tr/>');

        definition.map(function (defColumn) {
            var tdValue = $('<td/>').addClass('text-center');
            var inputRender = createInput(defColumn, row[defColumn.id], position, defColumn.id);

            tdValue.append(inputRender);
            trValue.append(tdValue);
        });

        trValue.append(createTdActions(row, position));

        return trValue;
    }

    function createBtnAddRow() {
        var btn = null;

        if (options.addable) {
            var btn = $('<div/>').html(options.labelAdd);
            btn.addClass('btn btn-info btn-sm');
            btn.on('click', function () {
                addRow();
            });
        }

        return btn;
    }

    function updateJson() {
        var ob = { list: dataValue };
        $('#' + id).val(JSON.stringify(ob));
    }

    function render() {
        if (!definition) {
            return;
        }

        var table = createTable();
        table.append(createTableHeader());

        dataValue.map(function (row, position) {
            table.append(createTableValues(row, position));
        });

        containerTable.html(table);

        containerFooter.html(createBtnAddRow);

        container.trigger('render');

        updateJson();
    }

    //Initialize
    if (dataValue.length == 0 && definition) {
        addRow();
    }

    render();
}
