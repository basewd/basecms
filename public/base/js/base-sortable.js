$(window.document)
    .ajaxComplete(function (event, xmlHttpRequest, ajaxOptions) {
        iframeLoadDatatable();
    })
    .ready(function () {
        iframeLoadDatatable();
    });

/**
 * Atualiza o DataTable quando é feito um submit de um form com target em um iframe
 */
function iframeLoadDatatable() {
    $('iframe').on('load', function () {
        var iframe = $(this).contents().get(0);

        if (!iframe) {
            return;
        }

        var iframeUrl = iframe.location.href;

        if (iframeUrl && iframeUrl != 'about:blank') {
            $('table').each(function () {
                if ($.fn.DataTable.isDataTable(this)) {
                    $(this).DataTable().ajax.reload(null, false);
                }
            });
        }
    });
}

/**
 * Move um registro p/ determina posição, método seta a posição no input p/ o valor ser enviado via "POST"
 */
function submitOrderTo(target) {
    var position = prompt('Para qual posição você deseja mover?', '');

    if (!position) {
        return false;
    }

    $(target)
        .find("input[name='position']")
        .val(position * 1);

    return true;
}
