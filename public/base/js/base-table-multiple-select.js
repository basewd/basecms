$(function () {
    $('.btn-multiple-select-table').on('click', function (evt) {
        var action = $(this).data('action');
        var tableSelector = $(this).data('table-selector');

        var table = $(tableSelector);

        var inputs = table.find("input[name='multiple_select_table[]']:checked");

        if (inputs.length == 0) {
            alert('Selecione ao menos um registro.');
            return;
        }

        var data = [];
        $.each(inputs, function (index, value) {
            data.push($(value).val());
        });

        Swal.fire({
            title: $(this).data('lang-text')
                ? $(this).data('lang-text')
                : 'Você tem certeza que deseja remover os registros?',
            showCancelButton: true,
            confirmButtonText: $(this).data('lang-confirm') ? $(this).data('lang-confirm') : 'Sim',
            cancelButtonText: $(this).data('lang-cancel') ? $(this).data('lang-cancel') : 'Cancelar',
            icon: 'warning',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: action,
                    data: {
                        multipleSelect: data,
                    },
                    headers: {
                        'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content'),
                    },
                    success: function (data) {
                        var datatable = table.DataTable();
                        datatable.search('').draw();
                    },
                    error: function (err) {
                        alert(err.responseJSON.message);
                    },
                });
            }
        });
    });
});
