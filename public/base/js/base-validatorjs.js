if (window['Validator']) {
    var validatorMessages = window['Validator'].getMessages('en');

    Validator.register('cnpj', isValidCnpj, validatorMessages.cnpj);
    Validator.register('cpf', isValidCpf, validatorMessages.cpf);
    Validator.registerImplicit('max', isValidMax, validatorMessages.max);
    Validator.registerImplicit('required', isCustomRequired, validatorMessages.required);
}

function isCustomRequired(val, req, attribute) {
    if (val === undefined || val === null) {
        return false;
    }

    if (val instanceof File && $("input[name='" + attribute + "']").attr('touched')) {
        var fileHide = document.getElementsByName(attribute + '_value_of_file')[0];
        return (val.name && val.name.length > 0) || (fileHide && fileHide.value && fileHide.value.length > 0);
    }

    var str = String(val).replace(/\s/g, '');

    return str.length > 0 ? true : false;
}

function isValidMax(val, req, attribute) {
    if (val instanceof File) {
        if (val.size == 0) {
            return undefined;
        }

        return val.size / 1204 <= req;
    }

    var size = this.getSize();
    return size <= req;
}

function isValidCnpj(cnpj) {
    if (cnpj.length == 0) return true;

    //formata o cnpj
    cnpj = cnpj.replace('.', '');
    cnpj = cnpj.replace('.', '');
    cnpj = cnpj.replace('.', '');
    cnpj = cnpj.replace('-', '');
    cnpj = cnpj.replace('/', '');

    if (cnpj.length != 14) return false;

    var firstLetter = cnpj.charAt(0);
    var testEqualsCnpj = new RegExp('^[' + firstLetter + ']{13}$').test(cnpj);
    if (testEqualsCnpj) return false;

    var multiplicador = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
    var multiplicador2 = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
    var digito1 = cnpj.charAt(12);
    var digito2 = cnpj.charAt(13);
    var r = 0;

    // Calculo do primeiro digito
    for (var i = 0; i < multiplicador.length; i++) {
        r += cnpj.charAt(i) * multiplicador[i];
    }
    r %= 11;
    r = r < 2 ? 0 : 11 - r;
    if (r != digito1) return false;

    // Calculo do segundo digito
    r = 0;
    for (i = 0; i < multiplicador2.length; i++) {
        r += cnpj.charAt(i) * multiplicador2[i];
    }
    r %= 11;
    r = r < 2 ? 0 : 11 - r;
    if (r == digito2) return true;
    else return false;
}

function isValidCpf(cpf) {
    if (cpf.length == 0) return true;

    //reformata o cpf caso esteja formtado =)
    cpf = cpf.replace('.', '');
    cpf = cpf.replace('.', '');
    cpf = cpf.replace('-', '');

    if (cpf.length != 11) return false;

    //Anula Cpf`s com mesmo numero 11111111111
    var firstLetter = cpf.charAt(0);
    ///g  o group do regexp
    var testEqualsCpf = new RegExp('^[' + firstLetter + ']{11}$').test(cpf);
    if (testEqualsCpf) return false;

    var multiplicador = [10, 9, 8, 7, 6, 5, 4, 3, 2];
    var digito1 = cpf.charAt(9);
    var digito2 = cpf.charAt(10);
    var r = 0;

    // Calculo do primeiro digito
    for (var i = 0; i < multiplicador.length; i++) {
        r += cpf.charAt(i) * multiplicador[i];
        multiplicador[i] += 1;
    }
    r %= 11;
    r = r < 2 ? 0 : 11 - r;
    if (r != digito1) return false;

    // Calculo do segundo digito
    multiplicador.push(2);
    r = 0;
    for (i = 0; i < multiplicador.length; i++) {
        r += cpf.charAt(i) * multiplicador[i];
    }
    r %= 11;
    r = r < 2 ? 0 : 11 - r;
    if (r == digito2) return true;
    else return false;
}
