<?php

use BaseCms\Http\Controllers\BaseController;
use BaseCms\Http\Controllers\AdminBaseController;

Route::group(
    [
        'prefix' => 'base',
        'as' => 'base.',
    ],
    function () {
        Route::get('/healthcheck', [BaseController::class, 'healthcheck'])->name('healthcheck');
    },
);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['web', 'session', 'admin']], function () {
    Route::group(
        [
            'prefix' => 'base',
            'as' => 'base.',
        ],
        function () {
            Route::post('/quillUpload', [AdminBaseController::class, 'quillUpload'])->name('quillUpload');
            Route::post('/tinyUpload', [AdminBaseController::class, 'tinyUpload'])->name('tinyUpload');

            if (!$this->app->environment('production')) {
                Route::get('routes', function () {
                    $routeCollection = Route::getRoutes();
                    echo "<table style='width:100%'>";
                    echo '<tr>';
                    echo "<td width='10%'><h4>HTTP Method</h4></td>";
                    echo "<td width='10%'><h4>Route</h4></td>";
                    echo "<td width='80%'><h4>Corresponding Action</h4></td>";
                    echo '</tr>';
                    foreach ($routeCollection as $value) {
                        echo '<tr>';
                        echo '<td>' . $value->methods()[0] . '</td>';
                        echo '<td>' . $value->uri() . '</td>';
                        echo '<td>' . $value->getActionName() . '</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                });
            }
        },
    );
});
