<?php

namespace BaseCms\BaseBlockBuilder;

use BaseCms\BaseBlockBuilder\DomainsFront\BlockBuilder\Http\Controllers\BlockBuilderController;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use BaseCms\Datatables\Helpers\BaseDatatableHelper;
use BaseCms\BaseField;

class BlockBuilder
{
    /**
     * Registro dos componentes
     *
     * @var array $_registredComponents
     * [
     *      key => "string",
     *      componentClass => "string",
     *      title => "string",
     *      thumb => "string"
     * ]
     */
    private static $_registredComponents = [];

    /**
     * Hide omponentes / Não mostra na lista de blocks e pages, geralmente são componentes filhos
     */
    private static $_hideComponents = [];

    /**
     * register
     *
     * @param  string $key
     * @param  string $componentClass
     * @param  string $title
     * @param  string $thumb
     * @return void
     */
    public static function register(
        string $key,
        string $componentClass,
        string $title,
        string $thumb = 'https://via.placeholder.com/400x250?text=BLOCKNAME'
    ) {
        if (isset(self::$_registredComponents[$key])) {
            throw new \Exception('blockbuilder:registred componentClass already exists');
        }

        self::$_registredComponents[$key] = [
            'key' => $key,
            'componentClass' => $componentClass,
            'title' => $title,
            'thumb' => $thumb,
        ];
    }

    /**
     * registredComponents
     *
     * @return \Illuminate\Database\Eloquent\Collection<string,string>
     */
    public static function registredComponents()
    {
        return collect(self::$_registredComponents)->pluck('componentClass', 'key');
    }

    /**
     * registredComponents
     *
     * @return \Illuminate\Database\Eloquent\Collection<string,array {
     *      key: int,
     *      componentClass: string,
     *      title: string,
     *      thumb: string
     * }>
     */
    public static function getRegistredComponents()
    {
        return self::$_registredComponents;
    }

    /**
     * getComponents
     *
     * @return \Illuminate\Database\Eloquent\Collection<string, array {
     *      key: int,
     *      componentClass: string,
     *      title: string,
     *      thumb: string
     * }>
     */
    public static function getComponentsByClass()
    {
        return collect(self::$_registredComponents)->keyBy('componentClass');
    }

    /**
     * isRegistredComponent
     *
     * @param string $className
     * @return bool
     */
    public static function isRegistredComponent($className)
    {
        return self::registredComponents()->contains($className);
    }

    /**
     * hide
     *
     * @param  string $componentClass
     * @return void
     */
    public static function hide(string $componentClass)
    {
        if (!self::$_hideComponents) {
            self::$_hideComponents = collect();
        }

        self::$_hideComponents->push($componentClass);
    }

    /**
     * hideComponents
     *
     * @return \Illuminate\Database\Eloquent\Collection<string>
     */
    public static function hideComponents()
    {
        return self::$_hideComponents;
    }

    /**
     * Page
     *
     * @param string $slug
     *
     * @return PageModel
     */
    public static function page($slug = '')
    {
        $page = PageModel::where('slug', $slug)
            ->with('blocks')
            ->first();

        return $page;
    }

    /**
     * @param string $key
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function renderBlockByKey($key, $locale = null, $customProps = null, $withDraft = false)
    {
        $block = BlockModel::where('locale', $locale ?: app()->getLocale())
            ->where('key', $key)
            ->first();

        if (!$block) {
            return null;
        }

        $renderBlock = self::renderBlock($block, 0, $customProps, $withDraft);

        return $renderBlock ? $renderBlock->render() : null;
    }

    /**
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function renderBlockById($id)
    {
        $block = BlockModel::where('id', $id)->first();

        if (!$block) {
            return null;
        }

        $renderBlock = self::renderBlock($block, 0, [], false);

        return $renderBlock ? $renderBlock->render() : null;
    }

    /**
     * @param string $key
     * @param string $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function renderPageByKey($key, $locale = null)
    {
        $pageModel = PageModel::where('locale', $locale ?: app()->getLocale())
            ->where('key', $key)
            ->first();

        if ($pageModel) {
            $page = (new BlockBuilderController())->page($pageModel, request());

            return $page;
        }

        return app()->abort(404);
    }

    /**
     * @param BlockModel $block
     * @param number $deph
     * @param mixed|array $forceParams
     *
     * @return mixed
     */
    public static function renderBlock(
        $block,
        $deph = 0,
        $forceParams = [],
        $parent = null,
        $withDraft = false
    ): ?\Illuminate\View\Component {
        if ($deph > 5) {
            // throw new \Exception('depth limit');
            \Log::warning('depth limit');
            return null;
        }

        if (!$block) {
            return null;
        }

        //skip draft
        if (!$withDraft && $block->fl_draft) {
            return null;
        }

        $component = null; //Componente não existe mais na plataforma

        //Força load da class, caso não exista, retorna vazio
        try {
            class_exists($block->componentClass);
        } catch (\Exception $e) {
            return null;
        }

        //remove subniveis (parent)
        unset($parent['parent']);

        if (class_exists($block->componentClass)) {
            $component = app()->makeWith(
                $block->componentClass,
                $blockParent =
                    ['id' => $block->id, 'componentClass' => $block->componentClass, 'parent' => $parent] +
                    (array) $forceParams +
                    (array) $block->params,
            );

            ++$deph;

            /**
             * Será que verifico se o componente esta no ALLOW p/ não renderizar mais? Tenho dúvidas ...
             * esse é um caso quando existe troca de parametros em um componente já criado. Avaliar no futuro!
             */
            $allowedChildrens = BlockBuilder::definitionBy($block->componentClass)->getAllowedChildrens();

            $component->children = $block->children->map(function ($v) use (
                $allowedChildrens,
                $deph,
                $blockParent,
                $withDraft
            ) {
                if (
                    !$allowedChildrens ||
                    $allowedChildrens->count() == 0 ||
                    $allowedChildrens->contains($v->componentClass)
                ) {
                    //Recursão
                    return self::renderBlock($v, $deph, [], $blockParent, $withDraft);
                }

                return null;
            });
        } else {
            \Log::error('blockbuilder:invalid registred component:' . $block->componentClass . ':' . $block->id);
        }

        return $component;
    }

    /**
     * @param BlockModel $block
     * @param number $deph
     *
     * @return mixed
     */
    public static function blockJson($block, $deph = 0): ?array
    {
        if ($deph > 5) {
            \Log::warning('depth limit');
            return null;
        }

        ++$deph;

        $data = [
            'id' => $block->id,
            'componentClass' => $block->componentClass,
            'params' => (array) $block->params,
            'children' => $block->children->map(function ($v) use ($deph) {
                //Recursão
                return self::blockJson($v, $deph);
            }),
        ];

        // if (!isset($data['children']) || $data['children']->count() == 0) {
        //     unset($data['children']);
        // }

        return $data;
    }

    /**
     * Otimizar? Podera ter cache de array
     *
     * @param string $componentClass
     * @return BlockBuilderDefinition|null
     */
    public static function definitionBy($componentClass)
    {
        if (!class_exists($componentClass) || !BlockBuilder::registredComponents()->search($componentClass)) {
            $componentClass = @BlockBuilder::registredComponents()[$componentClass];
        }

        return $componentClass ? $componentClass::definition() : null;
    }

    /**
     * @param  mixed $blockId
     * @return BlockBuilderDefinition|null
     */
    public static function definitionByBlockId($blockId)
    {
        $block = BlockModel::where('id', $blockId)->first();
        return self::definitionBy($block->componentClass);
    }

    /**
     * allowedComponents
     *
     * @param string $blockId
     * @return collect
     */
    public static function allowedComponents(string $blockId)
    {
        $block = BlockModel::where('id', $blockId)->first();

        try {
            $allowedComponents = BlockBuilder::definitionBy($block->componentClass)->getAllowedChildrens();
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        if ($allowedComponents) {
            $list = BlockBuilder::registredComponents()->filter(function ($v) use ($allowedComponents) {
                return $allowedComponents->contains($v);
            });

            return $list;
        }

        //Força o retorno de apenas componentes registrados que estão visíveis
        return BlockBuilder::registredComponents()->filter(function ($v) {
            return !BlockBuilder::hideComponents()->contains($v);
        });
    }

    /**
     * Chama o comando route:cache do Artisan do Laravel para limpar o cache de rotas da aplicação.
     *
     * O cache de rotas é criado para melhorar a performance das requisições, mas como as "pages" são dinâmicas, precisamos limpar o cache.
     */
    public static function clearRouteCache()
    {
        if (config('blockbuilder.cached_route')) {
            \BaseCms\BaseBlockBuilder\Jobs\RouteCacheJob::dispatchAfterResponse();
        } else {
            \BaseCms\BaseBlockBuilder\Jobs\RouteClearJob::dispatchAfterResponse();
        }
    }

    /**
     * @return bool
     */
    public static function isEdit()
    {
        return request()->blockbuilder;
    }

    public static function previewData($data, ?BlockBuilderDefinition $definition)
    {
        //Fix, o componente pode estar no banco de dados, mas não foi registrado o componente
        if (!$definition) {
            return null;
        }

        $fields = array_filter($definition->getFields(), function (BaseField $v) {
            return $v->filterable;
        });

        $list = [];
        foreach ($fields as $v) {
            try {
                $value = @BaseDatatableHelper::defaultRender($v->field, $definition)($data);
                $list[] = '<b>' . $v->title . '</b><p>' . $value . '</p>';
            } catch (\ErrorException $e) {
                throw new \ErrorException('Error Message: ' . $e->getMessage() . ' | Field: ' . $v->field . ' | ');
            }
        }

        return join('', $list);
    }

    public static function duplicateBlock($blockId)
    {
        $originalBlock = BlockModel::where('id', $blockId)
            ->with('children')
            ->first();

        // Duplicar o bloco atual
        $newBlock = $originalBlock->replicate();
        $newBlock->key = null;
        $newBlock->save();

        // Recursivamente duplicar os filhos
        self::duplicateBlockChildren($originalBlock, $newBlock);

        return $newBlock->id;
    }

    private static function duplicateBlockChildren(BlockModel $originalBlock, BlockModel $newBlock)
    {
        foreach ($originalBlock->children as $originalChild) {
            //reutilizável, não precisa avançar nos filhos
            if ($originalChild->fl_reusable) {
                $newBlock->children()->save($originalChild, ['order' => $originalChild->pivot->order]);
                continue;
            }

            //preload relationship
            $originalChild->load('children');

            //duplica o filho
            $newChild = $originalChild->replicate();
            $newChild->key = null;
            $newChild->save();

            //vincula o block_children (child ao parent)
            $newBlock->children()->save($newChild, ['order' => $originalChild->pivot->order]);

            self::duplicateBlockChildren($originalChild, $newChild);
        }
    }
}
