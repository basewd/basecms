<?php

namespace BaseCms\BaseBlockBuilder;

use BaseCms\BaseDefinition;

class BlockBuilderDefinition extends BaseDefinition
{
    /**
     * @var bool
     */
    protected $allowChildren = false;

    /**
     * @var int
     */
    protected $maxChildrens = PHP_INT_MAX;

    /**
     * @var \Illuminate\Support\Collection|null
     */
    protected $allowedChildrens = null;

    /**
     * @var bool
     */
    protected $allowReuseComponents = false;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var Closure ($model)
     */
    protected $hookSaved;

    /**
     * setFields
     *
     * @param array $fields
     * @return BlockBuilderDefinition
     */
    public function setFields($fields): BlockBuilderDefinition
    {
        parent::setFields($fields);

        $this->formFields = array_map(function ($field) {
            return $field->field;
        }, $fields);

        return $this;
    }

    /**
     * @param bool $allowChildren
     * @return BlockBuilderDefinition
     */
    public function setAllowChildren(bool $allowChildren = true): BlockBuilderDefinition
    {
        $this->allowChildren = $allowChildren;
        return $this;
    }

    /**
     * allowedChildrens
     *
     * @param \Illuminate\Support\Collection $allowedChildrens
     * @return BlockBuilderDefinition
     */
    public function setAllowedChildrens(array $allowedChildrens = null): BlockBuilderDefinition
    {
        foreach ($allowedChildrens as $v) {
            if (strpos($v, 'Components') === false) {
                throw new \Exception('blockbuilder:allowedChildrens: invalid component');
            }
        }

        $this->allowedChildrens = collect($allowedChildrens);

        return $this;
    }

    /**
     * getAllowedChildrens
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllowedChildrens()
    {
        return $this->allowedChildrens;
    }

    /**
     * isAllowChildrens
     *
     * @return bool
     */
    public function isAllowChildrens()
    {
        return $this->allowChildren;
    }

    /**
     * @param  bool $allowReuseComponents
     * @return BlockBuilderDefinition
     */
    public function setAllowReuseComponents(bool $allowReuseComponents)
    {
        $this->allowReuseComponents = $allowReuseComponents;

        return $this;
    }

    /**
     * @param int $maxChildrens
     * @return BlockBuilderDefinition
     */
    public function setMaxChildrens(int $maxChildrens)
    {
        $this->maxChildrens = $maxChildrens;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxChildrens()
    {
        return $this->maxChildrens;
    }

    /**
     * @return bool
     */
    public function isAllowReuseComponents()
    {
        return $this->allowReuseComponents;
    }

    /**
     * @param Closure|null $hookSaved
     * @return BlockBuilderDefinition
     */
    public function hookSaved(\Closure $hookSaved)
    {
        $this->hookSaved = $hookSaved;

        return $this;
    }

    /**
     * @return Closure
     */
    public function getHookSaved()
    {
        return $this->hookSaved;
    }
}
