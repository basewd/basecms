<?php

namespace BaseCms\BaseBlockBuilder\Console\Command;

use Illuminate\Console\GeneratorCommand;

/**
 *
 */
class BlockBuilderMakeComponent extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockbuilder:make:component {name : Nome do componente} {fields : Uma lista separada por vírgulas de nomes de campo}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crie um novo componente BlockBuilder';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Component';

    /**
     * @return string
     */
    protected function getStub()
    {
        return null;
    }

    /**
     * @return string
     */
    protected function getStubDefinition()
    {
        return __DIR__ . '/stubs/component_definition.stub';
    }

    /**
     * @return string
     */
    protected function getStubTypeDefinition($type)
    {
        return __DIR__ . '/stubs/fields/' . $type . '_field.stub';
    }

    /**
     * @return string
     */
    protected function getStubRegisterComponent()
    {
        return __DIR__ . '/stubs/component_register.stub';
    }

    /**
     *
     */
    protected function replaceView($name, $fieldsArray)
    {
        $path = resource_path('views/components/frontend/block/' . \Str::snake($name, '-') . '.blade.php');

        $content = file_get_contents($path);

        /**
         * @props
         */
        $propsString = '@props([\'' . implode('\', \'', $fieldsArray) . '\'])';

        $content = \Str::replaceFirst('<div>', $propsString . "\n\n<div>", $content);

        /**
         * @blockbuilderEdit
         */
        $content = \Str::replaceFirst(
            '<div>',
            "<div>\n    {{-- Criar a estrutura do componete aqui! --}}@fields@\n\n    {{-- Habilita o modo edição no Front --}}\n    @blockbuilderEdit\n\n    {{-- Caso o componente receba filhos no cadastro/edição do Block, essa diretiva defnie onde os filhos devem ir --}}\n    @blockbuilderChildren\n\n    {{-- Permite passar componentes filhos instanciando o componente direto --}}\n    {{ @\$slot }}\n",
            $content,
        );

        /**
         * Valores instanciados na view
         */
        $fieldsString = '';
        foreach ($fieldsArray as $field) {
            $fieldsString .= "\n" . '    <div>{{ $' . $field . ' }}</div>';
        }
        $content = \Str::replaceFirst('@fields@', $fieldsString, $content);

        /**
         * Class identify component
         */
        $content = \Str::replaceFirst('<div>', "<div class='" . \Str::camel($name) . "'>", $content);

        file_put_contents($path, $content);
    }

    /**
     * Replace the fields in the component file.
     *
     * @param  string  $name
     * @param  string  $fields
     * @return void
     */
    protected function replaceClass($name, $fieldsArray)
    {
        $path = $this->getPath($this->qualifyClass($name));

        $content = file_get_contents($path);

        /**
         * Fields
         */
        $fieldsArray = array_filter($fieldsArray, function ($v) {
            return $v != 'id' && $v != 'componentClass';
        });

        $fieldsArrayConstructor = array_slice($fieldsArray, 0);
        array_unshift($fieldsArrayConstructor, 'id');
        array_push($fieldsArrayConstructor, 'componentClass');
        $fieldsArrayConstructor = array_unique($fieldsArrayConstructor);

        /**
         * Atributos
         */
        $fieldsString = 'private $data; 

    public $children;
            ';
        $content = \Str::replaceFirst(
            '/**',
            $fieldsString .
                '
    /**',
            $content,
        );

        /**
         * Construtor
         */
        $constructorParams = '';
        foreach ($fieldsArrayConstructor as $field) {
            $constructorParams .= '$' . $field . ' = \'\', ';
        }
        $constructorParams = rtrim($constructorParams, ', ');

        $content = \Str::replaceFirst('__construct()', "__construct({$constructorParams})", $content);

        /**
         * Construtor / Inicialização
         */
        $content = \Str::replaceFirst('//', '$this->data = get_defined_vars();', $content);

        /**
         * render() - Parametros
         */
        $content = \Str::replaceLast(');', ', $this->data + [\'children\' => $this->children]);', $content);

        /**
         * Importação
         */
        $content = \Str::replaceFirst(
            'use Closure;',
            "use BaseCms\BaseBlockBuilder\BlockBuilderDefinition;
use BaseCms\BaseField;
use BaseCms\BaseRule;
use BaseCms\BaseUploadDefinition;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;\n",
            $content,
        );

        /**
         * Normalize return view()
         */
        $content = \Str::replaceFirst('View|Closure|string', 'View', $content);

        /**
         * Definition p/ criação do componente
         */
        $content = $this->replaceDefinition($content, $fieldsArray);

        file_put_contents($path, $content);
    }

    private function replaceDefinition($content, $fieldsArray)
    {
        $definition = file_get_contents($this->getStubDefinition());

        $fields = [];
        foreach ($fieldsArray as $v) {
            $fields[] = $this->getFieldDefinition($v);
        }

        $definition = \Str::replace('@FIELDS@', join("\n", $fields), $definition);

        $content = \Str::replaceLast('}', $definition . "\n}", $content);

        return $content;
    }

    private function getFieldDefinition($fieldName)
    {
        $componentName = \Str::snake($this->argument('name'));

        $contentField = file_get_contents($this->getStubTypeDefinition($this->getFieldType($fieldName)));

        $contentField = \Str::replace('dummy_field_id', $fieldName, $contentField);

        $contentField = \Str::replace('dummy_field_label', $this->translateLabel($fieldName), $contentField);

        $contentField = \Str::replace('dummy_table', $componentName . '/' . $fieldName, $contentField);

        return $contentField;
    }

    /**
     *  Label pt-BR
     */
    public function translateLabel($label)
    {
        $translate = file_get_contents(__DIR__ . '/stubs/translate.txt');

        $translate = explode("\n", $translate);
        foreach ($translate as $v) {
            $n = explode('|', $v);

            if (@$n[0] == \Str::lower($label)) {
                return trim(ucfirst($n[1]));
            }
        }

        $label = str_replace(['_', '-'], ' ', \Str::snake($label));

        return \Str::ucfirst($label);
    }

    private function getFieldType($fieldName)
    {
        $matchType = [
            'text' => ['text', 'resume', 'description'],
            'string_image' => ['image', 'thumb'],
            'string_video' => ['video'],
            'boolean' => ['fl_'],
        ];

        foreach ($matchType as $matchKey => $matchValues) {
            foreach ($matchValues as $value) {
                if (strpos($fieldName, $value) !== false) {
                    return $matchKey;
                }
            }
        }

        return 'string';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\View\Components';
    }

    protected function registerComponent($name, $className)
    {
        $path = app_path() . '/Providers/BlockBuilderRegisterServiceProvider.php';

        $content = file_get_contents($path);

        if (strpos($content, \Str::camel($name)) !== false) {
            return;
        }

        $register = file_get_contents($this->getStubRegisterComponent());

        $register = \Str::replace('@field@', \Str::camel($name), $register);
        $register = \Str::replace('@class@', '\\' . $this->qualifyClass($className) . '::class', $register);
        $register = \Str::replace('@title@', ucwords(\Str::snake($name, ' ')), $register);

        $content = \Str::replace('/*blockbuilder:add*/', "$register\n\n        /*blockbuilder:add*/", $content);

        file_put_contents($path, $content);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->argument('name');
        $className = 'Frontend/Block/' . \Str::ucfirst(\Str::camel($name));

        $fields = $this->argument('fields');

        $fieldsArray = array_map(function ($v) {
            return \Str::replace(' ', '', \Str::camel($v));
        }, explode(',', $fields));

        $this->call('make:component', [
            'name' => $className,
            '--force' => true,
        ]);

        $this->replaceClass($className, $fieldsArray);

        $this->replaceView($name, $fieldsArray);

        $this->registerComponent($name, $className);
    }
}
