<?php

namespace BaseCms\BaseBlockBuilder\Console\Command;

use Arcanedev\Support\Console\Command;
use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\BlockBuilderDefinition;
use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use BaseCms\Constants\BaseInputConst;

/**
 *  php artisan blockbuilder:make:page pagename hero,text,wave,key:wave,color,header1,header2,feature1,content1,content2
 */
class BlockBuilderMakePage extends Command
{
    protected $faker;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockbuilder:make:page {page} {blocks} {--lang=pt_BR : Language}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria a estrutura de uma página relacionamento os blocos com cadastros fakes. No final gera um PageSeed p/ manipulação dos dados.';

    public function __construct()
    {
        parent::__construct();

        $this->faker = \Faker\Factory::create();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $page = $this->argument('page');

        $blocks = $this->argument('blocks');

        $lang = $this->option('lang');

        \DB::transaction(function () use ($page, $lang, $blocks) {
            $pageModel = $this->createPage($page, $lang);

            $blocksModel = $this->createBlocks($page, $lang, $blocks);

            $this->associatePageBlocks($pageModel, $blocksModel);
        });

        \Artisan::call('blockbuilder:seed ' . $page, [], $this->getOutput());

        $this->info('Página ' . $page . ' criada com sucesso!');
    }

    protected function createBlocks($page, $lang, $blocks)
    {
        $blocksModels = [];

        $blocks = explode(',', str_replace(' ', '', $blocks));
        foreach ($blocks as $block) {
            $blocksModels[] = $this->createBlock($page, $lang, $block);
        }

        return $blocksModels;
    }

    protected function getComponent($block)
    {
        $components = BlockBuilder::getRegistredComponents();

        if (@!$components[$block]) {
            throw new \Error('invalid block key: ' . $block);
        }

        return $components[$block];
    }

    protected function getBlockByKey($block, $lang)
    {
        $list = explode(':', $block);

        $key = array_pop($list);

        $blockModel = BlockModel::where('key', $key)
            ->where('locale', $lang)
            ->first();

        return $blockModel;
    }

    protected function createBlock($page, $lang, $block)
    {
        //Bloco referenciando uma key existente
        if (strpos($block, 'key:') !== false) {
            $blockModel = $this->getBlockByKey($block, $lang);

            if (!$blockModel) {
                throw new \Error('invalid key: ' . $block . ' | locale:' . $lang);
            }

            return $blockModel;
        }

        //Componente
        $component = $this->getComponent($block);
        $componentClass = $component['componentClass'];
        $keyBlock = $component['key'];

        //Bloco já criado com a key passada
        $key = \Str::snake($page . ucfirst($keyBlock));
        $blockModel = $this->getBlockByKey($key, $lang);
        if ($blockModel) {
            return $blockModel;
        }

        //Cria um novo bloco caso não exista
        return BlockModel::create([
            'key' => $key,
            'componentClass' => $componentClass,
            'locale' => $lang,
            'fl_reusable' => false,
            'params' => $this->fakeParams(BlockBuilder::definitionBy($componentClass)),
        ]);
    }

    protected function fakeParams(BlockBuilderDefinition $definition)
    {
        $data = [];

        foreach ($definition->getFields() as $field) {
            $value = $this->faker->realText(14);

            if ($field->input == BaseInputConst::AireCheckbox) {
                $value = $this->faker->boolean();
            } elseif ($field->input == BaseInputConst::AireTextArea) {
                $value = $this->faker->realText(200);
            } elseif ($field->input == BaseInputConst::AireRichEditor) {
                $value =
                    $this->faker->realText(10, 1) .
                    ' <b>' .
                    $this->faker->realText(10, 1) .
                    '</b> ' .
                    $this->faker->realText(100, 2);
            } elseif ($field->input == BaseInputConst::AireDate || $field->input == BaseInputConst::AireDateTime) {
                $value = $this->faker->date();
            } elseif ($field->input == BaseInputConst::AireSelect) {
                $value = null;
            } elseif ($field->uploadDefinition) {
                if ($field->uploadDefinition->maxWidth || $field->uploadDefinition->maxHeight) {
                    $value = \BaseCms\Helpers\FakerHelper::getImageUrl(
                        $field->uploadDefinition->maxWidth,
                        $field->uploadDefinition->maxHeight,
                    );
                } else {
                    $value = $this->faker->url();
                }
            }

            $data[$field->field] = $value;
        }

        return $data;
    }

    protected function createPage($page, $lang)
    {
        $pageModel = PageModel::where(function ($q) use ($page) {
            $q->where('slug', $page);
            $q->orWhere('key', $page);
        })
            ->where('locale', $lang)
            ->first();

        if ($pageModel) {
            return $pageModel;
        }

        return PageModel::create([
            'key' => $page,
            'slug' => $page,
            'locale' => $lang,
        ]);
    }

    protected function associatePageBlocks($pageModel, $blocksModel)
    {
        $block_id = $pageModel->block_id;

        foreach ($blocksModel as $k => $v) {
            BlockChildrenModel::updateOrCreate(['block_id' => $block_id, 'children_id' => $v->id], ['order' => $k]);
        }
    }
}
