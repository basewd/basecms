<?php

namespace BaseCms\BaseBlockBuilder\Console\Command;

use Arcanedev\LogViewer\Commands\Command;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use Illuminate\Database\Migrations\MigrationCreator;

/**
 * php artisan blockbuilder:seed --page=pageKey
 * php artisan blockbuilder:seed --component=componentKey
 */
class BlockBuilderSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockbuilder:seed {--page= : Page key} {--component= : Component key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera o seeder da página informada de forma recursiva. Além da página, gera os blocos e seus relacionamentos.';

    protected function getSeedStub()
    {
        return __DIR__ . '/stubs/seed.stub';
    }

    protected function getSeedMigrationStub()
    {
        return __DIR__ . '/stubs/seed_migration.stub';
    }

    protected function getClassName($name)
    {
        return \Str::ucfirst(\Str::camel($name)) . 'Seeder';
    }

    protected function getNamespace($name)
    {
        return 'Database\Seeders\BlockBuilder\\' . $this->getClassName($name);
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $pageKey = $this->option('page');
        $componentKey = $this->option('component');

        $key = @$pageKey ? $pageKey : $componentKey;
        $type = @$pageKey ? 'page' : 'component';

        $this->createSeeder($key, $type);

        $this->createMigrationSeed($key, $type);
    }

    protected function createSeeder($componentKey, $type)
    {
        $path = database_path('seeders/BlockBuilder/' . $this->getClassName($componentKey) . '.php');

        $stub = file_get_contents($this->getSeedStub());

        $createOrUpdateModels =
            $type == 'page' ? $this->createOrUpdatePage($componentKey) : $this->createOrUpdateComponent($componentKey);

        $replace = [
            '{{createOrUpdateModels}}' => join("\n\n", $createOrUpdateModels),
            '{{classname}}' => $this->getClassName($componentKey),
        ];

        $stub = str_replace(array_keys($replace), array_values($replace), $stub);

        @mkdir(dirname($path), 0777, true);
        @touch($path);
        file_put_contents($path, $stub);

        $this->info('Seeder ' . $path . ' gerado com sucesso!');
    }

    protected function createOrUpdatePage($pageKey)
    {
        $list = PageModel::where('key', $pageKey)->get();

        $replaceIds = [];
        $listBlocks = [];

        $createOrUpdateModels = [];
        foreach ($list as $v) {
            $createOrUpdateModels[] = $this->deepBlockModels($v->block_id, 0, $listBlocks, $replaceIds);

            $createOrUpdateModels[] =
                'PageModel::updateOrCreate(' .
                $this->extractKeys($v) .
                ',' .
                str_replace(
                    "'block_id' => " . $v->block_id,
                    "'block_id' => " . '$block' . $v->block_id . '->id',
                    var_export($this->normalizeData($v->toArray()), true),
                ) .
                ');';
        }

        $createOrUpdateModels = array_filter(\Arr::flatten($createOrUpdateModels));

        return $createOrUpdateModels;
    }

    protected function createOrUpdateComponent($componentKey)
    {
        $list = BlockModel::where('key', $componentKey)->get();

        $replaceIds = [];
        $listBlocks = [];

        $createOrUpdateModels = [];
        foreach ($list as $v) {
            $createOrUpdateModels[] = $this->deepBlockModels($v->id, 0, $listBlocks, $replaceIds);
        }

        $createOrUpdateModels = array_filter(\Arr::flatten($createOrUpdateModels));

        return $createOrUpdateModels;
    }

    protected function deepBlockModels($blockId, $deep = 0, &$listBlocks, &$replaceIds)
    {
        if ($deep > 8) {
            return;
        }

        if (array_search($blockId, $listBlocks) !== false) {
            return;
        }

        $block = BlockModel::where('id', $blockId)->first();

        $listBlocks[] = $blockId;

        $createOrUpdateModels = [];
        $createOrUpdateModels[] =
            '$block' .
            $block->id .
            ' = BlockModel::updateOrCreate(' .
            $this->extractKeys($block) .
            ',' .
            var_export($this->normalizeData($block->toArray()), true) .
            ');';

        $this->info('key extraída: ' . $block->id . ' - ' . $block->key);

        foreach ($block->blockChildrens as $v) {
            $createOrUpdateModels[] = $this->deepBlockModels($v->children_id, $deep + 1, $listBlocks, $replaceIds);
            $createOrUpdateModels[] =
                'BlockChildrenModel::updateOrCreate(["block_id"=>$block' .
                $v->block_id .
                '->id, "children_id"=>$block' .
                $v->children_id .
                '->id],' .
                var_export(['order' => $v->order], true) .
                ');';
        }

        return $createOrUpdateModels;
    }

    private function extractKeys($data, $keys = ['locale', 'key'])
    {
        $str = [];

        foreach ($keys as $v) {
            $str[] = '"' . $v . '" => "' . $data[$v] . '"';
        }

        return '[' . join(',', $str) . ']';
    }

    private function normalizeData($list)
    {
        unset($list['id']);
        unset($list['created_at']);
        unset($list['updated_at']);
        return $list;
    }

    protected function createMigrationSeed($name, $type = 'page')
    {
        $fullname = 'seed_' . $type . '_' . $name;

        $creator = new MigrationCreator(app('files'), '');

        $file = $creator->create($fullname, database_path('migrations'));

        $migrationPath = $file;

        $migrationContent = file_get_contents($migrationPath);

        $seedMigrationContent = file_get_contents($this->getSeedMigrationStub());
        $seedMigrationContent = str_replace('{{className}}', $this->getClassName($name), $seedMigrationContent);

        $migrationContent = \Str::replaceFirst('//', $seedMigrationContent, $migrationContent);

        file_put_contents($migrationPath, $migrationContent);

        rename($migrationPath, $migrationPath . '.disabled');

        $this->info('Migration ' . $migrationPath . '.disabled' . ' gerado com sucesso!');
    }
}
