<?php

namespace BaseCms\BaseBlockBuilder\Domains\Block\Definition;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseDefinition;
use BaseCms\BaseField;
use BaseCms\BaseRule;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;
use BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController;
use BaseCms\BaseBlockBuilder\Domains\Block\Requests\BlockRequest;
use BaseCms\BaseDefaultRenderParams;
use BaseCms\BaseUploadDefinition;
use BaseCms\Datatables\Data\BaseDataArray;
use BaseCms\Datatables\Data\BaseDataModel;
use BaseCms\Models\LocaleModel;

class BlockDefinition extends BaseDefinition
{
    public function __construct()
    {
        $this->setTitle('Componente');

        $this->setTitlePlural('Componentes');

        $this->setRouteAlias('blocks');

        $this->setModelClass(BlockModel::class);

        $this->setControllerClass(BlockController::class);

        $this->setFormRequestClass(BlockRequest::class);

        $this->setExportable(false);

        $this->setFilterLocale(true);

        $this->setFields([
            BaseField::newObj('id', 'Código', BaseInputConst::AireHidden, BaseColumnConst::Number)->setPrimaryKey(true),

            BaseField::newObj('key', 'Chave de Registro', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Pode ser utilizado p/ instanciar um componente diretamente.<br>* Caso o componente seja multilingua, cadastrar a mesma key p/ as outras línguas.',
                    ],
                ]),

            BaseField::newObj('locale', 'Língua', BaseInputConst::AireSelect, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setData(new BaseDataModel(LocaleModel::class, 'id', 'name', [['orderBy', 'order']])),

            BaseField::newObj('tags', 'Tags', BaseInputConst::AireTextArea, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Adicione tags para facilitar a busca/entendimento do componente.<br/>* Tags separados por virgula.',
                    ],
                ]),

            BaseField::newObj('componentClass', 'Componente', BaseInputConst::AireHidden, BaseColumnConst::Text)
                ->setRule(new BaseRule('required'))
                ->setInputMethods([['readOnly', true]])
                ->setData(
                    new BaseDataArray(
                        BlockBuilder::getComponentsByClass()
                            ->pluck('title', 'componentClass')
                            ->toArray(),
                    ),
                ),

            BaseField::newObj('componentTitle', 'Componente', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setFilterable(false)
                ->setExportable(false)
                ->setOrderable(false)
                ->setValueRender(function ($data) {
                    return $data->component_data['title'] ?? 'Não registrado ' . $data['componentClass'];
                }),

            BaseField::newObj('params', 'Parâmetros', BaseInputConst::AireTextArea, BaseColumnConst::Text),

            BaseField::newObj('preview', 'Modelo/Preview', BaseInputConst::AireInput, BaseColumnConst::Image)
                ->setUploadDefinition(BaseUploadDefinition::newObj('components/'))
                ->setValueRender(function ($data) {
                    return storage_url($data->preview ? $data->preview : @$data->component_data['thumb']);
                })
                ->setFilterable(false),

            BaseField::newObj('preview_data', 'Dados')
                ->setValueRender(function ($data) {
                    return BlockBuilder::previewData($data->params, $data->definition());
                })
                ->setDefaultRenderParams(BaseDefaultRenderParams::newObj()->setTextEllipsisLength(null))
                ->setFilterable(false)
                ->setOrderable(false),

            BaseField::newObj('fl_reusable', 'Reutilizável?', BaseInputConst::AireCheckbox, BaseColumnConst::Boolean)
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Permite reutilizar o componente em outros locais, aparecendo na aba "Componente Já Existente".',
                    ],
                ]),

            BaseField::newObj('fl_draft', 'Rascunho?', BaseInputConst::AireCheckbox, BaseColumnConst::Boolean)
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([['helpText', 'Componente em modo rascunho, não irá aparecer no portal/aplicação.']]),

            BaseField::newObj('children_block_count', 'Utilizações', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setOrderable(false)
                ->setFilterable(false),

            BaseField::newObj(
                'created_at',
                'Cadastrado em',
                BaseInputConst::AireDateTime,
                BaseColumnConst::Date,
            )->setRule(new BaseRule('nullable')),

            BaseField::newObj(
                'updated_at',
                'Atualizado em',
                BaseInputConst::AireDateTime,
                BaseColumnConst::Date,
            )->setRule(new BaseRule('nullable')),
        ]);

        $this->setTableFields([
            'id',
            'locale',
            'key',
            'preview',
            'componentTitle',
            'preview_data',
            'tags',
            'children_block_count',
            'fl_reusable',
            'created_at',
            // 'updated_at',
        ]);

        $this->setFormFields(['locale', 'key', 'tags', 'componentClass', 'fl_reusable', 'fl_draft']);

        $this->setViewFields(['id', 'tags', 'componentClass', 'created_at', 'updated_at']);
    }

    public function setDefinitionCreate($parentModel = null)
    {
        if ($parentBlockId = request()->parentBlockId) {
            $this->getField('locale')->setInputMethods([
                ['readOnly', true],
                ['value', BlockModel::where('id', $parentBlockId)->first()->locale],
            ]);
        }

        $this->getField('componentClass')->setData(null);
    }

    public function setDefinitionUpdate($block, $parentModel = null)
    {
        if ($block) {
            $this->getField('componentClass')->setData(null);
        }
    }

    public function definitionGroupKeys()
    {
        $this->setTableFields(['id', 'locale', 'preview', 'componentTitle', 'preview_data']);

        $this->setFormFields(['locale', 'componentClass']);

        $this->setFormEditFields(['componentClass']);
    }

    private static $_instance;

    /**
     * @return BlockDefinition
     */
    public static function instance(): BaseDefinition
    {
        if (!self::$_instance) {
            self::$_instance = new BlockDefinition();
        }

        return self::$_instance;
    }
}
