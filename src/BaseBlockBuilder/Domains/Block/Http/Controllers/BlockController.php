<?php

namespace BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Domains\Block\Definition\BlockDefinition;
use BaseCms\BaseBlockBuilder\Domains\Block\Repositories\BlockRepository;
use BaseCms\BaseBlockBuilder\Domains\Block\Requests\BlockRequest;
use BaseCms\Datatables\Helpers\BaseDatatableExport;
use BaseCms\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    /**
     * @var BlockRepository
     */
    private $repository;

    /**
     * @var BlockDefinition
     */
    private $definition;

    /**
     * __construct
     *
     * @param BlockRepository $repository
     * @param BlockDefinition $definition
     * @return void
     */
    public function __construct(BlockRepository $repository, BlockDefinition $definition)
    {
        $this->repository = $repository;
        $this->definition = $definition;
    }

    /**
     * Listagem dos registros
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $definition = $this->definition;
        return view('base::backend.blockbuilder.blockbuilder_blocks.index', compact('definition'));
    }

    /**
     * Listagem dos registros
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexNotused()
    {
        $definition = $this->definition;
        return view('base::backend.blockbuilder.blockbuilder_blocks.index_notused', compact('definition'));
    }

    /**
     * API Datatable "index()"
     *
     * https://github.com/yajra/laravel-datatables
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatable(Request $request)
    {
        $query = BlockModel::select('blocks.*')
            ->whereNotIn('componentClass', BlockBuilder::hideComponents())
            ->withCount('childrenBlock');

        if ($request->key) {
            $query->whereIn('key', explode(',', $request->key));
        }

        if ($request->notused) {
            $query->whereDoesntHave('childrenBlock');
        }

        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $datatable = DataTables::eloquent($query)
            ->editColumn('tags', function ($row) {
                return tableTags($row->tags);
            })
            ->addColumn('actions', function ($row) {
                $editUrl = route('admin.blocks.edit', $row->id);
                $deleteUrl = route('admin.blocks.destroy', $row->id);
                $viewUrlBlank = route('frontend.blockbuilder.component', $row->id) . '?blockbuilder=true';

                $duplicateUrl = route('admin.blocks.duplicate', $row->id);

                return view(
                    'base::components.datatable-actions',
                    compact('editUrl', 'deleteUrl', 'viewUrlBlank', 'duplicateUrl', 'row'),
                );
            })
            ->rawColumns(['tags', 'preview', 'preview_data', 'componentThumb'])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }

    /**
     * @param Request $request
     *
     * @return StreamedResponse
     */
    public function exportCsv(Request $request)
    {
        $datatable = DataTables::eloquent(BlockModel::select('blocks.*'));

        return BaseDatatableExport::exportCsv($datatable, $this->definition);
    }

    /**
     * Visualização
     *
     * @param BlockModel $block
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function view(BlockModel $block)
    {
        $definition = $this->definition;
        $definition->setDefinitionView($block);

        return view('base::backend.blockbuilder.blockbuilder_blocks.view', compact('block', 'definition'));
    }

    public function selectComponent(
        bool $isModal = false,
        ?string $parentBlockId = null,
        ?string $keys = '',
        bool $inner = false
    ) {
        $parentBlockDefinition = null;

        //Hide pageBlock e childs components
        $list = BlockBuilder::registredComponents()->filter(function ($v) {
            return !BlockBuilder::hideComponents()->contains($v);
        });

        if ($parentBlockId) {
            $parentBlockDefinition = BlockBuilder::definitionByBlockId($parentBlockId);

            //Sobrescreve a lista com base no que foi liberado diretamente no componente
            $list = BlockBuilder::allowedComponents($parentBlockId);

            //Caso não tenha outro tipo de filho para ser adicionado, redireciona direto p/ o cadastro
            if (!$parentBlockDefinition->isAllowReuseComponents() && $list->count() == 1) {
                // http: //localhost:8000/admin/blocks/create?componentClass=App%5CView%5CComponents%5CFrontend%5CBlock%5CFeature%5CFeature1ChildBlock&isModal=1&parentBlockId=8
                return redirect()->route('admin.blocks.create', [
                    'componentClass' => $list->first(),
                    'isModal' => $isModal,
                    'parentBlockId' => $parentBlockId,
                    'keys' => $keys,
                    'inner' => $inner,
                ]);
            }
        }

        $definition = $this->definition;

        $components = BlockBuilder::getComponentsByClass();

        return view(
            'base::backend.blockbuilder.blockbuilder_blocks.select_component',
            compact(
                'definition',
                'list',
                'isModal',
                'parentBlockDefinition',
                'parentBlockId',
                'components',
                'keys',
                'inner',
            ),
        );
    }
    /**
     * API Datatable "index()"
     *
     * https://github.com/yajra/laravel-datatables
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatableSelect(Request $request)
    {
        $parentBlockId = $request->parentBlockId;

        $query = BlockModel::select('blocks.*')
            ->whereNotIn('componentClass', BlockBuilder::hideComponents())
            ->where('fl_reusable', 1);

        if (isset($parentBlockId)) {
            $parentBlock = BlockModel::where('id', $parentBlockId)->first();

            $parentBlockDefinition = BlockBuilder::definitionByBlockId($parentBlockId);
            $query->where('id', '!=', $parentBlockId); //Exclui ele mesmo
            $query->where('locale', '=', $parentBlock->locale);

            if (
                $parentBlockDefinition->isAllowChildrens() &&
                $parentBlockDefinition->getAllowedChildrens() &&
                $parentBlockDefinition->getAllowedChildrens()->count() > 0
            ) {
                $query->whereIn('componentClass', $parentBlockDefinition->getAllowedChildrens());
            }
        }

        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $datatable = DataTables::eloquent($query)
            ->addColumn('actions', function ($row) use ($parentBlockId) {
                $addUrl = null;

                if (isset($parentBlockId)) {
                    $addUrl = route('admin.blocks.addChildToBlock', [
                        'block' => $parentBlockId,
                        'children' => $row->id,
                    ]);
                }

                return view('base::components.datatable-actions', compact('addUrl', 'row'));
            })
            ->rawColumns(['preview', 'preview_data'])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }

    public function addChildToBlock(Request $request, BlockModel $block, string $children)
    {
        BlockChildrenModel::create(['block_id' => $block->id, 'children_id' => $children]);

        return '<script>parent.modalBlockBuilderClose(true);</script>';
    }

    /**
     * Cadastro
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create(Request $request)
    {
        $isModal = (bool) $request->isModal;
        $parentBlockId = @$request->parentBlockId;
        $keys = @$request->keys; //cadastro através do groupKeys
        $inner = @$request->inner;

        $definition = $this->definition;
        $definition->setDefinitionCreate();
        if ($keys) {
            $definition->definitionGroupKeys();
        }

        $urlSubmit = 'admin.blocks.store';

        $componentClass = $request->componentClass;

        if (isset($componentClass) && BlockBuilder::isRegistredComponent($componentClass)) {
            $definitionBlock = BlockBuilder::definitionBy($componentClass);
        } else {
            return redirect(route('admin.blocks.selectComponent'));
        }

        $key = '';
        if ($parentBlockId) {
            $componentClassKey = BlockBuilder::registredComponents()->flip()[$componentClass];
            $parentBlock = BlockModel::where('id', $parentBlockId)->first();
            $countItems = BlockModel::count();
            //Pode ser que não tenha key na página ou no parent
            if (@$parentBlock->key) {
                $key = $parentBlock->key . '_' . $componentClassKey . '_' . $countItems;
            }
        }

        return view(
            'base::backend.blockbuilder.blockbuilder_blocks.create_update',
            compact(
                'urlSubmit',
                'definition',
                'definitionBlock',
                'componentClass',
                'isModal',
                'parentBlockId',
                'key',
                'keys',
                'inner',
            ),
        );
    }

    /**
     * Cadastro registro (POST)
     *
     * @param BlockRequest $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function store(BlockRequest $request)
    {
        $block = $this->repository->create($request->validated());

        if ($request->redirect_to_edit) {
            return redirect(
                route('admin.blocks.edit', [
                    'block' => $block->id,
                    'isModal' => $request->isModal,
                    'isSaved' => true,
                ]) .
                    '#' .
                    $request->redirect_to_edit,
            )->withFlashSuccess('Registro criado com sucesso!');
        } elseif ($request->isModal) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        } else {
            return redirect(route('admin.blocks.index'))->withFlashSuccess('Registro criado com sucesso!');
        }
    }

    /**
     * Edição registro
     *
     * @param BlockModel $block
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Request $request, BlockModel $block)
    {
        $isModal = (bool) $request->isModal ?? false;
        $inner = (bool) $request->inner ?? false;

        $parentBlockId = @$request->parentBlockId;

        $definition = $this->definition;
        $definition->setDefinitionUpdate($block);

        $urlSubmit = 'admin.blocks.update';

        $componentClass = $block->componentClass;
        $definitionBlock = BlockBuilder::definitionBy($componentClass);

        return view(
            'base::backend.blockbuilder.blockbuilder_blocks.create_update',
            compact(
                'block',
                'urlSubmit',
                'definition',
                'componentClass',
                'definitionBlock',
                'isModal',
                'inner',
                'parentBlockId',
            ),
        );
    }

    /**
     * Atualizar registro (POST)
     *
     * @param BlockModel $block
     * @param BlockRequest $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function update(BlockModel $block, BlockRequest $request)
    {
        $this->repository->update($block, $request->validated());

        if ($request->isModal) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        } else {
            return redirect(route('admin.blocks.index'))->withFlashSuccess('Registro atualizado com sucesso!');
        }
    }

    /**
     * Remover registro
     *
     * @param BlockModel $block
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function destroy(BlockModel $block)
    {
        $this->repository->delete($block);

        return redirect(route('admin.blocks.index'))->withFlashSuccess('Registro removido com sucesso!');
    }

    /** Fim ordenamento **/

    /**
     * Ação em vários registros de uma vez só (precisa ser habilitado no Definition)
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function multipleSelect(Request $request)
    {
        $this->repository->multipleSelect($request->all());

        return redirect(route('admin.blocks.index'))->withFlashSuccess('Registros removidos com sucesso!');
    }

    /**
     *
     */

    /**
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexGroupKeys(string $keys, bool $inner = false)
    {
        $definition = $this->definition;
        $definition->definitionGroupKeys();

        $urlDatatable = route('admin.blocks.datatableGroupKeys', ['keys' => $keys, 'inner' => $inner]);

        return view(
            'base::backend.blockbuilder.blockbuilder_blocks.index_group_keys',
            compact('definition', 'urlDatatable', 'keys', 'inner'),
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatableGroupKeys(Request $request, string $keys, bool $inner = false)
    {
        $this->definition->definitionGroupKeys();

        $query = BlockModel::select('blocks.*')
            ->whereNotIn('componentClass', BlockBuilder::hideComponents())
            ->withCount('childrenBlock')
            ->where(function ($q) use ($keys) {
                $arKeys = explode(',', $keys);
                $q->whereIn('key', $arKeys)->orWhereIn('id', $arKeys);
            });

        $datatable = DataTables::eloquent($query)
            ->addColumn('actions', function ($row) use ($keys, $inner) {
                $editUrl = route('admin.blocks.editGroupKeys', [
                    'block' => $row->id,
                    'keys' => @$keys,
                    'inner' => $inner,
                ]);
                $viewUrlBlank = route('frontend.blockbuilder.component', $row->id);

                return view('base::components.datatable-actions', compact('editUrl', 'viewUrlBlank', 'row'));
            })
            ->rawColumns(['tags', 'preview', 'preview_data', 'componentThumb'])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }

    /**
     * Edição registro
     *
     * @param BlockModel $block
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function editGroupKeys(Request $request, BlockModel $block, string $keys, bool $inner = false)
    {
        $isModal = (bool) $request->isModal ?? false;
        $parentBlockId = @$request->parentBlockId;

        $definition = $this->definition;
        $definition->setDefinitionUpdate($block);
        $definition->definitionGroupKeys();

        $urlSubmit = 'admin.blocks.updateGroupKeys'; //, ['block' => $block, 'keys' => $keys]);

        $componentClass = $block->componentClass;
        $definitionBlock = BlockBuilder::definitionBy($componentClass);

        return view(
            'base::backend.blockbuilder.blockbuilder_blocks.create_update',
            compact(
                'keys',
                'inner',
                'block',
                'urlSubmit',
                'definition',
                'componentClass',
                'definitionBlock',
                'isModal',
                'parentBlockId',
            ),
        );
    }

    /**
     * Atualizar registro (POST)
     *
     * @param BlockModel $block
     * @param BlockRequest $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function updateGroupKeys(BlockModel $block, BlockRequest $request, string $keys, bool $inner = false)
    {
        $this->repository->update($block, $request->validated());

        if (@$request->isModal) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        }

        return redirect(route('admin.blocks.indexGroupKeys', ['keys' => $keys, 'inner' => $inner]))->withFlashSuccess(
            'Registro atualizado com sucesso!',
        );
    }

    public function selectComponentGroupKeys(
        string $keys,
        bool $isModal = false,
        ?string $parentBlockId = null,
        bool $inner = false
    ) {
        //TODO: inner
        return $this->selectComponent($isModal, $parentBlockId, $keys, $inner);
    }

    /**
     * Duplicar registro (GET)
     *
     * @param PageModel $page
     * @param Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function duplicate(BlockModel $block, Request $request)
    {
        $block = $this->repository->duplicate($block);

        if ($request->isModal) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        } else {
            return redirect(route('admin.blocks.index'))->withFlashSuccess('Registro atualizado com sucesso!');
        }
    }
}
