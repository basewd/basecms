<?php

namespace BaseCms\BaseBlockBuilder\Domains\Block\Policy;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class BlockPolicy
{
    use HandlesAuthorization;

    public function list(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.list') : true;
    }

    public function view(?User $user, BlockModel $block)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.view') : true;
    }

    public function create(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;
    }

    public function update(?User $user, BlockModel $block)
    {
        $permission = config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;

        return $permission && BlockBuilder::isRegistredComponent($block->componentClass);
    }

    public function delete(?User $user, BlockModel $block)
    {
        $permission = config('blockbuilder.permission') ? $user->can('admin.blockbuilder.delete') : true;
       
        return $permission && BlockChildrenModel::where('children_id', $block->id)->count() <= 1;
    }

    public function forceDelete(?User $user, BlockModel $blockDeleted)
    {
        return false;
    }

    public function listRestore(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.delete') : true;
    }

    public function restore(?User $user, BlockModel $blockDeleted)
    {
        return false;
    }

    public function listGroupKeys(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.list') : true;
    }

    public function duplicate(?User $user, BlockModel $block)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;
    }
}
