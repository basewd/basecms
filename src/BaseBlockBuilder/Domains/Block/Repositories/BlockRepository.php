<?php

namespace BaseCms\BaseBlockBuilder\Domains\Block\Repositories;

use Illuminate\Support\Facades\DB;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Domains\Block\Definition\BlockDefinition;
use BaseCms\Helpers\BaseUploadHelper;
use BaseCms\Repositories\BaseRepository;

use App\Exceptions\GeneralException;
use BaseCms\BaseBlockBuilder\Domains\Page\Repositories\PageRepository;
use BaseCms\BaseBlockBuilder\Models\PageModel;

class BlockRepository extends BaseRepository
{
    public function __construct(BlockModel $block)
    {
        $this->model = $block;
    }

    private function addChildrenTo($parentBlockId, $blockId)
    {
        BlockChildrenModel::create(['block_id' => $parentBlockId, 'children_id' => $blockId]);
    }

    private function hooks($model)
    {
        $definition = $model->definition();

        if ($hookSaved = $definition->getHookSaved()) {
            $hookSaved($model);
        }
    }

    private function getData($data)
    {
        unset($data['parentBlockId']);

        $listPrivateFields = app(BlockModel::class)->getFillable() + ['name'];
        // ['id', 'key', 'name', 'componentClass', 'tags', 'locale', 'fl_reusable'];

        $dataBlock = [];
        foreach ($listPrivateFields as $field) {
            //Não mexe nas propriedades que não são passadas pelo formulário (Ex: edição no groupKeys)
            if (isset($data[$field])) {
                $dataBlock[$field] = @$data[$field];
                unset($data[$field]);
            }
        }

        $dataBlock['params'] = $data;

        return $dataBlock;
    }

    public function saveBlockFiles($dataBlock, $block)
    {
        //Upload files
        $files = BaseUploadHelper::saveFilesFromRequest($block, BlockBuilder::definitionBy($block->componentClass));

        if (isset($files) && @count($files) > 0) {
            foreach ($files as $k => $v) {
                $dataBlock['params'][$k] = $v;
            }

            $block->update($dataBlock);
        }
        //End Upload files
    }
    /**
     * Criar
     *
     * @param  mixed $data
     * @return BlockModel
     */
    public function create(array $data): BlockModel
    {
        return DB::transaction(function () use ($data) {
            try {
                $parentBlockId = @$data['parentBlockId'];

                $dataBlock = $this->getData($data);

                $block = $this->model::create($dataBlock);

                // Não precisa, não tem upload
                // BaseUploadHelper::saveFilesFromRequest($block, BlockDefinition::instance());

                $this->saveBlockFiles($dataBlock, $block);

                if ($parentBlockId) {
                    $this->addChildrenTo($parentBlockId, $block->id);
                }

                $this->hooks($block);

                if ($block) {
                    return $block;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e, BlockDefinition::instance());
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, BlockDefinition::instance());
            }
        });
    }

    /**
     * Atualizar
     *
     * @param  BlockModel $block
     * @param  mixed $data
     * @return BlockModel
     */
    public function update(BlockModel $block, array $data): BlockModel
    {
        return DB::transaction(function () use ($block, $data) {
            try {
                $parentBlockId = @$data['parentBlockId'];

                $dataBlock = $this->getData($data);

                $block->update($dataBlock);

                // Não precisa, não tem upload
                // BaseUploadHelper::saveFilesFromRequest($block, BlockDefinition::instance());

                $this->saveBlockFiles($dataBlock, $block);

                if ($parentBlockId) {
                    $this->addChildrenTo($parentBlockId, $block->id);
                }

                $this->hooks($block);

                if ($block) {
                    return $block;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e, BlockDefinition::instance());
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, BlockDefinition::instance());
            }
        });
    }

    /**
     * Remover
     *
     * @param  BlockModel $block
     * @return bool
     */
    public function delete(BlockModel $block): bool
    {
        return DB::transaction(function () use ($block) {
            try {
                if ($block->delete()) {
                    return true;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e);
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, BlockDefinition::instance());
            }
        });
    }

    /**
     * Multipla seleção tabela
     *
     * @param mixed $data
     */
    public function multipleSelect(array $data)
    {
        return DB::transaction(function () use ($data) {
            $listSelected = $data['multipleSelect'];

            foreach ($listSelected as $v) {
                $block = BlockModel::findOrFail($v);
                if (\Auth::user()->can('delete', $block)) {
                    $block->delete();
                } else {
                    throw new \Exception(__('Não é possível remover o registro selecionado. Código:') . $v);
                }
            }

            return true;
        });
    }

    public function duplicate(BlockModel $block)
    {
        return DB::transaction(function () use ($block) {
            $blockId = BlockBuilder::duplicateBlock($block->id);

            if ($block->key) {
                $newBlock = BlockModel::where('id', $blockId)->first();
                $newBlock->key = $block->key . '_dup_' . $block->id;
                $newBlock->save();
            }

            return true;
        });
    }
}
