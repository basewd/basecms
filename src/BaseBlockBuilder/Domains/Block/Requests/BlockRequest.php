<?php

namespace BaseCms\BaseBlockBuilder\Domains\Block\Requests;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Domains\Block\Definition\BlockDefinition;
use Illuminate\Foundation\Http\FormRequest;

class BlockRequest extends FormRequest
{
    /**
     * Determine se o usuário está autorizado a fazer esta solicitação.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function definitionParams()
    {
        $block = \Route::current()->parameter('block');

        return BlockBuilder::definitionBy($block ? $block->componentClass : @request()->componentClass);
    }
    /**
     * Regras de validação que se aplicam à solicitação.
     *
     * @return array
     */
    public function rules()
    {
        $rules = BlockDefinition::instance()->formRequestRules();

        $def = $this->definitionParams();
        if ($def) {
            $rules += $def->formRequestRules();
        }

        $rules += ['parentBlockId' => 'nullable'];

        return $rules;
    }

    /**
     * Atributos personalizados para erros do validador.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = BlockDefinition::instance()->formRequestAttributes();

        $def = $this->definitionParams();
        if ($def) {
            $attributes += $def->formRequestAttributes();
        }

        return $attributes;
    }

    /**
     * Mensagens de erro para as regras de validação definidas.
     *
     * @return array
     */
    public function messages()
    {
        $messages = BlockDefinition::instance()->formRequestMessages();

        $def = $this->definitionParams();
        if ($def) {
            $messages += $def->formRequestMessages();
        }

        return $messages;
    }
}
