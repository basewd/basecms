<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Definition;

use BaseCms\BaseDefinition;
use BaseCms\BaseField;
use BaseCms\BaseRule;
use BaseCms\BaseUploadDefinition;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;
use BaseCms\Datatables\Data\BaseDataModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use BaseCms\BaseBlockBuilder\Domains\Page\Http\Controllers\PageController;
use BaseCms\BaseBlockBuilder\Domains\Page\Requests\PageCreateRequest;
use BaseCms\BaseBlockBuilder\Domains\Page\Requests\PageUpdateRequest;
use BaseCms\Models\LocaleModel;

class PageDefinition extends BaseDefinition
{
    public function __construct()
    {
        $this->setTitle('Página');

        $this->setTitlePlural('Páginas');

        $this->setRouteAlias('pages');

        $this->setModelClass(PageModel::class);

        $this->setControllerClass(PageController::class);

        $this->setFormRequestCreateClass(PageCreateRequest::class);

        $this->setFormRequestUpdateClass(PageUpdateRequest::class);

        $this->enableRouteRestore();

        $this->enableRouteForceDelete();

        //$this->enableMultipleSelectTable();

        $this->setFilterLocale(true);

        $this->setExportable(false);

        $this->setFields([
            BaseField::newObj('id', 'Código', BaseInputConst::AireHidden, BaseColumnConst::Number)->setPrimaryKey(true),

            BaseField::newObj('block_id', 'Bloco Pai', BaseInputConst::AireHidden)
                ->setRule(new BaseRule('nullable'))
                ->setFilterable(false),

            BaseField::newObj('locale', 'Língua', BaseInputConst::AireSelect, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setData(new BaseDataModel(LocaleModel::class, 'id', 'name', [['orderBy', 'order']])),

            BaseField::newObj('key', 'Chave de Registro', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setRule(new BaseRule('required'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Define o nome da rota, p/ ser chamado nos links. Ex: Caso o valor seja "about", o nome do route fica "front.pages.about".',
                    ],
                ]),

            BaseField::newObj('slug', 'Slug', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Url da página.<br>Ex: /sobre-nos, /sobre-nos/{parametro1}<br>PS: Não deve ser adicionado a lingua, isso é tratado automaticamente, quando necessário.',
                    ],
                ]),

            BaseField::newObj('breadcrumbs', 'Breadcrumbs', BaseInputConst::AireSelect, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setData(
                    new BaseDataModel(PageModel::class, 'route_key', 'meta_title', [['orderBy', 'meta_title']], null),
                )
                ->setInputMethods([['multiple', true]]),

            BaseField::newObj('template', 'Template', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setRule(new BaseRule('nullable'))
                ->setFilterable(false),

            BaseField::newObj('sep_seo', 'SEO', BaseInputConst::Separator)
                // ->setInputMethods([['description', 'foo-bar']])
                ->setFilterable(false)
                ->setExportable(false),

            BaseField::newObj('meta_title', 'Título', BaseInputConst::AireInput, BaseColumnConst::Text)
                ->setRule(new BaseRule('max:255'))
                ->setInputMethods([['helpText', 'Recomenda-se que o título tenha entre 50 a 60 caracteres.']])
                ->setFilterable(false),

            BaseField::newObj('meta_description', 'Descrição', BaseInputConst::AireTextArea, BaseColumnConst::Text)
                ->setRule(new BaseRule('max:160'))
                ->setInputMethods([
                    ['helpText', 'Recomenda-se que a meta description tenha entre 50 a 160 caracteres.'],
                ])
                ->setFilterable(false),

            // Não é mais recomendado sua utilização
            // BaseField::newObj('meta_keywords', 'Keywords', BaseInputConst::AireInput, BaseColumnConst::Text)
            //     ->setRule(new BaseRule('max|255'))
            //     ->setFilterable(false),

            BaseField::newObj('meta_image', 'Imagem (1200x675)', BaseInputConst::AireFile, BaseColumnConst::Image)
                ->setRule(new BaseRule('nullable|file|max:4096'))
                ->setUploadDefinition(BaseUploadDefinition::newObj('pages/meta_image', 1200, null, false))
                ->setInputMethods([['accept', 'image/*']])
                ->setFilterable(false),

            BaseField::newObj(
                'meta_all',
                'Metatags Personalizadas',
                BaseInputConst::AireTextArea,
                BaseColumnConst::Text,
            )
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Utilizado p/ adicionar metatags custom, como json+ld. Adicionar a tag completa, ex: "&#x3C;meta xxxx/&#x3E;"',
                    ],
                ])
                ->setFilterable(false),

            BaseField::newObj('sep_components', 'Componentes', BaseInputConst::Separator)
                ->setFilterable(false)
                ->setExportable(false),

            BaseField::newObj(
                'created_at',
                'Cadastrado em',
                BaseInputConst::AireDateTime,
                BaseColumnConst::Date,
            )->setRule(new BaseRule('nullable')),

            BaseField::newObj(
                'updated_at',
                'Atualizado em',
                BaseInputConst::AireDateTime,
                BaseColumnConst::Date,
            )->setRule(new BaseRule('nullable')),

            BaseField::newObj('sep_advanced', 'Avançado', BaseInputConst::Separator)
                ->setFilterable(false)
                ->setExportable(false),

            BaseField::newObj(
                'fl_hide_sitemap',
                'Esconder no Sitemap?',
                BaseInputConst::AireCheckbox,
                BaseColumnConst::Boolean,
            )
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([['helpText', 'Não aparecer no /sitemap.xml']]),

            BaseField::newObj(
                'fl_no_indexable',
                'Não indexar Página?',
                BaseInputConst::AireCheckbox,
                BaseColumnConst::Boolean,
            )
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    ['helpText', 'Força a utilização da metatag noindex, para não indexar no Google/etc.'],
                ]),

            BaseField::newObj(
                'fl_lock_page',
                'Bloquear alteração na Chave de Registro',
                BaseInputConst::AireCheckbox,
                BaseColumnConst::Boolean,
            )
                ->setRule(new BaseRule('nullable'))
                ->setInputMethods([
                    [
                        'helpText',
                        'Bloqueia alterações na chave de registro da página. Utilizar isso quando a página é carregada de forma direta no código.',
                    ],
                ]),
        ]);

        $this->setTableFields([
            'id',
            'locale',
            'slug',
            // 'preview',
            // 'template',
            'meta_title',
            'meta_description',
            // 'meta_image',
            // 'meta_all',
            // 'block_id',
            // 'created_at',
            // 'updated_at',
        ]);

        $this->setFormFields([
            'locale',
            'slug',
            'key',

            // 'template',

            'sep_seo',
            'breadcrumbs',

            'meta_title',
            'meta_description',
            'meta_image',
            'meta_all',

            'sep_advanced',
            'fl_hide_sitemap',
            'fl_no_indexable',
            'fl_lock_page',

            'sep_components',
            'block_id',
        ]);

        /*
        $this->setFormEditFields([
            "locale",
            "slug",
            "template",
            "meta_title",
            "meta_description",
            "meta_image",
            'meta_all',
            "block_id",
        ]);
        */

        $this->setViewFields([
            'id',
            'locale',
            'slug',
            'template',
            'meta_title',
            'meta_description',
            'meta_image',
            'meta_all',
            'block_id',
            'created_at',
            'updated_at',
        ]);
    }

    //public function setDefinitionCreate($parentModel=null) {}

    public function setDefinitionUpdate($page, $parentModel = null)
    {
        if ($page) {
            $this->getField('breadcrumbs')->setData(
                new BaseDataModel(
                    PageModel::class,
                    'route_key',
                    'meta_title',
                    [['orderBy', 'meta_title'], ['where', 'id', '!=', $page->id]],
                    null,
                ),
            );

            if ($page->fl_lock_page) {
                $this->getField('key')->setInputMethods([['readonly', true]]);
            }
        }
    }

    //public function setDefinitionView($page, $parentModel=null) {}

    private static $_instance;

    /**
     * @return PageDefinition
     */
    public static function instance(): BaseDefinition
    {
        if (!self::$_instance) {
            self::$_instance = new PageDefinition();
        }

        return self::$_instance;
    }
}
