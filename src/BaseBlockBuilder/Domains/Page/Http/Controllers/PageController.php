<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Http\Controllers;

use BaseCms\BaseBlockBuilder\Models\PageModel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

use BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition;
use BaseCms\BaseBlockBuilder\Domains\Page\Repositories\PageRepository;
use BaseCms\BaseBlockBuilder\Domains\Page\Requests\PageCreateRequest;
use BaseCms\BaseBlockBuilder\Domains\Page\Requests\PageUpdateRequest;
use BaseCms\Datatables\Helpers\BaseDatatableExport;
use Yajra\DataTables\Facades\DataTables;

use BaseCms\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * @var PageRepository
     */
    private $repository;

    /**
     * @var PageDefinition
     */
    private $definition;

    /**
     * __construct
     *
     * @param PageRepository $repository
     * @param PageDefinition $definition
     * @return void
     */
    public function __construct(PageRepository $repository, PageDefinition $definition)
    {
        $this->repository = $repository;
        $this->definition = $definition;
    }

    /**
     * Listagem dos registros
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $definition = $this->definition;
        return view('base::backend.blockbuilder.blockbuilder_pages.index', compact('definition'));
    }

    /**
     * API Datatable "index()"
     *
     * https://github.com/yajra/laravel-datatables
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatable(Request $request)
    {
        $query = PageModel::select('pages.*');
        if ($request->key) {
            $query->whereIn('key', explode(',', $request->key));
        }

        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $datatable = DataTables::eloquent($query)
            ->addColumn('actions', function ($row) {
                $editUrl = route('admin.pages.edit', $row->id);
                $deleteUrl = route('admin.pages.destroy', $row->id);

                $existRoute =
                    config('app.fallback_locale') == $row->locale
                        ? \Route::has('frontend.pages.' . $row->key)
                        : \Route::has("frontend.$row->locale.pages.$row->key");
                // : \Route::has("frontend.{lang}.pages.$row->key");

                $viewUrlBlank = $existRoute
                    ? routeLocaleForceLocale('frontend.pages.' . $row->key, [], $row->locale) . '?blockbuilder=true'
                    : route('frontend.blockbuilder.component', $row->block_id) . '?blockbuilder=true';

                $duplicateUrl = route('admin.pages.duplicate', $row->id);

                return view(
                    'base::components.datatable-actions',
                    compact('editUrl', 'deleteUrl', 'row', 'viewUrlBlank', 'duplicateUrl'),
                );
            })
            ->rawColumns(['preview', 'meta_image'])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }

    /**
     * @param Request $request
     *
     * @return StreamedResponse
     */
    public function exportCsv(Request $request)
    {
        $datatable = DataTables::eloquent(PageModel::select('pages.*'));

        return BaseDatatableExport::exportCsv($datatable, $this->definition);
    }

    /**
     * Visualização
     *
     * @param PageModel $page
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function view(PageModel $page)
    {
        $definition = $this->definition;
        $definition->setDefinitionView($page);

        return view('base::backend.blockbuilder.blockbuilder_pages.view', compact('page', 'definition'));
    }

    /**
     * Cadastro
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create(Request $request)
    {
        $isModal = (bool) $request->isModal;

        $definition = $this->definition;
        $definition->setDefinitionCreate();

        $urlSubmit = 'admin.pages.store';

        return view(
            'base::backend.blockbuilder.blockbuilder_pages.create_update',
            compact('urlSubmit', 'definition', 'isModal'),
        );
    }

    /**
     * Cadastro registro (POST)
     *
     * @param PageCreateRequest $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function store(PageCreateRequest $request)
    {
        $page = $this->repository->create($request->validated());

        if ($request->redirect_to_edit) {
            return redirect(route('admin.pages.edit', [$page->id, '#' . $request->redirect_to_edit]))->withFlashSuccess(
                'Registro criado com sucesso!',
            );
        } elseif (isset($request->isModal)) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        }

        return redirect(route('admin.pages.index'))->withFlashSuccess('Registro criado com sucesso!');
    }

    /**
     * Edição registro
     *
     * @param PageModel $page
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(PageModel $page, Request $request)
    {
        $isModal = (bool) $request->isModal;

        $definition = $this->definition;
        $definition->setDefinitionUpdate($page);

        $page->breadcrumbs = explode(',', $page->breadcrumbs);

        $urlSubmit = 'admin.pages.update';

        return view(
            'base::backend.blockbuilder.blockbuilder_pages.create_update',
            compact('page', 'urlSubmit', 'isModal', 'definition'),
        );
    }

    /**
     * Atualizar registro (POST)
     *
     * @param PageModel $page
     * @param PageUpdateRequest $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function update(PageModel $page, PageUpdateRequest $request)
    {
        $this->repository->update($page, $request->validated());

        if ($request->isModal) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        }

        return redirect(route('admin.pages.index'))->withFlashSuccess('Registro atualizado com sucesso!');
    }

    /**
     * Remover registro
     *
     * @param PageModel $page
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function destroy(PageModel $page)
    {
        $this->repository->delete($page);

        return redirect(route('admin.pages.index'))->withFlashSuccess('Registro removido com sucesso!');
    }

    /**
     * Ação em vários registros de uma vez só (precisa ser habilitado no Definition)
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function multipleSelect(Request $request)
    {
        $this->repository->multipleSelect($request->all());

        return redirect(route('admin.pages.index'))->withFlashSuccess('Registros removidos com sucesso!');
    }

    /**
     * Duplicar registro (GET)
     *
     * @param PageModel $page
     * @param Request $request
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function duplicate(PageModel $page, Request $request)
    {
        $page = $this->repository->duplicate($page);

        if (isset($request->isModal)) {
            return '<script>parent.modalBlockBuilderClose(true);</script>';
        }

        return redirect(route('admin.pages.index'))->withFlashSuccess('Registro duplicado com sucesso!');
    }

    /**
     * Restaurar registro
     *
     * @param PageModel $pageDeleted
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function restore(PageModel $pageDeleted)
    {
        $this->repository->restore($pageDeleted);

        return redirect(route('admin.pages.index'))->withFlashSuccess(__('Registro restaurado com sucesso!'));
    }

    /**
     * Remover para sempre o registro
     *
     * @param PageModel $pageDeleted
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function forceDelete(PageModel $pageDeleted)
    {
        $this->repository->forceDelete($pageDeleted);

        return redirect(route('admin.pages.index'))->withFlashSuccess(__('Registro removido com sucesso!'));
    }

    /**
     * Listagem dos registros p/ restaurar
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexRestore()
    {
        $definition = $this->definition;
        return view('base::backend.blockbuilder.blockbuilder_pages.index_restore', compact('definition'));
    }

    /**
     * API Datatable - Restore "indexRestore()"
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatableRestore(Request $request)
    {
        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $query = PageModel::select('pages.*')->onlyTrashed();
        if ($request->key) {
            $query->whereIn('key', explode(',', $request->key));
        }

        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $datatable = DataTables::eloquent($query)
            ->addColumn('actions', function ($row) {
                $restoreUrl = route('admin.pages.restore', $row->id);
                $forceDeleteUrl = route('admin.pages.forceDelete', $row->id);

                return view('base::components.datatable-actions', compact('row', 'restoreUrl', 'forceDeleteUrl'));
            })
            ->rawColumns(['preview', 'meta_image'])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }
}
