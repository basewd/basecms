<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Policy;

use BaseCms\BaseBlockBuilder\Models\PageModel;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Auth\User;

class PagePolicy
{
    use HandlesAuthorization;

    public function list(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.list') : true;
    }

    public function view(?User $user, PageModel $page)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.view') : true;
    }

    public function create(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;
    }

    public function update(?User $user, PageModel $page)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;
    }

    public function delete(?User $user, PageModel $page)
    {
        //Página bloqueada, está sendo utilizada diretamente no código do projeto.
        if (@$page->fl_lock_page) {
            return false;
        }

        if (config('blockbuilder.permission')) {
            return $user->can('admin.blockbuilder.delete');
        }

        return true;
    }

    public function forceDelete(?User $user, PageModel $pageDeleted)
    {
        return $user->isMasterAdmin();
    }

    public function listRestore(?User $user)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.delete') : true;
    }

    public function restore(?User $user, PageModel $pageDeleted)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.delete') : true;
    }

    public function duplicate(?User $user, PageModel $page)
    {
        return config('blockbuilder.permission') ? $user->can('admin.blockbuilder.create-update') : true;
    }
}
