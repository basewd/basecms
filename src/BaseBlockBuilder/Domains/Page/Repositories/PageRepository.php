<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Repositories;

use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition;
use BaseCms\Helpers\BaseUploadHelper;
use Illuminate\Support\Facades\DB;

use App\Exceptions\GeneralException;
use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\Repositories\BaseRepository;

class PageRepository extends BaseRepository
{
    public function __construct(PageModel $page)
    {
        $this->model = $page;
    }

    private function checkRouteExist($slug, $locale = '')
    {
        foreach (\Route::getRoutes() as $k => $v) {
            if (config('app.fallback_locale') == $locale && $v->uri == $slug) {
                return true;
            } elseif ($v->uri == $locale . '/' . $slug) {
                return true;
            }
        }

        return false;
    }

    private function checkKeyExist($key, $locale = '')
    {
        return PageModel::where('key', $key)
            ->where('locale', $locale)
            ->exists();
    }

    /**
     * Criar
     *
     * @param  mixed $data
     * @return PageModel
     */
    public function create(array $data): PageModel
    {
        if ($this->checkRouteExist($data['slug'], $data['locale'])) {
            throw new GeneralException(
                __('Esta rota: :rota já esta registrada manualmente no projeto.', ['rota' => $data['slug']]),
            );
        }

        if ($this->checkKeyExist($data['key'], $data['locale'])) {
            throw new GeneralException(
                __('A chave de registro <b>:key</b> já foi utilizada por outra página.', ['key' => $data['key']]),
            );
        }

        return DB::transaction(function () use ($data) {
            try {
                $data['key'] = @\Str::slug($data['key']);
                $data['breadcrumbs'] = join(',', @$data['breadcrumbs'] ?: []);

                $page = $this->model::create($data);

                BaseUploadHelper::saveFilesFromRequest($page, PageDefinition::instance());

                if ($page) {
                    return $page;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e, PageDefinition::instance());
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, PageDefinition::instance());
            }
        });
    }

    /**
     * Atualizar
     *
     * @param  PageModel $page
     * @param  mixed $data
     * @return PageModel
     */
    public function update(PageModel $page, array $data): PageModel
    {
        if ($page->slug != $data['slug'] && $this->checkRouteExist($data['slug'], $data['locale'])) {
            throw new GeneralException(
                __('Esta rota: :rota já esta registrada manualmente no projeto.', ['rota' => $data['slug']]),
            );
        }

        return DB::transaction(function () use ($page, $data) {
            try {
                $data['key'] = @\Str::slug($data['key']);
                $data['breadcrumbs'] = join(',', @$data['breadcrumbs'] ?: []);

                $page->update($data);

                BaseUploadHelper::saveFilesFromRequest($page, PageDefinition::instance());

                if ($page) {
                    return $page;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e, PageDefinition::instance());
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, PageDefinition::instance());
            }
        });
    }

    /**
     * Remover
     *
     * @param  PageModel $page
     * @return bool
     */
    public function delete(PageModel $page): bool
    {
        return DB::transaction(function () use ($page) {
            try {
                if ($page->delete()) {
                    return true;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e);
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, PageDefinition::instance());
            }
        });
    }

    /**
     * Multipla seleção tabela
     *
     * @param mixed $data
     */
    public function multipleSelect(array $data)
    {
        return DB::transaction(function () use ($data) {
            $listSelected = $data['multipleSelect'];

            foreach ($listSelected as $v) {
                $page = PageModel::findOrFail($v);
                if (\Auth::user()->can('delete', $page)) {
                    $page->delete();
                } else {
                    throw new \Exception(__('Não é possível remover o registro selecionado. Código:') . $v);
                }
            }

            return true;
        });
    }

    public function duplicate(PageModel $page)
    {
        return DB::transaction(function () use ($page) {
            $blockId = BlockBuilder::duplicateBlock($page->block_id);

            $newPage = $page->replicate();
            $newPage->block_id = $blockId;
            $newPage->key .= '_dup_' . $blockId;
            $newPage->slug .= '_dup_' . $blockId;
            $newPage->save();

            return true;
        });
    }

    /**
     * Restaurar
     *
     * @param  PageModel $model
     * @return bool
     */
    public function restore(PageModel $model): bool
    {
        return DB::transaction(function () use ($model) {
            try {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e);
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, PageDefinition::instance());
            }
        });
    }

    /**
     * Remover para sempre
     *
     * @param  PageModel $model
     * @return bool
     */
    public function forceDelete(PageModel $model): bool
    {
        return DB::transaction(function () use ($model) {
            try {
                $block = $model->block;

                if ($model->forceDelete()) {
                    //Não tem ninguém relacionado a ele
                    if (!BlockChildrenModel::where('children_id', $block->id)->exists()) {
                        $block->delete();
                    }

                    return true;
                }

                throw new GeneralException(__('Favor tentar novamente'));
            } catch (\Illuminate\Database\QueryException $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulQueryException($e);
            } catch (\Exception $e) {
                \BaseCms\Helpers\BaseHelper::dispatchBeatifulException($e, PageDefinition::instance());
            }
        });
    }
}
