<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Requests;

use BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition;
use Illuminate\Foundation\Http\FormRequest;

class PageCreateRequest extends FormRequest
{
    /**
     * Determine se o usuário está autorizado a fazer esta solicitação.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Regras de validação que se aplicam à solicitação.
     *
     * @return array
     */
    public function rules()
    {
        $rules = PageDefinition::instance()->formRequestRules();

        return $rules;

        // return [
        //    'first_name' => 'required|min:5|max:255',
        //    'last_name' => 'required',
        // ];
    }

    /**
     * Atributos personalizados para erros do validador.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = PageDefinition::instance()->formRequestAttributes();

        return $attributes;

        // return [
        //     'first_name' => 'Primeiro Nome',
        //     'last_name' => 'Segundo Nome',
        // ];
    }

    /**
     * Mensagens de erro para as regras de validação definidas.
     *
     * @return array
     */
    public function messages()
    {
        $messages = PageDefinition::instance()->formRequestMessages();

        return $messages;

        // return [
        //     'first_name.max' => 'Sample text',
        //     'last_name.required' => 'Sample text',
        // ];
    }
}
