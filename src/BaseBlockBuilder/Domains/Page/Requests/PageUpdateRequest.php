<?php

namespace BaseCms\BaseBlockBuilder\Domains\Page\Requests;

use BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition;
use Illuminate\Foundation\Http\FormRequest;

class PageUpdateRequest extends FormRequest
{
    /**
     * Determine se o usuário está autorizado a fazer esta solicitação.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Regras de validação que se aplicam à solicitação.
     *
     * @return array
     */
    public function rules()
    {
        PageDefinition::instance()->setDefinitionUpdate($this->page, $this->parentModel);

        $rules = PageDefinition::instance()->formRequestEditRules();

        return $rules;

        // Sample 1
        // É necessário o teste porque na validação Javascript (Aire/FormRequest) o model não existe.
        // if ($this->page) {
        //    $rules['name'] = "required|unique:foobar_table,name,{$this->page->id}";
        // }

        // Sample 2
        // return [
        //    'first_name' => 'required|min:5|max:255',
        //    'last_name' => 'required',
        // ];
    }

    /**
     * Atributos personalizados para erros do validador.
     *
     * @return array
     */
    public function attributes()
    {
        PageDefinition::instance()->setDefinitionUpdate($this->page, $this->parentModel);

        $attributes = PageDefinition::instance()->formRequestEditAttributes();

        return $attributes;

        // return [
        //     'first_name' => 'Primeiro Nome',
        //     'last_name' => 'Segundo Nome',
        // ];
    }

    /**
     * Mensagens de erro para as regras de validação definidas.
     *
     * @return array
     */
    public function messages()
    {
        PageDefinition::instance()->setDefinitionUpdate($this->page, $this->parentModel);

        $messages = PageDefinition::instance()->formRequestEditMessages();

        return $messages;

        // return [
        //     'first_name.max' => 'Sample text',
        //     'last_name.required' => 'Sample text',
        // ];
    }
}
