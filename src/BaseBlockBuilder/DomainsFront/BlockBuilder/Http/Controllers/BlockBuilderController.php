<?php

namespace BaseCms\BaseBlockBuilder\DomainsFront\BlockBuilder\Http\Controllers;

use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\Models\PageModel;
use BaseCms\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class BlockBuilderController.
 */
class BlockBuilderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page(PageModel $page, Request $request)
    {
        $block = $page->block->load('children');

        return view('base::frontend.blockbuilder.page-blockbuilder', compact('page', 'block'));
    }

    public function pageJson(PageModel $page, Request $request)
    {
        return response()->json($page->toArray() + ['childrens' => $page->block->json]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $elements = BlockModel::with('children')->get();

        $blocks = collect();
        foreach ($elements as $v) {
            //retorna todos drafts
            $blocks->push(BlockBuilder::renderBlock($v, 0, [], null, true));
        }

        return view('base::frontend.blockbuilder.page-blockbuilder', compact('blocks'));
    }

    // http://localhost:8000/pageblock-component/4?style=width:640px
    public function component(BlockModel $block, Request $request)
    {
        $style = @$request->style;
        $showEdit = @$style ? false : true;

        return view('base::frontend.blockbuilder.page-blockbuilder-component', compact('block', 'style', 'showEdit'));
    }
}
