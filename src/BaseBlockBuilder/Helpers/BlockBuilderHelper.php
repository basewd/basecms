<?php

namespace BaseCms\BaseBlockBuilder\Helpers;

use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\BaseBlockBuilder\View\Components\BlockBuilder\Container;
use BaseCms\BaseDefinition;
use BaseCms\Constants\BaseInputConst;
use Illuminate\Database\Eloquent\Model;

class BlockBuilderHelper
{
    public static function registerRoutesFrontend($locale)
    {
        include __DIR__ . '/../routes/front.php'; //Não pode ser include_once
    }

    public static function createBlockBuilderIds(Model $model, BaseDefinition $definition)
    {
        foreach ($definition->getFields() as $field) {
            if ($field->input == BaseInputConst::BlockBuilder) {
                if (@!$model->{$field->field}) {
                    $block = BlockModel::create([
                        'componentClass' =>
                            $field->preset && @$field->preset['componentClass']
                                ? $field->preset['componentClass']
                                : Container::class,
                        'params' => [],
                    ]);
                    $model->{$field->field} = $block->id;
                    $model->save();
                }
            }
        }
    }
}
