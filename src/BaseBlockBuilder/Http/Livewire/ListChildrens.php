<?php

namespace BaseCms\BaseBlockBuilder\Http\Livewire;

use BaseCms\BaseBlockBuilder\Models\BlockChildrenModel;
use BaseCms\BaseBlockBuilder\Models\BlockModel;
use Livewire\Component;

class ListChildrens extends Component
{
    public BlockModel $block;
    public $keys = '';

    private $childrens = [];

    protected $listeners = ['listChildrenRefresh' => 'refresh'];

    public function mount(BlockModel $block = null, $keys = null)
    {
        $this->block = $block;
        $this->keys = $keys;
        $this->childrens = $this->block->children;
    }

    private function getBlockChildren(string $blockChildrenId)
    {
        return BlockChildrenModel::where('id', $blockChildrenId)->first();
    }

    public function refresh()
    {
        $this->childrens = $this->block->children()->get();
    }

    public function orderUp(string $blockChildrenId)
    {
        $children = $this->getBlockChildren($blockChildrenId);
        $children->moveOrderUp();

        $this->refresh();
    }

    public function orderDown(string $blockChildrenId)
    {
        $children = $this->getBlockChildren($blockChildrenId);
        $children->moveOrderDown();

        $this->refresh();
    }

    public function remove(string $blockChildrenId)
    {
        $children = $this->getBlockChildren($blockChildrenId);
        $children->delete();

        $this->refresh();
    }

    public function render()
    {
        return view('base::backend.blockbuilder.blockbuilder_blocks.livewire.list-childrens', [
            'childrens' => $this->childrens,
            'keys' => $this->keys,
        ]);
    }
}
