<?php

namespace BaseCms\BaseBlockBuilder\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Foundation\Bus\Dispatchable;
use BaseCms\BaseBlockBuilder\Models\BlockModel;

class BlockPreviewJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $blockId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $blockId)
    {
        $this->blockId = $blockId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $block = BlockModel::where('id', $this->blockId)->first();
        if (!$block) {
            return;
        }

        $url = route('frontend.blockbuilder.component', ['block' => $block->id, 'style' => 'width:1340px;']);
        if (app()->isLocal()) {
            //php artisan serve não roda "multiprocess"
            $url = str_replace('8000', '8001', $url);
        }

        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            config('blockbuilder.preview.url') . '?element=' . urlencode('#blockcomponent') . '&url=' . urlencode($url),
        );
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        curl_close($ch);

        try {
            $jsonData = json_decode($output);

            $filename = 'components/' . $block->id . '.png';
            \Storage::put($filename, base64_decode($jsonData->body));

            $block->preview = $filename;
            $block->saveQuietly();
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * Get the unique ID for the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return $this->blockId;
    }
}
