<?php

namespace BaseCms\BaseBlockBuilder\Models;

use BaseCms\BaseBlockBuilder\Models\BlockModel;
use BaseCms\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * @property integer $id
 * @property integer $block_id
 * @property integer $children_id
 * @property int $order
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property Block $block
 * @property Block $block
 */
class BlockChildrenModel extends Model implements Sortable
{
    use SortableTrait;
    use BaseModelTrait;
    use LogsActivity;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'block_childrens';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['block_id', 'children_id', 'order', 'created_at', 'updated_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Logs
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function (BlockChildrenModel $model) {
            if ($model->block_id == $model->children_id) {
                throw new \Exception('blockbuilder:blockchildrenmodel:block_id == children_id, circular');
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block()
    {
        return $this->belongsTo(BlockModel::class, 'block_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function children()
    {
        return $this->belongsTo(BlockModel::class, 'children_id');
    }

    /**
     *
     */
    public function buildSortQuery()
    {
        return static::query()->where('block_id', $this->block_id);
    }
}
