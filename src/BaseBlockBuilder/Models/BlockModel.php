<?php

namespace BaseCms\BaseBlockBuilder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\Jobs\BlockPreviewJob;
use BaseCms\Models\BaseModelTrait;

/**
 * @property integer $id
 * @property string $componentClass
 * @property string $locale
 * @property string $tags
 * @property object $params
 * @property bool $fl_reusable
 * @property bool $fl_draft
 * @property mixed $created_at
 * @property mixed $updated_at
 */
class BlockModel extends Model
{
    use HasFactory;
    use BaseModelTrait;
    use LogsActivity;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blocks';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'locale',
        'componentClass',
        'tags',
        'params',
        'preview',
        'fl_reusable',
        'fl_draft',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'params' => 'object',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Logs
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::saved(function (BlockModel $model) {
            if (config('blockbuilder.preview.enabled')) {
                BlockPreviewJob::dispatchAfterResponse($model->id);

                $model->childrenBlock->map(function ($v) {
                    BlockPreviewJob::dispatchAfterResponse($v->id);
                });
            }
        });
    }

    /**
     *
     */
    public function getComponentDataAttribute()
    {
        return @BlockBuilder::getComponentsByClass()[$this->componentClass];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function locale()
    {
        return $this->belongsTo(\BaseCms\Models\LocaleModel::class, 'locale');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blockChildrens()
    {
        return $this->hasMany(BlockChildrenModel::class, 'block_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany

     * Cuidado: não utilizar, a lógica é para saber se o bloco é utilizado por algum outro bloco
     */
    public function childrenBlock()
    {
        return $this->hasMany(BlockChildrenModel::class, 'children_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function children()
    {
        return $this->belongsToMany(BlockModel::class, 'block_childrens', 'block_id', 'children_id')
            ->withPivot(['id', 'order'])
            ->orderByPivot('order');
    }

    /**
     * @return array
     */
    public function getJsonAttribute()
    {
        return BlockBuilder::blockJson($this, 0);
    }

    /**
     * @return string
     */
    public function getComponentClassKeyAttribute()
    {
        $key = BlockBuilder::registredComponents()->search($this->componentClass);
        return $key;
    }

    /**
     * @return BlockBuilderDefinition
     */
    public function definition()
    {
        $componentClass = $this->componentClass;

        return \Cache::store('array')->remember($componentClass, 30, function () use ($componentClass) {
            return BlockBuilder::definitionBy($componentClass);
        });
    }

    /**
     * Set Param
     */
    public function setParam($key, $value)
    {
        $params = $this->params;
        $params->{$key} = $value;
        $this->params = $params;
    }
}
