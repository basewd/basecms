<?php

namespace BaseCms\BaseBlockBuilder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use BaseCms\BaseBlockBuilder\BlockBuilder;
use BaseCms\BaseBlockBuilder\View\Components\BlockBuilder\Container;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $key
 * @property string $slug
 * @property string $locale
 * @property integer $block_id
 * @property string $template
 * @property string $breadcrumbs
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_image
 * @property string $meta_all
 * @property string $metas
 * @property boolean $published
 * @property boolean $fl_hide_sitemap
 * @property boolean $fl_no_indexable
 * @property boolean $fl_lock_page
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property Block $block
 */
class PageModel extends Model
{
    use HasFactory;
    use LogsActivity;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'slug',
        'locale',
        'template',
        'block_id',
        'breadcrumbs',
        'meta_title',
        'meta_description',
        'meta_image',
        'meta_all',
        'metas',
        'fl_hide_sitemap',
        'fl_no_indexable',
        'fl_lock_page',
        'created_at',
        'updated_at',
    ];

    /**
     * Logs
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();

        static::creating(function (PageModel $model) {
            if (!$model->block_id) {
                $block = BlockModel::create([
                    'locale' => $model->locale,
                    'componentClass' => Container::class,
                    'key' => 'page_' . $model->key, //Utilizado p/ cadastrar os filhos escopados em uma key
                    'tags' => 'page_' . @$model->slug,
                    'params' => [],
                ]);
                $model->block_id = $block->id;
            }
        });

        static::created(function (PageModel $model) {
            \DB::commit();
            BlockBuilder::clearRouteCache();
        });

        static::updated(function (PageModel $model) {
            if ($model->isDirty('locale') || $model->isDirty('key') || $model->isDirty('slug')) {
                BlockModel::where('id', $model->block_id)->update([
                    'locale' => $model->locale,
                    'key' => 'page_' . $model->key,
                    'tags' => 'page_' . $model->slug,
                ]);
            }

            //Altera "rota" ou "nome da rota"
            if ($model->isDirty('slug') || $model->isDirty('key')) {
                \DB::commit();
                BlockBuilder::clearRouteCache();
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function locale()
    {
        return $this->belongsTo(\BaseCms\Models\LocaleModel::class, 'locale');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block()
    {
        return $this->belongsTo(BlockModel::class, 'block_id');
    }

    /**
     * Route Url
     */
    public function getUrlAttribute()
    {
        return routeLocaleForceLocale('frontend.pages.' . $this->key, [], $this->locale);
    }

    /**
     *
     */
    public function getRouteKeyAttribute()
    {
        return 'frontend.pages.' . $this->key; //TODO: lang?
    }
}
