<?php

namespace BaseCms\BaseBlockBuilder\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

/**
 * Class AuthBlockBuilderServiceProvider.
 */
class AuthBlockBuilderServiceProvider extends ServiceProvider
{
    protected $policies = [
        /** Blockbuilder */
        \BaseCms\BaseBlockBuilder\Models\BlockModel::class => \BaseCms\BaseBlockBuilder\Domains\Block\Policy\BlockPolicy::class,
        \BaseCms\BaseBlockBuilder\Models\PageModel::class => \BaseCms\BaseBlockBuilder\Domains\Page\Policy\PagePolicy::class,
    ];

    /**
     * Register bindings in the container.
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
