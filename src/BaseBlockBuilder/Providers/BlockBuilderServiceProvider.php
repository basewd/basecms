<?php

namespace BaseCms\BaseBlockBuilder\Providers;

use BaseCms\BaseBlockBuilder\Models\PageModel;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

/**
 * Class BlockBuilderServiceProvider.
 */
class BlockBuilderServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function boot()
    {
        /**
         * Blades x Front
         */
        Blade::directive('block', function ($key, $locale = null) {
            return "<?php echo \BaseCms\BaseBlockBuilder\BlockBuilder::renderBlockByKey($key,$locale); ?>";
        });

        Blade::directive('blockById', function ($id) {
            return "<?php echo \BaseCms\BaseBlockBuilder\BlockBuilder::renderBlockById($id); ?>";
        });

        Blade::directive('page', function ($key, $locale = null) {
            return '<?php $pageBlockBuilder = \BaseCms\BaseBlockBuilder\BlockBuilder::renderPageByKey(' .
                $key .
                ',' .
                $locale .
                '); ?>';
        });

        //Só para manter a semântica
        Blade::directive('endpage', function () {
            return '<?php echo $pageBlockBuilder; ?>';
        });

        Blade::directive('blockbuilderChildren', function ($expression = '') {
            /**
             * Utilizado para passar componentes intermediarios em uma @page
             *
             * Ex: @section("page1") H! @endsection
             *  - Vai adicionar logo após o primeiro componente o elemento passado
             */
            $yield = $yieldStart = $yieldEnd = null;
            if ($expression) {
                $expression = str_replace("'", '', $expression);
                $yield =
                    '<?php echo view("base::frontend.blockbuilder._yield", ["name"=> "' .
                    $expression .
                    '$position"])?>';

                $yieldStart =
                    '<?php echo view("base::frontend.blockbuilder._yield", ["name"=> "' . $expression . 'Start"])?>';

                $yieldEnd =
                    '<?php echo view("base::frontend.blockbuilder._yield", ["name"=> "' . $expression . 'End"])?>';
            }

            return $yieldStart .
                '
                <?php if(@$children): ?>
                    <?php foreach(@$children as $position => $componentChildren): ?>
                        ' .
                $yield .
                '    
                        <?php echo $componentChildren ? $componentChildren->render() : "" ?>
                    <?php endforeach ?>
                <?php endif;?>
                ' .
                $yieldEnd;
        });

        /**
         * Blades x Backend e alguns p/ o Front
         *
         * Trabalham com a Manipulação dos Blocos
         */
        Blade::directive('blockbuilderFrontEditable', function () {
            return '<?php echo view("base::frontend.blockbuilder.modal-block-builder", ["isChildren"=>false]); ?>';
        });

        Blade::directive('ifBlockbuilderEdit', function () {
            return '
                <?php if(request()->input("blockbuilder")): ?>';
        });

        Blade::directive('blockbuilderEdit', function () {
            return '
                <?php if(request()->input("blockbuilder") && @$componentClass): ?>
                    <div class="blockbuilder-edit">
                        <?php if(@$componentClass && @$componentClass::definition()->isAllowChildrens() && count($children) < $componentClass::definition()->getMaxChildrens()):?>
                            <div class="btn-blockbuilder" onclick=window.modalBlockBuilder(event,null,{{ $id }})>+</div>
                        <?php endif;?>
                        <div class="btn-blockbuilder" onclick=window.modalBlockBuilder(event,{{ $id }})>✎</div>
                    </div>
                <?php endif;?>';
        });

        Blade::directive('blockbuilderAddComponentToPage', function () {
            return '
                <?php if(request()->input("blockbuilder")): ?>
                    <div class="blockbuilder-add" onclick=window.modalBlockBuilder(event,null,{{ $id }})></div>
                <?php endif;?>';
        });

        /**
         * Routes
         */

        \Route::bind('blockSlug', function (string $value) {
            return PageModel::where('slug', $value)
                ->where(function ($q) {
                    $localeSegment = request()->segment(1);
                    if (!in_array($localeSegment, config('translatable.valid_locales'))) {
                        $localeSegment = config('app.fallback_locale');
                    }

                    $q->where('locale', $localeSegment);
                    $q->orWhereNull('locale');
                })
                ->firstOrFail();
        });

        \Route::bind('pageDeleted', function ($id) {
            return \BaseCms\BaseBlockBuilder\Models\PageModel::onlyTrashed()->find($id);
        });

        \Livewire::component('base::list-children', \BaseCms\BaseBlockBuilder\Http\Livewire\ListChildrens::class);

        $this->loadMigrationsFrom(base_path('vendor/basedigital/cms/database/migrations'));

        $this->loadRoutesFrom(__DIR__ . '../../routes/admin.php');
    }
}

/*
        \Blade::directive('block2', function ($key, $locale = '') {
            return "<?php echo e(Blade::compileString('<x-frontend.block.dynamic key=\"$key\">')); ?>";
        });

        \Blade::directive('endblock2', function () {
            return "<?php echo e(Blade::compileString('</x-frontend.block.dynamic>')); ?>";
        });
*/

/*
        \Blade::directive('block2', function ($key, $locale = '') {
            return "<?php echo app('view')->make('components.frontend.block.dynamic', ['key' => $key]); ?>";
        });

        \Blade::directive('endblock2', function () {
            return "<?php echo '</x-frontend.block.dynamic>'; ?>";
        });
*/

/*

        \Blade::directive('block2', function ($key, $locale = '') {
            return "<?php echo (Blade::compileString('<x-frontend.block.dynamic key=\"hero\">')); ?>";
        });

        \Blade::directive('endblock2', function () {
            return "<?php echo (Blade::compileString('</x-frontend.block.dynamic>')); ?>";
        });
*/
