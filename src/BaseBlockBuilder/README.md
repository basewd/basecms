# BASE BlockBuilder

## Explicações por vídeo

[Introdução](http://www.auxteam.com.br/basecms/blockbuilder/1%20-%20intro.mp4)

[Código](http://www.auxteam.com.br/basecms/blockbuilder/2%20-%20codigo.mp4)

[Registros/Utilização](http://www.auxteam.com.br/basecms/blockbuilder/3%20-%20registro.mp4)

[Comandos/Artisan](http://www.auxteam.com.br/basecms/blockbuilder/4%20-%20gerador.mp4)

## Criar uma página inteira com componentes.

1. Criar página em `/admin/pages`
2. `slug` define a rota de acesso da página
3. P/ adicionar componentes, clicar no botão "Adicionar um novo componente"
4. É possível criar um novo ou selecionar um componente já existente

## Criar componentes para serem consumidos por uma página já existente.

1.  Criar blocos em `/admin/blocks`
2.  Criar acesso aos blocos no `sidebar.blade.php`, passando as keys dos blocos criados p/ permitir a edição no CMS

```
<x-blockbuilder.link-block text="Hero/Wave" key="hero,wave" />
```

3.  Criar página no front que consumirá o componente (ou utilizar uma já existente)
4.  Instanciar por uma diretiva o componentes através da `key`

```
@block("key")
```

## Criar uma página inteira e mesclar componentes que não foram registrados no BlockBuilder?

1. Criar Controller
2. Criar uma Blade
3. Instanciar blade no controller
4. Registrar a rota no `front.php` apontando para a function do controller
5. Na blade, chamar a directive

```
@page('key-cadastrado-no-cms')
```

6. Para inserir componentes em determinas posições

```
<!-- Insere no início -->
@section('page0')
Start
@endsection

<!-- Insere logo após o primeiro elemento da página -->
@section('page1')
Position 1
@endsection

<!-- Insere no início -->
@section('pageStart')
Position Start
@endsection

<!-- Insere no final -->
@section('pageEnd')
Position End
@endsection
```

7. Inserir essa página no CMS p/ ser editada

```
<x-blockbuilder.link-page text="Sobre" key="about" />
```

## Meu site é multilingua, como eu faço?

1. Tenho as línguas: pt-BR, en, es
2. Seguir os passos do tem `1.`, para cada língua, criar todas com a mesma "key"
3. Criar no sidebar.blade.php um acesso a página através da key, assim ficará mais fácil para o usuário editar os conteúdos da página "SOBRE" p/ todas as línguas

```
<x-blockbuilder.link-page text="Sobre" key="about" />
```

## Minha página não pode usar o template padrão do site, como eu faço?

1. Fazer a página na mão e utilizar os "blocos/componentes" diretos através das keys
2. Ou montar toda a tela na mão e não utilizar o BlockBuilder
3. Para passos 1. ou 2. é possível seguir a explicação do item 3. com uma página custom com componentes custom
   PS: futuramente, permitir alteração do template no cadastro da page?

# Utilizando Artisan p/ gerar Componentes/Páginas/Seeder/Migrations

Foi desenvolvido alguns comandos p/ facilitar o desenvolvimento/criação de componentes e páginas.

## Criar/registrar Componente

```
php artisan blockbuilder:make:component hero title,subtitle,text,label,url,target,image,fl_enabled
```

## Criar Página

```
php artisan blockbuilder:make:page pagename hero,text,wave,key:wave,color,header1,feature1,content1,content2
```

## Criar Seeder/Migration dos dados de uma página

```
   php artisan blockbuilder:seed --page=pageKey
   php artisan blockbuilder:seed --component=componentKey
```

## Componentes que possuem filhos, como funcionam?

//TODO: explicar Ocultos e Restrict

## Expondo páginas ou componentes p/ edição

Arquivo `sidebar.blade.php`

Parâmetro `key` recebe a `key` da página/block.

Ex:

```

<x-blockbuilder.link-page text="Home" key="home" />

<x-blockbuilder.link-page text="Sobre" key="about" />

<x-blockbuilder.link-block text="Hero/Wave" key="hero,wave" />


```

## Removendo um componente já criado

1. O ideal é que seja removido do banco os blocos

2. Depois comentar/remover do BlockBuilderRegisterServiceProvider

PS: O sistema foi tratado para ignorar elementos que não possuem componentes registrados, mas nesse caso, ficará lixo no banco. Ideal é remover.

## Não quero que o campo seja mostrado no preview

Setar a propriedade `setFilterable(false)` no definition() do componente.

```
   BaseField::newObj('subtitle', 'Subtítulo', BaseInputConst::AireRichEditor, BaseColumnConst::Text)
      ->setRule(new BaseRule('nullable'))
      ->setFilterable(false),
```

## Hook Saved - Componenes

```
      return (new BlockBuilderDefinition())
            ->setFields([
                BaseField::newObj('title', 'Título', BaseInputConst::AireInput, BaseColumnConst::Text)->setRule(
                    new BaseRule('required'),
                ),

                BaseField::newObj('subtitle', 'Subtítulo', BaseInputConst::AireRichEditor, BaseColumnConst::Text)
                    ->setRule(new BaseRule('nullable'))
                    ->setFilterable(false),

                BaseField::newObj('image', 'Image', BaseInputConst::AireFile, BaseColumnConst::Image)
                    ->setRule(new BaseRule('file|max:1024'))
                    ->setUploadDefinition(BaseUploadDefinition::newObj('hero_block', 1920, 400, true)) //->withSrcSet()
                    ->setInputMethods([['accept', 'image/*']]),

                BaseField::newObj('filesize', 'Filesize', BaseInputConst::AireHidden)->setRule(
                    new BaseRule('nullable'),
                ),
            ])
            ->setAllowChildren(true)
            ->setAllowedChildrens([TextBlock::class])
            ->setMaxChildrens(3)
            ->hookSaved(function ($model) {
                //Edita todos os valores
                $existImageUpload = request()->files->get('image');
                if ($existImageUpload) {
                    $model->setParam('filesize', $existImageUpload->getSize());
                }

                //Salva
                $model->saveQuietly();

                //dd($image->getSize());

                //primeira forma
                //$model->params = (array) $model->params + ['foobar' => 'rafa'];

                //primeira forma //Não funciona
                //($model->params)->foobar = 'rafa';

                //segunda forma
                //$params = $model->params;
                //$params->foobar = 'rafa';
                //$model->params = $params;

                //$model->saveQuietly();
            });
    }
```


## Abertura de Modal do Blockbuilder direto na página

Chamada dentro do /xxxx/create_update.blade.php

Permitindo adicionar um botão, ao clicar, abre um modal com os "components" do block 

```
@if (@$product)
    @include('base::frontend.blockbuilder.modal-block-builder', ['isChildren' => true])
    @foreach (@$product->translations as $v)
        <div class="btn btn-primary" onclick="window.modalBlockBuilder(event, {{ $v->block_product_id }})">
            COMPONENTES [{{ $v->locale }}]</div>
    @endforeach
@endif
```