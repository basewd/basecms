# TODO

-   separar cadastro de components e compoentes reutilizáveis em seções disintas (igual o que o Gus mostrou, parece fazer mais sentido)

-   melhorar o alias do componente, talvez vale esconder e gerar automaticamente ... assim fica mais facil o cadastro do componente ... porque teoricamente, não precisaria me preocupar com reutilizavel dentro do cadastro do componente, apenas no cadastro do componente reutilizável

-   breadcrumbs / futuramente listar as routes do projeto com algum titulo, hoje esta pegando apenas do PageModel ... avaliar necessidade

-   softdelete com key? //foi adicionado mas nao foi implementado ainda //avaliar

-   adicionar um texto melhor no erro da key, qdo da key duplicada no cadastro de um BlockModel

-   será que a pagina deve ser removida para sempre? ou vai para um soft/restore?

    -   levando em conta q os blocks não são removidos, é possível remontar a página, talvez não precise ser feito nada aqui

-   talvez as seções "páginas" e "blocos" não fiquem liberados p/ os clientes? Criar um usuário do tipo CLIENTE sem permissão p/ acessar "Páginas" e "Blocos"?

-   tags são utilizadas para algo? parece que não... avaliar

# page

-   page poderia nao estar publicada/published, para o pessoal conseguir fazer a validação? histórico de versões? / draft?

## front

-   FRONT, remover, ordenar na tela principal (avaliar)

## cms

-   [RASTA] criar uma árvore de componentes vs páginas, para saber qual pagina tem os compoentens //permitindo editar

# artisan

Pensando em uma esteira de criação do projeto / setup

-   [OK] php artisan blockbuilder:make:component hero title,subtitle,text,label,url,target,image,fl_enabled

-   [OK] php artisan blockbuilder:make:page pagename hero,text,wave,key:wave,color,header1,feature1,content1,content2

-   [OK] php artisan blockbuilder:seed pagename //recursão/multilingua

-   php artisan blockbuilder:make:seed --keys=wave,hero,foo //recursão/multilingua

-   php artisan blockbuilder:page name slug
    1. Quer adicionar componentes? (y)
    2. Nome do componente (hero)
    3. Campos do componente (title,text,image,target,label,url)
    4. Quer adicionar outro componente (y)
    5. Nome do componente (hero)
    6. Componente encontrado, adicionado a página
    7. Quer adicionar outro componente (y)
    8. Nome do componente (slider)
    9. Campos do componente ...
    10. finish

O que é gerado:

    1. migration com a criação da página e dos componentes (componentes já conectados a página)
    [OK] 2. class do componente
        - com construtor recebendo atributos
        - com definition gerado de acordo com os campos passados (dependendo o nome do campo, pode vir Text, Image, Upload ...)
    [OK] 3. view do componente printando na tela os parametros/campos/fields criados
    [OK] 4. registro do componente no sistema, p/ poder utilizar em outras páginas

PS: cuidar select com referencia em outras entidades

#refs

-   https://github.com/BuilderIO/builder
-   https://reactbricks.com/
-   https://blocks-ui.com/

# Arquivos fora do "app/BaseBlockBuilder" que tem relação com o pacote

-   App\Http\Livewire\Backend\Blocks\ListChildren
-   resources/frontend/layouts/app.blade.php
    @blockbuilderFrontEditable
-   App\Providers\BlockBuilderRegisterServiceProvider
-   config/app.php
    Providers registrados
-   routes/front.php
    include da rota
-   routes/admin.php
    include da rota
-   config/blockbuilder.php
    configuração
-   public\base-blockbuilder
    arquivos js
-   resources/components/blockbuilder
    todos arquivos da pasta

---

# Brain

A ideia é testar em algum job, p/ ver se faz sentido e quais são as complexidades que vão surgir

Uma complexidade que imagino é o registro dos componentes + dados, como tudo fica no banco, passar de HML p/ PROD talvez seja complicado (talvez eu precise gerar uns comandos no artisan p/ exportar os seeds.

Outro ponto é que tenho receio que o pessoal se perca na criação dos componentes … como a montagem da página é toda por componente, eles vão ter que estar criados de acordo. Pode ser que tenha componentes hibridos, onde parte dele é dinâmico e outra parte é fixo. EX: Slider de produtos, os produtos vem fixo do ProdutModel, tu não precisa cadastrar nada no componente, mas o título e texto desse slider poderia ser dinâmico.

---

Anexei um vídeo onde mostra a edição de uma página/componente/adição de filhos em determinados componentes e manipulação direta no admin (sem ser via front)

a) componentes determinam seus dados (imagem 2)
b) página tem os dados básicos de “SEO” e um apontando para um componente de tipo Página (imagem 4)
c) componente de tipo Página tem “filhos” que são outros componentes (imagem 3)
d) os componentes são registrados no projeto (um componente é basicamente uma blade/html) (imagem 5)
e) na definição do componente eu digo se ele pode receber “childrens” e quais os “allowed childrens” (imagem 6)
f) a blade tem algumas diretivas p/ permitir a edição e/ou renderizar os childrens (imagem 7)
g) essas diretivas permitem a montagem da imagem 8, onde só é carregado se a url apresentar ?blockbuilder=true, (http://localhost:8000/about123?blockbuilder=true)

---

Existe uma outra abordagem em tudo isso que estava pensando, depois que rascunhei tudo isso ai

a) Não criar páginas
b) Criar componentes com uma chave única
c) Quando o desenvolvedor montar a página, ele chama a “key” do componente e empilha como quiser … assim fica um hibrido entre componentes FIXOS e componetens Dinâmicos (editáveis no admin)
d) Esse componente poderia ter um “agrupador” (Sobre Nós, Contato, Home …)
e) No admin, montaria uma interface com os agrupadores, clica no agrupador, abre o “set” de componentes da página

Isso tira a liberdade do cliente criar páginas

Mas permite uma manipulação mais simples das páginas pelo desenvolvendor
