<?php

namespace BaseCms\BaseBlockBuilder\View\Components\BlockBuilder;

use BaseCms\BaseBlockBuilder\BlockBuilderDefinition;
use Illuminate\View\View;
use Illuminate\View\Component;

class Container extends Component
{
    private $data;

    public $children;

    public function __construct($id = '', $componentClass = '')
    {
        $this->data = get_defined_vars();
    }

    public function render(): View
    {
        return view('base::components.frontend.blockbuilder.container', $this->data + ['children' => $this->children]);
    }

    public static function definition(): BlockBuilderDefinition
    {
        return (new BlockBuilderDefinition())
            ->setFields([])
            ->setAllowChildren(true)
            ->setAllowReuseComponents(true);
    }
}
