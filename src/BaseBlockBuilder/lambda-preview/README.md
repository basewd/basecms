-   MAC m1, liberando Chrome Headless

`xattr -rc /Applications/Chromium.app`

/Applications/Chromium.app/Contents/MacOS/Chromium

-   Suporte Chrome Headless

https://github.com/Sparticuz/chromium

# Setup

1 - `netlify`

2 - `netlify dev`

3 - `netlify login`

4 - `netlify deploy`

5 - `netlify deploy --prod`

Logs: https://app.netlify.com/sites/basecms-preview/deploys/641a1d023cc2c913be6c8d4a
Website Draft URL: https://641a1d023cc2c913be6c8d4a--basecms-preview.netlify.app

6 - Projeto: basecms-preview

Logs: https://app.netlify.com/sites/basecms-preview/deploys/641a1d650be567143dddf3f1
Unique Deploy URL: https://641a1d650be567143dddf3f1--basecms-preview.netlify.app
Website URL: https://basecms-preview.netlify.app

7 - Url para extrair um preview:

https://basecms-preview.netlify.app/.netlify/functions/preview?url=https://base.digital

## DEV

http://localhost:8888/.netlify/functions/preview?url=http://localhost:8000/

## PROD

https://jetmatch-pdf.netlify.app/.netlify/functions/preview?url=https://base.digital
