const puppeteer = require('puppeteer-core');
const chromium = require('@sparticuz/chromium');

exports.handler = async function (event, context) {
    const browser = await puppeteer.launch({
        args: chromium.args,
        executablePath: process.env.PUPPETEER_EXECUTABLE_PATH || (await chromium.executablePath()),
        defaultViewport: chromium.defaultViewport,
        headless: chromium.headless,
    });

    const { url, element = '#blockcomponent' } = event.queryStringParameters;

    console.log('url', url);

    console.log('element', element);

    if (!url) {
        throw new Error('invalid url!');
    }

    const page = await browser.newPage();

    // page.setBypassCSP();

    await page.addStyleTag({ content: 'body { background-color: white !important; }' });

    console.log('page.goto');
    await page.goto(url, { waitUntil: 'networkidle0' });

    console.log('component');
    const component = element ? await page.$(element) : page;

    const screenshotBuffer = component ? await component.screenshot({ encoding: 'binary' }) : '';

    console.log('screenshotString');
    const screenshotString = screenshotBuffer.toString('base64');

    await browser.close();

    return {
        statusCode: 200,
        body: JSON.stringify({
            statusCode: 200,
            body: screenshotString,
            isBase64Encoded: true,
        }),
    };
};
