const puppeteer = require('puppeteer');
const stream = require('stream');

/**
 * @deprecated
 *
 * Criado inicialmente p/ testar a implementação via puppeter
 *
 * Rodando: npx node screenshot.js --url="http://localhost:8000/blockbuilder-component/1?style=width:640px" --path="../public/components/nome-do-componente.png"
 */
async function index() {
    const argv = require('yargs').argv;

    //http://localhost:8000/blockbuilder-component/1
    //'../public/components/nome-do-componente.png'
    const { url } = argv;

    if (!url) {
        return -1;
    }

    const browser = await puppeteer.launch();

    const page = await browser.newPage();
    await page.setBypassCSP(true);

    await page.addStyleTag({ content: 'body { background-color: white !important; }' });

    await page.goto(url, { waitUntil: 'networkidle0' });

    const component = await page.$('#blockcomponent');

    //omitBackground: true
    const screenshotBuffer = await component.screenshot({ encoding: 'binary' }); //stream

    const screenshotString = screenshotBuffer.toString('base64');

    console.log(screenshotString);

    // await componente.screenshot({ path, omitBackground: true });
    // console.log(path);

    // const screenshotBuffer = await component.screenshot({ type: 'png', omitBackground: true }); //stream

    await browser.close();

    // const screenshotStream = new stream.PassThrough();
    // screenshotStream.end(Buffer.from(screenshotBuffer, 'binary'));
    // return screenshotStream;
}

index();
