<?php

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['web', 'session', 'admin']], function () {
    Route::group(['prefix' => 'blocks', 'as' => 'blocks.'], function () {
        Route::any('/selectComponent/{isModal?}/{parentBlockId?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'selectComponent',
        ])->name('selectComponent');

        Route::get('/addChildToBlock/{block}/{children}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'addChildToBlock',
        ])
            ->middleware('can:update,block')
            ->name('addChildToBlock');

        Route::match(['post', 'get'], '/datatableSelect', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'datatableSelect',
        ])
            ->middleware('can:list,' . \BaseCms\BaseBlockBuilder\Models\BlockModel::class)
            ->name('datatableSelect');

        Route::get('/indexNotUsed', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'indexNotUsed',
        ])
            ->middleware('can:list,' . \BaseCms\BaseBlockBuilder\Models\BlockModel::class)
            ->name('indexNotUsed');

        Route::get('/indexGroupKeys/{keys}/{inner?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'indexGroupKeys',
        ])
            ->middleware('can:listGroupKeys,' . \BaseCms\BaseBlockBuilder\Models\BlockModel::class)
            ->name('indexGroupKeys');

        Route::match(['post', 'get'], '/datatableGroupKeys/{keys}/{inner?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'datatableGroupKeys',
        ])
            ->middleware('can:listGroupKeys,' . \BaseCms\BaseBlockBuilder\Models\BlockModel::class) //
            ->name('datatableGroupKeys');

        Route::get('/{block}/{keys}/editGroupKeys/{inner?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'editGroupKeys',
        ])
            ->middleware('can:update,block')
            ->name('editGroupKeys');

        Route::patch('/{block}/{keys}/updateGroupKeys/{inner?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'updateGroupKeys',
        ])
            ->middleware('can:update,block')
            ->name('updateGroupKeys');

        Route::any('/selectComponentGroupKeys/{keys}/{isModal?}/{parentBlockId?}/{inner?}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'selectComponentGroupKeys',
        ])->name('selectComponentGroupKeys');

        Route::get('/duplicate/{block}', [
            \BaseCms\BaseBlockBuilder\Domains\Block\Http\Controllers\BlockController::class,
            'duplicate',
        ])
            ->middleware('can:list,' . \BaseCms\BaseBlockBuilder\Models\BlockModel::class)
            ->name('duplicate');
    });

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
        Route::get('/index/{key}', [
            \BaseCms\BaseBlockBuilder\Domains\Page\Http\Controllers\PageController::class,
            'index',
        ])
            ->middleware('can:list,' . \BaseCms\BaseBlockBuilder\Models\PageModel::class)
            ->name('indexKey');

        Route::get('/duplicate/{page}', [
            \BaseCms\BaseBlockBuilder\Domains\Page\Http\Controllers\PageController::class,
            'duplicate',
        ])
            ->middleware('can:list,' . \BaseCms\BaseBlockBuilder\Models\PageModel::class)
            ->name('duplicate');
    });

    \BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition::instance()->makeGroupRoutes(null, 'page');
    \BaseCms\BaseBlockBuilder\Domains\Page\Definition\PageDefinition::instance()->makeBreadcrumbsRoutes();

    \BaseCms\BaseBlockBuilder\Domains\Block\Definition\BlockDefinition::instance()->makeGroupRoutes(null, 'block');
    \BaseCms\BaseBlockBuilder\Domains\Block\Definition\BlockDefinition::instance()->makeBreadcrumbsRoutes();
});
