<?php

use BaseCms\BaseBlockBuilder\DomainsFront\BlockBuilder\Http\Controllers\BlockBuilderController;
use BaseCms\BaseBlockBuilder\Models\PageModel;

Route::get('/blockbuilder', [BlockBuilderController::class, 'all'])->name('blockbuilder.index');

Route::get('/blockbuilder-component/{block}', [BlockBuilderController::class, 'component'])->name(
    'blockbuilder.component',
);

/**
 * Rotas dinâmicas
 */
if (is_string($locale)) {
    try {
        $pages = PageModel::where(function ($q) use ($locale) {
            $q->where('locale', $locale)->orWhereNull('locale');
        })
            ->whereNotNull('slug')
            ->get();
    } catch (\Exception $e) {
    }

    if (@$pages) {
        $pages->map(function ($v) {
            \Route::get('/' . trim($v->slug, '/'), [BlockBuilderController::class, 'page'])
                ->name('pages.' . $v->key)
                ->defaults('page', $v->toArray())
                ->breadcrumbs(function ($trail) use ($v) {
                    $breadcrumbs = explode(',', $v->breadcrumbs);
                    foreach ($breadcrumbs as $breadcrumb) {
                        if ($v->route_key != $breadcrumb) {
                            try {
                                $trail->parent($breadcrumb);
                            } catch (\Exception $e) {
                                //ignora caso nao tenha registro do breadcrumb na página
                            }
                        }
                    }

                    $trail->push(__($v->meta_title), routeLocale('frontend.pages.' . $v->key));
                });

            \Route::get('/' . trim($v->slug, '/') . '.json', [BlockBuilderController::class, 'pageJson'])
                ->name('pages.' . $v->key . '.json')
                ->defaults('page', $v->toArray());
        });
    }
}
