<?php

namespace BaseCms;

/**
 *
 */
class BaseDefaultRenderParams
{
    public $textEllipsisLength = '200';
    public $imageMaxWidth = '150px';
    public $videoWidth = '150';

    /**
     * @return BaseDefaultRenderParams
     */
    public static function newObj(): BaseDefaultRenderParams
    {
        return new BaseDefaultRenderParams();
    }

    /**
     * Tamanho máximo da imagem
     *
     * @param integer $textEllipsisLength
     *
     * @return $this
     */
    public function setTextEllipsisLength($textEllipsisLength)
    {
        $this->textEllipsisLength = $textEllipsisLength;
        return $this;
    }

    /**
     * Tamanho máximo do texto antes de adicionar '...' no final
     *
     * @param string $imageMaxWidth 150px
     *
     * @return $this
     */
    public function setImageMaxWidth($imageMaxWidth)
    {
        $this->imageMaxWidth = $imageMaxWidth;
        return $this;
    }

    /**
     * Tamanho do vídeo
     *
     * @param string $videoWidth 150
     *
     * @return $this
     */
    public function setVideoWidth($videoWidth)
    {
        $this->videoWidth = $videoWidth;
        return $this;
    }
}
