<?php

namespace BaseCms;

/**
 * Montagem das páginas defaults do CMS a partir de uma definição
 *
 * Este arquivo é o CORE do CMS da Base
 */
class BaseDefinition
{
    use BaseDefinitionHelpersTrait;

    /**
     * Título da sessão, utilizado em:
     *  - sidebar (itens)
     *  - sessão interna (cadastro, update)
     *  - breadcrumb (itens)
     *  - complemento em alguns botões
     *
     * @var string
     */
    protected $title;

    /**
     * Título da sessão, utilizado em:
     *  - sidebar (texto principal)
     *  - sessão interna (listagem)
     *  - breadcrumb (sessão)
     *
     * @var string
     */
    protected $titlePlural;

    /**
     * Path da rota, usado internamente p/ gerar as rotas defaults e retornar algumas urls para as view().
     *
     * NOTA: No controller geralmente a rota é criada na mão, sem utilizar o routeAlias.
     *
     * Caso seja alterado o routeAlias, deve ser atualizado no Controller as rotas (Ex: SampleController.php)
     *
     * @var string
     */
    protected $routeAlias;

    /**
     * Model utilizado pelo definition.
     *
     * É importante porque em algumas partes do sistema é referenciado o model automaticamente.
     *
     * @var string
     */
    protected $modelClass;

    /**
     * Controller utilizado pelo definition.
     *
     * É importante porque em algumas partes do sistema é referenciado o model automaticamente.
     *
     * @var string
     */
    protected $controllerClass;

    /**
     * FormRequest utilizado pelo definition/controller.
     *
     * Ele é passado para a view() e o Aire:: (Form builder) faz validação via javascript dos métodos.
     *
     * PS: Os métodos de validação precisam estar implementados nos dois lados do sistema. Ver BaseValidatorHelper.php
     *
     * @var string
     */
    protected $formRequestClass;

    /**
     * FormRequest utilizado pelo definition/controller p/ Edição
     *
     * Ele é passado para a view() e o Aire:: (Form builder) faz validação via javascript dos métodos.
     *
     * PS: Os métodos de validação precisam estar implementados nos dois lados do sistema. Ver BaseValidatorHelper.php
     *
     * @var string
     */
    protected $formRequestUpdateClass;

    /**
     * Colunas que podem ser consumidas pelo form, table, view
     *
     * @var BaseField[]
     */
    protected $fields;

    /**
     * Define quais campos vão ser mostrados na tabela.
     *
     * Ver: SamplesController.php@index e resources/views/backend/samples/index.blade.php
     *
     * @var string[]
     */
    protected $tableFields;

    /**
     * Define quais campos vão ser mostrados no formulário (cadastro ou cadastro/edição).
     *
     * A tela é montada automaticamente através do "BaseInputHelper::formInputs()"
     *
     * Ver: SamplesController.php@create e resources/views/backend/samples/create_update.blade.php
     *
     * @var string[]
     */
    protected $formFields;

    /**
     * Define quais campos vão ser mostrados no formulário de edição.
     *
     * A tela é montada automaticamente através do "BaseInputHelper::formInputs()"
     *
     * Ver: SamplesController.php@edit e resources/views/backend/samples/create_update.blade.php
     *
     * @var string[]
     */
    protected $formEditFields;

    /**
     * Define quais campos vão ser mostrados na tela de visualização.
     *
     * A tela é montada automaticamente através do "BaseViewHelper.php"
     *
     * Ver: SamplesController.php@view e resources/views/backend/samples/view.blade.php
     *
     * @var string[]
     */
    protected $viewFields;

    /**
     * Define se os registros são ou não exportáveis.
     *
     * Cada coluna também tem essa propriedade.
     *
     * Caso sim, vai adicionar um botão na Datatable p/ exportar os dados.
     *
     * O export dos dados funciona com o filtro aplicado.
     *
     * PS: Método exportCSV() deve estar declarado no controller. Ver SampleController.php
     *
     * @var bool
     */
    protected $exportable = true;

    /**
     * Rota p/ listagem dos registros
     *
     * @var bool
     */
    protected $routeList = true;

    /**
     * Rota p/ visualizar registro
     *
     * @var bool
     */
    protected $routeView = true;

    /**
     * Rota p/ criar registro
     *
     * @var bool
     */
    protected $routeCreate = true;

    /**
     * Rota p/ atualizar registro
     *
     * @var bool
     */
    protected $routeUpdate = true;

    /**
     * Rota p/ remover registro
     *
     * @var bool
     */
    protected $routeDelete = true;

    /**
     * Rota p/ ordenamento dos registros.
     *
     * PS: Deve ser adicionado no Controller as funções de ordenamento (Ver SampleController.php)
     *
     * @var bool
     */
    protected $routeOrderable = false;

    /**
     * Rota p/ restaurar o registros.
     *
     * PS: Deve ser adicionado no Controller (Ver SampleController.php)
     *
     * @var bool
     */
    protected $routeRestore = false;

    /**
     * Rota p/ remoção total do registro
     *
     * PS: Deve ser adicionado no Controller (Ver SampleController.php)
     *
     * @var bool
     */
    protected $routeForceDelete = false;

    /**
     * Rota p/ componente de check na tabela.
     *
     * Ex: No definition, adicionar no setFields()
     *
     * BaseField::newObj('checkbox_column', 'Label', BaseInputConst::AireCheckbox, BaseColumnConst::Boolean)
     *  ->setColumnRender(CheckboxColumnRender::class),
     *
     * PS: Deve ser adicionado no Controller a ação 'public function checkValue' (Ver SampleController.php)
     *
     * @var bool
     */
    protected $routeCheckValue = false;

    /**
     * Rota p/ componente de radio na tabela.
     *
     * Ex: No definition, adicionar no setFields()
     *
     * BaseField::newObj('radio_column', 'Label', BaseInputConst::AireCheckbox, BaseColumnConst::Boolean)
     *  ->setColumnRender(RadioColumnRender::class),
     *
     * PS: Deve ser adicionado no Controller a ação 'public function radioValue' (Ver SampleController.php)
     *
     * @var bool
     */
    protected $routeRadioValue = false;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    public $urlCsv;

    /**
     * @var
     */
    public $multipleSelectTable = null;

    /**
     * @var
     */
    public $filterLocale = false;

    /**
     * Título da sessão, utilizado em várias partes como
     *  - breadcrumbs
     *  - cadastro/atualização
     *  - listagem
     *  - menu (sidebar)
     *
     * @param string $title
     *
     * @return BaseDefinition
     */
    public function setTitle(string $title): BaseDefinition
    {
        $this->title = $title;

        return $this;
    }

    public function setTitlePlural(string $titlePlural): BaseDefinition
    {
        $this->titlePlural = $titlePlural;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return __($this->title);
    }

    /**
     * @return string
     */
    public function getTitlePlural()
    {
        if (!$this->titlePlural) {
            return __($this->title);
        }

        return __($this->titlePlural);
    }

    /**
     * Determina se a tabela é exportável ou não
     *
     * @param bool $exportable
     *
     * @return BaseDefinition
     */
    public function setExportable(bool $exportable)
    {
        $this->exportable = $exportable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExportable(): bool
    {
        return $this->exportable;
    }

    public function setUrlCsv(string $urlCsv): BaseDefinition
    {
        $this->urlCsv = $urlCsv;

        return $this;
    }

    /**
     *
     * Registro do Controller
     *  - Utilizado na montagem das rotas, através do método "BaseDefinition->makeGroupRoutes"
     *
     * @param string $controllerClass
     *
     * @return BaseDefinition
     */
    public function setControllerClass($controllerClass): BaseDefinition
    {
        $this->controllerClass = $controllerClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getControllerClass(): string
    {
        return $this->controllerClass;
    }

    /**
     * Registro do "Model" da Definition
     *  - Por hora, utilizado na tela de cadastro/atualização (create_update.blade.php) p/ saber se o usuário tem permissão para criar (Via Policy) ... isso permite generalizar a página do front
     *
     * @param string $modelClass
     *
     * @return BaseDefinition
     */
    public function setModelClass($modelClass): BaseDefinition
    {
        $this->modelClass = $modelClass;

        return $this;
    }

    /**
     * Retorna o nome da tabela
     * @return string
     */
    public function getTable()
    {
        if (!$this->tableName) {
            $this->tableName = app($this->modelClass)->getTable();
        }

        return $this->tableName;
    }

    /**
     * @return string
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * Registro do "FormRequest" p/ ser utilizado nas validações
     *  - Utilizado na tela de cadastro (create_update.blade.php), setado no Aire::form p/ validação do formulário via "js"
     *
     * @param string $formRequestClass
     *
     * @return BaseDefinition
     */
    public function setFormRequestClass($formRequestClass): BaseDefinition
    {
        $this->formRequestClass = $formRequestClass;
        return $this;
    }

    /**
     * Alias p/ o "setFormRequestClass"
     */
    public function setFormRequestCreateClass($formRequestClass): BaseDefinition
    {
        return $this->setFormRequestClass($formRequestClass);
    }

    /**
     * @return string
     */
    public function getFormRequestClass()
    {
        return $this->formRequestClass;
    }

    /**
     * Registro do "FormRequest" p/ ser utilizado nas validações (Update)
     *  - Utilizado na tela de atualização (create_update.blade.php), setado no Aire::form p/ validação do formulário via "js"
     *
     * @param string $formRequestClass
     *
     * @return BaseDefinition
     */
    public function setFormRequestUpdateClass($formRequestUpdateClass): BaseDefinition
    {
        $this->formRequestUpdateClass = $formRequestUpdateClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormRequestUpdateClass()
    {
        return $this->formRequestUpdateClass;
    }

    /**
     * Registro dos "fields",os fields setados são consumidos por várias partes do sistema
     *  - formulário - inputs (create_update.blade.php)
     *  - tabela - datatable - (index.blade.php)
     *  - validações
     *  - repository (upload de arquivo)
     *
     * @param BaseField[] $fields
     *
     * @return BaseDefinition
     */
    public function setFields($fields): BaseDefinition
    {
        foreach ($fields as $v) {
            $v->setBaseDefinition($this);
        }

        $this->fields = $fields;

        return $this;
    }

    /**
     * Adicionar um BaseField na listagem de fields.
     *
     * Importante executar esse método após o "setFields" (Caso seja usado)
     *
     * @param BaseField $field
     *
     * @return BaseDefinition
     */
    public function addField(BaseField $field): BaseDefinition
    {
        $field->setBaseDefinition($this);

        if (!$this->fields) {
            $this->fields = [];
        }

        $this->fields[] = $field;
        return $this;
    }

    /**
     * Inicialmente foi implementado o método, mas percebi depois que remover uma coluna influencia em muitas partes do sistema.
     *
     * Esse método só poderia ser chamado depois que foi adicionado uma coluna, consequentemente não faria sentido.
     *
     * Se ele é chamado depois (num update), vai quebrar outras partes do sistema
     *
     * O que eu aconselho:
     * 1 - Alterar o campo para hidden
     * 2 - Tirar as rules dele p/ não precisar manipular o FormRequest
     * 3 - Não renderizar no front, alterando a própria view.
     *
     */
    public function deleteField(string $columnName)
    {
        return null;

        // $this->fields = array_filter($this->fields, function ($v) use ($columnName) {
        //     return $v->field != $columnName;
        // });
    }

    /**
     * @return BaseField[]
     */
    public function getFields()
    {
        return collect($this->fields)
            ->keyBy('field')
            ->toArray();
    }

    /**
     * @return BaseField[]
     */
    // public function getModelFields() {
    //     return collect($this->fields)->filter(function($value, $key) {
    //         //Colunas com "." devem ser ignoradas, porque o Datatable faz um "select" por fora.
    //         //Data, é quando tem um campo Virtual (BaseDataVirtualRawSQL, BaseDataArray, ...)
    //         //Enum, o campo tem "data" mas é uma coluna válida
    //         return strpos($value->field, ".") === FALSE && (@!$value->data || $value->data instanceof BaseDataEnum);
    //     })->keyBy('field')->toArray();
    // }

    /**
     * Retorna o BaseField da coluna informada
     *
     * @param mixed $columnName
     *
     * @return BaseField
     */
    public function getField($columnName)
    {
        return $this->getFields()[$columnName];
    }

    /**
     * Fields que irão aparecer no Formulário de Cadastro/Edição (Inputs)
     *
     * PS: Caso seja definido setFormEditFields($fields), esta função só retornará os campos do formulário para CADASTRO
     *
     * @param string[] $formFields
     *
     * @return BaseDefinition
     */
    public function setFormFields($formFields): BaseDefinition
    {
        if ($ret = $this->existInFields($formFields)) {
            throw new \Exception('[setFormFields] Undefined index: ' . join(', ', $ret));
        }

        if (strpos(\Arr::query($formFields), '.') !== false) {
            throw new \Exception(
                'Não é permitido inputs com "." no nome. Motivo: Validator do Laravel não funciona (ele utiliza o "." p/ validações em input[])',
            );
        }

        $this->formFields = $formFields;

        if (!$this->formEditFields) {
            $this->formEditFields = $formFields;
        }

        return $this;
    }

    /**
     * Fields que irão aparecer no Formulário de EDIÇÃO - inputs
     *
     * @param string[] $formFields
     *
     * @return BaseDefinition
     */
    public function setFormEditFields($formEditFields): BaseDefinition
    {
        if ($ret = $this->existInFields($formEditFields)) {
            throw new \Exception('[setFormEditFields] Undefined index: ' . join(', ', $ret));
        }

        if (strpos(\Arr::query($formEditFields), '.') !== false) {
            throw new \Exception(
                'Não é permitido inputs com "." no nome. Motivo: Validator do Laravel não funciona (ele utiliza o "." p/ validações em input[])',
            );
        }

        $this->formEditFields = $formEditFields;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getFormFields()
    {
        return $this->formFields;
    }

    /**
     * @return string[]
     */
    public function getFormEditFields()
    {
        return $this->formEditFields;
    }

    /**
     * Fields que irão aparecer na tabela (Colunas)
     *
     * @param string[] $tableFields
     *
     * @return BaseDefinition
     */
    public function setTableFields($tableFields): BaseDefinition
    {
        if ($ret = $this->existInFields($tableFields)) {
            throw new \Exception('[setTableFields] Undefined index: ' . join(', ', $ret));
        }

        $this->tableFields = $tableFields;

        return $this;
    }

    /**
     * Fields que irão aparecer no Visualizar (Caso utilize a blade de "auto-view")
     *
     * @param string[] $viewFields
     *
     * @return BaseDefinition
     */
    public function setViewFields($viewFields): BaseDefinition
    {
        if ($ret = $this->existInFields($viewFields)) {
            throw new \Exception('[setViewFields] Undefined index: ' . join(', ', $ret));
        }

        $this->viewFields = $viewFields;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getViewFields()
    {
        return $this->viewFields;
    }

    /**
     * Retorna os "fields" que não foram registrados pelo método setFields()
     *
     * @return string[]
     */
    private function existInFields($list)
    {
        $keys = collect($this->fields)
            ->pluck('field')
            ->toArray();
        $values = array_diff($list, $keys);

        return $values;
    }

    /**
     * @return string[]
     */
    public function getTableFields()
    {
        return $this->tableFields;
    }

    /**
     * @param string $routeAlias
     *
     * @return BaseDefinition
     */
    public function setRouteAlias($routeAlias): BaseDefinition
    {
        $this->routeAlias = $routeAlias;

        return $this;
    }

    /**
     * @return string
     */
    public function getRouteAlias()
    {
        return $this->routeAlias;
    }

    /**
     * Key da primaryKey
     *
     * @return string
     */
    public function getPrimaryKey()
    {
        $primaryKey = collect($this->fields)
            ->pluck('primaryKey', 'field')
            ->search(true);
        return $primaryKey;
    }

    /**
     * Chamada no update() p/ substituir algumas informações do definition.
     *
     * Este método é reimplementado no definition do Domínio
     *
     * IMPORTANTE: Se for manipulado o BaseRule, deve ser chamado essa função dentro do /Requests/NomeRequest.php, para ele entender as alterações nas rules.
     *
     * Ex: Na atualização quero que um determinado campo seja readonly
     *
     * @return BaseDefinition
     */
    public function setDefinitionUpdate($model, $parentModel = null)
    {
        return $this;
    }

    /**
     * Chamada no create() p/ substituir algumas informações do definition.
     *
     * Este método é reimplementado no definition do Domínio
     *
     * Ex: Na criação quero que o RawSQL pegue dados a partir de outra informação
     *
     * @return BaseDefinition
     */
    public function setDefinitionCreate($parentModel = null)
    {
        return $this;
    }

    /**
     * Chamada no view() p/ substituir algumas informações do definition.
     *
     * Este método é reimplementado no definition do Domínio
     *
     * @return BaseDefinition
     */
    public function setDefinitionView($model, $parentModel = null)
    {
        return $this;
    }

    /**
     * Habilita o uso de multipla seleção na tabela (rotas e afins)
     *
     * @return BaseDefinition
     */
    public function enableMultipleSelectTable()
    {
        $this->multipleSelectTable = true;
        return $this;
    }

    /**
     *
     * Para utilizar é necessário ter ao menos:
     * a) Campo 'locale' no $fillable do Model
     * b) $translatedAttributes  no Model
     *
     * @param bool $enabled
     *
     * @return BaseDefinition
     */
    public function setFilterLocale(bool $enabled): BaseDefinition
    {
        $this->filterLocale = $enabled;

        return $this;
    }
}
