<?php

namespace BaseCms;

use BaseCms\Datatables\Helpers\BaseDatatableHelper;
use BaseCms\Helpers\BaseFormRequestHelper;
use BaseCms\Helpers\BaseRoutesHelper;
use BaseCms\Helpers\BaseHelper;

/**
 * Funções de auxilio (Helpers) p/
 *  - formRequest (
 *      - rules
 *      - atributos
 *      - mensagens
 *  - Datatable
 *      - colunas em html (th)
 *      - colunas em objeto (js)
 *      - html (blade) do custom filtro (filtro empilhado)
 *  - Sidebar
 *      - dropdown menu montado (blade) - lista/cadastro
 *      - link único de acesso (blade) - lista
 *  - Helpers
 *      - habilita apenas rota de listagem
 *      - habilita apenas rota de listagem / visualização (Ex: Páginas como contato)
 *      - retorno dos fields (colunas) com base em uma tipagem
 *  - Rotas
 *      - Criar as rotas defaults com base no definition
 *      - Criar o breadcrumbs com base no definition
 *      - urls bases (index, create, update ...)
 *      - habilitar/desabilitar rotas padrões
 */
trait BaseDefinitionHelpersTrait
{
    /**
     * Form Request - Rules
     *
     * @return array[string]string
     */
    public function formRequestRules()
    {
        return BaseFormRequestHelper::rules($this);
    }

    /**
     * Form Request Edit - Rules
     *
     * @return array[string]string
     */
    public function formRequestEditRules()
    {
        return BaseFormRequestHelper::rulesEdit($this);
    }

    /**
     * Form Request - Attributes
     *
     * @return array[string]string
     */
    public function formRequestAttributes()
    {
        return BaseFormRequestHelper::attributes($this);
    }

    /**
     * Form Request Edit - Attributes
     *
     * @return array[string]string
     */
    public function formRequestEditAttributes()
    {
        return BaseFormRequestHelper::attributesEdit($this);
    }

    /**
     * Form Request - Messages
     *
     * @return array[string]string
     */
    public function formRequestMessages()
    {
        return BaseFormRequestHelper::messages($this);
    }

    /**
     * Form Request Edit - Messages
     *
     * @return array[string]string
     */
    public function formRequestEditMessages()
    {
        return BaseFormRequestHelper::messagesEdit($this);
    }

    /**
     * @return string
     */
    public function getThColumns()
    {
        return BaseDatatableHelper::getThColumns($this);
    }

    /**
     * @return string
     */
    public function getThColumnsNoAction()
    {
        return BaseDatatableHelper::getThColumnsNoAction($this);
    }

    /**
     * @return array
     */
    public function getObjColumns()
    {
        return json_encode(BaseDatatableHelper::getObjColumns($this));
    }

    /**
     * Shortcut: Habilita apenas a rota de listagem
     */
    public function onlyList()
    {
        $this->routeList = true;
        $this->routeView = false;
        $this->routeCreate = false;
        $this->routeUpdate = false;
        $this->routeDelete = false;
    }

    /**
     * Shortcut: Habilita apenas a rota de listagem e visualização
     */
    public function onlyView()
    {
        $this->routeView = true;
        $this->routeList = true;
        $this->routeCreate = false;
        $this->routeUpdate = false;
        $this->routeDelete = false;
    }

    /**
     * Helper - Criar rotas default
     *
     * @param string|null $prefixAlias
     *
     * @return \Illuminate\Support\Facades\Route::group
     */
    public function makeGroupRoutes(?string $prefixAlias = null, ?string $modelName = 'model')
    {
        if (!$prefixAlias) {
            $prefixAlias = $this->getRouteAlias();
        }

        $include = [];
        if ($this->routeList) {
            $include[] = 'list';
        }
        if ($this->routeView) {
            $include[] = 'view';
        }
        if ($this->routeCreate) {
            $include[] = 'create';
        }
        if ($this->routeUpdate) {
            $include[] = 'edit';
        }
        if ($this->routeDelete) {
            $include[] = 'delete';
        }
        if ($this->routeOrderable) {
            $include[] = 'orderable';
        }
        if ($this->routeCheckValue) {
            $include[] = 'checkValue';
        }
        if ($this->routeRadioValue) {
            $include[] = 'radioValue';
        }
        if ($this->exportable) {
            $include[] = 'exportable';
        }
        if ($this->routeRestore) {
            $include[] = 'restore';
        }
        if ($this->routeForceDelete) {
            $include[] = 'forceDelete';
        }
        if ($this->multipleSelectTable) {
            $include[] = 'multipleSelect';
        }

        return BaseRoutesHelper::makeGroupRoutes(
            $this->getControllerClass(),
            $this->getModelClass(),
            $prefixAlias,
            $this->getRouteAlias(),
            $include,
            $modelName,
        );
    }

    /**
     * Breadcrumbs com base do "makeGroupRoutes()"
     *
     * @return void
     */
    public function makeBreadcrumbsRoutes()
    {
        return BaseRoutesHelper::makeBreadcrumbsRoutes(
            $this->getTitlePlural(),
            $this->getTitle(),
            $this->getRouteAlias(),
        );
    }

    /**
     * Rota - "index" (listagem datatable)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlIndex(?array $params = null)
    {
        return route('admin.' . $this->getRouteAlias() . '.index', $params);
    }

    /**
     * Rota - "datatable" (api Datatable)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlDatatable(?array $params = null)
    {
        return route('admin.' . $this->getRouteAlias() . '.datatable', $params);
    }

    /**
     * Rota - "create" (cadastro)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlCreate(?array $params = null)
    {
        return route('admin.' . $this->getRouteAlias() . '.create', $params);
    }

    /**
     * Rota - "back"
     *
     * @param  mixed $params
     * @return string
     */
    public function urlBack(?array $params = null)
    {
        return BaseHelper::prevUrl($this->urlIndex($params));
    }

    /**
     * Rota - "index restore" (listagem restaurar datatable)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlRestore(?array $params = null)
    {
        return route('admin.' . $this->getRouteAlias() . '.indexRestore', $params);
    }

    /**
     * Rota - "datatable restore" (api Datatable)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlDatatableRestore(?array $params = null)
    {
        return route('admin.' . $this->getRouteAlias() . '.datatableRestore', $params);
    }

    /**
     * Rota - "CSV" (export csv)
     *
     * @param  mixed $params
     * @return string
     */
    public function urlCSV(?array $params = null)
    {
        $route = 'admin.' . $this->getRouteAlias() . '.exportCsv';

        if (@$this->urlCsv) {
            $route = $this->urlCsv;
        }

        if (\Route::has($route)) {
            return route($route, $params);
        }

        return '';
    }

    /**
     * Lista de fields de um array de types (BaseColumnConst)
     *
     * @param string|array $type
     *
     * @return BaseField[]
     */
    public function getFieldsType($type)
    {
        if (!is_array($type)) {
            $type = [$type];
        }

        return array_filter($this->getFields(), function (BaseField $v) use ($type) {
            return in_array($v->type, $type) &&
                (in_array($v->field, $this->getFormFields()) || in_array($v->field, $this->getFormEditFields()));
        });
    }

    /**
     * Sidebar / Menu com dropdown
     *
     * @param string $icon
     * @param string $iconLabel
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function makeSidebarNavDropdown($icon = 'cil-description', $iconLabel = '')
    {
        $title = $this->getTitle();

        $titlePlural = $this->getTitlePlural();

        $urlIndex = $this->routeList ? $this->urlIndex() : null;
        $urlCreate = $this->routeCreate ? $this->urlCreate() : null;
        $urlEdit = $this->routeUpdate ? 'admin.' . $this->getRouteAlias() . '.edit' : null;

        $activeRoute = 'admin.' . $this->getRouteAlias() . '.*';

        if ($iconLabel) {
            $icon = 'base-icon-text ' . $icon;
        }

        return view(
            'base::components.sidebar-nav-dropdown',
            compact('title', 'titlePlural', 'icon', 'iconLabel', 'urlCreate', 'urlIndex', 'urlEdit', 'activeRoute'),
        );
    }

    /**
     * Sidebar / Link direto
     *
     * @param string $icon
     * @param string $iconLabel
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function makeSidebarNavLink($icon = 'cil-description', $iconLabel = '')
    {
        $title = $this->getTitlePlural();
        $url = $this->urlIndex();
        $activeRoute = 'admin.' . $this->getRouteAlias();

        if ($iconLabel) {
            $icon = 'base-icon-text ' . $icon;
        }

        return view('base::components.sidebar-nav-link', compact('title', 'url', 'icon', 'iconLabel', 'activeRoute'));
    }

    /**
     * Sidebar / Link direto
     *
     * @param string $url Ex: route('xxxx', [])
     * @param string $icon
     * @param string $iconLabel
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function makeSidebarNavCustomLink($url, $icon = 'cil-description', $iconLabel = '')
    {
        $title = $this->getTitlePlural();

        $activeRoute = 'admin.' . $this->getRouteAlias();

        if ($iconLabel) {
            $icon = 'base-icon-text ' . $icon;
        }

        return view('base::components.sidebar-nav-link', compact('title', 'url', 'icon', 'iconLabel', 'activeRoute'));
    }

    /**
     * Retorno da view do filtro custom (Filtro dos fields com isFilterable = true)
     *
     * @param string $tableId
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function addFilterAndExport($tableId = 'table', $params = null)
    {
        return view('base::components.datatable-filter-and-export', [
            'tableId' => $tableId,
            'params' => $params,
            'definition' => $this,
            'hash' => $tableId,
        ]);
    }

    /**
     * Rotas list, view, create, update, delete, order, checkValue, radioValue
     */
    public function enableRouteList()
    {
        $this->routeList = true;
    }
    public function disableRouteList()
    {
        $this->routeList = false;
    }
    public function enableRouteView()
    {
        $this->routeView = false;
    }
    public function disableRouteView()
    {
        $this->routeList = false;
    }
    public function enableRouteCreate()
    {
        $this->routeCreate = true;
    }
    public function disableRouteCreate()
    {
        $this->routeCreate = false;
    }
    public function enableRouteUpdate()
    {
        $this->routeUpdate = true;
    }
    public function disableRouteUpdate()
    {
        $this->routeUpdate = false;
    }
    public function enableRouteDelete()
    {
        $this->routeDelete = true;
    }
    public function disableRouteDelete()
    {
        $this->routeDelete = false;
    }
    public function enableRouteOrderable()
    {
        $this->routeOrderable = true;
    }
    public function disableRouteOrderable()
    {
        $this->routeOrderable = false;
    }
    public function enableRouteCheckValue()
    {
        $this->routeCheckValue = true;
    }
    public function disableRouteCheckValue()
    {
        $this->routeCheckValue = false;
    }
    public function enableRouteRadioValue()
    {
        $this->routeRadioValue = true;
    }
    public function disableRouteRadioValue()
    {
        $this->routeRadioValue = false;
    }
    public function enableRouteRestore()
    {
        $this->routeRestore = true;
    }
    public function disableRouteRestore()
    {
        $this->routeRestore = false;
    }
    public function enableRouteForceDelete()
    {
        $this->routeForceDelete = true;
    }
    public function disableRouteForceDelete()
    {
        $this->routeForceDelete = false;
    }
    /** Fim rotas */
}
