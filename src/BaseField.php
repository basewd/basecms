<?php

namespace BaseCms;

use BaseCms\Constants\BaseInputConst;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Datatables\Data\BaseDataVirtualRawSql;
use BaseCms\Datatables\Interfaces\BaseData;
use Closure;
use Illuminate\Database\Eloquent\Model;

/**
 * Arquivo de definição do "field" (input/coluna da tabela)
 *
 * Através dele é estruturado diversas partes do sistema.
 */
class BaseField
{
    /**
     * Coluna da Datatable, campo do banco, etc
     *
     * PS: Pode ser um relationship (Ex: sampleOther.name) p/ caso de relacionamentos (utilizando só na Tabela como se fosse um campo virtual)
     *
     * @var string
     */
    public $field;

    /**
     * Label do campo (mostra no formulário e na tabela)
     *
     * @var string
     */
    public $title;

    /**
     * Define o tipo de render do Input.
     *
     * PS: Para inputs customs, criar uma Classe e extender o BaseInput (ver método make() do BaseInputHelper)
     *
     * @var string|BaseInputConst
     */
    public $input;

    /**
     * Métodos que podem ser chamados no "input".
     *
     * Se o método existir no objeto/classe do "input", ele será chamado.
     *
     * Ex:
     *
     * [
     *   ['placeholder' => 'Foo']
     * ]
     *
     * @var array[string]string
     */
    public $inputMethods;

    /**
     * Render custom para uma coluna do DataTable
     *
     * @var fn
     */
    public $columnRender;
    public $columnRenderMethods;

    /**
     * @var BaseRule
     */
    public $rule;

    /**
     * @var \BaseCms\Datatables\Interfaces\BaseData
     */
    public $data;

    /**
     * Tipo do campo,
     *  - utilizado p/ renderizar o campo na tabela com um custom render
     *
     * @var string|BaseColumnConst
     */
    public $type;

    /**
     * @var bool
     */
    public $exportable;

    /**
     * Define se o campo pode ser filtrado p/ Datatables::customRenderFilter"
     *
     * @var bool
     */
    public $filterable;

    /**
     * Define se o campo pode ser ordenado na tabela, Datatables"
     *
     * @var bool
     */
    public $orderable;

    /**
     * Utilizado p/ inputs do tipo "file" (BaseInputConst::File, BaseInputConst::Image)
     *
     * Contém as definições p/ upload e resize da imagem
     *
     * @var BaseUploadDefinition
     */
    public $uploadDefinition;

    /**
     * @var bool
     */
    public $primaryKey;

    /**
     * @var BaseDefaultRenderParams
     */
    public $defaultRenderParams;

    /**
     * @var function
     */
    public $valueRender;

    /**
     * @var function
     */
    public $exportRender;

    /**
     * @var function
     */
    public $renderInput;

    /**
     * @var BaseDefinition
     */
    private $baseDefinition;

    /**
     * @var function
     */
    public $preset;

    public function __construct(
        string $field,
        string $title,
        string $input = BaseInputConst::AireInput,
        string $type = BaseColumnConst::Text
    ) {
        $this->field = $field;

        $this->title = $title;
        $this->input = $input;
        $this->type = $type;

        $this->rule = null;

        $this->defaultRenderParams = new BaseDefaultRenderParams();

        $this->exportable = true;
        $this->filterable = true;
        $this->orderable = true;
    }

    /**
     * Cria e retorna um BaseField
     *
     * @param string $field
     * @param string $title
     * @param string $input
     * @param string $type
     *
     * @return BaseField
     */
    public static function newObj(
        string $field,
        string $title,
        string $input = BaseInputConst::AireInput,
        string $type = BaseColumnConst::Text
    ): BaseField {
        return new BaseField($field, $title, $input, $type);
    }

    /**
     * Referencia o BaseField a um BaseDefinition
     *
     * @param BaseDefinition $baseDefinition
     *
     * @return $this
     */
    public function setBaseDefinition(BaseDefinition $baseDefinition)
    {
        $this->baseDefinition = $baseDefinition;

        return $this;
    }

    /**
     * @return BaseDefinition
     */
    public function getBaseDefinition()
    {
        return $this->baseDefinition;
    }

    /**
     * Array de arrays.
     *
     * Propaga métodos para o "input" definido.
     *
     * O "input" é uma classe/objeto. Os métodos que estão dentro dessa "classe" podem ser acessadas através do "setInputMethods".
     *
     * Ex:
     *
     * Render: Aire::input (https://github.com/glhd/aire)
     * Método: placeholder();
     *
     * setInputMethods(
     *  [
     *      ['placeholder', 'Texto do Placeholder'],
     *      ['helpText', 'Aqui vai o helperText'],
     *  ]
     * );
     *
     * //TODO Futuramente, encontrar uma forma mais elegante p/ as chamadas internas das classes.
     *
     * Referências dos métodos:
     * use \Galahad\Aire\Elements\Input
     * use \Galahad\Aire\Elements\Textarea
     * use \Galahad\Aire\Elements\Select
     * use \Galahad\Aire\Elements\Checkbox
     * use \Galahad\Aire\Elements\Label
     * use \Galahad\Aire\Elements\Element
     * use \Galahad\Aire\Elements\Element;
     *
     * @param array[string]string $inputMethods
     *
     * @return BaseField
     */
    public function setInputMethods(array $inputMethods): BaseField
    {
        $this->inputMethods = $inputMethods;

        return $this;
    }

    /**
     * Adiciona uma nova chamada no inputMethods
     *
     * @param array $inputMethods
     *
     * @return BaseField
     */
    public function addInputMethod(array $method): BaseField
    {
        if (!$this->inputMethods) {
            $this->inputMethods = [];
        }

        $this->inputMethods[] = $method;

        return $this;
    }

    /**
     * Seta definição p/ upload/resize de arquivos
     *
     * @param BaseUploadDefinition $uploadDefinition
     *
     * @return BaseField
     */
    public function setUploadDefinition(BaseUploadDefinition $uploadDefinition): BaseField
    {
        if (!$uploadDefinition instanceof BaseUploadDefinition) {
            throw new \Exception('Upload Definition deve ser do tipo "BaseUploadDefinition"');
        }

        $this->uploadDefinition = $uploadDefinition;

        return $this;
    }

    /**
     * Rule para o campo (field)
     *
     * @param BaseRule|null $rule
     *
     * @return BaseField
     */
    public function setRule(?BaseRule $rule): BaseField
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Definição de dados para o formulário/tabela
     *
     * @param BaseData|null $data
     *
     * @return BaseField
     */
    public function setData(?BaseData $data): BaseField
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param bool $exportable
     *
     * @return BaseField
     */
    public function setExportable(bool $exportable): BaseField
    {
        $this->exportable = $exportable;

        return $this;
    }

    /**
     * Campo pode ser filtrado ou não (Datatable)
     *
     * @param bool $filterable
     *
     * @return BaseField
     */
    public function setFilterable(bool $filterable): BaseField
    {
        $this->filterable = $filterable;

        return $this;
    }

    /**
     * Campo pode ser ordenado ou não (Datatable)
     *
     * @param bool $orderable
     *
     * @return BaseField
     */
    public function setOrderable(bool $orderable): BaseField
    {
        $this->orderable = $orderable;

        return $this;
    }

    /**
     * Define um custom render para a coluna na tabela (Datatable)
     *
     * Ex:
     * Para Classe, ver CheckboxColumnRender::class
     * Para function, setColumnRender(function($field, $fieldDefinition, $data, $routeAlias) { return  'foo_bar'; } )
     *
     * @param string|Closure $columnRender Nome da classe que implementa o BaseColumnRender ou function($field, $fieldDefinition, $data, $routeAlias)
     *
     * @return BaseField
     */
    public function setColumnRender($columnRender, $columnRenderMethods = null): BaseField
    {
        $this->columnRender = $columnRender;
        $this->columnRenderMethods = $columnRenderMethods;

        return $this;
    }

    /**
     * Define que o campo é primarKey
     *
     * @param bool $primaryKey
     *
     * @return BaseField
     */
    public function setPrimaryKey(bool $primaryKey): BaseField
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }

    /**
     * Define parametros que podem ser utilizados dentro do defaultRender
     *
     * @param BaseDefaultRenderParams $defaultRenderParams
     *
     * @return BaseField
     */
    public function setDefaultRenderParams(BaseDefaultRenderParams $defaultRenderParams): BaseField
    {
        $this->defaultRenderParams = $defaultRenderParams;

        return $this;
    }

    /**
     * Define uma function p/ ser chamada no momento de retornar o valor para a coluna do DataTable
     *
     * Ex: ->setValueRender(function($data) { return $data->field; });
     *
     * @param Closure $valueRender
     *
     * @return BaseField
     */
    public function setValueRender(Closure $valueRender): BaseField
    {
        $this->valueRender = $valueRender;

        return $this;
    }

    /**
     * Define uma function p/ ser chamada no momento de renderizar o input no Form, ignorando o tipo do campo
     *
     * Ex: ->setRenderInput(function() { return 'foo_bar'; });
     *
     * @param Closure $renderInput
     *
     * @return BaseField
     */
    public function setRenderInput(Closure $renderInput): BaseField
    {
        $this->renderInput = $renderInput;

        return $this;
    }

    /**
     * Retorna o valor do campo com base do tipo do campo.
     *
     * Retorna "plain", formato mais simples possível p/ ser utilizado posteriormente
     *
     * IMPORTANTE: O VALOR FORMATADO É IGNORADO NA BUSCA, p/ resolver isso deve ser formatado a nível de Query
     *
     * @param object|array|Model $row
     * @param bool $resolveVirtualRawSql
     *
     * @return [type]
     */
    public function getValue($row)
    {
        //Chama fn do valueRender
        if (@$this->valueRender) {
            $method = $this->valueRender;
            return $method($row);
        }

        $value = \Arr::get($row, $this->field);

        //Relationship
        if (!$row instanceof Model && !$value) {
            $value = @$row->{$this->field};
            if (!$value && strpos($this->field, '.') !== false) {
                $value = @$row->{\Str::snake($this->field)};
            }
        }

        //Boolean
        if ($this->type == BaseColumnConst::Boolean) {
            return @$value && ($value == 'true' || $value === true || $value == '1' || $value == 1);
        }

        //Data do tipo "virtual" (raw sql)
        if ($this->data && $this->data instanceof BaseDataVirtualRawSql) {
            return $value;
        }

        if (@$this->data) {
            //Já foi resolvido o dado através da chamada \Arr::get($row, $field).
            if (strpos($this->field, '.') !== false) {
                return $value;
            }

            $value = @$row->{$this->field};
            return $this->data->getValue($value, $row);
        }

        //Arquivo / Imagem
        if (
            ($this->type == BaseColumnConst::Image ||
                $this->type == BaseColumnConst::File ||
                $this->type == BaseColumnConst::Video) &&
            @$value &&
            @$this->uploadDefinition
        ) {
            if (filter_var($value, FILTER_VALIDATE_URL) !== false) {
                return $value;
            }

            $value = $this->uploadDefinition->saveWithFolder
                ? \Storage::url($value)
                : \Storage::url($this->uploadDefinition->path . '/' . $value);

            return $value;
        }

        if (@is_null($value)) {
            return null;
        }

        //Default
        return $value;
    }

    /**
     * Futuramente pode ser implementado um custom render para o exportable.
     *
     * @param Model $row
     *
     * @return mixed
     */
    public function getExportableValue($row)
    {
        $value = $this->getValue($row);

        if (@$this->exportRender) {
            $method = $this->exportRender;
            return $method($row, $value);
        }

        if (@is_null($value)) {
            return null;
        }

        if ($this->type == BaseColumnConst::Date && $this->input == BaseInputConst::AireDateTime) {
            return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y H:i');
        } elseif ($this->type == BaseColumnConst::Date && $this->input == BaseInputConst::AireTime) {
            return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'H:i');
        } elseif ($this->type == BaseColumnConst::Date) {
            return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y');
        }

        return $value;
    }

    /**
     * Define uma function p/ ser chamada no momento de retornar o valor para o Export
     *
     * Ex: ->setExportRender(function($data, $value) { return $data->field. ' - '.$value; });
     *
     * @param Closure $exportRender
     *
     * @return BaseField
     *
     */
    public function setExportRender(Closure $exportRender): BaseField
    {
        $this->exportRender = $exportRender;

        return $this;
    }

    /**
     * @param array[string]string $preset
     *
     * @return BaseField
     */
    public function setPreset(array $preset): BaseField
    {
        $this->preset = $preset;

        return $this;
    }
}
