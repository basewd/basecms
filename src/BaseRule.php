<?php

namespace BaseCms;

/**
 * Arquivo para setar uma "rule" no "field" do BaseDefinition
 *
 * Ex: BaseField::newObj('cpf', 'CPF com validação')
 *  ->setRule(new BaseRule('required|cpf')),
 *
 * P/ criar validações custom: "Validator::extendImplicit", exemplo no arquivo "BaseServiceProviders"
 *
 * Referência: https://laravel.com/docs/8.x/validation#form-request-validation
 */
class BaseRule
{
    /**
     * @var string
     */
    public $rule;

    /**
     * @var string
     */
    public $messages;

    /**
     * @param string $rule
     * @param array[string]string $messages
     */
    public function __construct($rule = 'required', $messages = ['required' => 'Campo obrigatório'])
    {
        $this->rule = $rule;
        $this->messages = $messages;
    }
}
