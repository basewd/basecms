<?php

namespace BaseCms;

use BaseCms\Console\BaseGeneratorsServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class BaseServiceProviders extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(BaseGeneratorsServiceProvider::class);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

        $this->loadViewsFrom(__DIR__ . '/../views', 'base');

        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('resources/views/vendor/base'),
            ],
            'basecms-resources',
        );

        $this->publishes(
            [
                __DIR__ . '/../public' => base_path('public'),
            ],
            'basecms-public',
        );

        $this->bootValidator();
        $this->bootAire();
        $this->bootDataTables();

        $this->backwardBackendPageListCreateUpdate();
    }

    private function backwardBackendPageListCreateUpdate()
    {
        if (app()->isLocal() && !file_exists(resource_path('views/components/backend/pages/create-update.blade.php'))) {
            \Artisan::call('base:backward-backend-page-list-create-update');
        }
    }

    /**
     * Validação
     *  - CPF
     *  - CNPJ
     */
    private function bootValidator()
    {
        Validator::extendImplicit(
            'cpf',
            'BaseCms\Helpers\BaseValidatorHelper@isValidCpf',
            'O campo :attribute apresenta um CPF inválido.',
        );
        Validator::extendImplicit(
            'cnpj',
            'BaseCms\Helpers\BaseValidatorHelper@isValidCnpj',
            'O campo :attribute apresenta um CNPJ inválido.',
        );
    }

    /**
     * Aire
     *  - alias para placeholder
     *  - alias para mascara de input
     *  - adicionar form-group-@ID@ na <div pai dos componentes Aire
     */
    private function bootAire()
    {
        \Galahad\Aire\DTD\Select::macro('readOnly', function (string $value) {
            $this->setAttribute('readonly', $value);
            return $this;
        });

        \Galahad\Aire\DTD\Select::macro('placeholder', function (string $value) {
            $this->prependEmptyOption($value, '');
            return $this;
        });

        \Galahad\Aire\Elements\Input::macro('mask', function (string $mask) {
            $this->data('inputmask-mask', $mask);
            return $this;
        });

        \Galahad\Aire\Elements\Checkbox::registerElementMutator(function (\Galahad\Aire\Elements\Checkbox $element) {
            $element->attributes->registerMutator('name', function ($name) use ($element) {
                $element->group->addClass('form-group-' . $name);
                return $name;
            });
        });

        \Galahad\Aire\Elements\Select::registerElementMutator(function (\Galahad\Aire\Elements\Select $element) {
            $element->attributes->registerMutator('name', function ($name) use ($element) {
                $element->group->addClass('form-group-' . $name);
                return $name;
            });
        });

        \Galahad\Aire\Elements\Textarea::registerElementMutator(function (\Galahad\Aire\Elements\Textarea $element) {
            $element->attributes->registerMutator('name', function ($name) use ($element) {
                $element->group->addClass('form-group-' . $name);
                return $name;
            });
        });

        \Galahad\Aire\Elements\Input::registerElementMutator(function (\Galahad\Aire\Elements\Input $input) {
            $input->attributes->registerMutator('name', function ($name) use ($input) {
                $input->group->addClass('form-group-' . $name);
                return $name;
            });
        });

        \Galahad\Aire\Elements\Wysiwyg::registerElementMutator(function (\Galahad\Aire\Elements\Wysiwyg $wysiwyg) {
            $wysiwyg->attributes->registerMutator('name', function ($name) use ($wysiwyg) {
                $wysiwyg->group->addClass('form-group-' . $name);
                return $name;
            });
        });
    }

    /**
     * Boot DataTables
     *  - Injeta algumas funções para funcionar o Export e o Filtro Not Like
     */
    private function bootDataTables()
    {
        /**
         * @return array
         */
        \Yajra\DataTables\DataTableAbstract::macro('getColumnDef', function () {
            return $this->columnDef;
        });

        /**
         * @return DataTableAbstract
         */
        \Yajra\DataTables\QueryDataTable::macro('prepareExportQuery', function (
            \BaseCms\BaseDefinition $definition,
            array $ignoreFields = []
        ) {
            \BaseCms\Datatables\Helpers\BaseDatatableHelper::baseRender($this, $definition, $ignoreFields);
            $this->filterRecords();
            $this->ordering();
        });

        /**
         * @return DataTableAbstract
         */
        \Yajra\DataTables\EloquentDataTable::macro('baseRender', function (
            \BaseCms\BaseDefinition $definition,
            array $ignoreFields = []
        ) {
            \BaseCms\Datatables\Helpers\BaseDatatableHelper::baseRender($this, $definition, $ignoreFields);
            return $this;
        });

        /**
         * @return DataTableAbstract
         */
        \Yajra\DataTables\EloquentDataTable::macro('baseFilter', function (\BaseCms\BaseDefinition $definition) {
            \BaseCms\Datatables\Helpers\BaseDatatableHelper::baseFilter($this, $definition);
            return $this;
        });

        /**
         * Fix para aceitar "NOT LIKE" através de Macros
         */
        \Yajra\DataTables\EloquentDataTable::macro('publicCompileQuerySearch', function (
            $query,
            $columnName,
            $keyword,
            $boolean = 'or',
            $like = 'LIKE'
        ) {
            $parts = explode('.', $columnName);
            $column = array_pop($parts);
            $relation = implode('.', $parts);

            if ($this->isNotEagerLoaded($relation)) {
                return parent::compileQuerySearchWithNotLike($query, $columnName, $keyword, $boolean, $like);
            }

            $query->{$boolean . 'WhereHas'}($relation, function ($query) use ($column, $keyword, $like) {
                parent::compileQuerySearchWithNotLike($query, $column, $keyword, '', $like);
            });
        });

        \Yajra\DataTables\QueryDataTable::macro('compileQuerySearchWithNotLike', function (
            $query,
            $column,
            $keyword,
            $boolean = 'or',
            $like = 'LIKE'
        ) {
            $column = $this->addTablePrefix($query, $column);
            $column = $this->castColumn($column);
            $sql = $column . " {$like} ?";

            if ($this->config->isCaseInsensitive()) {
                $sql = "LOWER({$column}) {$like} ?";
            }

            $query->{$boolean . 'WhereRaw'}($sql, [$this->prepareKeyword($keyword)]);
        });

        \Yajra\DataTables\QueryDataTable::macro('publicApplyFilterColumn', function (
            $query,
            $columnName,
            $keyword,
            $boolean = 'and'
        ) {
            $this->applyFilterColumn($query, $columnName, $keyword, $boolean);
        });
    }
}
