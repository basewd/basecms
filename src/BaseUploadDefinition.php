<?php

namespace BaseCms;

use Closure;

/**
 * Arquivo de definição p/ suporte de upload de arquivos e resize das imagens.
 *
 * Ele deve ser utilizado junto com o BaseDefinition, ao adicionar um "field" novo.
 *
 * Ex:
 *
 *  BaseField::newObj('image', 'Image', BaseInputConst::AireFile, BaseColumnConst::Image)
 *      ->setUploadDefinition(new BaseUploadDefinition('samples_image', 300, 100, true, 'top')),
 */
class BaseUploadDefinition
{
    /**
     * Caminho do arquivo
     *
     * @var string
     */
    public $path;

    /**
     * width - largura da imagem
     * @var int
     */
    public $maxWidth;

    /**
     * height - altura da imagem
     * @var int
     */
    public $maxHeight;

    /**
     * Imagem ocupa toda a área definida no "maxWidth/maxHeight", similar ao método "cover" do background-size;
     *
     * @var bool
     */
    public $cover;

    /**
     * Alinhamento quando é aplicado o resize na imagem
     *
     * Tipos válidos: top-left, top, top-right, left, center (default), right, bottom-left, bottom, bottom-right
     * @var string
     */
    public $position;

    /**
     * Define se apenas imagens com a altura/largura maior que a definida é aplicada o "resize".
     *
     * @var bool
     */
    public $onlyLargeImage;

    /**
     * Dados p/ aplicar watermark na imagem. Contém o path da image, posição, offsetX e offsetY
     *
     * @var array
     */
    public $watermark;

    /**
     * Determina se vai salvar no banco = @folder@/@nome_do_arquivo@
     *
     * @var array
     */
    public $saveWithFolder;

    /**
     * Recebe um callback p/ manipular o arquivo
     *
     * @var Closure
     */
    public $onAfterResize;

    /**
     * Determina a função que vai resolver o nome do arquivo.
     *
     * ->setResolverFilename(function($model, $id, $extension) { }) {
     *      return $data->title.'-'.$id.'-'.$extension;
     * });
     *
     * @var Closure
     */
    public $resolverFilename;

    /**
     * Encode (https://image.intervention.io/v2/api/encode)
     * @var string
     */
    public $encode;

    /**
     * Encode Quality
     * @var int
     */
    public $encodeQuality = 90;

    /**
     * Resizes Multiplos (2x,3x)
     * @var BaseUploadDefinition[]
     */
    public $multipleResizes;

    /**
     * Resize nas imagens para ser utilizado pela tag <img através da função srcset();
     *
     * Exemplo de uso: <img src="{{storage_url($model->image)}}" srcset="{{srcset($model->image_srcset)}}" loading="lazy" />
     *
     * Importante adicionar loading="lazy", p/ garantir apenas um carregamento
     *
     * Ao utilizar essa propriedade, os dados serão salvos no $field.'_srcset'.
     *
     * Deve ser criado na migration este campo.
     *
     * Exemplo na migration:
     *
     * $table->string("image")->nullable();
     * $table->text("image_srcset")->nullable();
     *
     * --
     *
     * Para utilizar local, através do `php artisan serve`, rodar o server na porta 8000 e 8001. O artisan não aceita concorrência, se estiver QUEUE_CONNECTION=sync, a imagem na porta 8000 não pode ser acessada.
     *
     * @var int[]
     */
    public $srcSet;

    /**
     * Gera nome do arquivo com o nome do "valor" do atributo do model (coluna)
     *
     * @var string
     */
    public $seoFriendlyColumnName;

    /**
     * @param string $path
     * @param int|null $maxWidth
     * @param int|null $maxHeight
     * @param bool $cover
     * @param string $position
     * @param bool $onlyLargeImage
     * @param string $encode
     */
    public function __construct(
        string $path,
        int $maxWidth = null,
        int $maxHeight = null,
        bool $cover = true,
        string $position = 'center',
        bool $onlyLargeImage = false,
        string $encode = null
    ) {
        $this->path = config('base.upload.base_path')
            ? trim(config('base.upload.base_path'), '/') . '/' . $path
            : $path;
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;

        $this->cover = $cover;
        $this->position = $position;

        $this->onlyLargeImage = $onlyLargeImage;

        $this->saveWithFolder = config('base.upload.save_with_folder', false);

        $this->encode = $encode;
    }

    /**
     * Cria um objeto do tipo BaseUploadDefinition p/ usuário conseguir encadear chamadas
     *
     * @param string $path
     * @param int|null $maxWidth
     * @param int|null $maxHeight
     * @param bool $cover
     * @param string $position
     * @param bool $onlyLargeImage
     * @param string $encode
     *
     * @return BaseUploadDefinition
     */
    public static function newObj(
        string $path,
        int $maxWidth = null,
        int $maxHeight = null,
        bool $cover = true,
        string $position = 'center',
        bool $onlyLargeImage = false,
        string $encode = null
    ): BaseUploadDefinition {
        return new BaseUploadDefinition($path, $maxWidth, $maxHeight, $cover, $position, $onlyLargeImage, $encode);
    }

    /**
     * Adiciona um "watermark" na imagem.
     *
     * Referência: http://image.intervention.io/api/insert
     *
     * @param string $imagePath
     * @param string $position
     * @param int $offsetX
     * @param int $offsetY
     *
     * @return BaseUploadDefinition
     */
    public function setWatermark(string $imagePath, string $position = 'center', int $offsetX = 0, int $offsetY = 0)
    {
        $this->watermark = [
            'imagePath' => $imagePath,
            'position' => $position,
            'offsetX' => $offsetX,
            'offsetY' => $offsetY,
        ];

        return $this;
    }

    /**
     * Determina se vai ser salvo (ou não) o path junto com o nome do arquivo
     *
     * @param bool $saveWithFolder
     *
     * @return BaseUploadDefinition
     */
    public function setSaveWithFolder(bool $saveWithFolder = false)
    {
        $this->saveWithFolder = $saveWithFolder;

        return $this;
    }

    /**
     * Determina a função que vai resolver o nome do arquivo
     *
     * @param Closure $resolverFilename
     *
     * @return BaseUploadDefinition
     */
    public function setResolverFilename(Closure $resolverFilename)
    {
        $this->resolverFilename = $resolverFilename;

        return $this;
    }

    /**
     * Permite manipular o arquivo após o resize da imagem
     *
     * @var Closure
     */
    public function afterResize(Closure $function)
    {
        $this->onAfterResize = $function;
        return $this;
    }

    /**
     * Determina o encode do arquivo salvo
     *
     * @param string $encode
     * @param number $quality
     *
     * @return BaseUploadDefinition
     */
    public function setEncode(string $encode, int $quality = 90)
    {
        $this->encode = $encode;
        $this->encodeQuality = $quality;

        return $this;
    }

    /**
     *
     * @param string $quality
     *
     * @return BaseUploadDefinition
     */
    public function setEncodeQuality(int $quality = 90)
    {
        $this->encodeQuality = $quality;

        return $this;
    }

    /**
     * Aplicação de N resizes na imagem principal. Método utilizado p/ criar resizes 2x, 3x, 4x ...
     *
     * Será salvo no banco um JSON com os paths de cada resize.
     *
     * Nome do campo: $field.'_resize' (Deve ser criado esse campo)
     *
     * Ex: thumb, thumb_resize
     *
     *  BaseUploadDefinition::newObj('clients/logo_dark')
     *                   ->setSaveWithFolder(true)
     *                   ->setEncode('webp')
     *                   ->setMultipleResizes([
     *                       BaseUploadDefinition::newObj('clients/logo_dark/2x', 200, 200),
     *                       BaseUploadDefinition::newObj('clients/logo_dark/3x', 400, 400),
     *                       BaseUploadDefinition::newObj('clients/logo_dark/4x', 800, 800),
     *                   ]),
     *
     * @param BaseUploadDefinition[] $multipleResizes
     *
     * @return BaseUploadDefinition
     */
    public function setMultipleResizes(array $multipleResizes)
    {
        $this->multipleResizes = $multipleResizes;

        return $this;
    }

    /**
     *  BaseUploadDefinition::newObj('clients/logo_dark')
     *                   ->withSrcSet(),
     *
     * @param array[] $srcSet
     *
     * @return BaseUploadDefinition
     */
    public function withSrcSet(array $srcSet = [1920, 1600, 1300])
    {
        $this->srcSet = $srcSet;

        return $this;
    }

    /**
     *  BaseUploadDefinition::newObj('clients/logo_dark')
     *                   ->seoFriendly('title'),
     *
     * @param array[] $srcSet
     *
     * @return BaseUploadDefinition
     */
    public function seoFriendly($columnName)
    {
        $this->seoFriendlyColumnName = $columnName;

        return $this;
    }

    /**
     * Permite otimizar automaticamente as imagens
     *
     * https://github.com/spatie/image-optimizer
     *
     * @var Closure
     */
    public function optimize()
    {
        return $this->afterResize(function ($file) {
            \Spatie\LaravelImageOptimizer\OptimizerChainFactory::create(config('image-optimizer'))->optimize($file);
        });
    }
}
