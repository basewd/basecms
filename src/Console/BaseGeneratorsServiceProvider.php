<?php

namespace BaseCms\Console;

use BaseCms\Console\Commands\ControllerBaseCommand;
use BaseCms\Console\Commands\ControllerReportBaseCommand;
use BaseCms\Console\Commands\DefinitionBaseCommand;
use BaseCms\Console\Commands\DefinitionReportBaseCommand;
use BaseCms\Console\Commands\DomainBaseCommand;
use BaseCms\Console\Commands\DomainReportBaseCommand;
use BaseCms\Console\Commands\EnumBaseCommand;
use BaseCms\Console\Commands\ModelBaseCommand;
use BaseCms\Console\Commands\PolicyBaseCommand;
use BaseCms\Console\Commands\RepositoryBaseCommand;
use BaseCms\Console\Commands\RequestBaseCommand;
use BaseCms\Console\Commands\RequestUpdateBaseCommand;
use BaseCms\Console\Commands\SubControllerCommand;
use BaseCms\Console\Commands\CreateBackwardBackendPageListCreateUpdate;
use BaseCms\Console\Commands\DatabaseFactoryCommand;
use Illuminate\Support\ServiceProvider;

class BaseGeneratorsServiceProvider extends ServiceProvider
{
    protected $commands = [
        ModelBaseCommand::class,
        ControllerBaseCommand::class,
        SubControllerCommand::class,
        RepositoryBaseCommand::class,
        PolicyBaseCommand::class,
        RequestBaseCommand::class,
        RequestUpdateBaseCommand::class,
        DefinitionBaseCommand::class,
        DomainBaseCommand::class,
        EnumBaseCommand::class,
        DefinitionReportBaseCommand::class,
        ControllerReportBaseCommand::class,
        DomainReportBaseCommand::class,
        CreateBackwardBackendPageListCreateUpdate::class,
        DatabaseFactoryCommand::class,

        \BaseCms\BaseBlockBuilder\Console\Command\BlockBuilderMakeComponent::class,
        \BaseCms\BaseBlockBuilder\Console\Command\BlockBuilderSeed::class,
        \BaseCms\BaseBlockBuilder\Console\Command\BlockBuilderMakePage::class,
    ];

    /**
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
