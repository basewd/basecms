<?php

namespace BaseCms\Console\Commands;

class ControllerBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:controller';

    protected $signature = 'base:controller {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} {--lang : Translate}';

    protected $description = 'Gerar Controller junto com os resources. O controller contém listagem, api da listagem, create, store, edit, update, delete, orderUp, orderDown, orderStart, orderEnd';

    protected $type = 'Controller';

    protected $path = '/Http/Controllers/';

    protected $endFile = 'Controller.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/controller.stub';
    }

    protected function getStubOrder()
    {
        return __DIR__ . '/../stubs/controller/order/order.stub';
    }

    protected function getStubCreateOrUpdateBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_create_or_update.blade.stub';
    }

    protected function getStubIndexBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_index.blade.stub';
    }

    protected function getStubIndexRestoreBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_index_restore.blade.stub';
    }

    protected function getStubIndexRestoreSnippet()
    {
        return __DIR__ . '/../stubs/controller/restore/index_restore.stub';
    }

    protected function getStubViewBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_view.blade.stub';
    }

    protected function getStubRestore()
    {
        return __DIR__ . '/../stubs/controller/restore/restore.stub';
    }

    private function getRouteServiceProviderPhp()
    {
        return $this->laravel['path'] . '/Providers/RouteServiceProvider.php';
    }

    private function getStubLangCreateOrUpdateBlade()
    {
        return __DIR__ . '/../stubs/controller/lang/resource_create_or_update_lang.blade.stub';
    }

    private function addSoftDeleteServiceProvider()
    {
        $content = file_get_contents($this->getRouteServiceProviderPhp());

        $class = '\App\Domains\\' . $this->getNameInput() . '\Models\\' . $this->getNameInput() . 'Model';

        $str =
            'Route::bind("' .
            $this->getCamelName() .
            'Deleted", function ($id) {
            return ' .
            $class .
            '::onlyTrashed()->find($id);
        });';

        $searchRouteProvider = '/* artisan:base:domain */';
        $str .= "\n\t\t" . $searchRouteProvider;

        //Só adiciona caso não exista
        if (strpos($content, $class) === false) {
            $content = str_replace($searchRouteProvider, $str, $content);
            file_put_contents($this->getRouteServiceProviderPhp(), $content);
        }

        return true;
    }

    protected function replaceOrderStubs(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        if (array_search('order', $attributes) !== false) {
            $stubOrder = file_get_contents($this->getStubOrder());
            $stubOrder = str_replace('DummyModel', $this->getNameInput() . 'Model', $stubOrder);

            foreach ($this->injectParams as $k => $v) {
                $stubOrder = str_replace($k, $v, $stubOrder);
            }

            $stub = str_replace('StubOrderDummy', $stubOrder, $stub);
            $stub = str_replace(
                'StubDatatableOrderDummy',
                ",'orderUpUrl', 'orderDownUrl', 'orderStartUrl', 'orderEndUrl', 'orderToUrl'",
                $stub,
            );
            $stub = str_replace('//$order', '$order', $stub);
        } else {
            $stub = str_replace('StubOrderDummy', '', $stub);
            $stub = str_replace('StubDatatableOrderDummy', '', $stub);
        }

        $stub = str_replace('$model', '$' . $this->getCamelName(), $stub);
        $stub = str_replace("'model'", "'" . $this->getCamelName() . "'", $stub);

        return $this;
    }

    protected function replaceSoftDeleteStubs(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        if (array_search('deleted_at', $attributes) !== false) {
            $stubRestore = file_get_contents($this->getStubRestore());
            $stubRestore = str_replace('DummyModel', $this->getNameInput() . 'Model', $stubRestore);

            foreach ($this->injectParams as $k => $v) {
                $stubRestore = str_replace($k, $v, $stubRestore);
            }

            $stub = str_replace('StubRestoreDummy', $stubRestore, $stub);
            $stub = str_replace('dummy_route', $this->argument('name'), $stub);
            $stub = str_replace('dummy_table', $this->argument('name'), $stub);
        } else {
            $stub = str_replace('StubRestoreDummy', '', $stub);
        }

        return $this;
    }

    protected function replaceRawColumns(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        $rawColumns = [];
        foreach ($attributes as $k => $v) {
            if (strpos($v, 'file') !== false || strpos($v, 'image') !== false) {
                $rawColumns[] = $v;
            }
        }

        if (count($rawColumns) > 0) {
            $stub = str_replace('DummyRawColumns', '"' . join("\",\"", $rawColumns) . '"', $stub);
        } else {
            $stub = str_replace('DummyRawColumns', '', $stub);
        }

        return $this;
    }

    private function makeResources()
    {
        $pathResources = $this->laravel['path'] . '/../resources/views/backend/' . $this->argument('name') . '/';

        //Se já existe, skip
        if (is_file($pathResources . 'index.blade.php')) {
            return $this;
        }

        @mkdir($pathResources, 0755, true);

        $attributes = $this->getAttributesModel();

        $indexContent = file_get_contents($this->getStubIndexBlade());
        $this->replaceNameStrings($indexContent);
        if (array_search('deleted_at', $attributes) !== false) {
            $indexRestoreSnippet = file_get_contents($this->getStubIndexRestoreSnippet());
            $indexContent = str_replace('@INDEX_RESTORE@', $indexRestoreSnippet, $indexContent);
        } else {
            $indexContent = str_replace('@INDEX_RESTORE@', '', $indexContent);
        }
        file_put_contents($pathResources . 'index.blade.php', $indexContent);

        $viewContent = file_get_contents($this->getStubViewBlade());
        $this->replaceNameStrings($viewContent);
        file_put_contents($pathResources . 'view.blade.php', $viewContent);

        $createUpdateContent = file_get_contents($this->getStubCreateOrUpdateBlade());
        $this->replaceNameStrings($createUpdateContent);
        file_put_contents($pathResources . 'create_update.blade.php', $createUpdateContent);

        if (array_search('deleted_at', $attributes) !== false) {
            $indexRestoreContent = file_get_contents($this->getStubIndexRestoreBlade());
            $this->replaceNameStrings($indexRestoreContent);
            file_put_contents($pathResources . 'index_restore.blade.php', $indexRestoreContent);
        }

        return $this;
    }

    private function updateResounceLang()
    {
        if ($this->option('lang')) {
            $name = str_replace('_translations', '', $this->argument('name'));

            $pathResources = $this->laravel['path'] . '/../resources/views/backend/' . $name . '/';

            if (is_dir($pathResources)) {
                $contentCreateOrUpdate = file_get_contents($this->getStubLangCreateOrUpdateBlade());

                $pathCreateOrUpdate = $pathResources . 'create_update.blade.php';

                $content = file_get_contents($pathCreateOrUpdate);
                if (strpos($content, 'x-backend.nav-lang') === false) {
                    $contentCreateOrUpdate = str_replace(
                        '@MODEL@',
                        "@$" . \Str::singular($name),
                        $contentCreateOrUpdate,
                    );
                    $contentCreateOrUpdate = str_replace(
                        '@ROUTE_TRANSLATION@',
                        $this->argument('name'),
                        $contentCreateOrUpdate,
                    );
                    $contentCreateOrUpdate = str_replace(
                        '@MODEL_TRANSLATION@',
                        $this->getCamelName(),
                        $contentCreateOrUpdate,
                    );

                    $content = str_replace('<x-slot name="footerBottom"></x-slot>', $contentCreateOrUpdate, $content);

                    $formLangEmpty =
                        '@if (!@$' .
                        \Str::singular($name) .
                        ')
                    <x-backend.form-lang-empty />
                @endif
                
                {!! Aire::close() !!}';

                    $content = str_replace('{!! Aire::close() !!}', $formLangEmpty, $content);

                    file_put_contents($pathCreateOrUpdate, $content);
                }
            }
        }

        return $this;
    }

    private function updateControllerLang()
    {
        if ($this->option('lang') && $this->argument('fkName')) {
            $name = str_replace('_translations', '', $this->argument('name'));
        
            $normalizeName = \Str::singular(\Str::ucfirst(\Str::camel($name)));
            $modelName = $normalizeName . 'Controller';

            $pathController =
            public_path() .
            DIRECTORY_SEPARATOR .
            '..' .
            DIRECTORY_SEPARATOR .
            'app' .
            DIRECTORY_SEPARATOR .
            'Domains' .
            DIRECTORY_SEPARATOR .
            $normalizeName .
            DIRECTORY_SEPARATOR .
            'Http' .
            DIRECTORY_SEPARATOR .
            'Controllers' .
            DIRECTORY_SEPARATOR .
            $modelName .
            '.php';

            $content = file_get_contents($pathController);
           
            if (strpos($content, "->with('lang')") === FALSE) {
                $search = $this->argument('fkName').".*')";

                $withLang = "->with('lang')->withTranslation()->distinct()";

                $content = str_replace($search, $search.''.$withLang, $content);

                file_put_contents($pathController, $content);
            }
        }
        
        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceSoftDeleteStubs($stub, $name)
            ->replaceOrderStubs($stub, $name)
            ->replaceRawColumns($stub, $name)
            ->makeResources()
            ->updateResounceLang()
            ->updateControllerLang();

        return $stub;
    }

    public function handle()
    {
        parent::handle();

        if (array_search('deleted_at', $this->getAttributesModel()) !== false) {
            $this->addSoftDeleteServiceProvider();
        }
    }
}
