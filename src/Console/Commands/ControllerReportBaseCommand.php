<?php

namespace BaseCms\Console\Commands;

class ControllerReportBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:report-controller';

    protected $signature = 'base:report-controller {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar Controller Report junto com os resources. O controller contém listagem, api da listagem';

    protected $type = 'Controller';

    protected $path = '/Http/Controllers/';

    protected $endFile = 'ReportController.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/controller_report.stub';
    }

    protected function getStubIndexBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_report_index.blade.stub';
    }

    private function makeResources()
    {
        $pathResources = $this->laravel['path'] . '/../resources/views/backend/' . $this->argument('name') . '_report/';

        //Se já existe, skip
        if (is_file($pathResources . 'index.blade.php')) {
            return $this;
        }

        @mkdir($pathResources, 0755, true);

        $indexContent = file_get_contents($this->getStubIndexBlade());
        $this->replaceNameStrings($indexContent);
        $indexContent = str_replace('@INDEX_RESTORE@', '', $indexContent);
        file_put_contents($pathResources . 'index.blade.php', $indexContent);

        return $this;
    }

    protected function replaceRawColumns(&$stub, $name)
    {
        $stub = str_replace('DummyRawColumns', '', $stub);
        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceRawColumns($stub, $name)->makeResources();

        return $stub;
    }

    public function handle()
    {
        parent::handle();
    }
}
