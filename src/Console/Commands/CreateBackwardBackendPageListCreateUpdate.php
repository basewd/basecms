<?php

namespace BaseCms\Console\Commands;

use Illuminate\Console\Command;

class CreateBackwardBackendPageListCreateUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:backward-backend-page-list-create-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria components defaults para list/view/create, para manter o gerador funcionando em projetos antigos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!file_exists(resource_path('views/components/backend/pages/create-update.blade.php'))) {
            @mkdir(resource_path('views/components/backend/pages'));

            \File::put(resource_path('views/components/backend/pages/create-update.blade.php'), '{{ $slot }}');
            \File::put(resource_path('views/components/backend/pages/list.blade.php'), '{{ $slot }}');
            \File::put(resource_path('views/components/backend/pages/view.blade.php'), '{{ $slot }}');
        }

        $this->info('BackwardBackendPageListCreateUpdate created successfully.');
    }
}
