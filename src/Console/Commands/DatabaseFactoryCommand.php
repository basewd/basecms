<?php

namespace BaseCms\Console\Commands;

use BaseCms\BaseField;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;
use BaseCms\Interfaces\BaseInput;
use Illuminate\Console\GeneratorCommand;

/**
 *
 */
class DatabaseFactoryCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:database:factory {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria uma Factory de um Model e gera o Seeder através do Definition gerado.';

    /**
     * @return string
     */
    protected function getStub()
    {
        return null;
    }

    /**
     * @return string
     */
    protected function getStubSeeder()
    {
        return __DIR__ . '/../stubs/database_seeder.stub';
    }

    /**
     * @return string
     */
    protected function getStubFactory()
    {
        return __DIR__ . '/../stubs/database_factory.stub';
    }

    /**
     * @return string
     */
    protected function getStubModelNewFactory()
    {
        return __DIR__ . '/../stubs/database_model_new_factory.stub';
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->argument('name');
        $force = $this->option('force');

        $className = \Str::singular(\Str::studly($name));

        $this->createFactory($className, $force);
        $this->addFactoryToModel($className, $force);
        $this->createSeeder($className, $force);

        $this->info(
            "\nAtualizar MANUALMENTE o arquivo: Database\Seeders\Dev\DevSeeder\n\n\$this->call(" .
                $className .
                'Seeder::class);',
        );
    }

    protected function createFactory($className, $force)
    {
        $ignoreFields = ['deleted_at'];

        $factoryContent = file_get_contents($this->getStubFactory());

        $factoryContent = str_replace('{{classname}}', $className, $factoryContent);

        $definitionPath = 'App\Domains\\' . $className . '\Definition\\' . $className . 'Definition';

        $strFakers = [];
        if (class_exists($definitionPath)) {
            $definitionInstance = call_user_func([$definitionPath, 'instance']);

            $fields = $definitionInstance->getFields();

            $modelClass = $definitionInstance->getModelClass();
            $fillable = app($modelClass)->getFillable();

            foreach ($fields as $field) {
                if (in_array($field->field, $ignoreFields)) {
                    continue;
                }

                if (array_search($field->field, $fillable) !== false) {
                    $faker = $this->getFakerType($field);
                    if (@$faker) {
                        $strFakers[] = $faker;
                    }
                }
            }

            $strFakers = join("\n", $strFakers);
            $factoryContent = str_replace('{{fields}}', $strFakers, $factoryContent);
        }

        $path = database_path('factories/' . $className . 'Factory.php');
        if (!$force && file_exists($path)) {
            $this->info('Factory not created, use --force to overwrite.');
            return;
        }

        file_put_contents($path, $factoryContent);

        $this->info('Factory created: ' . $path);
    }

    /**
     * getFakerType
     *
     * @param BaseField $baseField
     * @return string
     */
    private function getFakerType(BaseField $baseField)
    {
        $field = $baseField->field;
        $inputConst = $baseField->input;
        $columnConst = $baseField->type;

        $faker = null;

        //FieldName
        if ($field == 'id') {
            return null;
        } elseif ($field == 'order') {
            $faker = '++$order';
        } elseif ($field == 'lat') {
            $faker = '$this->faker->latitude()';
        } elseif ($field == 'lng' || $field == 'long') {
            $faker = '$this->faker->longitude()';
        } elseif ($field == 'created_at' || $field == 'updated_at') {
            $faker = '\Carbon\Carbon::now()';
        } elseif (strpos($field, 'url') !== false) {
            $faker = '$this->faker->url()';
        } elseif (strpos($field, 'target') !== false) {
            $faker = 'rand(0, 1) < 0.5 ? \'_self\' : \'_target\'';
        } elseif (strpos($field, 'year') !== false) {
            $faker = 'rand(1980, 2020)';
        }
        //Upload
        elseif ($baseField->uploadDefinition) {
            //BaseColumnConst::Image, BaseColumnConst::File
            if ($baseField->uploadDefinition->maxWidth || $baseField->uploadDefinition->maxHeight) {
                $faker =
                    'FakerHelper::getImageUrl(' .
                    $baseField->uploadDefinition->maxWidth .
                    ',' .
                    $baseField->uploadDefinition->maxHeight .
                    ')';
            } elseif ($columnConst === BaseColumnConst::Video) {
                $faker = 'FakerHelper::getVideoUrl()';
            } else {
                $faker = '$this->faker->url()';
            }
        }
        //Column
        elseif ($columnConst === BaseColumnConst::Number) {
            $faker = '$this->faker->randomNumber()';
        } elseif ($columnConst === BaseColumnConst::Boolean) {
            $faker = '$this->faker->boolean()';
        } elseif ($baseField->data || $inputConst == BaseInputConst::AireSelect) {
            //BaseInputConst::AireSelect
            $faker = '//criar relacionamento no seeder ou fixar um ID existente';
        }
        //Input
        elseif ($inputConst === BaseInputConst::Youtube) {
            $faker = '$this->faker->randomNumber()';
        } elseif ($inputConst == BaseInputConst::AireTime) {
            $faker = '$this->faker->time("H:i")';
        } elseif ($inputConst === BaseInputConst::AireDate) {
            $faker = '$this->faker->date()';
        } elseif ($inputConst === BaseInputConst::AireDateTime) {
            $faker = '$this->faker->dateTime()';
        } elseif ($inputConst === BaseInputConst::AireEmail) {
            $faker = '$this->faker->email()';
        } elseif ($inputConst === BaseInputConst::AirePassword) {
            $faker = '$this->faker->password()';
        } elseif ($inputConst == BaseInputConst::AireRichEditor) {
            $faker =
                '$this->faker->realText(10, 1) .\' <b>\' .$this->faker->realText(10, 1) .\'</b> \' .$this->faker->realText(100, 2)';
        } elseif ($inputConst == BaseInputConst::AireTextArea) {
            $faker = '$this->faker->realText(200)';
        } elseif ($inputConst == BaseInputConst::AireColor) {
            $faker = '$this->faker->hexColor()';
        } elseif ($inputConst == BaseInputConst::AireRange) {
            $faker = '$this->faker->randomElement([10, 12, 14, 16, 18, 20])';
        } elseif ($inputConst == BaseInputConst::AireRadioGroup) {
            $faker = '$this->faker->randomElement([1, 2])';
        } elseif ($inputConst == BaseInputConst::AireHidden) {
            $faker = '\'hidden_value\'';
        } elseif ($inputConst == BaseInputConst::AireInput) {
            $faker = '$this->faker->realText(14)';
        }

        $ignoreInput = [BaseInputConst::Separator, BaseInputConst::LatLng];
        if (in_array($inputConst, $ignoreInput)) {
            return null;
        }

        if (!$faker) {
            return null;
        }

        $comments = '';
        if (\Str::startsWith($faker, '//')) {
            $comments = '//';
        }

        return "            $comments'$field' => " . $faker . ',';
    }

    protected function addFactoryToModel($className, $force)
    {
        $contentFactory = file_get_contents($this->getStubModelNewFactory());

        $pathModel = app_path('Domains/' . $className . '/Models/' . $className . 'Model.php');

        $contentModel = file_get_contents($pathModel);

        if (strpos($contentModel, 'newFactory') === false) {
            $contentFactory = str_replace('{{classname}}', $className, $contentFactory);
            $contentModel = \Str::replaceLast('}', $contentFactory, $contentModel);
            file_put_contents($pathModel, $contentModel);
            $this->info('newFactory created: ' . $pathModel);
        } else {
            $this->info('newFactory exist: ' . $pathModel);
        }
    }

    protected function createSeeder($className, $force)
    {
        $seederContent = file_get_contents($this->getStubSeeder());

        $seederContent = str_replace('{{classname}}', $className, $seederContent);

        $path = database_path('seeders/Dev/' . $className . 'Seeder.php');
        if (!$force && file_exists($path)) {
            $this->info('Seeder not created, use --force to overwrite.');
            return;
        }

        file_put_contents($path, $seederContent);

        $this->info('Seeder created: ' . $path);
    }
}
