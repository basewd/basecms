<?php

namespace BaseCms\Console\Commands;

class DefinitionBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:definition';

    protected $signature = 'base:definition {name : Nome da tabela} {fkName? : Nome da tabela de relacionamento } {fkId? : Nome da coluna de relacionamento} {--skip-add : Desabilita a criação do router e do sidebar p/ o definition passado. Utilizado em subrotas. }  {--force : Sobrescreve arquivos já gerados} {--lang : Traducao}'; //fkName e fkId é passado pelo DomainBaseCommand.php p/ SubControllers

    protected $description = 'Gerar o arquivo de definição de uma tabela especifica.';

    protected $type = 'Definition';

    protected $path = '/Definition/';

    protected $endFile = 'Definition.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/definition.stub';
    }

    protected function getFieldStub($type)
    {
        return __DIR__ . '/../stubs/definition/' . $type . '_field.stub';
    }

    private function getFilepathSidebar()
    {
        return $this->laravel['path'] . '/../resources/views/backend/includes/sidebar.blade.php';
    }

    private function getFilepathRouteAdmin()
    {
        return $this->laravel['path'] . '/../routes/backend/admin.php';
    }

    private function getColumnsOrdinalPosition($tableName)
    {
        //return \Schema::getColumnListing($tableName);

        $query = "SHOW COLUMNS FROM $tableName";
        return collect(\DB::select($query))
            ->pluck('Field')
            ->toArray();
    }

    private function replaceSetField(&$stub, $name)
    {
        $table = $this->argument('name');
        $attributes = $this->getColumnsOrdinalPosition($table);

        $stubFields = [];

        //Remove os campos que são do tipo "relationship"
        $relationships = $this->getRelationshipsFields($this->getModelClass());
        $attributes = array_filter($attributes, function ($v) use ($relationships) {
            return array_search($v, $relationships) === false;
        });

        foreach ($attributes as $k => $v) {
            $type = \Schema::getColumnType($table, $v);

            //Parametros gerais
            $params = ['dummy_field_id' => $v, 'dummy_field_label' => $this->translateLabel($v)];

            //Not null
            $columnDoctrine = \DB::connection()->getDoctrineColumn($table, $v);

            $isNotNull = $columnDoctrine->getNotnull();

            if ($isNotNull) {
                if ($type == 'string') {
                    $params['dummy_not_null'] =
                        "\n\t\t\t\t->setRule(new BaseRule('required" .
                        ($columnDoctrine->getLength() > 0 ? '|max:' . $columnDoctrine->getLength() : '') .
                        "'))";
                } else {
                    $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('required'))";
                }
            } else {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('nullable'))";
            }

            //Custom fields
            if ($v == $this->getPrimaryKey()) {
                //Primary Key
                $type = 'primary_key';
            } elseif (strpos($v, 'email') !== false) {
                $type = 'string_email';
            } elseif (strpos($v, 'file') !== false) {
                $type = 'string_file';
            } elseif (strpos($v, 'video') !== false) {
                $type = 'string_video';
            } elseif (strpos($v, 'img') !== false || strpos($v, 'image') !== false || strpos($v, 'thumb') !== false) {
                $type = 'string_image';
            }

            $stubFields[] = $this->replaceFieldStub($type, $params);
        }

        $stubFields = join("\n", $stubFields);
        $this->replaceNameStrings($stubFields);

        $stub = str_replace('DummySetField', $stubFields, $stub);

        return $this;
    }

    private function replaceSetFieldRelationship(&$stub)
    {
        $table = $this->argument('name');

        $stubFields = [];

        $relationships = $this->getRelationships();

        foreach ($relationships as $k => $v) {
            //Ignora relacionamentos != BelongsTo
            if ($v['type'] != 'BelongsTo') {
                continue;
            }

            $id = $v['foreignKey'];
            $name = \Str::ucfirst(\Str::camel($id));

            $params = [
                'dummy_field_id' => $id,
                'dummy_field_label' => $this->translateLabel($name),
                'FkModelDummy' => '\\' . $v['relatedClass'],
                'column_name_dummy' => $v['relatedFirstColumnString'],
            ];

            //Not null
            $isNotNull = \DB::connection()
                ->getDoctrineColumn($table, $id)
                ->getNotnull();
            if ($isNotNull) {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('required'))";
            } else {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('nullable'))";
            }

            $stubFields[] = $this->replaceFieldStub('select', $params);
        }

        $stubFields = join("\n", $stubFields);
        $this->replaceNameStrings($stubFields);

        $stub = str_replace('RelationshipSetField', $stubFields, $stub);

        return $this;
    }

    private function replaceFieldStub($type, $params)
    {
        $stubField = file_get_contents($this->getFieldStub($type));
        foreach ($params as $k => $v) {
            $stubField = str_replace($k, $v, $stubField);
        }

        return $stubField;
    }

    private function replaceSetTable(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        $excludeParams = [@$this->argument('fkId')];
        foreach ($excludeParams as $k => $v) {
            if (@$v) {
                unset($attributes[array_search($v, $attributes)]);
            }
        }

        $stub = str_replace('DummySetTable', '"' . join("\",\n\t\t\t\"", $attributes) . '"', $stub);

        return $this;
    }

    private function replaceSetForm(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        $primaryKey = $this->getPrimaryKey();

        $excludeParams = [
            $primaryKey,
            'order',
            'ordem',
            'deleted_at',
            'created_at',
            'updated_at',
            @$this->argument('fkId'),
        ];

        foreach ($excludeParams as $k => $v) {
            if (@$v) {
                unset($attributes[array_search($v, $attributes)]);
            }
        }

        $stub = str_replace('DummySetForm', '"' . join("\",\n\t\t\t\"", $attributes) . '"', $stub);

        return $this;
    }

    private function addSidebar()
    {
        $file = $this->getFilepathSidebar();
        $search = '{{-- artisan:base:domain:makeSidebarNavDropdown --}}';

        $definition =
            'App\Domains\\' .
            $this->getNameInput() .
            '\Definition\\' .
            $this->getNameInput() .
            'Definition::instance()->';

        $replace = $search;

        //Comenta a linha do sidebar, porque não tem roda direta para ele
        if (@$this->argument('fkName')) {
            $replace .= "\n\n\t\t" . '{{-- ' . $definition . 'makeSidebarNavDropdown() --}}';
        } else {
            $replace .= "\n\n\t\t" . '{{ ' . $definition . 'makeSidebarNavDropdown() }}';
        }

        $this->replaceStrInFile($search, $replace, $definition, $file);

        return $this;
    }

    private function addRoute()
    {
        $file = $this->getFilepathRouteAdmin();
        $search = '/* artisan:base:domain:makeGroupRoutes */';

        $definition =
            'App\Domains\\' .
            $this->getNameInput() .
            '\Definition\\' .
            $this->getNameInput() .
            'Definition::instance()->';

        //Replaces
        $replace = $search;

        if (@$this->argument('fkName')) {
            //Subrota
            $replace .=
                "\n\n" .
                $definition .
                "makeGroupRoutes('" .
                $this->argument('fkName') .
                '/{parentModel}/' .
                $this->argument('name') .
                "','" .
                $this->getCamelName() .
                "');";
        } else {
            //Rota default
            $replace .= "\n\n" . $definition . "makeGroupRoutes(null, '" . $this->getCamelName() . "');";
        }

        $replace .= "\n" . $definition . 'makeBreadcrumbsRoutes();';

        $this->replaceStrInFile($search, $replace, $replace, $file);

        return $this;
    }

    private function replaceStrInFile($search, $replace, $testMatch, $file)
    {
        $content = file_get_contents($file);

        //Só adiciona caso não exista
        if (strpos($content, $testMatch) === false) {
            $content = str_replace($search, $replace, $content);
            file_put_contents($file, $content);
        }
    }

    private function replaceOrderable(&$stub, $name)
    {
        if (array_search('order', $this->getAttributesModel()) !== false) {
            $stub = str_replace(
                'DummyRouteOrderable',
                '$this->enableRouteOrderable();
            ',
                $stub,
            );
        } else {
            $stub = str_replace('DummyRouteOrderable', '', $stub);
        }

        return $this;
    }

    private function replaceRestore(&$stub, $name)
    {
        if (array_search('deleted_at', $this->getAttributesModel()) !== false) {
            $stub = str_replace(
                'DummyRouteRestore',
                '$this->enableRouteRestore();
            ',
                $stub,
            );
        } else {
            $stub = str_replace('DummyRouteRestore', '', $stub);
        }

        return $this;
    }

    private function replaceForceDelete(&$stub, $name)
    {
        if (array_search('deleted_at', $this->getAttributesModel()) !== false) {
            $stub = str_replace(
                'DummyRouteForceDelete',
                '$this->enableRouteForceDelete();
            ',
                $stub,
            );
        } else {
            $stub = str_replace('DummyRouteForceDelete', '', $stub);
        }

        return $this;
    }

    private function replaceLocaleReadonly(&$stub, $name)
    {
        if (array_search('locale', $this->getAttributesModel()) !== false) {
            $pattern = '/BaseField::newObj\(\\\'locale\\\',[^;]+setInputMethods\(\[[^;]/';

            $stub = preg_replace($pattern, '$0"readOnly", true],[', $stub);
        }

        return $this;
    }

    private function replaceRelationshipSubroute(&$stub, $name)
    {
        $fkId = $this->argument('fkId');

        if (array_search('locale', $this->getAttributesModel()) !== false) {
            $pattern = '/BaseField::newObj\(\\\'' . $fkId . '\\\',.*\)/';

            preg_match($pattern, $stub, $matches);

            if (count($matches) > 0) {
                $stub = str_replace(
                    $matches[0],
                    str_replace('BaseInputConst::AireSelect', 'BaseInputConst::AireInput', $matches[0]),
                    $stub,
                );
            }
        }

        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceSetField($stub, $name)
            ->replaceSetFieldRelationship($stub)
            ->replaceSetTable($stub, $name)
            ->replaceSetForm($stub, $name)
            ->replaceOrderable($stub, $name)
            ->replaceRestore($stub, $name)
            ->replaceForceDelete($stub, $name)
            ->replaceLocaleReadonly($stub, $name)
            ->replaceRelationshipSubroute($stub, $name);

        return $stub;
    }

    public function handle()
    {
        parent::handle();

        if (!$this->option('skip-add')) {
            $this->addSidebar()->addRoute();
        }
    }
}
