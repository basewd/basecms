<?php

namespace BaseCms\Console\Commands;

class DefinitionReportBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:report-definition';

    protected $signature = 'base:report-definition {name : Nome da tabela} {--skip-add : Desabilita a criação do router e do sidebar p/ o definition passado. Utilizado em subrotas. }  {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar o arquivo de definição de uma tabela especifica.';

    protected $type = 'Definition';

    protected $path = '/Definition/';

    protected $endFile = 'ReportDefinition.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/definition_report.stub';
    }

    protected function getFieldStub($type)
    {
        return __DIR__ . '/../stubs/definition/' . $type . '_field.stub';
    }

    private function getFilepathSidebar()
    {
        return $this->laravel['path'] . '/../resources/views/backend/includes/sidebar.blade.php';
    }

    private function getFilepathRouteAdmin()
    {
        return $this->laravel['path'] . '/../routes/backend/admin.php';
    }

    private function replaceSetField(&$stub, $name)
    {
        $table = $this->argument('name');
        $attributes = \Schema::getColumnListing($table);

        $stubFields = [];

        //Remove os campos que são do tipo "relationship"
        $relationships = $this->getRelationshipsFields($this->getModelClass());
        $attributes = array_filter($attributes, function ($v) use ($relationships) {
            return array_search($v, $relationships) === false;
        });

        foreach ($attributes as $k => $v) {
            $type = \Schema::getColumnType($table, $v);

            //Parametros gerais
            $params = ['dummy_field_id' => $v, 'dummy_field_label' => $this->translateLabel($v)];

            //Not null
            $isNotNull = \DB::connection()
                ->getDoctrineColumn($table, $v)
                ->getNotnull();
            if ($isNotNull) {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('required'))";
            } else {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('nullable'))";
            }

            //Custom fields
            if ($v == $this->getPrimaryKey()) {
                //Primary Key
                $type = 'primary_key';
            } elseif (strpos($v, 'email') !== false) {
                $type = 'string_email';
            } elseif (strpos($v, 'file') !== false) {
                $type = 'string_file';
            } elseif (strpos($v, 'video') !== false) {
                $type = 'string_video';
            } elseif (strpos($v, 'img') !== false || strpos($v, 'image') !== false || strpos($v, 'thumb') !== false) {
                $type = 'string_image';
            }

            $stubFields[] = $this->replaceFieldStub($type, $params);
        }

        $stubFields = join("\n", $stubFields);
        $this->replaceNameStrings($stubFields);

        $stub = str_replace('DummySetField', $stubFields, $stub);

        return $this;
    }

    private function replaceSetFieldRelationship(&$stub)
    {
        $table = $this->argument('name');

        $stubFields = [];

        $relationships = $this->getRelationships();

        foreach ($relationships as $k => $v) {
            //Ignora relacionamentos != BelongsTo
            if ($v['type'] != 'BelongsTo') {
                continue;
            }

            $id = $v['foreignKey'];
            $name = \Str::ucfirst(\Str::camel($id));

            $params = [
                'dummy_field_id' => $id,
                'dummy_field_label' => $this->translateLabel($name),
                'FkModelDummy' => '\\' . $v['relatedClass'],
                'column_name_dummy' => $v['relatedFirstColumnString'],
            ];

            //Not null
            $isNotNull = \DB::connection()
                ->getDoctrineColumn($table, $id)
                ->getNotnull();
            if ($isNotNull) {
                $params['dummy_not_null'] = "\n\t\t\t\t->setRule(new BaseRule('required'))";
            } else {
                $params['dummy_not_null'] = '';
            }

            $stubFields[] = $this->replaceFieldStub('select', $params);
        }

        $stubFields = join("\n", $stubFields);
        $this->replaceNameStrings($stubFields);

        $stub = str_replace('RelationshipSetField', $stubFields, $stub);

        return $this;
    }

    private function replaceFieldStub($type, $params)
    {
        $stubField = file_get_contents($this->getFieldStub($type));
        foreach ($params as $k => $v) {
            $stubField = str_replace($k, $v, $stubField);
        }

        return $stubField;
    }

    private function replaceSetTable(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        $stub = str_replace('DummySetTable', '"' . join("\",\n\t\t\t\"", $attributes) . '"', $stub);

        return $this;
    }

    private function addSidebar()
    {
        $file = $this->getFilepathSidebar();
        $search = '{{-- artisan:base:domain:makeSidebarNavDropdown --}}';

        $definition =
            'App\Domains\\' .
            $this->getNameInput() .
            '\Definition\\' .
            $this->getNameInput() .
            'ReportDefinition::instance()->';

        $name = $this->argument('name');

        $replace = $search;

        //Comenta a linha do sidebar, porque não tem roda direta para ele
        $replace .=
            "\n\n\t\t" . '{{ ' . $definition . 'makeSidebarNavCustomLink(route("admin.' . $name . '.indexReport")) }}';

        $this->replaceStrInFile($search, $replace, $definition, $file);

        return $this;
    }

    private function addRoute()
    {
        $file = $this->getFilepathRouteAdmin();
        $search = '/* artisan:base:domain:makeGroupRoutes */';

        //Replaces
        $name = $this->argument('name');
        $controller =
            'App\Domains\\' .
            $this->getNameInput() .
            '\\Http\\Controllers\\' .
            $this->getNameInput() .
            'ReportController::class';

        $replace = $search;
        $replace .= "\n\n";
        $replace .= "Route::group(['prefix' => '$name', 'as' => '$name.'], function () {";
        $replace .= "    Route::match(['post', 'get'], '/datatable_report', [$controller, 'datatable'])->name('datatable_report');";
        $replace .= "    Route::get('/report', [$controller, 'index'])->name('indexReport');";
        $replace .= "    Route::match(['get', 'post'], '/exportCsvReport', [$controller, 'exportCsv'])->name('exportCsvReport');";
        $replace .= '});';

        $this->replaceStrInFile($search, $replace, $replace, $file);

        return $this;
    }

    private function replaceStrInFile($search, $replace, $testMatch, $file)
    {
        $content = file_get_contents($file);

        //Só adiciona caso não exista
        if (strpos($content, $testMatch) === false) {
            $content = str_replace($search, $replace, $content);
            file_put_contents($file, $content);
        }
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceSetField($stub, $name)
            ->replaceSetFieldRelationship($stub)
            ->replaceSetTable($stub, $name);

        return $stub;
    }

    public function handle()
    {
        parent::handle();

        if (!$this->option('skip-add')) {
            $this->addSidebar()->addRoute();
        }
    }
}
