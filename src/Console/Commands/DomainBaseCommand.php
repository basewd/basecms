<?php

namespace BaseCms\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class DomainBaseCommand extends GeneratorCommand
{
    protected $name = '';

    //php artisan base:domain sample_authors --subroute samples sampleId
    protected $signature = 'base:domain {name : Nome da tabela} {--subroute : Caso seja uma subrota} {fkName? : Nome da tabela de relacionamento} {fkId? : Nome da coluna de relacionamento} {--force : Sobrescreve arquivos já gerados} {--lang : Domain para internacionalizar a tabela principal}';

    protected $description = 'Gerar o Domínio de uma tabela. (Definition, Controller, Model, Policy, Repository, resources, Request)';

    protected $type = 'Domain';

    public function getStub()
    {
        return null;
    }

    public function handle()
    {
        $name = $this->argument('name');

        if (!\Schema::hasTable($name)) {
            echo 'Não é possível gerar, a tabela "' . $name . '" não existe';
            die();
        }

        $params = [];
        if ($this->option('force')) {
            $params[] = '--force';
        }
        $params = join(' ', $params);

        $paramLang = $this->option('lang') || strpos($name, '_translations') !== FALSE ? ' --lang ' : '';

        $columns = \Schema::getColumnListing($name);
        foreach ($columns as $v) {
            $type = \Schema::getColumnType($name, $v);
            if ($type == 'enum') {
                \Artisan::call('base:enum ' . $name . ' ' . $v . ' ' . $params);
                echo \Artisan::output();
            }
        }

        \Artisan::call('base:model ' . $name . ' ' . $params . $paramLang);
        echo \Artisan::output();

        if ($this->option('subroute')) {
            \Artisan::call(
                'base:subcontroller ' .
                    $this->argument('name') .
                    ' ' .
                    $this->argument('fkName') .
                    ' ' .
                    $this->argument('fkId') .
                    ' ' .
                    $params .
                    $paramLang,
            );
        } else {
            \Artisan::call('base:controller ' . $name . ' ' . $params . $paramLang);
        }
        echo \Artisan::output();

        \Artisan::call('base:policy ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call('base:repository ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call('base:request ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call('base:request-update ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call(
            'base:definition ' .
                $name .
                ' ' .
                @$this->argument('fkName') .
                ' ' .
                @$this->argument('fkId') .
                ' ' .
                $params .
                $paramLang,
        );
        echo \Artisan::output();

        $this->info('Success!');

        // $this->info('Próximos passos:');
        // $this->info('sidebar.blade.php, App\Domains\@NAME_DOMAIN@\Definition\@NAME_DEFINITION@::instance()->makeSidebarNavDropdown()');
        // $this->info('admin.php, @NAME_DEFINITION@::instance()->makeGroupRoutes(null, true)');
        // $this->info('admin.php, @NAME_DEFINITION@::instance()->makeBreadcrumbsRoutes()');
        // $this->info('AuthServiceProvider.php, \App\Domains\@NAME_DOMAIN@\Models\@MODEL@::class => \App\Domains\@NAME_DOMAIN@\Policy\@POLICY@::class');
    }
}
