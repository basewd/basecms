<?php

namespace BaseCms\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class DomainReportBaseCommand extends GeneratorCommand
{
    protected $name = 'base:report-domain';

    protected $signature = 'base:report-domain {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados}';

    protected $description = 'Gerar o Domínio de uma tabela. (Definition, Controller, resources)';

    protected $type = 'Domain';

    public function getStub()
    {
        return null;
    }

    public function handle()
    {
        $name = $this->argument('name');

        if (!\Schema::hasTable($name)) {
            echo 'Não é possível gerar, a tabela "' . $name . '" não existe';
            die();
        }

        $params = '';
        if ($this->option('force')) {
            $params = '--force';
        }

        \Artisan::call('base:model ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call('base:report-controller ' . $name . ' ' . $params);
        echo \Artisan::output();

        \Artisan::call('base:report-definition ' . $name . ' ' . $params);
        echo \Artisan::output();

        $this->info('Success!');
    }
}
