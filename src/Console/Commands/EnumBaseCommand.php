<?php

namespace BaseCms\Console\Commands;

class EnumBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:enum';

    protected $signature = 'base:enum {name : Nome da tabela} {column : Nome da coluna} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar os Enums de uma determinada tabela';

    protected $type = 'Enum';

    protected $path = '/Enums/';

    protected $endFile = 'Enum.php';

    private $searchServiceProvider = '/* artisan:base:domain:enum */';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/enum.stub';
    }

    /**
     * Sobrescreve método porque o nome do arquivo é o nome da coluna
     */
    protected function getPath($name)
    {
        $name = str_replace($this->laravel->getNamespace(), '', $name);

        return $this->laravel['path'] .
            '/Domains/' .
            $this->getNameTable() .
            $this->path .
            $this->getNameInput() .
            $this->endFile;
    }

    /**
     * Nome principal não é a tabela, mas sim a coluna passada
     */
    protected function getNameInput()
    {
        return \Str::singular(\Str::ucfirst(\Str::camel($this->argument('column'))));
    }

    private function getLangPhp()
    {
        return $this->laravel['path'] . '/../resources/lang/pt_BR/enums.php';
    }

    private function getEnumValues($table, $column)
    {
        $expression = \DB::raw('SHOW COLUMNS FROM ' . $table . ' WHERE Field = "' . $column . '"');
        $expression =
            app()->version() >= 10 ? $expression->getValue(\DB::connection()->getQueryGrammar()) : $expression;

        $type = \DB::select($expression)[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = [];
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum[] = $v;
        }
        return $enum;
    }

    /**
     * Extrai os enuns do Banco e aplica um replace
     */
    private function replaceContants(&$stub, $name)
    {
        $stub = str_replace('DummyEnum', $this->getNameInput() . 'Enum', $stub);

        $constants = [];
        $enumValues = $this->getEnumValues($this->argument('name'), $this->argument('column'));

        foreach ($enumValues as $v) {
            $field = \Str::ucfirst(\Str::camel($v));
            $constants[] =
                'const ' .
                $field .
                " = '" .
                $v .
                "';
    ";
        }

        $stub = str_replace('DummyConstants', trim(join('', $constants)), $stub);

        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceContants($stub, $name);

        return $stub;
    }

    private function addLangs()
    {
    }

    public function handle()
    {
        parent::handle();

        $this->addLangs();
    }
}
