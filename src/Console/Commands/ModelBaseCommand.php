<?php

namespace BaseCms\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Eloquent\Relations\Relation;
use ReflectionClass;
use ReflectionMethod;

class ModelBaseCommand extends GeneratorCommand
{
    protected $name = 'base:model';

    protected $signature = 'base:model {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} {--lang : Translate}';

    protected $description = 'Gerar o Model de uma tabela';

    protected $type = 'Model';

    public function getStub()
    {
        return null;
    }

    public function getActivityLogStub()
    {
        return __DIR__ . '/../stubs/model/activity_log.stub';
    }

    public function getSlubStub()
    {
        return __DIR__ . '/../stubs/model/slug.stub';
    }

    public function getLangStub()
    {
        return __DIR__ . '/../stubs/model/lang.stub';
    }

    public function handle()
    {
        $normalizeName = \Str::singular(\Str::ucfirst(\Str::camel($this->argument('name'))));

        $separator = DIRECTORY_SEPARATOR === '\\' ? DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR : DIRECTORY_SEPARATOR; //Fix Windows

        $outputPath =
            (DIRECTORY_SEPARATOR === '\\' ? '' : '.') .
            $separator .
            'Domains' .
            $separator .
            $normalizeName .
            $separator .
            'Models' .
            $separator;

        $modelName = $normalizeName . 'Model';

        $pathModel =
            public_path() .
            DIRECTORY_SEPARATOR .
            '..' .
            DIRECTORY_SEPARATOR .
            'app' .
            DIRECTORY_SEPARATOR .
            'Domains' .
            DIRECTORY_SEPARATOR .
            $normalizeName .
            DIRECTORY_SEPARATOR .
            'Models' .
            DIRECTORY_SEPARATOR .
            $modelName .
            '.php';

        //Se já existe, não cria (skip)
        if (file_exists($pathModel) && !$this->option('force')) {
            echo 'Model already exist!';
            return;
        }

        try {
            \Artisan::call(
                'krlove:generate:model' .
                    ' ' .
                    $modelName .
                    ' --table-name=' .
                    $this->argument('name') .
                    ' --output-path=' .
                    $outputPath .
                    ' --no-timestamps',
            );
            echo \Artisan::output();

            $modelClass = 'App\Domains\\' . $normalizeName . '\Models\\' . $modelName;

            $this->normalizeMakeModels($pathModel, $normalizeName);

            $this->removeDuplicatedFunctions($pathModel);

            include_once $pathModel;

            $this->setDeletedAt($pathModel, $modelClass)
                ->setTimestamp($pathModel, $modelClass)
                ->setOrderable($pathModel, $modelClass)
                ->setHasFactory($pathModel)
                ->setSlug($pathModel, $modelClass)
                ->addActivityModel($pathModel);

            if ($this->option('lang')) {
                $this->setLangToModel($this->argument('name'), $modelClass);
            }

            $this->artisanModelRelationship($modelClass);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    private function setLangToModel($tableTranslation, $modelTranslation)
    {
        $table = str_replace('_translations', '', $tableTranslation);

        if (\Schema::hasTable($table)) {
            $normalizeName = \Str::singular(\Str::ucfirst(\Str::camel($table)));
            $modelName = $normalizeName . 'Model';

            $pathModel =
                public_path() .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'app' .
                DIRECTORY_SEPARATOR .
                'Domains' .
                DIRECTORY_SEPARATOR .
                $normalizeName .
                DIRECTORY_SEPARATOR .
                'Models' .
                DIRECTORY_SEPARATOR .
                $modelName .
                '.php';

            if (file_exists($pathModel)) {
                $contentModel = file_get_contents($pathModel);

                if (strpos($contentModel, 'use App\Models\Traits\BaseTranslation') !== false) {
                    return;
                }

                $model = new $modelTranslation();

                $fkFieldLang = \Str::singular($table) . '_id';

                $listFields = array_diff($model->getFillable(), ['created_at', 'updated_at', 'locale', $fkFieldLang]);
                $listFields = "'" . implode("', '", $listFields) . "'";

                $import = "
    use Illuminate\Database\Eloquent\Model;
    use App\Models\Traits\BaseTranslation;
    use Astrotomic\Translatable\Translatable;
    ";

                $contentModel = str_replace('use Illuminate\Database\Eloquent\Model;', $import, $contentModel);

                $translateContent = file_get_contents($this->getLangStub());
                $translateContent = str_replace('@FIELDS_LANG@', $listFields, $translateContent);
                $translateContent = str_replace('@FK_LANG@', $fkFieldLang, $translateContent);
                $translateContent = str_replace('@MODEL_LANG@', '\\' . get_class($model), $translateContent);

                $contentModel = \Str::replaceFirst('];', "];\n\n" . $translateContent, $contentModel);

                file_put_contents($pathModel, $contentModel);
            }
        }
    }

    private function removeDuplicatedFunctions($file)
    {
        $content = file_get_contents($file);

        $pattern = '/function\s+(\w+)\s*\([^)]*\)\s*{[^}]*}/';

        preg_match_all($pattern, $content, $matches);

        $duplicateFunctions = [];

        foreach ($matches[1] as $functionName) {
            if (in_array($functionName, $duplicateFunctions)) {
                $content = \Str::replaceLast($functionName . '(', $functionName . '_(', $content);
                continue;
            }

            $occurrences = substr_count($content, $functionName);

            if ($occurrences > 1) {
                $duplicateFunctions[] = $functionName;
            }
        }

        file_put_contents($file, $content);
    }

    private function artisanModelRelationship($modelClass)
    {
        $model = new $modelClass();

        $relationshipsNotfound = [];

        foreach ((new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if (
                $method->class != get_class($model) ||
                !empty($method->getParameters()) ||
                $method->getName() == __FUNCTION__
            ) {
                continue;
            }

            try {
                $return = $method->invoke($model);
                if ($return instanceof Relation) {
                    $reflection = new ReflectionClass($return);
                }
            } catch (\Throwable $e) {
                $relationshipsNotfound[] = $method->getName();
            }
        }

        foreach ($relationshipsNotfound as $k => $v) {
            \Artisan::call('base:model ' . \Str::snake($v));
            \Artisan::call('base:model ' . \Str::plural(\Str::snake($v)));
            \Artisan::call('base:model ' . \Str::plural($v));
            \Artisan::call('base:model ' . $v);
            echo \Artisan::output();
        }
    }

    /**
     * Normaliza os Models p/ padrão de Domínios
     */
    private function normalizeMakeModels($fullpath, $name)
    {
        $contentFileModel = file_get_contents($fullpath);

        $regexp = '/\'App\\\\(\w*)/';
        $matches = [];
        preg_match_all($regexp, $contentFileModel, $matches, PREG_SET_ORDER, 0);

        $separator = '\\';
        $namespace = 'App' . $separator . 'Domains' . $separator . $name . $separator . 'Models';

        foreach ($matches as $v) {
            $model = array_pop($v);

            $namespaceCustom = 'App' . $separator . 'Domains' . $separator . $model . $separator . 'Models';
            $contentFileModel = str_replace(
                "'App\\" . $model . "'",
                "'" . $namespaceCustom . $separator . $model . "Model'",
                $contentFileModel,
            );
        }

        //Namespace do topo
        $contentFileModel = str_replace('namespace App;', 'namespace ' . $namespace . ';', $contentFileModel);

        file_put_contents($fullpath, $contentFileModel);

        return $this;
    }

    private function setTimestamp($fullpath, $modelClass)
    {
        $model = new $modelClass();

        $list = ['created_at', 'createdAt', 'criadoEm', 'criado_em'];

        $found = false;
        foreach ($model->getFillable() as $k => $v) {
            if (array_search($v, $list) !== false) {
                $found = true;
                break;
            }
        }

        if ($found) {
            $content = file_get_contents($fullpath);
            $content = str_replace('public $timestamps = false;', 'public $timestamps = true;', $content);
            file_put_contents($fullpath, $content);
        }

        return $this;
    }

    private function setDeletedAt($fullpath, $modelClass)
    {
        $model = new $modelClass();

        $list = ['deleted_at', 'deletedAt', 'removidoEm', 'removido_em'];

        $found = false;
        foreach ($model->getFillable() as $k => $v) {
            if (array_search($v, $list) !== false) {
                $found = true;
                break;
            }
        }

        if ($found) {
            $deletedImport = "
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;";

            $useTrait = "{
    use SoftDeletes;
";
            $content = file_get_contents($fullpath);

            $content = str_replace('use Illuminate\Database\Eloquent\Model;', $deletedImport, $content);
            $content = str_replace_once('{', $useTrait, $content);

            file_put_contents($fullpath, $content);
        }

        return $this;
    }

    private function setSlug($fullpath, $modelClass)
    {
        $model = new $modelClass();

        $list = ['slug', 'alias'];

        $found = false;
        foreach ($model->getFillable() as $k => $v) {
            if (array_search($v, $list) !== false) {
                $found = true;
                break;
            }
        }

        if ($found) {
            $import = "
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;";

            $useTrait = "{
    use HasSlug;
";
            $content = file_get_contents($fullpath);

            $content = str_replace('use Illuminate\Database\Eloquent\Model;', $import, $content);
            $content = str_replace_once('{', $useTrait, $content);

            $slugContent = file_get_contents($this->getSlubStub());
            $content = \Str::replaceFirst('];', "];\n\n" . $slugContent, $content);

            file_put_contents($fullpath, $content);
        }

        return $this;
    }

    private function setHasFactory($fullpath)
    {
        $import = "use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
";

        $useTrait = "{
    use HasFactory;
    use LogsActivity;
        ";

        $content = file_get_contents($fullpath);
        $content = str_replace('use Illuminate\Database\Eloquent\Model;', $import, $content);
        $content = str_replace_once('{', $useTrait, $content);

        file_put_contents($fullpath, $content);

        return $this;
    }

    private function setOrderable($fullpath, $modelClass)
    {
        $model = new $modelClass();

        $list = ['order', 'ordem'];

        $found = false;
        foreach ($model->getFillable() as $k => $v) {
            if (array_search($v, $list) !== false) {
                $found = true;
                break;
            }
        }

        if ($found) {
            $sorterableImport = "
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use BaseCms\Models\BaseModelTrait;";

            $extendImplements = 'extends Model implements Sortable';

            $useTrait = "{
    use SortableTrait;
    use BaseModelTrait;
";

            $content = file_get_contents($fullpath);

            $content = str_replace('use Illuminate\Database\Eloquent\Model;', $sorterableImport, $content);
            $content = str_replace('extends Model', $extendImplements, $content);
            $content = str_replace_once('{', $useTrait, $content);

            file_put_contents($fullpath, $content);
        }

        return $this;
    }

    public function addActivityModel($fullpath)
    {
        if (!config('base.scaffold.model.activityLog')) {
            return;
        }

        $content = file_get_contents($fullpath);

        $activityLog = file_get_contents($this->getActivityLogStub());

        $content = \Str::replaceFirst('];', "];\n\n" . $activityLog, $content);

        file_put_contents($fullpath, $content);
    }
}

function str_replace_once($str_pattern, $str_replacement, $string)
{
    if (strpos($string, $str_pattern) !== false) {
        $occurrence = strpos($string, $str_pattern);
        return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
    }

    return $string;
}
