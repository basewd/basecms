<?php

namespace BaseCms\Console\Commands;

class PolicyBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:policy';

    protected $signature = 'base:policy {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar o Policy de uma determinada tabela';

    protected $type = 'Policy';

    protected $path = '/Policy/';

    protected $endFile = 'Policy.php';

    private $searchServiceProvider = '/* artisan:base:domain:policy */';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/policy.stub';
    }

    private function getAuthServiceProviderPhp()
    {
        return $this->laravel['path'] . '/Providers/AuthServiceProvider.php';
    }

    private function addPolicyServiceProvider()
    {
        $content = file_get_contents($this->getAuthServiceProviderPhp());
        $policyClass = '\App\Domains\\' . $this->getNameInput() . '\Policy\\' . $this->getNameInput() . 'Policy';
        $modelClass = '\\' . $this->getModelClass();

        $str = $modelClass . '::class => ' . $policyClass . '::class,';
        $str .= "\n\t\t" . $this->searchServiceProvider;

        //Só adiciona caso não exista
        if (strpos($content, $policyClass) === false) {
            $content = str_replace($this->searchServiceProvider, $str, $content);
            file_put_contents($this->getAuthServiceProviderPhp(), $content);
        }

        return true;
    }

    protected function replaceRestoreStubs(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        if (array_search('deleted_at', $attributes) !== false) {
            $stub = str_replace('StubDummyRestore', 'return $' . $this->getCamelName() . 'Deleted->deleted_at;', $stub);
            $stub = str_replace(
                'StubDummyDelete',
                'return false; //return $' . $this->getCamelName() . 'Deleted->deleted_at; //Desabilitado por default',
                $stub,
            );
        } else {
            $stub = str_replace('StubDummyRestore', 'return false;', $stub);
            $stub = str_replace('StubDummyDelete', 'return false;', $stub);
        }

        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceRestoreStubs($stub, $name);

        return $stub;
    }

    public function handle()
    {
        parent::handle();

        $this->addPolicyServiceProvider();
    }
}
