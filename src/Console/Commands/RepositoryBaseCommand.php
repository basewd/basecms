<?php

namespace BaseCms\Console\Commands;

class RepositoryBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:repository';

    protected $signature = 'base:repository {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar o arquivo Repository';

    protected $type = 'Repository';

    protected $path = '/Repositories/';

    protected $endFile = 'Repository.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/repository.stub';
    }

    protected function getStubRestore()
    {
        return __DIR__ . '/../stubs/repository/restore.stub';
    }

    protected function replaceRestoreStubs(&$stub, $name)
    {
        $attributes = $this->getAttributesModel();

        if (array_search('deleted_at', $attributes) !== false) {
            $stubRestore = file_get_contents($this->getStubRestore());
            $stubRestore = str_replace('DummyModel', $this->getNameInput() . 'Model', $stubRestore);

            foreach ($this->injectParams as $k => $v) {
                $stubRestore = str_replace($k, $v, $stubRestore);
            }

            $stub = str_replace('StubDummyRestore', $stubRestore, $stub);
        } else {
            $stub = str_replace('StubDummyRestore', '', $stub);
        }

        return $this;
    }

    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        $this->replaceRestoreStubs($stub, $name);

        return $stub;
    }
}
