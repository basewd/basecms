<?php

namespace BaseCms\Console\Commands;

class RequestBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:request';

    protected $signature = 'base:request {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar o arquivo de Request';

    protected $type = 'Request';

    protected $path = '/Requests/';

    protected $endFile = 'CreateRequest.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/requestCreate.stub';
    }
}
