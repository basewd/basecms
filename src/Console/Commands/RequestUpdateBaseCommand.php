<?php

namespace BaseCms\Console\Commands;

class RequestUpdateBaseCommand extends DefaultBaseCommand
{
    protected $name = 'base:request-update';

    protected $signature = 'base:request-update {name : Nome da tabela} {--force : Sobrescreve arquivos já gerados} ';

    protected $description = 'Gerar o arquivo de Request Update';

    protected $type = 'Request';

    protected $path = '/Requests/';

    protected $endFile = 'UpdateRequest.php';

    protected function getStub()
    {
        return __DIR__ . '/../stubs/requestUpdate.stub';
    }
}
