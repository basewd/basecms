<?php

namespace BaseCms\Console\Commands;

class SubControllerCommand extends ControllerBaseCommand
{
    protected $name = 'base:subcontroller';

    protected $signature = 'base:subcontroller {name : Nome da tabela} {fkName : Nome da tabela que tem relacionamento} {fkId : Nome da coluna que tem relacionamento} {--force : Sobrescreve arquivos já gerados}  {--lang : Translate}';

    protected $description = 'Gera Controller p/ ser consumido através de uma subrota. Ver arquivo SampleAuthorDefinition.php (SampleController) p/ referência';

    protected function getStub()
    {
        if ($this->option('lang')) {
            return __DIR__ . '/../stubs/controller_with_parent_lang.stub';
        } else {
            return __DIR__ . '/../stubs/controller_with_parent.stub';
        }
    }

    protected function getStubOrder()
    {
        return __DIR__ . '/../stubs/controller/order/order_with_parent.stub';
    }

    protected function getStubCreateOrUpdateBlade()
    {
        if ($this->option('lang')) {
            return __DIR__ . '/../stubs/controller/lang/resource_create_or_update_with_parent.blade.stub';
        } else {
            return __DIR__ . '/../stubs/controller/resource_create_or_update_with_parent.blade.stub';
        }
    }

    protected function getStubIndexBlade()
    {
        return __DIR__ . '/../stubs/controller/resource_index_with_parent.blade.stub';
    }

    protected function getParentNameInput()
    {
        return \Str::singular(\Str::ucfirst(\Str::camel($this->argument('fkName'))));
    }

    protected function getParentDefaultNamespace()
    {
        return 'App\Domains\\' . $this->getParentNameInput();
    }

    protected function buildClass($name)
    {
        $this->injectParams['DummyParentNamespace'] = $this->getParentDefaultNamespace();
        $this->injectParams['DummyParentModel'] = $this->getParentNameInput() . 'Model';
        $this->injectParams['dummy_relationship_id'] = $this->argument('fkId');

        $stub = parent::buildClass($name);

        return $stub;
    }
}
