/**
     * Restaurar registro
     *
     * @param DummyModel $model
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function restore(DummyModel $modelDeleted)
    {
        $this->repository->restore($modelDeleted);

        return redirect(route('admin.dummy_route.index'))->withFlashSuccess(__('Registro restaurado com sucesso!'));
    }
    
    /**
     * Remover para sempre o registro
     *
     * @param DummyModel $model
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function forceDelete(DummyModel $modelDeleted)
    {
        $this->repository->forceDelete($modelDeleted);

        return redirect(route('admin.dummy_route.index'))->withFlashSuccess(__('Registro removido com sucesso!'));
    }
    
     /**
     * Listagem dos registros p/ restaurar
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function indexRestore()
    {
        $definition = $this->definition;
        return view('backend.dummy_route.index_restore', compact('definition'));
    }

    /**
     * API Datatable - Restore "indexRestore()"
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function datatableRestore(Request $request)
    {
        //Caso tenha algum dado sensível na tabela, deve ser montado o select com as colunas (por causa do Datatable). Ex: select("tabela.coluna1", "tabela.coluna2" ...).
        $datatable = DataTables::eloquent(
            DummyModel::select('dummy_table.*')->onlyTrashed()
        )
            ->addColumn('actions', function ($row) {
                $restoreUrl = route('admin.dummy_route.restore', $row->id); 
                $forceDeleteUrl = route('admin.dummy_route.forceDelete', $row->id);
             
                return view('base::components.datatable-actions', compact('row', 'restoreUrl','forceDeleteUrl'));
            })
            ->rawColumns([])
            ->baseRender($this->definition, [])
            ->baseFilter($this->definition);

        return $datatable->make(true);
    }