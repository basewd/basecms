<?php

namespace BaseCms\Constants;

/**
 * Tipagens utilizadas para renderizar o valor de forma diferente na tabela
 *
 * Para BaseColumnConst::Image, BaseColumnConst::File, BaseColumnConst::Video, existe uma implementação custom nas seguintes classes.
 *  - BaseFormRequestHelper::rules
 *  - BaseUploadHelper::saveFilesFromRequest
 *
 */
class BaseColumnConst
{
    const Text = 'text';
    const File = 'file';
    const Video = 'video';
    const Image = 'image';
    const Number = 'number';
    const Date = 'date';
    const DateNoTimezone = 'date_no_timezone';
    const Boolean = 'boolean';
}
