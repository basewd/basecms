<?php

namespace BaseCms\Constants;

use BaseCms\Input\InputBlockBuilder;
use BaseCms\Input\InputLatLng;
use BaseCms\Input\InputVimeo;
use BaseCms\Input\InputYouTube;
use BaseCms\View\Components\Backend\Separator;

/**
 * Mapeamento dos inputs válidos
 *
 * Por hora os inputs pegam os elementos do Aire (Referência: https://airephp.com/components)
 *
 * É possível criar inputs custom, ver especificação do arquivo @var Base\Interfaces\BaseInput
 */
class BaseInputConst
{
    /**
     * Tipo texto (BaseColumnConst::Text)
     */
    const AireInput = 'Aire.input';

    const AireTextArea = 'Aire.textArea';

    const AireEmail = 'Aire.email';

    const AireSelect = 'Aire.select';

    const AireRange = 'Aire.range';

    const AireColor = 'Aire.color';

    const AireHidden = 'Aire.hidden';

    /**
     * P/ desabilitar o upload de imagem:
     *    ->setInputMethods([
     *      ['setAttribute', 'no-image', true]
     *    ])
     */
    const AireRichEditor = 'Aire.wysiwyg';

    const AireRadioGroup = 'Aire.radioGroup';

    const AireCheckboxGroup = 'Aire.checkboxGroup';

    const AirePassword = 'Aire.password';

    /**
     * Tipo Número
     */
    const AireNumber = 'Aire.number'; //BaseColumnConst::Number

    /**
     * Tipo Bool (BaseColumnConst::Bool)
     */
    const AireCheckbox = 'Aire.checkbox';

    /**
     * Tipo Data (BaseColumnConst::Date, BaseColumnConst::time)
     */
    const AireDate = 'Aire.date';

    const AireTime = 'Aire.time';

    const AireDateTime = 'Aire.dateTimeLocal';

    /**
     * Tipo Arquivo (BaseColumnConst::File || BaseColumnConst::Image || BaseColumnConst::Video)
     */
    const AireFile = 'Aire.file';

    /**
     * Input - Embed Vimeo
     */
    const Vimeo = InputVimeo::class;

    /**
     * Input Lat/Lng
     */
    const LatLng = InputLatLng::class;

    /**
     * Input - Embed Youtube
     */
    const Youtube = InputYouTube::class;

    /**
     * Separador com Título p/ ser utilizado nos formulários
     */
    const Separator = Separator::class;

    /**
     * Componente BlockBuilder
     */
    const BlockBuilder = InputBlockBuilder::class;
}
