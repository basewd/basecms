<?php

namespace BaseCms\Datatables\ColumnRenders;

use BaseCms\BaseField;
use BaseCms\Datatables\Interfaces\BaseColumnRender;

class ColorColumnRender implements BaseColumnRender
{
    const VALID_HEX = "/^#(?>[[:xdigit:]]{3}){1,2}$/";
    private $field;
    private $fieldDefinition;
    private $row;
    private $routeAlias;

    public function __construct(string $field, BaseField $fieldDefinition, $row, string $routeAlias)
    {
        $this->field = $field;
        $this->fieldDefinition = $fieldDefinition;
        $this->row = $row;
        $this->routeAlias = $routeAlias;
    }

    protected function validateColor($color)
    {
        if (!preg_match(self::VALID_HEX, $color)) {
            $this->row->{$this->field} = '#FFFF';
        }
    }

    public function render()
    {
        $this->validateColor($this->row->{$this->field});
        return view('base::components.color-column-render')->with(['color' => $this->row->{$this->field}]);
    }
}
