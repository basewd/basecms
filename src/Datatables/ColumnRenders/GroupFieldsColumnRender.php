<?php

namespace BaseCms\Datatables\ColumnRenders;

use BaseCms\BaseField;
use BaseCms\Datatables\Helpers\BaseDatatableHelper;
use BaseCms\Datatables\Interfaces\BaseColumnRender;
use Illuminate\Database\Eloquent\Model;

class GroupFieldsColumnRender implements BaseColumnRender
{
    private $field;
    private $fieldDefinition;
    private $row;
    private $routeAlias;

    private $columns;

    /**
     * @param string $field
     * @param BaseField $fieldDefinition
     * @param Model|object $row
     * @param string $routeAlias
     */
    public function __construct(string $field, BaseField $fieldDefinition, $row, string $routeAlias)
    {
        $this->field = $field;
        $this->fieldDefinition = $fieldDefinition;
        $this->row = $row;
        $this->routeAlias = $routeAlias;
    }

    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    public function render()
    {
        $definition = $this->fieldDefinition->getBaseDefinition();

        $list = [];
        foreach ($this->columns as $k => $v) {
            $value = @BaseDatatableHelper::defaultRender($v, $definition)($this->row);

            $baseField = $definition->getFields()[$v];

            $list[] = '<b>' . $baseField->title . '</b><p>' . $value . '</p>';
        }

        return join('', $list);
    }
}
