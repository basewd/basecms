<?php

namespace BaseCms\Datatables\ColumnRenders;

use BaseCms\BaseField;
use BaseCms\Datatables\Interfaces\BaseColumnRender;
use Illuminate\Database\Eloquent\Model;

class JSONColumnRender implements BaseColumnRender
{
    private $field;
    private $fieldDefinition;
    private $row;
    private $routeAlias;

    /**
     * @param string $field
     * @param BaseField $fieldDefinition
     * @param Model|object $row
     * @param string $routeAlias
     */
    public function __construct(string $field, BaseField $fieldDefinition, $row, string $routeAlias)
    {
        $this->field = $field;
        $this->fieldDefinition = $fieldDefinition;
        $this->row = $row;
        $this->routeAlias = $routeAlias;
    }

    public function render()
    {
        $data = $this->row[$this->field];
        if (!is_array($data)) {
            $data = json_decode($this->row[$this->field]);
        }

        $str = '<pre>';
        $str .= json_encode($data, JSON_PRETTY_PRINT);
        $str .= '</pre>';

        return $str;
    }
}
