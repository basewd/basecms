<?php

namespace BaseCms\Datatables\ColumnRenders;

use BaseCms\BaseField;
use BaseCms\Datatables\Interfaces\BaseColumnRender;
use Illuminate\Database\Eloquent\Model;

class RadioColumnRender implements BaseColumnRender
{
    private $field;
    private $fieldDefinition;
    private $row;
    private $routeAlias;

    /**
     * @param string $field
     * @param BaseField $fieldDefinition
     * @param Model|object $row
     * @param string $routeAlias
     */
    public function __construct(string $field, BaseField $fieldDefinition, $row, string $routeAlias)
    {
        $this->field = $field;
        $this->fieldDefinition = $fieldDefinition;
        $this->row = $row;
        $this->routeAlias = $routeAlias;
    }

    public function render()
    {
        if ($this->row instanceof Model) {
            $pk = $this->row->getKeyName();
            $id = $this->row->{$pk};
        } else {
            $id = $this->row->id;
        }

        $value = $this->row->{$this->field};

        return view('base::components.radio-column-render', [
            'id' => $this->field . '-' . $id,
            'value' => $value,
            'field' => $this->field,
            'url' => $this->routeAlias . '/' . $id . '/radioValue',
        ]);
    }
}
