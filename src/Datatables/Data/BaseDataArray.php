<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;

/**
 * Dados do tipo array
 *
 * Ex: new BaseDataArray(['value'=>'label'])
 */
class BaseDataArray implements BaseData
{
    public $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Retorna a lista de dados no formato "value => label"
     *
     * @return array[string|int]string
     */
    public function getData()
    {
        return $this->data;

        // return collect($this->data)->mapWithKeys(function ($item) {
        //     if (!@$item['text']) {
        //         throw new \Exception('Formato: [{ "value": 1, "text": "Sim" }]');
        //     }

        //     return [$item['value'] => $item['text']];
        // });
    }

    /**
     * Retorna o valor
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        return $this->getData()[$value];
    }
}
