<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;

/**
 * Dados do tipo Enum
 *
 * Ex: new BaseDataEnum(SampleEnum::class)
 */
class BaseDataEnum implements BaseData
{
    public $enum;

    /**
     * @param string $enum
     */
    public function __construct(string $enum)
    {
        // \BenSampo\Enum\Enum
        $this->enum = $enum;
    }

    /**
     * Retorna a lista de dados no formato "value => label"
     *
     * @return array[string|int]string
     */
    public function getData()
    {
        return $this->enum::asSelectArray();
    }

    /**
     * Retorna o valor
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        return @$this->enum::asSelectArray()[$value] ?? $value;
    }
}
