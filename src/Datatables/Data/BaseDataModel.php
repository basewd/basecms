<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;

use BaseCms\Helpers\BaseInputHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * Dados do tipo Model (Define um model direto)
 *
 * Este formato não é performatico, no DataTable ele vai retornar todos registros p/ encontrar o valor. (Por isso cache de 30 segundos no getValue())
 *
 * Ex: new BaseDataModel(SampleOthersModel::class, "id", "name", [['orderBy', 'name']])
 */
class BaseDataModel implements BaseData
{
    private static $allowedModelMethods = [
        'orderBy',
        'pluck',
        'with',
        'all',
        'last',
        'first',
        'where',
        'whereRaw',
        'whereBetween',
        'whereHas',
        'withTranslation',
        'orderByTranslation',
    ];

    public $model;
    public $dataMethods;

    public $valueField;
    public $textField;
    public $pluckField;

    /**
     * @param string $model String do model
     * @param string $valueField coluna ID do model
     * @param string $textField coluna Texto do model, que tem na tabela
     * @param array $dataMethods chamadas no model, como ordenamento
     * @param string $pluckField método p/ retorno do texto, pode ser vazio (pega o textField) ou pode ser um getXXXXXAttribute setado no model
     */
    public function __construct(
        string $model,
        string $valueField,
        string $textField,
        array $dataMethods = [],
        $pluckField = null
    ) {
        if (!is_a($model, Model::class, true)) {
            throw new \Exception('BaseDataModel precisa extender \Illuminate\Database\Eloquent\Model');
        }

        $this->model = $model;

        $this->valueField = $valueField;
        $this->textField = $textField;
        $this->pluckField = $pluckField ? $pluckField : $textField;

        $this->dataMethods = $dataMethods;
    }

    /**
     * Retorna a lista de dados no formato "value => label"
     *
     * @return array[string|int]string
     */
    public function getData()
    {
        $callMethods = array_merge($this->dataMethods);

        $data = BaseInputHelper::callMethodParams($this->model, $callMethods, self::$allowedModelMethods);

        /**
         * TODO: Aqui existe uma questão
         *
         * O getData() é utilizado pelo "formulário" e pelo "dataTable"
         *
         * No formulário, dados "withTrashed" não são retornados
         *
         * Na tabela, os registros podem ter sido removidos, mas estão sendo listados na coluna... significa que o "getValue()" precisa funcionar
         *
         * Porém o método "getData()" é utilizado para a filtragem dos dados ... como não é passado "withTrashed" no médodo "$data->get()->pluck" ... o filtro não vai funcionar
         *
         * Solução poderia ser um getData() p/ DataTable e um "getDataInput()" p/ formulários
         */
        $data = $data->get()->pluck($this->pluckField, $this->valueField);

        return $data;
    }

    /**
     * Retorna o valor
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        $key = $this->model . '_field_' . $this->valueField . '_value_' . $value;

        return \Cache::remember($key, 30, function () use ($value) {
            $data = $this->model::where($this->valueField, '=', $value);

            if (method_exists($this->model, 'trashed')) {
                $data->withTrashed();
            }

            $row = $data->first();

            return @$row ? (@$row->{$this->pluckField} ?: null) : null;
        });
    }
}
