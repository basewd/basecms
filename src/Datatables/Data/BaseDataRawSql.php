<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;
use Illuminate\Database\Eloquent\Model;

/**
 * Permite utilização de SQL (Raw SQL) p/ montagem dos <selects/> e colunas da tabela.
 *
 * É possível acessar os parametros da "row" (todos os atributos do model) através da utilização do ":nome_da_variavel".
 *
 * Ex: new BaseDataRawSql('select id, title from program', array(), 'select title from program where id = :selectRawSql')
 *
 * PS: Os SQLS não podem ser complexos (Ex: group_concat), p/ isso utilizar o BaseDataVirtualRawSql.
 *
 */
class BaseDataRawSql implements BaseData
{
    private $sql;
    private $params;

    private $sqlValue;

    /**
     * $sql:
     *
     * Primeira coluna é o valor, segunda coluna é o texto.
     *
     * Ex: select id, name from xxxx;
     *
     *
     * $sqlValue: serve p/ o render do Datatable, assim conseguimos otimizar a chamada.
     *
     * Ex: select name from xxx where id = :id
     *
     * @param string|null $sql
     * @param array|null $params
     * @param string|null $sqlValue
     */
    public function __construct(?string $sql, ?array $params = [], string $sqlValue = null)
    {
        $this->sql = $sql;
        $this->params = $params;

        $this->sqlValue = $sqlValue;
    }

    /**
     * Retorna a lista de dados no formato "value => label"
     *
     * @return array[string|int]string
     */
    public function getData()
    {
        //Fix params array()
        if (!$this->sql) {
            return [];
        }

        $params = $this->params ? $this->params : [];

        $expression = \DB::raw($this->sql);
        $expression =
            app()->version() >= 10 ? $expression->getValue(\DB::connection()->getQueryGrammar()) : $expression;

        $data = collect(\DB::select($expression, $params))->mapWithKeys(function ($v) {
            $v = (array) $v;

            $keys = array_keys($v);
            return [$v[$keys[0]] => $v[$keys[1]]];
        });

        return $data;
    }

    /**
     * Retorna o valor
     *
     * @param mixed $value
     * @param Model|object $rowData
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        //Extract only binding values
        $bindings = [];

        $sqlValuesExplode = explode(' ', $this->sqlValue);

        //DB::table or Model
        $keys = $rowData instanceof Model ? $rowData->getAttributes() : get_object_vars($rowData);

        foreach ($keys as $k => $v) {
            if (in_array(':' . $k, $sqlValuesExplode)) {
                $bindings[$k] = $v;
            }
        }

        $expression = \DB::raw($this->sqlValue);
        $expression =
            app()->version() >= 10 ? $expression->getValue(\DB::connection()->getQueryGrammar()) : $expression;

        $values = (array) @\DB::selectOne($expression, $bindings);

        $newValue = @array_pop($values);

        return $newValue;
    }

    /**
     * Altera o SQL do BaseDataRawSql
     *
     * @return $this
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    /**
     * Altera os parametros do SQL
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }
}
