<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;
use BaseCms\Helpers\BaseInputHelper;

/**
 * Permite chamadas de funções estaticas
 *
 * Ex: new BaseDataStaticFn(SamplesController::class, 'getDatasource')
 *
 * [1 => 'Oi', 2 => 'Tchau'];
 */
class BaseDataStaticFn implements BaseData
{
    public $class;
    public $methods;

    /**
     * @param string $class
     * @param mixed $methods
     */
    public function __construct(string $class, $methods)
    {
        $this->class = $class;
        $this->methods = $methods;
    }

    /**
     * Ex de retorno: [1 => 'Oi', 2 => 'Tchau'];
     *
     * @return array[string|int]string
     *
     */
    public function getData()
    {
        return BaseInputHelper::callMethodParams($this->class, $this->methods);
    }

    /**
     * @param string $value
     * @param Model|object $rowData
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        return $this->getData()[$value];
    }
}
