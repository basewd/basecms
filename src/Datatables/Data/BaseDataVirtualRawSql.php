<?php

namespace BaseCms\Datatables\Data;

use BaseCms\Datatables\Interfaces\BaseData;

/**
 * BaseData especifico para SQL`s complexos. (Ex: group_concat)
 *
 * Não deve ser utilizado para renderizar INPUTS
 *
 * Deve ser utilizado apenas para TABELAS
 */
class BaseDataVirtualRawSql implements BaseData
{
    public $sql;

    /**
     * $sql: serve p/ o render do Datatable, assim conseguimos otimizar a chamada.
     *
     * A coluna "samples" é o "parent/tabela do model"
     *
     * Ex 1: select name from sample_others where sample_others.id = samples.sampleOtherId
     *
     * No query do Datatable vai gerar algo como: "select *,  (select name from sample_others where sample_others.id = samples.sampleOtherId) as virtual2  from `samples`"
     *
     * Ex 2: select group_concat(sample_stuffs.`name`) from sample_stuffs, sample_many_sample_stuffs where samples.id = sample_many_sample_stuffs.sampleId and sampleStuffId = sample_stuffs.id group by samples.id
     *
     */
    public function __construct(string $sql = null)
    {
        $this->sql = $sql;
    }

    /**
     * Não existe "dados" nesse tipo de BaseData
     *
     * Apenas valor, porque ele é uma RAW SQL injetado direto no builder da consulta.
     *
     * @return array[string|int]string
     */
    public function getData()
    {
        return null;
    }

    /**
     * @param string $value
     * @param Model|object $rowData
     *
     * @return string
     */
    public function getValue($value, $rowData = null)
    {
        return null;
    }
}
