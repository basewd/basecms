<?php

namespace BaseCms\Datatables\Helpers;

use BaseCms\BaseDefinition;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Yajra\DataTables\DataTableAbstract;

class BaseDatatableExport
{
    /**
     * Gera o arquivo CSV a partir de um Datatable (Com o filtro aplicado)
     *
     * @param DataTableAbstract $datatable
     * @param BaseDefinition $definition
     *
     * @return StreamedResponse
     */
    public static function exportCsv(DataTableAbstract $datatable, BaseDefinition $definition)
    {
        $filename = \Str::snake($definition->getTitle() . '_' . date('d_m_Y')) . '.csv';

        $fields = collect($definition->getFields())->where('exportable', '=', true);

        //Aplica o filtro custom
        $datatable->baseFilter($definition);

        //Aplica os filtros na query do Datatable
        $datatable->prepareExportQuery($definition);

        if ($datatable->getQuery() instanceof \Illuminate\Database\Query\Builder && !$datatable->getQuery()->orders) {
            throw new \Exception('DB:table() precisa ter um orderBy() p/ o getQuery()->chunk funcionar.');
        }

        $response = new StreamedResponse(
            function () use ($fields, $datatable) {
                $handle = fopen('php://output', 'w');

                fwrite($handle, $bom = chr(0xef) . chr(0xbb) . chr(0xbf));

                $columnsTitle = $fields->pluck('title')->toArray();
                fputcsv($handle, $columnsTitle);

                $datatable->getQuery()->chunk(100, function ($list) use ($handle, $fields) {
                    foreach ($list as $row) {
                        $values = [];

                        //Tratamento para caso não tenha ob na infra
                        try {
                            ob_flush();
                            flush();
                        } catch (\Exception $e) {
                        }

                        foreach ($fields as $field) {
                            try {
                                $values[] = $field->getExportableValue($row);
                            } catch (\Exception $e) {
                                die('<br>' . $e->getMessage());
                            }
                        }

                        fputcsv($handle, $values);
                    }
                });

                fclose($handle);
            },
            200,
            [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            ],
        );

        return $response;
    }
}
