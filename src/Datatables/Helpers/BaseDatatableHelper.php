<?php

namespace BaseCms\Datatables\Helpers;

use BaseCms\BaseDefinition;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;
use BaseCms\Datatables\Data\BaseDataEnum;
use BaseCms\Datatables\Data\BaseDataVirtualRawSql;
use BaseCms\Helpers\BaseInputHelper;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\DataTableAbstract;

class BaseDatatableHelper
{
    public static function getFieldCustomFilter($field)
    {
        $customFilter = request()->get('customFilter');

        return @array_filter($customFilter, function ($v) use ($field) {
            return $v['field'] == $field;
        })[0];
    }

    /**
     * Render customizado no DataTable por tipo de "field/campo"
     *
     * 0 - Sobrescreve qualquer render, utilizado p/ renders complexos que manipulam dados e até fazem POSTS (Ex: CheckboxColumnRender)
     * 1 - Campos que possuem data, relacionamento (sampleOther.name) ou texto normal
     * 2 - Campos do tipo Model/relacionamento (sampleOther.name)
     *
     * @param DataTableAbstract $datatable
     * @param BaseDefinition $definition
     * @param array $ignoreFields
     */
    public static function baseRender(DataTableAbstract $datatable, BaseDefinition $definition, $ignoreFields = [])
    {
        $fields = $definition->getFields();
        $columDef = collect($datatable->getColumnDef()['edit'])->keyBy('name');

        //Multiple Select Table
        if ($definition->multipleSelectTable) {
            $primaryKey = $definition->getPrimaryKey();

            $datatable->addColumn('multiple_select_table', function ($row) use ($primaryKey) {
                return '<input name="multiple_select_table[]" type="checkbox" value="' . @$row->{$primaryKey} . '"/>';
            });

            $datatable->rawColumns(array_merge($datatable->getColumnDef()['raw'] ?? [], ['multiple_select_table']));
        }

        //1 - Render
        foreach ($definition->getTableFields() as $v) {
            if (in_array($v, $ignoreFields) || @$columDef[$v]) {
                continue;
            }

            $fieldDefinition = $fields[$v];
            $datatable->editColumn($v, self::defaultRender($v, $definition));
        }

        //2 - Campo virtual - BaseDataVirtualRawSql (Injeta um campo "virtual" no "select)
        $customRenderVirtual = [];
        foreach ($definition->getTableFields() as $v) {
            $fieldDefinition = $fields[$v];

            if (@$fieldDefinition->data && $fieldDefinition->data instanceof BaseDataVirtualRawSql) {
                array_push(
                    $customRenderVirtual,
                    \DB::raw(" ({$fieldDefinition->data->sql}) as {$fieldDefinition->field} "),
                );
            }
        }

        if (count($customRenderVirtual) > 0) {
            $columns =
                $datatable->getQuery() instanceof \Illuminate\Database\Query\Builder
                    ? $datatable->getQuery()->columns
                    : $datatable->getQuery()->getQuery()->columns;
            if (!$columns) {
                $columns = ['*'];
            }

            /**
             * Não é feito mais esse tratamento, porque as colunas podem levar ".".
             *
             * Ex: id, productId, name
             *
             * Definition (DataTable):  id, product.name, name
             *
             * Ele vai montar a query retornando "id, name", sem passar o "productId"
             *
             * Método "$definition->getModelFields()" não devolve o "productId" ... mesmo devolvendo, o "array_intersect" vai retirar, porque não é utilizado no "getTableFields()"
             **/

            // if ($datatable->getQuery() instanceof \Illuminate\Database\Eloquent\Builder) {
            //     //Foi adicionado este tratamento para não executar um SQL retornando todos os dados da Tabela
            //     if (count($datatable->getQuery()->getQuery()->columns) == 1 && strpos($datatable->getQuery()->getQuery()->columns[0], '*') !== FALSE) {
            //         $columns = array_intersect($definition->getTableFields(), array_keys($definition->getModelFields()));
            //     } else {
            //         $columns = $datatable->getQuery()->getQuery()->columns;
            //     }
            // } else {
            //     //Table
            //     $columns = $datatable->getQuery()->columns ? $datatable->getQuery()->columns : ['*'];
            // }

            $datatable->getQuery()->select(array_merge($columns, $customRenderVirtual));
        }
    }

    /**
     * Default Render p/ ser utilizada na DataTable
     *
     * @param string $field
     * @param BaseDefinition $definition
     *
     * @return string
     */
    public static function defaultRender($field, BaseDefinition $definition)
    {
        if (!$definition->getFields()[$field]) {
            throw new \Exception(
                'O "field" requisitado não existe. Você deve conferir o "Definition". Campo requisitado: ' . $field,
            );
        }

        $baseField = $definition->getFields()[$field];
        $defaultRenderParams = $baseField->defaultRenderParams;

        //Custom Render
        if ($baseField->columnRender) {
            $routeAlias = $definition->getRouteAlias();

            return function ($row) use ($field, $baseField, $routeAlias) {
                //Classe
                if (is_string($baseField->columnRender)) {
                    $classColumnRender = $baseField->columnRender;

                    $c = new $classColumnRender($field, $baseField, $row, $routeAlias);
                    if ($baseField->columnRenderMethods) {
                        BaseInputHelper::callMethodParams($c, $baseField->columnRenderMethods);
                    }

                    return $c->render();
                } else {
                    //Function
                    $methodColumnRender = $baseField->columnRender;
                    return $methodColumnRender($field, $baseField, $row, $routeAlias);
                }
            };
        }

        //Default Render
        return function ($row) use ($field, $baseField, $defaultRenderParams) {
            $value = $baseField->getValue($row);

            //Boolean
            if ($baseField->type == BaseColumnConst::Boolean) {
                return @$value ? 'Sim' : 'Não';
            }

            if (@is_null($value)) {
                return null;
            }

            if ($baseField->type == BaseColumnConst::Date && $baseField->input == BaseInputConst::AireDateTime) {
                return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y H:i');
            } elseif ($baseField->type == BaseColumnConst::Date && $baseField->input == BaseInputConst::AireTime) {
                return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'H:i');
            } elseif ($baseField->type == BaseColumnConst::Date) {
                return \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y');
            } elseif (
                $baseField->type == BaseColumnConst::DateNoTimezone &&
                $baseField->input == BaseInputConst::AireDateTime
            ) {
                return \Carbon\Carbon::parse($value)->format('d/m/Y H:i');
            } elseif (
                $baseField->type == BaseColumnConst::DateNoTimezone &&
                $baseField->input == BaseInputConst::AireTime
            ) {
                return \Carbon\Carbon::parse($value)->format('H:i');
            } elseif ($baseField->type == BaseColumnConst::DateNoTimezone) {
                return \Carbon\Carbon::parse($value)->format('d/m/Y');
            }

            //Imagem
            if ($baseField->type == BaseColumnConst::Image) {
                //max-width to width por causa do Datatable scrollY / bug no primeiro render
                return '<a href="' .
                    $value .
                    '" target="_blank" class="base-datatable-render-image" style="width: ' .
                    $defaultRenderParams->imageMaxWidth .
                    '"> <img style="width:100%; height:auto;" src="' .
                    $value .
                    '"/> </a>';
            }

            //Arquivo
            if ($baseField->type == BaseColumnConst::File) {
                return '<a href="' . $value . '" target="_blank">' . basename($value) . '</a>';
            }

            //Video
            if ($baseField->type == BaseColumnConst::Video) {
                return '<video class="base-datatable-render-video" width="' .
                    $defaultRenderParams->videoWidth .
                    '" height="auto" controls preload="none"><source src="' .
                    $value .
                    '">Browser not supported</video>';
            }

            //Text com RichEditor
            if ($baseField->type == BaseColumnConst::Text && $baseField->input == BaseInputConst::AireRichEditor) {
                if (!$defaultRenderParams->textEllipsisLength) {
                    return trim(self::plainText($value));
                }

                return nl2br(
                    trim(mb_strimwidth(self::plainText($value), 0, $defaultRenderParams->textEllipsisLength, '...')),
                );
            }

            //Vimeo
            if ($baseField->input == BaseInputConst::Vimeo) {
                return '<a href="https://vimeo.com/' . $value . '" target="_blank">' . $value . '</a>';
            }

            //Text
            if ($baseField->type == BaseColumnConst::Text) {
                if (!$defaultRenderParams->textEllipsisLength) {
                    return $value;
                }

                return mb_strimwidth($value, 0, $defaultRenderParams->textEllipsisLength, '...');
            }

            //Default
            return $value;
        };
    }

    /**
     * Helper para formatar um código html em um plain text
     *
     * @return string
     */
    private static function plainText($text)
    {
        $text = strip_tags($text, '<br><p><li>');
        $text = preg_replace('/<[^>]*>/', PHP_EOL, $text);

        return $text;
    }

    /**
     * Filtro customizado, empilhando filtros de diversos tipos (inteiro, string, data, bool)
     *
     * Ele resolve as variações que o DataTables aceita
     *
     * 1 - Filtro adicionado através do editFilter() no DataTable (Controller)
     * 2 - Filtro de um model
     *     Quando utilizado NomeDoRelacionamento.NomeDaColuna, utilizando os relacionamentos do Model principal p/ filtrar (whereHas)
     * 3 - Filtro de um Raw Sql - Queries complexas
     *     Campo Virtual, utilizando "data instaceOf BaseDataVirtualRawSql", utilizado quando temos queries mais complexas, como group_concat
     * 4 - Filtro com campo "data" setado no field do Definition.
     *     Campo Data pode ser (e o tipo do input é um <select/>)
     *      - BaseDataArray (Array)
     *      - BaseDataEnum (Enum)
     *      - BaseDataModel (Model)
     *      - BaseDataRawSql (Raw SQL)
     *      - BaseDataStaticFn (Função estatica de uma classe)
     * 5 - Filtro com campos "simples" de acordo com o tipo da coluna
     *
     * Referências de codificação:
     * https://yajrabox.com/docs/laravel-datatables/master/filter-column
     * https://github.com/yajra/laravel-datatables/blob/2d15fb2ce521008374e640d06cea2cb979f4a281/src/EloquentDataTable.php
     *
     * @param DataTableAbstract $datatable
     * @param BaseDefinition $definition
     */
    public static function baseFilter(DataTableAbstract $datatable, BaseDefinition $definition)
    {
        if (request()->get('customFilter')) {
            $customFilter = request()->get('customFilter');

            $fields = $definition->getFields();

            $model = $definition->getModelClass();

            //CollectionDataTable
            if ($datatable instanceof CollectionDataTable) {
                throw new \Exception(
                    'CollectionDataTable ainda não é suportado no filtro custom. Deve ser passado um Model ou uma Query no DataTables::of(). Ver exemplo em SamplesController.php.',
                );

                //TODO: implementar tipos de filtro aqui
                // $datatable->collection = $datatable->collection->filter(function ($row) use (
                //     $customFilter,
                //     $fields,
                //     $datatable
                // ) {
                //     return true;
                // });
            } else {
                //EloquentDataTable ou QueryDataTable
                $datatable->filter(function ($query) use ($customFilter, $fields, $datatable, $model) {
                    foreach ($customFilter as $v) {
                        $field = $v['field'];
                        $value = $v['value'];
                        $type = $v['type'];
                        $filterType = $v['filterType'];
                        $fieldDefinition = @$fields[$field];

                        // referencia: EloquentDataTable.php
                        // TODO avaliar futuramente
                        // Retirado porque DB:table("table")->join("other_table")-> ... retorna prefix errado
                        // $prefix = ($query instanceof Builder ? $query->getQuery() : $query)->from;
                        // $column = $prefix . '.' . $field;
                        $column = $field;

                        // 1 - Filtro adicionado diretamente no DataTables
                        if ($datatable->hasFilterColumn($field)) {
                            // throw new \Exception("A coluna {$field} possuí um filtro custom que esta sendo IGNORADO. P/ utilizar filtros custom, retirar a chamada 'baseFilter' e remover a chamada '\$definition->addFilterAndExport()' da view (blade). Adicione o filtro custom do Datatables, adicionando a letra 'f' nos parametros do DataTables ('dom')");
                            $datatable->publicApplyFilterColumn($datatable->getQuery(), $field, $value);
                            continue;
                        }

                        //Tratamento especial para filtro de locale
                        if ($filterType == 'locale') {
                            $newModel = new $model();
                            if (@array_search('locale', $newModel->getFillable()) !== false) {
                                $query->where('locale', '=', $value);
                            } elseif ($newModel->translatedAttributes) {
                                $query->translatedIn($value);
                            }
                            continue;
                        }

                        //2 - Filtro de um model
                        if (strpos($field, '.') !== false) {
                            //Terceiro formato, injetado via ServiceProvider um método novo para aceitar LIKE/NOTLIKE
                            if ($filterType == 'like' || $filterType == 'like2') {
                                $datatable->publicCompileQuerySearch(
                                    $datatable->getQuery(),
                                    $field,
                                    $value,
                                    'or',
                                    'LIKE',
                                );
                            } elseif ($filterType == 'notLike' || $filterType == 'notLike2') {
                                $datatable->publicCompileQuerySearch(
                                    $datatable->getQuery(),
                                    $field,
                                    $value,
                                    'or',
                                    'NOT LIKE',
                                );
                            } else {
                                //TODO: Tratar filterType = "=, >, < ..."
                                $datatable->publicCompileQuerySearch(
                                    $datatable->getQuery(),
                                    $field,
                                    $value,
                                    'or',
                                    $filterType,
                                );
                            }
                        } elseif ($fieldDefinition->data && $fieldDefinition->data instanceof BaseDataVirtualRawSql) {
                            //3 - Filtro com Raw SQL - Queries complexas
                            //Custom filter quando o "field" possue "data"
                            //Campo virtual, bypass getData() e getValue(), injeta o SQL direto no WHERE
                            if ($filterType == 'like' || $filterType == 'like2') {
                                $query->where(\DB::raw("({$fieldDefinition->data->sql})"), 'like', "%{$value}%");
                            } elseif ($filterType == 'notLike' || $filterType == 'notLike2') {
                                $query->where(\DB::raw("({$fieldDefinition->data->sql})"), 'not like', "%{$value}%");
                            } else {
                                self::filterSimple(
                                    $query,
                                    $type,
                                    $filterType,
                                    \DB::raw("({$fieldDefinition->data->sql})"),
                                    $value,
                                    $v,
                                );
                                // $query->where(\DB::raw("({$fieldDefinition->data->sql})"), $filterType, $value);
                            }
                        } elseif ($fieldDefinition->data && $fieldDefinition->data instanceof BaseDataEnum) {
                            if ($filterType == 'like' || $filterType == 'like2') {
                                $query->where($column, 'like', "%{$value}%");
                            } elseif ($filterType == 'notLike' || $filterType == 'notLike2') {
                                $query->where($column, 'not like', "%{$value}%");
                            } else {
                                $query->where($column, $filterType, $value);
                            }
                        } elseif ($fieldDefinition->data) {
                            //4 - Filtro com campo "data" setado no field do Definition
                            $data = $fieldDefinition->data->getData();

                            //Extrai todos os registros possíveis do select e faz uma busca por "String" nos valores do array
                            $found = collect($data)
                                ->filter(function ($item) use ($value, $filterType) {
                                    return stripos($item, $value) !== false;
                                })
                                ->keys();

                            $found[] = $value;

                            $query->where(function ($qq) use ($column, $found, $value, $filterType) {
                                if ($filterType == 'like' || $filterType == 'like2') {
                                    $qq->whereIn($column, $found);
                                } else {
                                    $qq->whereNotIn($column, $found);
                                }
                            });
                        } else {
                            //5 - Filtro com campos "simples"
                            self::filterSimple($query, $type, $filterType, $column, $value, $v);
                        }
                    }
                });
            }
        }

        return;
    }

    private static function filterSimple($query, $type, $filterType, $column, $value, $v)
    {
        switch ($type) {
            case BaseColumnConst::Number:
                if ($filterType == 'in') {
                    $query->whereIn(
                        $column,
                        array_map(function ($v) {
                            return trim($v);
                        }, explode(',', $value)),
                    );
                } else {
                    $query->where($column, $filterType, $value);
                }
                break;

            case BaseColumnConst::Boolean:
                $query->where($column, '=', $filterType == 'true' ? true : false);
                break;

            case BaseColumnConst::Date:
                $startOf = \Carbon\Carbon::parse($value)
                    ->startOfDay()
                    ->format('Y-m-d\TH:i:s');

                $endOf = \Carbon\Carbon::parse($value)
                    ->endOfDay()
                    ->format('Y-m-d\TH:i:s');

                if (@$v['value2']) {
                    $endOfValue2 = \Carbon\Carbon::parse($v['value2'])
                        ->endOfDay()
                        ->format('Y-m-d\TH:i:s');
                }

                switch ($filterType) {
                    case 'equal':
                        $query->whereBetween($column, [$startOf, $endOf]);
                        break;
                    case 'after':
                        $query->whereDate($column, '>', $startOf);
                        break;
                    case 'before':
                        $query->whereDate($column, '<', $endOf);
                        break;
                    case 'equalOrAfter':
                        $query->whereDate($column, '>=', $startOf);
                        break;
                    case 'equalOrBefore':
                        $query->whereDate($column, '<=', $endOf);
                        break;
                    case 'exist':
                        $query->whereNotNull($column);
                        break;
                    case 'not_exist':
                        $query->whereNull($column);
                        break;
                    case 'between':
                        $query->whereBetween($column, [$startOf, $endOfValue2]);
                        break;
                    default:
                        break;
                }
                break;
            case BaseColumnConst::Text:
            case BaseColumnConst::File:
            case BaseColumnConst::Image:
            case BaseColumnConst::Video:
            default:
                if ($filterType == 'like') {
                    $query->where($column, 'like', "%{$value}%");
                } elseif ($filterType == 'notLike') {
                    $query->where($column, 'not like', "%{$value}%");
                } elseif ($filterType == 'like2') {
                    $query->where($column, 'like', "%{$value}");
                } elseif ($filterType == 'notLike2') {
                    $query->where($column, 'not like', "%{$value}");
                }
                break;
        }
    }

    /**
     * Retorna um array de arrays com as colunas liberadas no arquivo de definição (BaseDefinition::table())
     *
     * Objeto consumido no Datatable:
     * [ { "data":"id", "name":"id" }, { "data":"name", "name":"name" }]
     *
     * @param BaseDefinition $definition
     * @return array
     */
    public static function getObjColumns(BaseDefinition $definition)
    {
        $ret = [];

        if ($definition->multipleSelectTable) {
            $ret[] = [
                'data' => 'multiple_select_table',
                'name' => 'multiple_select_table',
                'searchable' => false,
                'orderable' => false,
            ];
        }

        foreach ($definition->getTableFields() as $v) {
            $field = $definition->getFields()[$v];

            //Se for "relationship", seta valor com underline (funcionamento do Datatable p/ funcionar o relacionamento (eager))
            $ret[] = [
                'data' => strpos($v, '.') > -1 ? \Str::snake($v) : $v,
                'name' => $v,
                'searchable' => $field->filterable,
                'orderable' => $field->orderable,
            ];
        }

        return $ret;
    }

    /**
     * Retorna um array de strings (<th/>) com as colunas liberadas no arquivo de definição (BaseDefinition::table())
     *
     * @param BaseDefinition $definition
     * @param string $widthActions Tamanho da coluna "Ações"
     * @return string
     */
    public static function getThColumns(BaseDefinition $definition, $widthActions = '120px')
    {
        $ret = [];

        if ($definition->multipleSelectTable) {
            $ret[] = '<th width="40px">##</th>';
        }

        foreach ($definition->getTableFields() as $v) {
            $column = $definition->getFields()[$v];

            $width = $column->primaryKey ? 'width="60px"' : '';
            $ret[] = "<th $width>" . __($column->title) . '</th>';
        }
        $ret[] = '<th class="column-actions" width="' . $widthActions . '">' . __('Ações') . '</th>';

        return join("\n", $ret);
    }

    public static function getThColumnsNoAction(BaseDefinition $definition)
    {
        $ret = [];

        foreach ($definition->getTableFields() as $v) {
            $column = $definition->getFields()[$v];

            $width = $column->primaryKey ? 'width="60px"' : '';
            $ret[] = "<th $width>" . __($column->title) . '</th>';
        }

        return join("\n", $ret);
    }
}
