<?php

namespace Yajra\DataTables {
    /**
     * @method DataTableAbstract baseFilter(BaseDefinition $definition)
     * @method DataTableAbstract baseRender(BaseDefinition $definition, array $ignoreFields)
     */
    class DataTableAbstract
    {
    }
}
