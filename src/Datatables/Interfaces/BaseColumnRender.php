<?php

namespace BaseCms\Datatables\Interfaces;

use BaseCms\BaseField;

use Illuminate\Database\Eloquent\Model;

interface BaseColumnRender
{
    /**
     * @param string $field
     * @param BaseField $fieldDefinition
     * @param Model|object $row
     * @param string $routeAlias
     */
    public function __construct(string $field, BaseField $fieldDefinition, $row, string $routeAlias);

    /**
     * Render p/ a coluna
     *
     * Pode retoranr uma string ou uma view() (blade)

     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function render();
}
