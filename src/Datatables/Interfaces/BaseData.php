<?php

namespace BaseCms\Datatables\Interfaces;

interface BaseData
{
    /**
     * Normaliza o retorno de dados
     *
     * Ex: [
     *  [1=>'Nome 1'],
     *  [2=>'Nome 2']
     * ]
     *
     * @return array[array[int]string]
     */
    public function getData();

    /**
     * @param mixed $value Valor salvo na coluna da tabela
     * @param null $rowData Valores do registro da tabela (todos da mesma linha), não é usado ainda, mas talvez será ... rs
     *
     * @return mixed
     */
    public function getValue($value, $rowData = null);
}
