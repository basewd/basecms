<?php

namespace BaseCms\Helpers;

use BaseCms\BaseDefinition;
use BaseCms\Constants\BaseColumnConst;

class BaseFormRequestHelper
{
    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function rules(BaseDefinition $definition)
    {
        return self::_rules($definition, $definition->getFormFields());
    }

    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function rulesEdit(BaseDefinition $definition)
    {
        return self::_rules($definition, $definition->getFormEditFields());
    }

    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function attributes(BaseDefinition $definition)
    {
        return self::_attributes($definition, $definition->getFormFields());
    }

    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function attributesEdit(BaseDefinition $definition)
    {
        return self::_attributes($definition, $definition->getFormEditFields());
    }

    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function messages(BaseDefinition $definition)
    {
        return self::_messages($definition, $definition->getFormFields());
    }

    /**
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    public static function messagesEdit(BaseDefinition $definition)
    {
        return self::_messages($definition, $definition->getFormEditFields());
    }

    /**
     * Retorno as regras de cada coluna (BaseDefinition::form::columns::input->rule)
     *
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    private static function _rules(BaseDefinition $definition, $formFields = [])
    {
        $return = [];

        foreach ($formFields as $k => $v) {
            $column = $definition->getFields()[$v];

            if (@$column->rule) {
                //Ignora a regra quando for "PATCH", "<input type='file'/>" não retorna se usuário não subir um novo arquivo
                if (
                    in_array($column->type, [BaseColumnConst::File, BaseColumnConst::Image, BaseColumnConst::Video]) &&
                    request()->method() == 'PATCH' &&
                    request()->{$column->field . '_value_of_file'} &&
                    !request()->{$column->field}
                ) {
                    continue;
                }

                $return[$v] = $column->rule->rule;
            }
        }

        return $return;
    }

    /**
     * Retorno o nome dos atributos (BaseDefinition::form::columns::field->name)
     *
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    private static function _attributes(BaseDefinition $definition, $formFields = [])
    {
        $return = [];

        foreach ($formFields as $k => $v) {
            $column = $definition->getFields()[$v];
            $return[$column->field] = $column->title;
        }

        return $return;
    }

    /**
     * Retorno das mensagems de erro definidos no arquivo de definição (BaseDefinition::form::columns::input->rulesMessages)
     *
     * @param BaseDefinition $definition
     * @return array[string]string
     */
    private static function _messages(BaseDefinition $definition, $formFields = [])
    {
        $return = [];

        foreach ($formFields as $k => $v) {
            $column = $definition->getFields()[$v];

            $customMessages = @$column->rule->messages;

            if (is_array($customMessages)) {
                foreach (@$customMessages as $kMessage => $vMessage) {
                    $return[$v . '.' . $kMessage] = @$column->title . ' - ' . $vMessage;
                }
            }
        }

        return $return;
    }

    /**
     * @return array
     */
    public static function rulesPassword()
    {
        return ['required', 'string', 'min:6'];
    }
}
