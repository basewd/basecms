<?php

namespace BaseCms\Helpers;

use Exception;
use Illuminate\Database\QueryException;
use BaseCms\BaseDefinition;
use BaseCms\Datatables\Data\BaseDataEnum;
use App\Exceptions\GeneralException;

class BaseHelper
{
    /**
     * Retorna a url anterior caso exista.
     *
     * Caso não tenha, retorna a defaultUrl passada
     *
     * Essa função é utilizada para os botões de "voltar" das páginas (caderno de campo, formulários aplicados, plano de ação ... ) (view, edit, create ...)
     *
     * @param  string $defaultUrl
     *
     * @return string
     */
    public static function prevUrl($defaultUrl)
    {
        $backUrl = redirect()
            ->back()
            ->getTargetUrl();
        $currentUrl = url()->current();

        //Previne entrar direto na url e não ter "url" para voltar. url()->previous() retorna a última url gerada pelo sistema, não é a mesma coisa que o redirect()->back()
        if ($backUrl && $backUrl !== $currentUrl) {
            return redirect()
                ->back()
                ->getTargetUrl();
        } else {
            return $defaultUrl;
        }
    }

    /**
     * Combines SQL and its bindings
     *
     * @param \Eloquent $query
     *
     * @return string
     */
    public static function getEloquentSqlWithBindings($query)
    {
        return vsprintf(
            str_replace('?', '%s', $query->toSql()),
            collect($query->getBindings())
                ->map(function ($binding) {
                    $binding = addslashes($binding);
                    return is_numeric($binding) ? $binding : "'{$binding}'";
                })
                ->toArray(),
        );
    }

    /**
     * Formata a data em um "datetime local", para ser consumido pelo <input type="datetime-local"/>
     *
     * @param mixed $datetime
     *
     * @return string
     */
    public static function datetimeLocal($datetime)
    {
        return \Carbon\Carbon::parse($datetime)->format('Y-m-d\TH:i');
    }

    /**
     *
     * Formata a mensagem da Exception para algo "legível"
     *  - Exception de imagem inválida
     *
     * @param QueryException $e
     *
     * @return Exception
     */
    public static function dispatchBeatifulException(Exception $e, BaseDefinition $baseDefinition = null)
    {
        if ($e instanceof \Intervention\Image\Exception\NotReadableException) {
            throw new GeneralException($e->getMessage());
        } else {
            throw $e;
        }
    }

    /**
     *
     * Formata a mensagem da Exception para algo "legível"
     *  - null
     *  - duplicated
     *  - cannot deleted
     *
     * @param QueryException $e
     *
     * @return Exception
     */
    public static function dispatchBeatifulQueryException(QueryException $e, BaseDefinition $baseDefinition = null)
    {
        $errorInfoCode = $e->errorInfo[1];

        // SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry
        if ($errorInfoCode == 1062) {
            preg_match("/1062 Duplicate entry '(.*)' for.*key ((')(.*)(')|.*)/", trim($e->getMessage()), $matches);
            $extractIndex = explode('.', $matches[4]);
            $index = array_pop($extractIndex);
            $value = $matches[1];

            if (!$baseDefinition) {
                $message = sprintf('\'%s\' com valor \'%s\' já foi registrado.', $index, $value);
            } else {
                //Formata com o "label" da Coluna setado no Definition
                $indexes = \DB::connection()
                    ->getDoctrineSchemaManager()
                    ->listTableIndexes($baseDefinition->getTable());

                if (@$indexes[strtolower($index)]) {
                    $columns = $indexes[strtolower($index)]->getColumns();
                    $fields = $baseDefinition->getFields();

                    $labels = [];
                    foreach ($columns as $k => $v) {
                        $columnBaseField = $fields[$v];
                        $labels[] = $columnBaseField->title;
                    }

                    if ($columnBaseField->data instanceof BaseDataEnum) {
                        if (@$columnBaseField->data->getValue($value)) {
                            $value = $columnBaseField->data->getValue($value);
                        }
                    }
                    $message = __(':labels com valor :value já foi registrado.', [
                        'labels' => implode(',', $labels),
                        'value' => $value,
                    ]);
                } else {
                    $message = sprintf('\'%s\' com valor \'%s\' já foi registrado.', $index, $value);
                }
            }

            throw new GeneralException($message);
        } elseif ($errorInfoCode == 1048) {
            //SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'title' cannot be null
            preg_match("/1048 Column '(.*)' .*/", trim($e->getMessage()), $matches);
            $column = $matches[1];
            if (@$baseDefinition) {
                $column = $baseDefinition->getFields()[$column]->title;
            }
            $message = __('Campo :column não pode ser nulo.', ['column' => $column]);

            throw new GeneralException($message);
        } elseif ($errorInfoCode == 1451) {
            //SQLSTATE[23000]: Integrity constraint violation: 1451 Cannot delete or update a parent row: a foreign key constraint fails
            $reg = '/`([^`]+)`/m';
            $value = $e->getMessage();
            $matches = [];
            preg_match_all($reg, $value, $matches);

            $table = str_replace('_', ' ', str_replace('`', '', $matches[0][1]));
            throw new GeneralException(
                __('Não foi possível remover, o registro esta sendo utilizado em outra sessão.') . ' (' . $table . ')',
            );
        } elseif ($errorInfoCode == 1406) {
            //SQLSTATE[22001]: String data, right truncated: 1406 Data too long for column 'name' at row 1
            preg_match("/column '(.*?)'/", trim($e->getMessage()), $matches);

            $column = $matches[1];
            if (@$baseDefinition) {
                $column = $baseDefinition->getFields()[$column]->title;
            }
            $message = __('Campo ":column" muito longo.', ['column' => $column]);

            throw new GeneralException($message);
        } elseif ($errorInfoCode == 1292) {
            //SQLSTATE[22007]: Invalid datetime format: 1292 Incorrect datetime value: '1111-11-11 11:11:00' for column 'start_at' at row 1
            preg_match("/column '(.*?)'/", trim($e->getMessage()), $matches);

            $column = $matches[1];
            if (@$baseDefinition) {
                $column = $baseDefinition->getFields()[$column]->title;
            }
            $message = __('Formato de data inválida, campo ":column".', ['column' => $column]);

            throw new GeneralException($message);
        }

        throw new GeneralException($e->getMessage());
    }
}
