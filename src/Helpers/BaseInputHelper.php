<?php

namespace BaseCms\Helpers;

use BaseCms\BaseDefinition;
use BaseCms\BaseField;
use BaseCms\Datatables\Data\BaseDataVirtualRawSql;

class BaseInputHelper
{
    /**
     * Retorna os elementos do formulário (utilizado na blade) de cadastro/edição (create_update.blade.php)
     *
     * Utilizando os parametros columnStart e columnEnd é possível mesclar outros componentes dentro do formulário.
     *
     * Ex:
     *
     * {!! BaseInputHelper::formInputs($definition, 'id', 'title'); !!}
     * <input type="text" name="middle_field"/>
     * {!! BaseInputHelper::formInputs($definition, 'title', 'created_at'); !!}
     *
     * @param BaseDefinition $definition
     * @param Model|array|null $model
     * @param string|null $columnStart
     * @param string|null $columnEnd
     *
     * @return string
     */
    public static function formInputs(
        BaseDefinition $definition,
        $model = null,
        string $columnStart = null,
        string $columnEnd = null
    ) {
        $ret = [];

        $listColumns = $model ? $definition->getFormEditFields() : $definition->getFormFields();

        //Permite retornar um "range" de campos" p/ ser adicionado alguma outra coisa no front
        if ($columnStart && $columnEnd) {
            $start = array_search($columnStart, $listColumns);
            $end = array_search($columnEnd, $listColumns);

            $listColumns = array_slice($listColumns, $start, $end - $start + 1);
        }

        foreach ($listColumns as $v) {
            $column = $definition->getFields()[$v];

            if ($column->data instanceof BaseDataVirtualRawSql) {
                throw new \Exception(
                    'Field: ' .
                        $v .
                        '. Este campo só pode utilizado na tabela porque ele trabalha com queries complexas. "BaseDataVirtualRawSql"',
                );
            }

            $ret[] = self::make($column, $model);
        }

        return join("\n", $ret);
    }

    /**
     * Retorna o componente do formulário
     *
     * @param BaseField $column
     * @param Model|array|null $model
     *
     * @return mixed
     */
    public static function make(BaseField $column, $model = null)
    {
        $class = null;
        $method = null;
        $inputMethods = @$column->inputMethods;

        if (strpos($column->input, '.') !== false) {
            $input = explode('.', $column->input);
            $method = array_pop($input);
            $class = resolve(join('.', $input));
        }

        $data = @$column->data;

        //Custom Render Input
        if ($column->renderInput) {
            //Custom render (Function)
            $customData = [];
            if (isset($data)) {
                $customData = $data->getData();
            }

            $method = $column->renderInput;
            return $method($column, $customData, $model);
        }

        //Aire
        if ($class instanceof \Galahad\Aire\Support\Facades\Aire) {
            //Se for algum object que possui "DATA", o "DATA" vai antes do "FIELD" E "NAME"
            if (isset($data) || $column->input == 'Aire.select') {
                $customData = $data ? $data->getData() : [];

                $input = $class::$method($customData, $column->field, $column->title);
            } elseif ($column->input == 'Aire.hidden') {
                $input = $class::$method($column->field);
            } else {
                $input = $class::$method($column->field, $column->title);
            }
        } elseif (class_exists($column->input)) {
            //Elemento Genérico (BaseInput)
            $customData = [];
            if (isset($data)) {
                $customData = $data->getData();
            }

            $class = $column->input;
            $input = new $class($column, $customData, $model);
        }

        if (!@$input) {
            throw new \Exception('Invalid input render ' . $column->field . ' / ' . @get_class($class));
        }

        if ($column->uploadDefinition && $class instanceof \Galahad\Aire\Support\Facades\Aire) {
            if (!$column->uploadDefinition->saveWithFolder) {
                $inputMethods[] = ['data', 'path', $column->uploadDefinition->path];
            }
        }

        self::callMethodParams($input, $inputMethods);

        return $input;
    }

    /**
     * Chamada dos métodos de um objeto
     *
     * Ex: Elementos do tipo AIRE -> https://airephp.com/working-with-elements)
     *
     * callMethodParams(
     *   $input, [
     *     ['placeholder' => 'Teste]
     *   ]
     * );
     *
     * @param mixed $object
     * @param mixed $methodParams
     * @param null $allowedMethods
     *
     * @return mixed
     */
    public static function callMethodParams($object, $methodParams, $allowedMethods = null)
    {
        if (!$methodParams || (is_array($methodParams) && count($methodParams) == 0)) {
            return;
        }

        //Normalize methods
        if (!is_array($methodParams)) {
            $methodParams = [$methodParams];
        }

        $resultFn = $object;
        foreach ($methodParams as $v) {
            $method = null;
            //Pode receber um array (primeiro valor do array é o método, resto é parametro)
            //Ou string, que é o método direto sem parametros
            if (is_array($v)) {
                $method = array_shift($v);
            } else {
                $method = $v;
                $v = null;
            }

            $params = !$v || count($v) == 0 ? null : $v;

            //Valida se contém métodos validos (por hora só testa o Model)
            if ($allowedMethods && !in_array($method, $allowedMethods)) {
                throw new \Exception(
                    'Invalid method ' . get_class($object) . ' - ' . $method . ' - ' . json_encode($params),
                );
            }

            if (is_array($params)) {
                $resultFn = call_user_func_array([$resultFn, $method], $params);
            } elseif ($params) {
                $resultFn = call_user_func([$resultFn, $method], $params);
            } else {
                $resultFn = call_user_func([$resultFn, $method]);
            }
        }

        return $resultFn;
    }
}
