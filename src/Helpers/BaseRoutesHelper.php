<?php

namespace BaseCms\Helpers;

use Tabuna\Breadcrumbs\Breadcrumbs;
use Route;

class BaseRoutesHelper
{
    /**
     * Cria as rotas defaults com base de uma lógica simples (linkando com os Policies dos Models):
     *  - listagem
     *  - api da listagem (datatable)
     *  - view
     *  - create
     *  - store
     *  - edit
     *  - update
     *  - delete
     *  - order (up, down, start, end)
     *  - export
     *  - checkValue (render custom na tabela, p/ selecionar um boolean)
     *  - radioValue (render custom na tabela, p/ selecionar um boolean)
     *
     * @param string $controllerClass
     * @param string $modelClass
     * @param string $prefixAlias
     * @param string $routeAlias
     * @param array $include
     * @param string $modelName
     *
     * @return void
     */
    public static function makeGroupRoutes(
        $controllerClass,
        $modelClass,
        $prefixAlias,
        $routeAlias,
        array $include = ['list', 'view', 'create', 'edit', 'delete'],
        $modelName = 'model'
    ) {
        Route::group(['prefix' => $prefixAlias, 'as' => $routeAlias . '.'], function () use (
            $controllerClass,
            $modelClass,
            $include,
            $modelName
        ) {
            if (in_array('list', $include)) {
                Route::match(['post', 'get'], '/datatable', [$controllerClass, 'datatable'])
                    ->middleware('can:list,' . $modelClass)
                    ->name('datatable');
                Route::get('/', [$controllerClass, 'index'])
                    ->middleware('can:list,' . $modelClass)
                    ->name('index');
            }

            if (in_array('view', $include)) {
                Route::get('/view/{' . $modelName . '}', [$controllerClass, 'view'])
                    ->middleware('can:view,' . $modelName)
                    ->name('view');
            }

            if (in_array('create', $include)) {
                Route::get('/create', [$controllerClass, 'create'])
                    ->middleware('can:create,' . $modelClass)
                    ->name('create');
                Route::post('/', [$controllerClass, 'store'])
                    ->middleware('can:create,' . $modelClass)
                    ->name('store');
            }

            if (in_array('edit', $include)) {
                Route::get('/{' . $modelName . '}/edit', [$controllerClass, 'edit'])
                    ->middleware('can:update,' . $modelName)
                    ->name('edit');
                Route::patch('/{' . $modelName . '}', [$controllerClass, 'update'])
                    ->middleware('can:update,' . $modelName)
                    ->name('update');
            }

            if (in_array('delete', $include)) {
                Route::delete('/{' . $modelName . '}', [$controllerClass, 'destroy'])
                    ->middleware('can:delete,' . $modelName)
                    ->name('destroy');
            }

            if (in_array('orderable', $include)) {
                Route::post('/{' . $modelName . '}/orderUp', [$controllerClass, 'orderUp'])
                    ->middleware('can:update,' . $modelName)
                    ->name('orderUp');
                Route::post('/{' . $modelName . '}/orderDown', [$controllerClass, 'orderDown'])
                    ->middleware('can:update,' . $modelName)
                    ->name('orderDown');
                Route::post('/{' . $modelName . '}/orderStart', [$controllerClass, 'orderStart'])
                    ->middleware('can:update,' . $modelName)
                    ->name('orderStart');
                Route::post('/{' . $modelName . '}/orderEnd', [$controllerClass, 'orderEnd'])
                    ->middleware('can:update,' . $modelName)
                    ->name('orderEnd');
                Route::post('/{' . $modelName . '}/orderTo', [$controllerClass, 'orderTo'])
                    ->middleware('can:update,' . $modelName)
                    ->name('orderTo');
            }

            if (in_array('restore', $include)) {
                Route::post('/{' . $modelName . 'Deleted}/restore', [$controllerClass, 'restore'])
                    ->middleware('can:restore,' . $modelName . 'Deleted')
                    ->name('restore');
                Route::match(['post', 'get'], '/datatableRestore', [$controllerClass, 'datatableRestore'])
                    ->middleware('can:listRestore,' . $modelClass)
                    ->name('datatableRestore');
                Route::get('/indexRestore', [$controllerClass, 'indexRestore'])
                    ->middleware('can:listRestore,' . $modelClass)
                    ->name('indexRestore');
            }

            if (in_array('forceDelete', $include)) {
                Route::delete('/{' . $modelName . 'Deleted}/forceDelete', [$controllerClass, 'forceDelete'])
                    ->middleware('can:forceDelete,' . $modelName . 'Deleted')
                    ->name('forceDelete');
            }

            if (in_array('checkValue', $include)) {
                Route::post('/{' . $modelName . '}/checkValue', [$controllerClass, 'checkValue'])
                    ->middleware('can:update,' . $modelName)
                    ->name('checkValue');
            }

            if (in_array('radioValue', $include)) {
                Route::post('/{' . $modelName . '}/radioValue', [$controllerClass, 'radioValue'])
                    ->middleware('can:update,' . $modelName)
                    ->name('radioValue');
            }

            if (in_array('exportable', $include)) {
                Route::match(['get', 'post'], '/exportCsv', [$controllerClass, 'exportCsv'])->name('exportCsv');
            }

            if (in_array('multipleSelect', $include)) {
                Route::match(['post'], '/multipleSelect', [$controllerClass, 'multipleSelect'])->name('multipleSelect');
            }
        });

        return true;
    }

    /**
     * Cria os breadcrumbs das rotas defaults criadas pelo método "BaseRoutesHelper::makeGroupRoutes"
     *
     * @param string $title
     * @param string $routeAlias
     *
     * @return void
     */
    public static function makeBreadcrumbsRoutes($titlePlural, $title, $routeAlias)
    {
        /**
         * @var BaseDefinition
         */
        Breadcrumbs::for('admin.' . $routeAlias, function ($trail) use ($title, $titlePlural, $routeAlias) {
            $trail->push($titlePlural, route('admin.' . $routeAlias . '.index'));
        });

        Breadcrumbs::for('admin.' . $routeAlias . '.index', function ($trail) use ($title, $routeAlias) {
            $trail->parent('admin.' . $routeAlias);
            $trail->push(__('Listagem'));
        });

        Breadcrumbs::for('admin.' . $routeAlias . '.indexRestore', function ($trail) use ($title, $routeAlias) {
            $trail->parent('admin.' . $routeAlias);
            $trail->push(__('Listagem - Restaurar'));
        });

        Breadcrumbs::for('admin.' . $routeAlias . '.view', function ($trail) use ($title, $routeAlias) {
            $trail->parent('admin.' . $routeAlias);
            $trail->push(__('Visualizar') . ' ' . $title);
        });

        Breadcrumbs::for('admin.' . $routeAlias . '.create', function ($trail) use ($title, $routeAlias) {
            $trail->parent('admin.' . $routeAlias);
            $trail->push(__('Adicionar') . ' ' . $title);
        });

        Breadcrumbs::for('admin.' . $routeAlias . '.edit', function ($trail) use ($title, $routeAlias) {
            $trail->parent('admin.' . $routeAlias);
            $trail->push(__('Editar') . ' ' . $title);
        });

        return true;
    }
}
