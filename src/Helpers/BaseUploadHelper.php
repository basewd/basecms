<?php

namespace BaseCms\Helpers;

use BaseCms\BaseBlockBuilder\Helpers\BlockBuilderHelper;
use BaseCms\BaseDefinition;
use BaseCms\BaseUploadDefinition;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Jobs\ImageSrcsetResizeJob;
use Closure;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BaseUploadHelper
{
    /**
     * Método default para fazer upload de arquivos a partir de um campo com uma definição de Upload (BaseUploadDefinition)
     *
     * @param Model $model
     * @param BaseDefinition $definition
     *
     * Retorna os paths por field
     *
     * @return array[string]string
     */
    public static function saveFilesFromRequest(Model $model, BaseDefinition $definition)
    {
        //[TRICK] propagation model to create blocks when need
        BlockBuilderHelper::createBlockBuilderIds($model, $definition);

        //[START] saveFilesFromRequest
        $fieldsFile = $definition->getFieldsType([
            BaseColumnConst::File,
            BaseColumnConst::Image,
            BaseColumnConst::Video,
        ]);

        $uploads = [];

        foreach ($fieldsFile as $k => $v) {
            $file = request()->file($v->field);

            if ($file && $file->getError()) {
                throw new FileException($file->getErrorMessage());
            }

            //Verifica se tem arquivo p/ fazer upload
            if ($file) {
                if (!$v->uploadDefinition) {
                    throw new \Exception(
                        'Você precisa setar o "BaseUploadDefinition" no BaseField. Campo: ' .
                            $v->field .
                            ', método setUploadDefinition(new BaseUploadDefinition(path))',
                    );
                }

                $filenameMultiple = [];

                $filename = self::saveFileUpload($file, $model, $v->uploadDefinition);

                //Return
                $uploads[$v->field] = $filename;

                //Multiplos Resizes
                if (@$v->uploadDefinition->multipleResizes) {
                    $filenameMultiple['default'] = $filename;
                    foreach ($v->uploadDefinition->multipleResizes as $vResizeUploadDefinition) {
                        $filenameResize = self::saveFileUpload($file, $model, $vResizeUploadDefinition);
                        $kPath = explode('/', $vResizeUploadDefinition->path);
                        $filenameMultiple[end($kPath)] = $filenameResize;
                    }
                    $uploads[$v->field . '_resize'] = $filenameMultiple;
                }

                //Update model field
                if (array_search($v->field, $model->getFillable()) !== false) {
                    if (@$v->uploadDefinition->multipleResizes) {
                        $model->setRawAttributes(
                            [$v->field => $filename, $v->field . '_resize' => json_encode($filenameMultiple)] +
                                $model->getAttributes(),
                        );
                    } else {
                        $model->{$v->field} = $filename;
                    }

                    $model->save();
                }

                //SrcSet
                if (@$v->uploadDefinition->srcSet) {
                    $urlImage = \Storage::url($filename);
                    if (app()->isLocal() && config('queue.default') == 'sync') {
                        //Bypass artisan in queue sync, run 8001 to work
                        $urlImage = str_replace('8000', '8001', $urlImage);
                    }
                    ImageSrcsetResizeJob::dispatch(
                        $urlImage,
                        $v->uploadDefinition->path,
                        $v->uploadDefinition->srcSet,
                        get_class($model),
                        $v->field,
                        $model->{$model->getKeyName()},
                    );
                }
            } elseif (!request()->{$v->field . '_value_of_file'}) {
                //Implementação p/ conseguir remover um arquivo do <input type="file"/>
                if (array_search($v->field, $model->getFillable()) !== false) {
                    $model->{$v->field} = null;

                    if (@$v->uploadDefinition->multipleResizes) {
                        $model->{$v->field . '_resize'} = null;
                    }

                    $model->save();

                    $uploads[$v->field] = '';
                }
            } elseif (@request()->{$v->field . '_value_of_file'}) {
                //Mantem estrutura do arquivo antigo
                $uploads[$v->field] = request()->{$v->field . '_value_of_file'};
            }
        }

        return $uploads;
    }

    /**
     * @param  mixed $fileRequest
     * @param  Model $model
     * @param  BaseUploadDefinition $uploadDefinition
     *
     * @return string
     */
    private static function saveFileUpload($fileRequest, $model, BaseUploadDefinition $uploadDefinition)
    {
        //Resize if type "Image"
        if ($uploadDefinition->maxWidth || $uploadDefinition->maxHeight || $uploadDefinition->watermark) {
            BaseUploadHelper::resizeImage($fileRequest, $uploadDefinition);

            if ($uploadDefinition->onAfterResize) {
                $fn = $uploadDefinition->onAfterResize;
                $fn($fileRequest, $uploadDefinition);
            }
        } else {
            if (@$uploadDefinition->onAfterResize) {
                $fn = $uploadDefinition->onAfterResize;
                $fn($fileRequest, $uploadDefinition);
            }
        }

        //Upload file
        $filename = BaseUploadHelper::uploadFile(
            $fileRequest,
            $uploadDefinition->path,
            $model,
            $uploadDefinition->resolverFilename,
            @$uploadDefinition->seoFriendlyColumnName,
        );

        //Ao salvar no banco, concatena folder (Default:false)
        if ($uploadDefinition->saveWithFolder) {
            $filename = $uploadDefinition->path . '/' . $filename;
        }

        return $filename;
    }

    /**
     * Upload do arquivo no Storage
     *
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]|array|null $fileRequest
     * @param string $path
     * @param Model $model
     *
     * @return string
     */
    private static function uploadFile(
        $fileRequest,
        string $path,
        Model $model,
        Closure $fnResolverName = null,
        $seoFriendlyColumnName = null
    ) {
        if (!@$fnResolverName) {
            $filename = self::resolverFilename(
                $model,
                $fileRequest,
                config('base.upload.filename.withId', true),
                config('base.upload.filename.withFilename', true),
                config('base.upload.filename.withUniq', false),
                @$seoFriendlyColumnName ? @$model->{$seoFriendlyColumnName} : null,
            );
        } else {
            //Custom resolver
            $filename = $fnResolverName($model, $fileRequest);
        }

        $fullpath = $path . '/' . $filename;

        //Upload
        \Storage::put($fullpath, \fopen($fileRequest->getRealPath(), 'r+'));

        return $filename;
    }

    /**
     * Resolver padrão do nome do arquivo
     *
     * É possível utilizar esse mesmo método para retornar outras variações de nome de arquivo, o objetivo foi simplificar
     *
     * @param Model $model
     * @param \Illuminate\Http\UploadedFile $fileRequest
     *
     * @return string
     */
    public static function resolverFilename(
        $model,
        $fileRequest,
        $withId = true,
        $withFilename = true,
        $withUniq = false,
        $forceFilename = ''
    ) {
        $extension = @$fileRequest->baseExtension ?: $fileRequest->guessClientExtension();

        $filename = [];

        if ($withFilename) {
            $v = explode('.', @$forceFilename ? $forceFilename . '.' : $fileRequest->getClientOriginalName());
            array_pop($v);
            $filename[] = implode('.', $v);
        }

        if ($withUniq) {
            $filename[] = uniqid();
        }

        if ($withId) {
            $filename[] = $model->{$model->getKeyName()};
        }

        $name = \Str::snake(strtolower(\Str::ascii(implode('-', $filename))), '-');
        $name = preg_replace('/[^A-Za-z0-9-_\. ]/', '', $name);

        return $name . '.' . $extension;
    }

    /**
     * Resize da imagem através do BaseUploadDefinition
     *
     * Referência: http://image.intervention.io/api/resize
     *
     * @param \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]|array|null $fileRequest
     * @param BaseUploadDefinition $uploadDefinition
     *
     * @return \Illuminate\Http\UploadedFile
     */
    private static function resizeImage($fileRequest, BaseUploadDefinition $uploadDefinition)
    {
        if (!$uploadDefinition->maxWidth && !$uploadDefinition->maxHeight && !$uploadDefinition->watermark) {
            return $fileRequest;
        }

        if (@$fileRequest->guessClientExtension() === 'svg') {
            return $fileRequest;
        }

        $img = Image::make($fileRequest->getRealPath());

        if ($uploadDefinition->encode) {
            $currentMime = $img->mime();
            if (strpos($currentMime, $uploadDefinition->encode) === false) {
                //Otimização //Intervene não faz o tratamento
                $img->encode($uploadDefinition->encode, $uploadDefinition->encodeQuality);
                $fileRequest->baseMimeType = $img->mime();
                $fileRequest->baseExtension =
                    \Symfony\Component\Mime\MimeTypes::getDefault()->getExtensions($fileRequest->baseMimeType)[0] ??
                    null;
            } else {
                $fileRequest->baseMimeType = $currentMime;
                $fileRequest->baseExtension =
                    \Symfony\Component\Mime\MimeTypes::getDefault()->getExtensions($currentMime)[0] ?? null;
            }
        }

        //Resize
        if ($uploadDefinition->maxWidth || $uploadDefinition->maxHeight) {
            if (
                ($uploadDefinition->maxWidth && $uploadDefinition->maxWidth != $img->getWidth()) ||
                ($uploadDefinition->maxHeight && $uploadDefinition->maxHeight != $img->getHeight())
            ) {
                if ($uploadDefinition->cover) {
                    $img->fit(
                        $uploadDefinition->maxWidth,
                        $uploadDefinition->maxHeight,
                        function ($constraint) use ($uploadDefinition) {
                            if ($uploadDefinition->onlyLargeImage) {
                                $constraint->upsize();
                            }
                        },
                        $uploadDefinition->position,
                    );
                } else {
                    $img->resize(
                        $uploadDefinition->maxWidth,
                        $uploadDefinition->maxHeight,
                        function ($constraint) use ($uploadDefinition) {
                            $constraint->aspectRatio();

                            if ($uploadDefinition->onlyLargeImage) {
                                $constraint->upsize();
                            }
                        },
                        $uploadDefinition->position,
                    );
                }
            }
        }

        //Watermark
        if ($uploadDefinition->watermark) {
            $img->insert(
                $uploadDefinition->watermark['imagePath'],
                $uploadDefinition->watermark['position'],
                $uploadDefinition->watermark['offsetX'],
                $uploadDefinition->watermark['offsetY'],
            );
        }

        //Workaround porque o método ->save() do "Intervention" não funciona com arquivos sem extensão (tmp_file())
        file_put_contents($fileRequest->getPathName(), $img->stream(null, $uploadDefinition->encodeQuality));

        return $fileRequest;
    }
}
