<?php

namespace BaseCms\Helpers;

class BaseValidatorHelper
{
    /**
     * Implementação da validação do CPF
     *
     * Referência: https://github.com/geekcom/validator-docs/blob/master/src/validator-docs/Rules/Cpf.php
     *
     * @param string $attribute
     * @param string  $value
     * @param mixed $parameters
     * @param mixed $validator
     *
     * @return bool
     */
    public static function isValidCpf($attribute, $value, $parameters, $validator)
    {
        $c = preg_replace('/[^\d]/', '', $value);

        if (mb_strlen($c) != 11 || preg_match("/^{$c[0]}{11}$/", $c)) {
            return false;
        }

        for ($s = 10, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--) {
        }

        if ($c[9] != (($n %= 11) < 2 ? 0 : 11 - $n)) {
            return false;
        }

        for ($s = 11, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--) {
        }

        if ($c[10] != (($n %= 11) < 2 ? 0 : 11 - $n)) {
            return false;
        }

        return true;
    }

    /**
     * Implementação da validação do Cnpj
     *
     * Ref: https://github.com/geekcom/validator-docs/blob/master/src/validator-docs/Rules/Cnpj.php
     *
     * @param string $attribute
     * @param string $value
     * @param mixed $parameters
     * @param mixed $validator
     *
     * @return bool
     */
    public static function isValidCnpj($attribute, $value, $parameters, $validator)
    {
        $c = preg_replace('/[^\d]/', '', $value);

        if (mb_strlen($c) != 14 || preg_match("/^{$c[0]}{14}$/", $c)) {
            return false;
        }

        $b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

        for ($i = 0, $n = 0; $i < 12; $n += $c[$i] * $b[++$i]) {
        }

        if ($c[12] != (($n %= 11) < 2 ? 0 : 11 - $n)) {
            return false;
        }

        for ($i = 0, $n = 0; $i <= 12; $n += $c[$i] * $b[$i++]) {
        }

        if ($c[13] != (($n %= 11) < 2 ? 0 : 11 - $n)) {
            return false;
        }

        return true;
    }
}
