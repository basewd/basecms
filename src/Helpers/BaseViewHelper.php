<?php

namespace BaseCms\Helpers;

use BaseCms\BaseDefinition;
use BaseCms\BaseField;
use BaseCms\Constants\BaseColumnConst;
use BaseCms\Constants\BaseInputConst;

/**
 * Classe EXPERIMENTAL
 *
 * Não sei se faz sentido fazer a "view" ser automatica, acredito que as telas são muito dinâmicas
 *
 * As informações podem vir de vários locais, mesclando muitas tabelas ...
 *
 * Fazer automatico engessa a aplicação, dificultando a vida.
 *
 * PS: UTILIZAR SOMENTE SE A PÁGINA DE VISUALIZAÇÃO É "SIMPLES"
 */
class BaseViewHelper
{
    /**
     * Retorna os elementos (utilizado na blade) de visualização (view.blade.php)
     *
     * @param BaseDefinition $definition
     * @param Model|array|null $model
     * @param string|null $columnStart
     * @param string|null $columnEnd
     *
     * @return string
     */
    public static function viewFields(
        BaseDefinition $definition,
        $model = null,
        string $columnStart = null,
        string $columnEnd = null
    ) {
        $ret = [];

        $listColumns = $definition->getViewFields();

        //Permite retornar um "range" de campos" p/ ser adicionado alguma outra coisa no front
        if ($columnStart && $columnEnd) {
            $start = array_search($columnStart, $listColumns);
            $end = array_search($columnEnd, $listColumns);

            $listColumns = array_slice($listColumns, $start, $end - $start + 1);
        }

        foreach ($listColumns as $v) {
            $column = $definition->getFields()[$v];

            $ret[] = self::renderElement($column, $model, $definition);
        }

        return join("\n", $ret);
    }

    /**
     * Retorna uma Datatable Simples p/ ser utilizado p/ visualizaçào dos dados
     *
     * @param string $id
     * @param string $title
     * @param mixed $data
     * @param array $columns
     * @param array $columnsData
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public static function viewDataTable(string $id, string $title, $data, array $columns, array $columnsData)
    {
        return view(
            'base::components.datatable-view',
            compact('id', 'title', 'data', 'columns', 'columnsData'),
        )->render();
    }

    /**
     *
     * Render do elemento
     *
     * @param BaseField $column
     * @param Model|array|null $model
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    private static function renderElement(BaseField $baseField, $model = null, BaseDefinition $definition)
    {
        $label = $baseField->title;
        $value = $html = $baseField->getValue($model);

        //Boolean
        if ($baseField->type == BaseColumnConst::Boolean) {
            $html = @$value ? 'Sim' : 'Não';
        } elseif (!$value) {
            $html = '';
        } else {
            //Data com Time
            if ($baseField->type == BaseColumnConst::Date && $baseField->input == BaseInputConst::AireDateTime) {
                $html = $value ? \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y H:i') : '';
            } elseif ($baseField->type == BaseColumnConst::Date && $baseField->input == BaseInputConst::AireTime) {
                $html = $value ? \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'H:i') : '';
            } elseif ($baseField->type == BaseColumnConst::Date) {
                $html = $value ? \Timezone::convertToLocal(\Carbon\Carbon::parse($value), 'd/m/Y') : '';
            }

            //Data no timezone
            if (
                $baseField->type == BaseColumnConst::DateNoTimezone &&
                $baseField->input == BaseInputConst::AireDateTime
            ) {
                $html = $value ? \Carbon\Carbon::parse($value)->format('d/m/Y H:i') : '';
            } elseif (
                $baseField->type == BaseColumnConst::DateNoTimezone &&
                $baseField->input == BaseInputConst::AireTime
            ) {
                $html = $value ? \Carbon\Carbon::parse($value)->format('H:i') : '';
            } elseif ($baseField->type == BaseColumnConst::DateNoTimezone) {
                $html = $value ? \Carbon\Carbon::parse($value)->format('d/m/Y') : '';
            }

            //Imagem
            if ($baseField->type == BaseColumnConst::Image) {
                $html =
                    '<a href="' .
                    $value .
                    '" target="_blank"> <img style="width:250px; height:auto;" src="' .
                    $value .
                    '"/> </a>';
            }

            //Arquivo
            if ($baseField->type == BaseColumnConst::File) {
                $html = '<a href="' . $value . '" target="_blank">Visualizar arquivo</a>';
            }

            //Vimeo
            if ($baseField->input == BaseInputConst::Vimeo) {
                $html = '<a href="https://vimeo.com/' . $value . '" target="_blank">' . $value . '</a>';
            }
        }

        return view('base::components.field-view', compact('label', 'html'))->render();
    }
}
