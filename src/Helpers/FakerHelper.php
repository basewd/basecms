<?php

namespace BaseCms\Helpers;

/**
 * @see https://github.com/fzaninotto/Faker/issues/1884
 */
class FakerHelper
{
    /**
     * Generate the URL that will return a random image
     *
     * Set randomize to false to remove the random GET parameter at the end of the url.
     *
     * @param integer $width
     * @param integer $height
     * @param string|null $_category NOT USED
     * @param bool $randomize
     * @param string|null $_word NOT USED
     * @param bool $gray
     *
     * @return string
     * @see vendor/fzaninotto/faker/src/Faker/Provider/Image.php > imageUrl
     */
    public static function getImageUrl($width = 640, $height = 480)
    {
        $baseUrl = 'https://dummyimage.com/';

        if ($width == $height) {
            return $baseUrl . $width;
        }
        return $baseUrl . $width . 'x' . $height;
    }

    public static function getVideoUrl()
    {
        $videoUrls = [
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4',
            'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4',
        ];

        return $videoUrls[array_rand($videoUrls)];
    }
}
