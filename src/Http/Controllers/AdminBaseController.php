<?php

namespace BaseCms\Http\Controllers;

use Illuminate\Http\Request;
use BaseCms\Http\Controllers\Controller;

/**
 * Class AdminBaseController.
 */
class AdminBaseController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function quillUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file_upload' => config('boilerplate.richeditor.formrequest_upload', 'required|image|max:2048'),
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 404);
        }

        $file = $request->file('file_upload');

        $imageName = time() . '.' . $file->guessClientExtension();
        $path = 'quill/' . $imageName;

        \Storage::put($path, \fopen($file->getRealPath(), 'r+'));

        //Para 'local' retorna relativo.
        if ($this->isUsingDiskLocal()) {
            return response()->json(['path' => str_replace(url('/'), '', \Storage::url($path))]);
        }

        //P/ S3 vai retornar a url do S3
        return response()->json(['path' => \Storage::url($path)]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tinyUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' => config('boilerplate.richeditor.formrequest_upload', 'required|image|max:2048'),
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 404);
        }

        $file = $request->file('file');

        //filename
        $v = explode('.', $file->getClientOriginalName());
        array_pop($v);
        $filename[] = implode('.', $v) ?: '';
        $name = \Str::snake(strtolower(\Str::ascii(implode('-', $filename))), '-');
        $name = preg_replace('/[^A-Za-z0-9-_\. ]/', '', $name) . '-' . time();

        //ext
        $imageName = $name . '.' . $file->guessClientExtension();
        $path = 'tiny/' . $imageName;

        \Storage::put($path, \fopen($file->getRealPath(), 'r+'));

        //Para 'local' retorna relativo.
        if ($this->isUsingDiskLocal()) {
            return response()->json(['location' => str_replace(url('/'), '', \Storage::url($path))]);
        }

        //P/ S3 vai retornar a url do S3
        return response()->json(['location' => \Storage::url($path)]);
    }

    /**
     * Foi adicioando este método p/ resolver o caso de quando o S3 é utilizado com URL relativa do Site
     *
     * Ao invés de https://s3-sa-east-1.amazonaws.com/xxxx ele fica https://meusite.com.br/path_configurado_no_cdn/...S3
     */
    private function isUsingDiskLocal()
    {
        return config('filesystems.default') === 'public' || config('filesystems.default') === 'local';
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function runMigrate(Request $request)
    {
        //TODO: só pode ser rodado por admin / administrador
        //TODO: rodar artisan migrate
    }
}
