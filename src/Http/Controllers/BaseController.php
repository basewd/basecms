<?php

namespace BaseCms\Http\Controllers;

use Illuminate\Http\Request;
use BaseCms\Http\Controllers\Controller;

/**
 * Class BaseController.
 */
class BaseController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function healthcheck(Request $request)
    {
        try {
            \DB::table('cms_jobs')
                ->limit(1)
                ->get();
        } catch (\Exception $e) {
            return response()->json('err', 500);
        }

        return response()->json(\Carbon\Carbon::now(), 200);
    }
}
