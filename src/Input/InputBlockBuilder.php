<?php

namespace BaseCms\Input;

use BaseCms\BaseBlockBuilder\View\Components\BlockBuilder\Container;
use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

/**
 * Componente BlockBuilder
 *
 * Ele pega o "field" e da um explode "_", p/ determinar quais são os campos no form.
 *
 * Como utilizar:
 *
 * Em algum "Definition"
 *
 * $this->setFields([
 *           BaseField::newObj('lat', 'Lat', BaseInputConst::AireHidden),
 *           BaseField::newObj('lng', 'Lng',  BaseInputConst::AireHidden),
 *           BaseField::newObj('lat_lng', 'Latitude / Longitude',  InputLatLng::class . '.render')
 *               ->setRule(new BaseRule('required'))
 * ]);
 *
 */
class InputBlockBuilder implements BaseInput
{
    private $field;
    private $customData;
    private $model;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->customData = $customData;
        $this->model = $model;
    }

    public function __toString()
    {
        return view('base::components.input-block-builder', [
            'id' => $this->field->field,
            'name' => $this->field->field,
            'title' => $this->field->title,
            'value' => @$this->model->{$this->field->field},
        ])->render();
    }
}
