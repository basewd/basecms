<?php

namespace BaseCms\Input;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

/**
 * Componente de Lat/Lng
 *
 * Ele pega o "field" e da um explode "_", p/ determinar quais são os campos no form.
 *
 * Como utilizar:
 *
 * Em algum "Definition"
 *
 * $this->setFields([
 *           BaseField::newObj('lat', 'Lat', BaseInputConst::AireHidden),
 *           BaseField::newObj('lng', 'Lng',  BaseInputConst::AireHidden),
 *           BaseField::newObj('lat_lng', 'Latitude / Longitude',  InputLatLng::class . '.render')
 *               ->setRule(new BaseRule('required'))
 * ]);
 *
 */
class InputLatLng implements BaseInput
{
    private $field;
    private $customData;
    private $model;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->customData = $customData;
        $this->model = $model;
    }

    public function __toString()
    {
        $fieldLat = explode('_', $this->field->field)[0];
        $fieldLng = explode('_', $this->field->field)[1];

        return view('base::components.input-latlng', [
            'id' => $this->field->field,
            'name' => $this->field->field,
            'label' => $this->field->title,
            'lat' => @$this->model->{$fieldLat},
            'lng' => @$this->model->{$fieldLng},
            'inputLat' => $fieldLat,
            'inputLng' => $fieldLng,
            'google_maps_key' => config('base.google_maps_key'),
        ])->render();
    }
}
