<?php

namespace BaseCms\Input;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

/**
 *
 */
class InputTableJson implements BaseInput
{
    private $field;
    private $customData;
    private $model;

    private $definition;
    private $options;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->customData = $customData;
        $this->model = $model;
    }

    public function __toString()
    {
        return view('base::components.input-table-json', [
            'id' => $this->field->field,
            'name' => $this->field->field,
            'label' => $this->field->title,
            'value' => @old($this->field->field) ?: @$this->model->{$this->field->field},
            'definition' => $this->definition,
            'options' => $this->options,
        ])->render();
    }

    public function setDefinition(array $definition)
    {
        $this->definition = $definition;

        return $this;
    }

    public function setOptions(?array $options)
    {
        $this->options = $options;

        return $this;
    }
}
