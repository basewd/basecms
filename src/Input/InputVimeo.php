<?php

namespace BaseCms\Input;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

/**
 * Componente p/ extrair o ID de uma url do Vimeo
 *
 * $this->setFields([
 *           BaseField::newObj('vimeo_id', 'Vimeo',  BaseInputConst::Vimeo)
 *               ->setRule(new BaseRule('required'))
 * ]);
 *
 */
class InputVimeo implements BaseInput
{
    private $field;
    private $customData;
    private $model;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->customData = $customData;
        $this->model = $model;
    }

    public function foo($bar)
    {
        dd('foo ' . $bar);
    }

    public function __toString()
    {
        return view('base::components.input-vimeo', [
            'id' => $this->field->field,
            'name' => $this->field->field,
            'label' => $this->field->title,
            'value' => @$this->model->{$this->field->field},
        ])->render();
    }
}
