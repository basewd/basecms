<?php

namespace BaseCms\Input;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

class InputYouTube implements BaseInput
{
    private $field;
    private $customData;
    private $model;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->customData = $customData;
        $this->model = $model;
    }

    public function __toString()
    {
        return view('base::components.input-youtube', [
            'id' => $this->field->field,
            'name' => $this->field->field,
            'label' => $this->field->title,
            'value' => @$this->model->{$this->field->field},
        ])->render();
    }
}
