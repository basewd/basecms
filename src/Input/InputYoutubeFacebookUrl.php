<?php

namespace BaseCms\Input;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;

class InputYoutubeFacebookUrl implements BaseInput
{
    private $field;
    private $customData;
    private $helpText;
    private $model;

    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
        $this->model = $model;
    }

    public function helpText(
        $helpText = 'Ao colar uma Url ou Id de vídeo do YouTube ou Facebook, é extraído a código embed do vídeo.'
    ) {
        $this->helpText = $helpText;
    }

    public function __toString()
    {
        return view('base::components.input-youtube-facebook-url', [
            'id' => $this->field->field,
            'helpText' => $this->helpText,
            'name' => $this->field->field,
            'label' => $this->field->title,
            'value' => @$this->model->{$this->field->field},
        ])->render();
    }
}
