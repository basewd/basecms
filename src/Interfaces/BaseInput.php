<?php

namespace BaseCms\Interfaces;

use BaseCms\BaseField;
use Illuminate\Database\Eloquent\Model;

/**
 * Utilizado p/ criar inputs custom p/ o formulário.
 */
interface BaseInput
{
    /**
     * @param BaseField $field
     * @param array[array[int]string] $customData (Ele vem do BaseData::getData())
     * @param Model|array|null $model
     *
     * @return string|\Illuminate\Contracts\View\View
     */
    public function __construct(BaseField $field, $customData, $model);

    /**
     *
     * Deve retornar uma string.
     *
     * P/ .blade.php
     *
     * public function __toString() {
     *   return view('backend.foo.bar')->render();
     * }
     *
     * @return string
     */
    public function __toString();
}
