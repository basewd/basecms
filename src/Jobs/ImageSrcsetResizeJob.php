<?php

namespace BaseCms\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Foundation\Bus\Dispatchable;
use Intervention\Image\Facades\Image;

class ImageSrcsetResizeJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $filename;
    public $imageUrl;
    public $toFolder;
    public $resizes;

    public $classModel;
    public $fieldModel;
    public $idModel;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        string $imageUrl,
        string $toFolder,
        array $resizes,
        string $classModel,
        string $fieldModel,
        $idModel
    ) {
        $this->imageUrl = $imageUrl;
        $this->filename = basename($imageUrl);
        $this->toFolder = $toFolder;

        $this->resizes = $resizes;

        $this->classModel = $classModel;
        $this->fieldModel = $fieldModel;
        $this->idModel = $idModel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $image = Image::make($this->imageUrl);

        // array:8 [▼
        //     0 => 1600
        //     1 => 1338
        //     2 => 1120
        //     3 => 937
        //     4 => 783
        //     5 => 655
        //     6 => 548
        //     7 => 32
        // ]
        // $this->resizes = $this->calculateWidths((int) get_headers($this->imageUrl, true)['Content-Length'], $image->getWidth(), $image->getHeight());

        //Blurred image
        array_push($this->resizes, 32);

        arsort($this->resizes, SORT_NUMERIC);

        // dd($this->resizes);

        $resizedImages = [];
        foreach ($this->resizes as $dimension) {
            //resize
            $resizedImage = $image
                ->resize($dimension, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->encode('webp', 92);

            if ($dimension == 32) {
                $resizedImage->blur(1);
            }

            //webp
            $filenameWithoutExt = explode('.', $this->filename);
            array_pop($filenameWithoutExt);
            $path = $this->toFolder . '/' . $dimension . '/' . join('.', $filenameWithoutExt) . '.webp';

            //upload
            \Storage::put($path, $resizedImage);

            //Ret
            $resizedImages[] = [
                'u' => $path,
                'w' => $dimension,
            ];
        }

        $resizedImages = array_reverse($resizedImages); //menor -> maior

        //model update
        $modelData = $this->classModel::find($this->idModel);

        if (strpos($this->fieldModel, '.') !== false) {
            $field = explode('.', $this->fieldModel);

            $params = $modelData->{$field[0]};
            $params->{$field[1] . '_srcset'} = $resizedImages;

            $modelData->update([$field[0] => $params]);
        } else {
            $updateData = [];
            $updateData[$this->fieldModel . '_srcset'] = json_encode($resizedImages);

            $modelData->update($updateData);
        }
    }

    /**
     * https://github.com/spatie/laravel-medialibrary/blob/main/src/ResponsiveImages/WidthCalculator/FileSizeOptimizedWidthCalculator.php
     */
    private function calculateWidths(int $fileSize, int $width, int $height): array
    {
        $targetWidths = [];

        $targetWidths[] = $width;

        $ratio = $height / $width;
        $area = $height * $width;

        $predictedFileSize = $fileSize;
        $pixelPrice = $predictedFileSize / $area;

        while (true) {
            $predictedFileSize *= 0.7;

            $newWidth = (int) floor(sqrt($predictedFileSize / $pixelPrice / $ratio));

            if ($this->finishedCalculating((int) $predictedFileSize, $newWidth)) {
                return $targetWidths;
            }

            $targetWidths[] = $newWidth;
        }
    }

    protected function finishedCalculating(int $predictedFileSize, int $newWidth): bool
    {
        if ($newWidth < 20) {
            return true;
        }

        if ($predictedFileSize < 1024 * 10) {
            return true;
        }

        return false;
    }
}
