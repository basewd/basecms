<?php

namespace BaseCms\Models;

trait BaseModelTrait
{
    /**
     * Move um registro para uma determinada posição. (E normaliza todos os registros)
     *
     * Deve ser utilizado junto com o "use SortableTrait;"
     *
     * @param int $position
     */
    public function moveTo($position)
    {
        $orderColumnName = $this->determineOrderColumnName();

        if ($this->{$orderColumnName} == $position || !$position) {
            return;
        }

        if ($position < $this->{$orderColumnName}) {
            $this->buildSortQuery()
                ->where($orderColumnName, '>=', $position)
                ->increment($orderColumnName);
        } else {
            $this->buildSortQuery()
                ->where($orderColumnName, '<=', $position)
                ->decrement($orderColumnName);
        }

        $this->order = $position;
        $this->save();

        //Normaliza os valores de ordenamento
        $this->solveOrders();
    }

    /**
     * Normaliza os valores de ordenamento
     *
     * Deve ser utilizado junto com o "use SortableTrait;"
     */
    public function solveOrders()
    {
        $orderColumnName = $this->determineOrderColumnName();

        $this->buildSortQuery()
            ->ordered()
            ->whereRaw('0 = (@rownum:=0)')
            ->update([$orderColumnName => \DB::raw('(@rownum := 1 + @rownum)')]);
    }

    /**
     * on/off value, utilizado p/ alterar o valor no render da tabela
     *
     * @param string $field
     * @param bool $checked
     *
     * @return bool
     */
    public function checkValue($field, $checked)
    {
        $value = !$checked || $checked === 'false' ? 0 : 1;

        return $this->update([$field => $value]);
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function radioValue($field)
    {
        \DB::table($this->table)->update([$field => 0]);

        return $this->update([$field => 1]);
    }
}
