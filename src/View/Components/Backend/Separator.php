<?php

namespace BaseCms\View\Components\Backend;

use BaseCms\BaseField;
use BaseCms\Interfaces\BaseInput;
use Illuminate\View\Component;

class Separator extends Component implements BaseInput
{
    private $field;

    public $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(BaseField $field, $customData, $model)
    {
        $this->field = $field;
    }

    public function description($description)
    {
        $this->description = $description;
    }

    public function __toString()
    {
        return $this->render()->render();
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('base::components.backend.separator', [
            'title' => $this->field->title,
            'description' => $this->description,
        ]);
    }
}
