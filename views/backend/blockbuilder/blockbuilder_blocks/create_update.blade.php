@extends($isModal || $inner ? 'backend.layouts.app-only-content' : 'backend.layouts.app')

@section('title', @$definition->getTitle())

@php
    $urlBack = @$block ? route('admin.blocks.index') : route('admin.blocks.selectComponent');
    if (@$keys) {
        $urlBack = @$keys ? route('admin.blocks.indexGroupKeys', ['keys' => $keys, 'inner' => $inner]) : route('admin.blocks.selectComponentGroupKeys', ['keys' => $keys, 'inner' => $inner]);
    }
@endphp

@section('content')
    <x-backend.pages.create-update>
        <x-backend.card class="{{ $isModal ? 'mb-0' : '' }}">
            <x-slot name="header">
                @if (!$isModal)
                    <a class="btn btn-sm btn-arrow-back mr-2" href="{{ $urlBack }}">
                        <em class="fa fa-chevron-left"></em>
                    </a>
                @endif

                @if ($isModal)
                    <div class="btn btn-danger position-fixed" onclick="parent.modalBlockBuilderClose()"
                        style="top:0px; right:0px; z-index:1;">
                        X
                    </div>
                @endif

                <h1 class="h4 d-inline-block">
                    {{ $definition->getTitle() }}
                </h1>
            </x-slot>

            <x-slot name="body">
                @php
                    $formData = @$block ? array_merge($block->toArray(), (array) @$block->params) : ['componentClass' => $componentClass, 'key' => $key];
                @endphp

                {!! Aire::open()->id('form')->encType('multipart/form-data')->route($urlSubmit, ['block' => @$block, 'keys' => @$keys, 'inner' => @$inner])->bind($formData)->formRequest($definition->getFormRequestClass())->class('needs-validation')->novalidate() !!}

                <input type="hidden" name="isModal" value="{{ @$isModal }}" />

                @if (isset($parentBlockId))
                    <input type="hidden" name="parentBlockId" value="{{ $parentBlockId }}" />
                @endif

                @foreach ($definitionBlock->getFields() as $vv)
                    {!! \BaseCms\Helpers\BaseInputHelper::make($vv, @$block->params) !!}
                @endforeach

                <hr />

                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button type="button" class="flex flex-row justify-between items-center w-full"
                                data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne">
                                <h5>Avançado</h5>
                                <em class="fa fa-chevron-down"></em>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse hide" style="visibility: visible !important;"
                            aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                {!! \BaseCms\Helpers\BaseInputHelper::formInputs($definition, @$block) !!}
                            </div>
                        </div>
                    </div>
                </div>

                @if ($definitionBlock->isAllowChildrens())
                    @if (isset($block))
                        <livewire:base::list-children :block="$block" :keys="@$keys" />
                    @else
                        @include('base::components.card-iframe-add', [
                            'id' => 'card-iframe-add-class',
                            'title' => __('Childrens'),
                            'label' => __('Adicionar Childrens'),
                        ])
                    @endif
                @endif

                {!! Aire::close() !!}
            </x-slot>

            <x-slot name="footer">
                <div class="row">
                    @if (!$isModal)
                        <div class="col">
                            <a class="btn btn-danger px-5" href="{{ $urlBack }}">{{ __('Voltar') }}</a>
                        </div>
                    @endif

                    <div class="col text-right">
                        {{ Aire::submit(__('Salvar') . ' ' . $definition->getTitle())->form('form') }}
                    </div>
                </div>
            </x-slot>
        </x-backend.card>
    </x-backend.pages.create-update>
@endsection

@push('after-scripts')
    <script>
        if (window.location.hash == "#card-iframe-add-class") {
            window.location.replace("#");
            setTimeout(function() {
                $(".btn-add-block").trigger("click");
            }, 1000)
        }
    </script>
@endpush
