@extends('backend.layouts.app')

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.view>
        <x-backend.card>
            <x-slot name="header">
                <h1 class="h4 d-inline-block">
                    {{ request()->key ? __('Componente(s):') . ' ' . request()->key : $definition->getTitlePlural() }}
                </h1>
            </x-slot>

            @if (!request()->key)
                <x-slot name="headerActions">
                    @can('create', $definition->getModelClass())
                        <a href="{{ route('admin.blocks.indexNotUsed') }}" class="btn btn-sm btn-danger px-5 mr-4"
                            title="Novo">{{ __('Ver blocos não utilizados') }}</a>

                        <a href="{{ route('admin.blocks.selectComponent') }}" class="btn btn-sm btn-primary px-5"
                            title="Novo">{{ __('Adicionar') }} {{ $definition->getTitle() }}</a>
                    @endcan
                </x-slot>
            @endif

            <x-slot name="body">
                {!! $definition->addFilterAndExport('blocks') !!}

                @if (isset($definition->multipleSelectTable))
                    @include('base::components.table-multiple-select', [
                        'action' => route('admin.blocks.multipleSelect'),
                        'label' => __('Remover Selecionados'),
                        'tableSelector' => '#blocks',
                    ])
                @endif

                <div class="table-responsive">
                    <table id="blocks" class="table-striped table" aria-label="Tabela {{ $definition->getTitle() }}">
                        {{-- <tr><th>Empty</th></tr> --}}
                        <thead>
                            <tr>
                                {!! $definition->getThColumns() !!}
                            </tr>
                        </thead>
                    </table>
                </div>
            </x-slot>

            {{-- <x-slot name="footer"></x-slot> --}}
        </x-backend.card>
    </x-backend.pages.view>
@endsection

@push('after-scripts')
    <script>
        var columns = {!! $definition->getObjColumns() !!};

        $(document).ready(function() {
            baseDataTable("#blocks", "{{ $definition->urlDatatable() . '?key=' . request()->key }}",
                columns, false);
        });
    </script>
@endpush
