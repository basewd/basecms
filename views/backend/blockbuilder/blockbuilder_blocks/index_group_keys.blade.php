@extends($inner ? 'backend.layouts.app-only-content' : 'backend.layouts.app', ['iframe' => $inner, 'inner' => $inner])

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.view>
        <x-backend.card>
            <x-slot name="header">
                <h1 class="h4 d-inline-block">
                    {{ __('Componente(s)') }}
                </h1>
            </x-slot>

            <x-slot name="body">
                {!! $definition->addFilterAndExport('blocks') !!}

                <div class="table-responsive">
                    <table id="blocks" class="table-striped table" aria-label="{{ $definition->getTitle() }}">
                        {{-- <tr><th>Empty</th></tr> --}}
                        <thead>
                            <tr>
                                {!! $definition->getThColumns() !!}
                            </tr>
                        </thead>
                    </table>
                </div>
            </x-slot>

            {{-- <x-slot name="footer"></x-slot> --}}
        </x-backend.card>
    </x-backend.pages.view>
@endsection

@push('after-scripts')
    <script>
        var columns = {!! $definition->getObjColumns() !!};

        $(document).ready(function() {
            baseDataTable("#blocks", "{{ $urlDatatable }}",
                columns, false);
        });
    </script>
@endpush
