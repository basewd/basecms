@extends('backend.layouts.app')

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.list>
        <x-backend.card>
            <x-slot name="header">
                <h1 class="h4 d-inline-block">
                    {{ $definition->getTitlePlural() }}
                </h1>
            </x-slot>

            <x-slot name="headerActions">
                @can('create', $definition->getModelClass())
                    <a href="{{ route('admin.blocks.selectComponent') }}" class="btn btn-sm btn-primary px-5"
                        title="Novo">{{ __('Adicionar') }} {{ $definition->getTitle() }}</a>
                @endcan
            </x-slot>

            <x-slot name="body">
                {!! $definition->addFilterAndExport('blocks') !!}

                @if (isset($definition->multipleSelectTable))
                    @include('base::components.table-multiple-select', [
                        'action' => route('admin.blocks.multipleSelect'),
                        'label' => __('Remover Selecionados'),
                        'tableSelector' => '#blocks',
                    ])
                @endif

                <div class="table-responsive">
                    <table id="blocks" class="table-striped table" aria-label="{{ $definition->getTitle() }}">
                        {{-- <tr><th>Empty</th></tr> --}}
                        <thead>
                            <tr>
                                {!! $definition->getThColumns() !!}
                            </tr>
                        </thead>
                    </table>
                </div>
            </x-slot>

            {{-- <x-slot name="footer"></x-slot> --}}
        </x-backend.card>
    </x-backend.pages.list>
@endsection

@push('after-scripts')
    <script>
        var columns = {!! $definition->getObjColumns() !!};

        $(document).ready(function() {
            baseDataTable("#blocks", "{{ route('admin.blocks.datatable') . '?notused=true' }}", columns, false);
        });
    </script>
@endpush
