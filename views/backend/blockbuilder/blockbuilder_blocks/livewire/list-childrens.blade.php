<div style="overflow: auto;">
    @if (isset($block) && isset($childrens))
        @php($totalChildrens = $childrens->count())

        <table class="table-striped table dataTable" style="padding-top:0px; padding-bottom:0px;" aria-label="Components">
            @if ($totalChildrens > 0)
                <tr>
                    <th width="50px">{{ __('Código') }}</th>
                    <th width="150px">
                        @if (config('blockbuilder.preview.enabled'))
                            <div>{{ __('Preview') }}</div>
                            <div class="small text-muted">
                                {{ __('* A imagem pode demorar alguns minutos para ser atualizada.') }}</div>
                        @else
                            <div>{{ __('Modelo') }}</div>
                        @endif
                    </th>
                    <th>{{ __('Componente') }}</th>
                    <th>Dados</th>
                    {{-- <th width="200px">{{ __('Tags') }}</th> --}}
                    <th width="50px">{{ __('Reutilizável') }}</th>
                    <th width="50px">{{ __('Rascunho') }}</th>
                    <th width="50px">{{ __('Ordem') }}</th>
                    {{-- <th>{{ __('Atualizado em') }}</th> --}}
                    <th width="200px">{{ __('Ações') }}</th>
                </tr>
            @endif

            @foreach ($childrens as $k => $childrenBlock)
                <?php try { ?>
                @if (@!$childrenBlock->componentData)
                    @continue
                @endif

                <tr>
                    <td>{{ $childrenBlock->id }}</td>
                    @if (isset($childrenBlock->preview))
                        <td>
                            <img src="{{ storage_url($childrenBlock->preview) }}" style="max-width: 100%; height:auto;"
                                alt="Preview" />
                        </td>
                    @else
                        <td>
                            <img src="{{ storage_url($childrenBlock->componentData['thumb']) }}"
                                style="max-width: 100%; height:auto;" alt="Thumb" />
                            {{-- <pre style="white-space: pre-wrap">{{ json_encode($childrenBlock->params, JSON_PRETTY_PRINT) }}</pre> --}}
                        </td>
                    @endif
                    <td>{{ $childrenBlock->componentData['title'] }}</td>
                    <td>{!! \BaseCms\BaseBlockBuilder\BlockBuilder::previewData($childrenBlock->params, $childrenBlock->definition()) !!}</td>
                    <td>{!! $childrenBlock->fl_reusable ? __('Yes') : __('No') !!}</td>
                    <td>{!! $childrenBlock->fl_draft ? __('Yes') : __('No') !!}</td>
                    {{-- <td>{!! tableTags($childrenBlock->tags) !!}</td> --}}
                    <td>{{ $childrenBlock->pivot->order }}</td>
                    {{-- <td>{{$childrenBlock->updated_at}}</td> --}}
                    <td class="column-actions" width="150px">
                        <button type="button" class="btn-add-block btn btn-primary"
                            onclick="window.modalBlockBuilder(event,{{ $childrenBlock->id }})">✎</button>
                        <button type="button" class="btn btn-info" {{ $k == 0 ? 'disabled' : '' }}
                            wire:click="orderUp({{ $childrenBlock->pivot->id }})">▲</button>
                        <button type="button" class="btn btn-info" {{ $k == $totalChildrens - 1 ? 'disabled' : '' }}
                            wire:click="orderDown({{ $childrenBlock->pivot->id }})">▼</button>
                        <button type="button" class="btn btn-danger"
                            onclick="confirm('{{ __('Tem certeza? Será removido a relação entre os componentes, p/ não mostrar mais ele. O componente filho não será removido, pois ele pode estar sendo utilizado em outras partes.') }}') || event.stopImmediatePropagation()"
                            wire:click="remove({{ $childrenBlock->pivot->id }})">♻</button>
                    </td>
                </tr>
                <?php } catch(\ErrorException $e) {
                    throw new \ErrorException( $e->getMessage(). ' - '.$childrenBlock->toJson());
                } ?>
            @endforeach
        </table>

        @if (count($childrens) < $block->definition()->getMaxChildrens())
            <div class="w-full">
                <button type="button" class="btn-add-block button btn-primary w-full"
                    onclick="window.modalBlockBuilder(event,null,{{ $block->id }})">+
                    {{ __('Adicionar Componente') }}</button>
            </div>
        @endif

        <br /><br />
    @endif

    @include('base::frontend.blockbuilder.modal-block-builder', ['isChildren' => true, 'keys' => @$keys])
</div>
