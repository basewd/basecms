@extends($isModal || $inner ? 'backend.layouts.app-only-content' : 'backend.layouts.app')

@section('title', 'Blocos')

@section('content')
    <x-backend.pages.list>
        <x-backend.card class="{{ @$isModal ? 'mb-0' : '' }}">
            <x-slot name="header">
                @if (!$isModal)
                    <a class="btn btn-sm btn-arrow-back mr-2"
                        href="{{ route('admin.blocks.index', ['isModal' => $isModal, 'parentBlockId' => $parentBlockId, 'inner' => $inner]) }}">
                        <em class="fa fa-chevron-left"></em>
                    </a>
                @endif

                @if ($isModal)
                    <div class="btn btn-danger position-fixed " onclick="parent.modalBlockBuilderClose()"
                        style="top:0px; right:0px; z-index:1;">
                        X
                    </div>
                @endif

                <h1 class="h4 d-inline-block" style="padding-top:2px;">
                    {{ __('Componentes') }}
                </h1>
            </x-slot>

            <x-slot name="body">

                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-new-tab" data-toggle="tab" data-target="#nav-new"
                            type="button" role="tab" aria-controls="nav-new"
                            aria-selected="true">{{ __('Novo Componente') }}</button>

                        @if (isset($parentBlockId) && $parentBlockDefinition->isAllowReuseComponents())
                            <button class="nav-link" id="nav-old-tab" data-toggle="tab" data-target="#nav-old"
                                type="button" role="tab" aria-controls="nav-old"
                                aria-selected="false">{{ __('Componente Já Existente') }}</button>
                        @endif
                    </div>
                </nav>

                <div class="tab-content py-4" id="nav-tabContent">
                    <div class="tab-pane  show active" id="nav-new" role="tabpanel" aria-labelledby="nav-new-tab">
                        <div class="grid grid-cols-4 gap-8">
                            @foreach ($list as $k => $v)
                                <a href="{{ route('admin.blocks.create', ['componentClass' => $v, 'isModal' => @$isModal, 'parentBlockId' => @$parentBlockId, 'keys' => @$keys, 'inner' => @$inner]) }}"
                                    class="block bg-primary rounded overflow-hidden">
                                    <div>
                                        <div class="text-xl font-bold text-center text-white py-2">
                                            {{ __($components[$v]['title']) }}
                                        </div>
                                        <img src="{{ storage_url($components[$v]['thumb']) }}" class="w-full h-auto"
                                            alt="Thumb" />
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>

                    @if (isset($parentBlockId) && $parentBlockDefinition->isAllowReuseComponents())
                        <div class="tab-pane " id="nav-old" role="tabpanel" aria-labelledby="nav-old-tab">
                            <table id="blocks" class="table-striped table" style="padding-bottom:0px"
                                aria-label="Blocks">
                                {{-- <tr><th>Empty</th></tr> --}}
                                <thead>
                                    <tr>
                                        {!! $definition->getThColumns() !!}
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    @endif
                </div>
            </x-slot>

            @if (!$isModal)
                <x-slot name="footer">
                    <div class="row">
                        <div class="col">
                            <a class="btn btn-danger px-5"
                                href="{{ route('admin.blocks.index', ['isModal' => $isModal, 'parentBlockId' => $parentBlockId, 'inner' => $inner]) }}">{{ __('Voltar') }}</a>
                        </div>
                    </div>
                </x-slot>
            @endif
        </x-backend.card>
    </x-backend.pages.list>
@endsection

@if (isset($parentBlockId))
    @push('after-scripts')
        <script>
            var columns = {!! $definition->getObjColumns() !!};

            $(document).ready(function() {
                var dataTable = baseDataTable("#blocks",
                    "{{ route('admin.blocks.datatableSelect', ['parentBlockId' => $parentBlockId]) }}", columns,
                    true, {});

                $('button[data-toggle="tab"]').on("click", function(e) {
                    if ($(this).data("target") == "#nav-old") {
                        dataTable.draw();
                    }
                });

            });
        </script>
    @endpush
@endif
