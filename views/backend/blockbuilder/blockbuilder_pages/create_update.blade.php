@extends($isModal ? 'backend.layouts.app-only-content' : 'backend.layouts.app')

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.create-update>
        {!! Aire::open()->id('form')->encType('multipart/form-data')->route($urlSubmit, @$page)->bind(@$page)->formRequest(@$page ? $definition->getFormRequestUpdateClass() : $definition->getFormRequestClass())->class('needs-validation')->novalidate() !!}

        <x-backend.card class="{{ $isModal ? 'mb-0' : '' }}">
            <x-slot name="header">
                @if (!$isModal)
                    <a class="btn btn-sm btn-arrow-back mr-2" href="{{ route('admin.pages.index') }}">
                        <em class="fa fa-chevron-left"></em>
                    </a>
                @endif

                @if ($isModal)
                    <div class="btn btn-danger position-fixed" onclick="parent.modalBlockBuilderClose()"
                        style="top:0px; right:0px; z-index:1;">
                        X
                    </div>
                @endif

                <h1 class="h4 d-inline-block">
                    {{ __($definition->getTitle()) }}
                </h1>

                @if (isset($page))
                    <x-slot name="headerActions">
                        <div class="text-right">
                            <x-utils.link icon="fas fa-eye" class="btn btn-sm btn-primary"
                                href="{{ $page->url . '?blockbuilder=true' }}" target="_blank"
                                text="{{ __('Acessar Página Atual') }}" />
                        </div>
                    </x-slot>
                @endif
            </x-slot>

            <x-slot name="body">
                <input type="hidden" name="isModal" value="{{ @$isModal }}" />

                {!! \BaseCms\Helpers\BaseInputHelper::formInputs($definition, @$page) !!}

                @if (isset($page) && $page->block)
                    <livewire:base::list-children :block="$page->block" />
                @endif
            </x-slot>

            <x-slot name="footer">
                <div class="row">
                    @if (!$isModal)
                        <div class="col">
                            <a class="btn btn-danger px-5" href="{{ $definition->urlBack() }}">{{ __('Voltar') }}</a>
                        </div>
                    @endif

                    <div class="col text-right">
                        @if (!isset($page))
                            @include('base::components.btn-redirect', [
                                'id' => 'card-iframe-add-class',
                                'class' => '',
                                'label' => __('Continuar'),
                                'form' => 'form',
                            ])
                        @else
                            {{ Aire::submit(__('Salvar') . ' ' . $definition->getTitle())->form('form') }}
                        @endif
                    </div>
                </div>
            </x-slot>
        </x-backend.card>

        {!! Aire::close() !!}
    </x-backend.pages.create-update>
@endsection

@push('after-scripts')
    <script>
        if (window.location.hash == "#card-iframe-add-class") {
            window.location.replace("#");
            setTimeout(function() {
                $(".btn-add-block").trigger("click");
            }, 1000)
        }
        $(".form-group-breadcrumbs select").select2({
            multiple: true
        }).on('select2:select', function(e) {
            var element = e.params.data.element;
            var $element = $(element);
            $(this).append($element);
            $(this).trigger("change");
        })
    </script>
@endpush
