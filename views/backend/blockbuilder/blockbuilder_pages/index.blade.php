@extends('backend.layouts.app')

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.list>
        <x-backend.card>
            <x-slot name="header">
                <h1 class="h4 d-inline-block">
                    {{ request()->key ? 'Página: ' . request()->key : $definition->getTitlePlural() }}
                </h1>
            </x-slot>

            @if (!request()->key)
                <x-slot name="headerActions">
                    @can('listRestore', $definition->getModelClass())
                        <a href="{{ $definition->urlRestore() }}" class="btn btn-sm btn-info mr-4 px-5"
                            title="Novo">{{ __('Restaurar') }} {{ $definition->getTitlePlural() }}</a>
                    @endcan

                    @can('create', $definition->getModelClass())
                        <a href="{{ $definition->urlCreate() }}" class="btn btn-sm btn-primary px-5"
                            title="{{ __('Adicionar') }}">{{ __('Adicionar') }} {{ $definition->getTitle() }}</a>
                    @endcan
                </x-slot>
            @endif

            <x-slot name="body">
                {!! $definition->addFilterAndExport('pages') !!}

                @if (@$definition->multipleSelectTable)
                    @include('base::components.table-multiple-select', [
                        'action' => route('admin.pages.multipleSelect'),
                        'label' => __('Remover Selecionados'),
                        'tableSelector' => '#pages',
                    ])
                @endif

                <div class="table-responsive">
                    <table id="pages" class="table table-striped" aria-label="{{ $definition->getTitle() }}">
                        {{-- <tr><th>Empty</th></tr> --}}
                        <thead>
                            <tr>
                                {!! $definition->getThColumns() !!}
                            </tr>
                        </thead>
                    </table>
                </div>
            </x-slot>

            {{-- <x-slot name="footer"></x-slot> --}}
        </x-backend.card>
    </x-backend.pages.list>
@endsection

@push('after-scripts')
    <script>
        var columns = {!! $definition->getObjColumns() !!};

        $(document).ready(function() {
            baseDataTable("#pages", "{{ $definition->urlDatatable() . '?key=' . request()->key }}", columns,
                false, {});
        });
    </script>
@endpush
