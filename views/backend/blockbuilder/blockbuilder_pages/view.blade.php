@extends('backend.layouts.app')

@section('title', @$definition->getTitle())

@section('content')
    <x-backend.pages.view>
        <x-backend.card>
            <x-slot name="header">
                <a class="btn btn-sm mr-2 btn-arrow-back" href="{{ $definition->urlBack() }}">
                    <em class="fa fa-chevron-left"></em>
                </a>
                <h1 class="h4  d-inline-block">
                    {{ $definition->getTitle() }}
                </h1>
            </x-slot>

            <x-slot name="body">
                <table class="dataTable table table-striped table-view" aria-label="{{ $definition->getTitle() }}">
                    {{-- <tr><th>Empty</th></tr> --}}
                    {!! \BaseCms\Helpers\BaseViewHelper::viewFields($definition, $page) !!}
                </table>
            </x-slot>

            <x-slot name="footer">
                <div class="row">
                    <div class="col">
                        <a class="btn btn-danger px-5" href="{{ $definition->urlBack() }}">{{ __('Voltar') }}</a>
                    </div>
                </div>
            </x-slot>
        </x-backend.card>
    </x-backend.pages.view>
@endsection
