<div class="py-2 px-3 bg-light rounded-lg mb-2">
    <div class="font-bold text-dark text-base">{{ $title }}</div>

    @if (@$description)
        <div class="font-regular text-dark text-sm opacity-80">{{ $description }}</div>
    @endif
</div>
