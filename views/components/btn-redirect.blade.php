<div class="btn-redirect {{ @$id }}">
    <input type="hidden" name="redirect_to_edit" val="" />
    <button type="submit" class="btn-save-and-redirect btn btn-primary px-5 float-right {{ $class }}"
        data="{{ $id }}" form="{{ $form }}">{{ $label }}</button>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".{{ @$id }} .btn-save-and-redirect").on("click", function() {
                $("input[name='redirect_to_edit'").val($(this).attr("data"));
            });
        });
    </script>
@endpush
