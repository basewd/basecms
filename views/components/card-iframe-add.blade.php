<div class="card card-iframe-add {{ @$id }}">
    <div class="card-header d-flex justify-content-between align-items-center">
        <h5 class="card-title mb-0">{{ $title }}</h5>
        <input type="hidden" name="redirect_to_edit" val="" />
        <button class="btn btn-save-and-redirect btn btn-primary px-5 float-right"
            data="{{ $id }}">{{ $label }}</button>
    </div>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            $(".{{ @$id }} .btn-save-and-redirect").on("click", function() {
                $("input[name='redirect_to_edit'").val($(this).attr("data"));
            });
        });
    </script>
@endpush
