<div class="btn-group dataTables_actions">
    @can('update', @$row)
        @if (@$orderUpUrl)
            <form target="o-up-{{ $row->id }}" method="POST" action="{{ $orderUpUrl }}">
                <iframe class='iframe-datatable' style='display:none' name="o-up-{{ $row->id }}" title="Order Up"></iframe>
                @csrf
                <button class="btn btn-primary" type="submit"
                    style="margin-left:-1px; border-top-right-radius: 0; border-bottom-right-radius: 0;">
                    <em class="fa fa-chevron-up"></em>
                </button>
            </form>
        @endif

        @if (@$orderDownUrl)
            <form target="o-down-{{ $row->id }}" method="POST" action="{{ $orderDownUrl }}">
                <iframe class='iframe-datatable' style='display:none' name="o-down-{{ $row->id }}"
                    title="Order Down"></iframe>
                @csrf
                <button class="btn btn-primary rounded-0" type="submit" style="margin-left:-1px;">
                    <em class="fa fa-chevron-down"></em>
                </button>
            </form>
        @endif
    @endcan

    @if (@$viewUrl)
        @can('view', @$row)
            <a href="{!! $viewUrl !!}" aria-label="{{ __('Visualizar item') }}" class="btn btn-primary"
                title="{{ __('Visualizar item') }}">
                <em class="fas fa-eye"></em>
            </a>
        @endcan
    @endif

    @if (@$viewUrlBlank)
        @can('view', @$row)
            <a href="{!! $viewUrlBlank !!}" aria-label="{{ __('Visualizar item') }}" class="btn btn-primary"
                title="{{ __('Visualizar item') }}" target="_blank" rel="noopener">
                <em class="fas fa-eye"></em>
            </a>
        @endcan
    @endif

    @if (@$editUrl)
        @can('update', @$row)
            <a href="{!! $editUrl !!}" aria-label="{{ __('Editar') }}" class="btn btn-primary"
                title="{{ __('Editar') }}">
                <em class="fa fa-pencil-alt"></em>
            </a>
        @endcan
    @endif

    @if (@$addUrl)
        @can('create', @$row)
            <a href="{!! $addUrl !!}" aria-label="{{ __('Adicionar') }}" class="btn btn-primary"
                title="{{ __('Adicionar') }}">
                <em class="fa fa-plus"></em>
            </a>
        @endcan
    @endif

    @if (
        (@$forceDeleteUrl && Auth::user()->can('forceDeleteUrl', @$row)) ||
            (@$restoreUrl && Auth::user()->can('restore', @$row)) ||
            (@$deleteUrl && Auth::user()->can('delete', @$row)) ||
            ((@$orderStartUrl || @$orderEndUrl) && Auth::user()->can('update', @$row)) ||
            (@$duplicateUrl && Auth::user()->can('duplicate', @$row)))
        <div class="btn-group btn-group-sm" role="group">
            <button id="userActions" type="button" class="btn bg-light dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                {{ __('Mais') }}
            </button>

            <div class="dropdown-menu dropdown-menu-right fix-dropdown-menu p-0 m-0 rounded-0 border-0"
                aria-labelledby="userActions" data-boundary="window">
                @if (@$deleteUrl)
                    @can('delete', @$row)
                        <form method="POST" action="{{ $deleteUrl }}" name="delete-item"
                            data-lang-text="{{ __('Você tem certeza que deseja deletar o registro?') }}"
                            data-lang-cancel="{{ __('Cancelar') }}" data-lang-confirm="{{ __('Sim') }}">
                            @csrf
                            @method('delete')

                            <button type="submit" class="dropdown-item">
                                {{ __('Deletar') }}
                            </button>
                        </form>
                    @endcan
                @endif

                @if (@$restoreUrl)
                    @can('restore', @$row)
                        <form method="POST" action="{{ $restoreUrl }}" name="delete-item"
                            data-lang-text="{{ __('Você tem certeza que deseja restaurar o registro?') }}"
                            data-lang-cancel="{{ __('Cancelar') }}" data-lang-confirm="{{ __('Sim') }}">
                            @csrf

                            <button type="submit" class="dropdown-item">
                                {{ __('Restaurar') }}
                            </button>
                        </form>
                    @endcan
                @endif

                @if (@$forceDeleteUrl)
                    @can('forceDelete', @$row)
                        <form method="POST" action="{{ $forceDeleteUrl }}" name="delete-item"
                            data-lang-text="{{ __('Você tem certeza que deseja remover o registro para sempre?') }}"
                            data-lang-cancel="{{ __('Cancelar') }}" data-lang-confirm="{{ __('Sim') }}">
                            @csrf
                            @method('delete')

                            <button type="submit" class="dropdown-item">
                                {{ __('Remover p/ sempre') }}
                            </button>
                        </form>
                    @endcan
                @endif

                @if (@$orderStartUrl || @$orderEndUrl)
                    @can('update', @$row)
                        @if (@$orderStartUrl)
                            <form target="o-start-{{ $row->id }}" method="POST" action="{{ $orderStartUrl }}">
                                <iframe class='iframe-datatable' style='display:none' name="o-start-{{ $row->id }}"
                                    title="{{ __('Mover para o Início') }}"></iframe>
                                @csrf
                                <button class="dropdown-item" type="submit">
                                    {{ __('Mover para o Início') }}
                                </button>
                            </form>
                        @endif

                        @if ($orderToUrl)
                            <form target="o-move-{{ $row->id }}" method="POST" action="{{ $orderToUrl }}"
                                onsubmit="return submitOrderTo(this);">
                                <iframe class='iframe-datatable' style='display:none' name="o-move-{{ $row->id }}"
                                    title="{{ __('Mover para posição') }}"></iframe>
                                <input name="position" type="hidden" />
                                @csrf
                                <button class="dropdown-item" type="submit">
                                    {{ __('Mover para posição') }}
                                </button>
                            </form>
                        @endif

                        @if (@$orderEndUrl)
                            <form target="o-end-{{ $row->id }}" method="POST" action="{{ $orderEndUrl }}">
                                <iframe class='iframe-datatable' style='display:none' name="o-end-{{ $row->id }}"
                                    title="{{ __('Mover para o Final') }}"></iframe>
                                @csrf
                                <button class="dropdown-item" type="submit">
                                    {{ __('Mover para o Final') }}
                                </button>
                            </form>
                        @endif
                    @endcan
                @endif

                @if (@$duplicateUrl)
                    @can('duplicate', @$row)
                        <a href="{!! $duplicateUrl !!}" data-method="post"
                            data-trans-button-cancel="{{ __('Cancelar') }}"
                            data-trans-button-confirm="{{ __('Sim') }}"
                            data-trans-title="{{ __('Você deseja duplicar?') }}"
                            class="dropdown-item">{{ __('Duplicar') }}</a>
                    @endcan
                @endif
            </div>
        </div>
    @endif
</div>
