<div class="datatable-filter base-datatable-filter base-datatable-filter-{{ $tableId }}">
    <div class="content-filter">
        <form class="form-inline">
            <div class="form-group">
                <label>{{ __('Filtrar por:') }}</label>

                <select class="fields form-control">
                    <option value="">{{ __('Selecione') }}</option>

                    @foreach ($definition->getFields() as $v)
                        @if ($v->filterable)
                            <option value="{{ $v->field }}" data-type="{{ $v->type }}">{{ __($v->title) }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group filter-content d-none">
                <div class="filter-type filter-text filter-file filter-image d-none">
                    <select class="form-control">
                        <option value="like">{{ __('Contendo o trecho') }}</option>
                        <option value="notLike">{{ __('Não contendo o trecho') }}</option>
                        {{-- <option value="like2">Contendo a palavra</option>
                        <option value="notLike2">Não contendo a palavra</option> --}}
                    </select>

                    <input type="text" class="form-control" placeholder="{{ __('Palavra') }}" />
                </div>

                <div class="filter-type filter-int filter-number d-none">
                    <select class="form-control">
                        <option value="=">{{ __('Igual à') }}</option>
                        <option value=">">{{ __('Maior que') }}</option>
                        <option value="<">{{ __('Menor que') }}</option>
                        <option value=">=">{{ __('Maior ou igual à') }}</option>
                        <option value="<=">{{ __('Menor ou igual à') }}</option>
                        <option value="in">{{ __('Contendo') }}</option>
                    </select>

                    <div class="position-relative d-inline-block">
                        <input type="text" class="form-control" placeholder="{{ __('Número') }}" />
                        <small class="muted d-none">{{ __('Valores separados por virgula.') }}</small>
                    </div>
                </div>

                <div class="filter-type filter-date d-none">
                    <select class="form-control">
                        <option value="equal">{{ __('Igual à') }} </option>
                        <option value="equalOrAfter">{{ __('Depois ou igual à') }}</option>
                        <option value="before">{{ __('Antes de') }} </option>
                        <option value="equalOrBefore">{{ __('Antes ou antes de') }}</option>
                        <option value="exist">{{ __('Existe') }}</option>
                        <option value="not_exist">{{ __('Não existe') }}</option>
                        {{-- <option value="between">Entre </option> --}}
                    </select>
                    <input type="date" class="form-control" />
                    {{-- <input type="text" class="form-control ml-2"/> --}}
                </div>

                <div class="filter-type filter-boolean d-none">
                    <select class="form-control">
                        <option value="true">{{ __('Sim') }}</option>
                        <option value="false">{{ __('Não') }}</option>
                    </select>
                    <input type="hidden" class="form-control" />
                </div>

                @foreach ($definition->getFields() as $v)
                    @if ($v->data instanceof BaseCms\Datatables\Data\BaseDataEnum)
                        <div class="filter-type filter-enum d-none filter-enum-{{ $v->field }}">
                            <select class="form-control">
                                @foreach ($v->data->getData() as $kk => $vv)
                                    <option value="{{ $kk }}">{{ $vv }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                @endforeach

                <div class="btn btn-primary btn-add ml-2">
                    +
                </div>
            </div>
        </form>
    </div>

    <div class="filter-data"></div>

    <div class="datatable-buttons-right">
        @if (@$definition->filterLocale)
            @php
                try {
                    $locales = \BaseCms\Models\LocaleModel::where('enabled', true)
                        ->orderBy('order')
                        ->get();
                } catch (\Exception $e) {
                    $locales = [];
                }
            @endphp

            @if ($locales && count($locales) > 1)
                <div class="btn-group btn-group-toggle mr-3" data-toggle="buttons">
                    @foreach ($locales as $k => $locale)
                        <label class="btn btn-primary {{ $locale->id == config('app.locale') ? 'active' : '' }}">
                            <input type="radio" name="locale" value="{{ $locale->id }}" autocomplete="off"
                                {{ $locale->id == config('app.locale') ? 'checked' : '' }} />
                            {{ $locale->initials }}
                        </label>
                    @endforeach
                </div>
            @endif
        @endif

        <a class="btn btn-primary btn-refresh mr-3" data-toggle="tooltip" data-placement="left"
            title="{{ __('Atualizar dados da tabela') }}">
            <em class="fas fa-redo text-white" aria-hidden="true"></em>
        </a>

        @if ($definition->isExportable() && $definition->urlCSV($params))
            <div class="base-datatable-export base-datatable-export-{{ $tableId }}">
                <a data-href="{{ $definition->urlCSV($params) }}" target="_self" download class="btn btn-primary"
                    data-toggle="tooltip" data-placement="left"
                    title="{{ __('Exportar dados filtrados em formato CSV') }}">
                    <em class="fa fa-download text-white" aria-hidden="true"></em>
                </a>
            </div>
        @endif
    </div>
</div>

<div class="table-responsive-scroll-top d-none">
    <div>&nbsp;</div>
</div>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            var tableId = "{{ $tableId }}";
            var hash = "{{ $hash }}";

            datatableFilter(tableId, hash);
            datatableExport(tableId);
        });
    </script>
    <style>
        .filter-int small {
            position: absolute;
            bottom: -12px;
            left: 6px;
        }
    </style>
@endpush
