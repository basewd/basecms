<div class="card mt-4">
    <div class="card-header">
        <h4>{{ $title }}</h4>
    </div>

    <div class="card-body">
        <table id="{{ $id }}" class="table table-striped dataTable py-0" aria-label="View">
            <thead>
                <tr>
                    @foreach ($columns as $v)
                        <th scope="col">{!! $v !!}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $v)
                    <tr>
                        @foreach ($columnsData as $key)
                            @if (is_callable($key))
                                <td>{!! $key($v) !!}</td>
                            @else
                                <td>{!! \Arr::get($v, $key) !!}</td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@push('after-scripts')
    <script>
        simpleDataTable('#{{ $id }}');
    </script>
@endpush
