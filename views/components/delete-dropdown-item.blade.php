@props(['href' => '#', 'text' => __('Delete'), 'lang-text' => '', 'lang-cancel' => '', 'lang-confirm' => ''])

<form method="POST" action="{{ $href }}" name="delete-item" data-lang-text="{{ $langText }}"
    data-lang-cancel="{{ $langCancel }}" data-lang-confirm="{{ $langConfirm }}">
    @csrf
    @method('delete')

    <button type="submit" class="dropdown-item">
        {{ __('Deletar') }}
    </button>
</form>
