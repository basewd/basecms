@if (@$srcIframe)
    <div class="mt-5">
        <div id="{{ $id }}">
            @include('base::components.iframe', ['id' => 'iframe-' . $id, 'src' => $srcIframe])
        </div>
    </div>
@endif

@if (!@$srcIframe)
    @include('base::components.card-iframe-add', ['id' => $id, 'title' => $title, 'label' => $labelAdd])
@endif
