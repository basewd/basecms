<iframe id="{{ $id }}" background="#FFF" aria-label="{{ $label ?? '' }}" width="100%" height="100%" border=0
    style="border: 0px solid;" src="{{ $src }}" title="">
</iframe>

@push('after-scripts')
    <script>
        $(document).ready(function() {
            setInterval(function() {
                autosizeIframe('#{{ $id }}');
            }, 60);
        });
    </script>
@endpush
