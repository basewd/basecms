<div class="input-block-builder mb-3">
    @include('base::components.backend.separator', [
        'title' => $title,
        'description' => @!$value ? 'Cadastro será liberado apenas na edição.' : '',
    ])

    @if (@$value)
        <livewire:base::list-children :block="BaseCms\BaseBlockBuilder\Models\BlockModel::find($value)" />
    @endif
</div>
