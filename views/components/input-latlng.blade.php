<div class="input-latlng mb-3">
    <div class="form-row">
        <div class="form-group col-md-10">
            <label for="{{ $id }}">{{ $label }}</label>
            <input id="search-latlng" type="text" placeholder="{{ __('Digite o endereço') }}" class="form-control" />
        </div>

        <div class="form-group col-md-2">
            <label>&nbsp;</label>
            <input id="{{ $id }}" name="{{ $id }}" type="text" class="form-control" readonly />
        </div>
    </div>

    <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>

    <div id="map-latlng"></div>
</div>

@push('after-styles')
    <style>
        .input-latlng #map-latlng {
            width: 100%;
            height: 300px;
        }
    </style>
@endpush

@push('after-scripts')
    <script>
        function initGoogleMaps() {
            var inputLat = $("[name='{{ $inputLat }}']")[0];
            var inputLng = $("[name='{{ $inputLng }}']")[0];

            initMaps("map-latlng", "{{ $id }}", "{{ $lat }}", "{{ $lng }}", inputLat,
                inputLng);
        }

        function initMaps(idMap, idLatLng, lat, lng, inputLat, inputLng) {
            document.getElementById(idLatLng).value = lat + "," + lng;

            const map = new google.maps.Map(document.getElementById(idMap), {
                center: {
                    lat: lat ? lat * 1 : -29.174615,
                    lng: lng ? lng * 1 : -51.173768
                },
                zoom: 13,
                mapTypeId: "roadmap",
            });

            const input = document.getElementById("search-latlng");
            const searchBox = new google.maps.places.SearchBox(input);

            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            let markers = [];

            //Init
            if (lat && lng) {
                let marker = new google.maps.Marker({
                    map,
                    position: {
                        lat: lat * 1,
                        lng: lng * 1
                    },
                });

                markers.push(marker);
            }

            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                markers.forEach((marker) => {
                    marker.setMap(null);
                });

                markers = [];

                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        return;
                    }

                    const icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25),
                    };

                    const location = place.geometry.location;

                    let marker = new google.maps.Marker({
                        map,
                        icon,
                        title: place.name,
                        position: location,
                        draggable: true,
                    });

                    markers.push(marker);

                    google.maps.event.addListener(marker, 'dragend', function() {
                        var locationMarker = marker.getPosition();
                        inputLat.value = locationMarker.lat();
                        inputLng.value = locationMarker.lng();
                        document.getElementById(idLatLng).value = locationMarker.lat() + "," +
                            locationMarker.lng();
                    });

                    inputLat.value = location.lat();
                    inputLng.value = location.lng();

                    document.getElementById(idLatLng).value = location.lat() + "," + location.lng();

                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });

                map.fitBounds(bounds);
            });
        }
    </script>
@endpush

@push('after-scripts')
    {{-- src="https://polyfill.io/v3/polyfill.min.js?features=default" --}}
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ $google_maps_key }}&loading=async&callback=initGoogleMaps&libraries=places&v=weekly"
        crossorigin="anonymous"></script>
@endpush
