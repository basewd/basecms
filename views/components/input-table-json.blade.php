<?php
$value = @old($id) ? old($id) : $value;
?>

<div class="input-table-json input-table-json-{{ $id }} form-group-{{ $id }}">
    <div class="card">
        <div class="card-header">
            <h5>{{ $label }}</h5>
        </div>

        <div class="card-body">
            <div class="content-table-json"></div>
        </div>

        <div class="card-footer text-right"></div>
    </div>

    <div class="mt-3">
        <input id="{{ $id }}" name="{{ $id }}" value="{{ $value }}" type="hidden"
            class="form-control" readonly />
        <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>
    </div>
</div>

@push('after-scripts')
    <style>
        .input-table-json .card-body {
            overflow-x: auto;
        }

        .input-table-json .td-actions {
            white-space: nowrap;
        }
    </style>

    <script src="{{ asset('base/js/base-input-table-json.js') }}"></script>

    <script>
        InputTableJson("{{ $id }}", {!! json_encode($definition) !!}, '{!! addcslashes($value, "'") !!}', {!! @json_encode($options) !!});
    </script>
@endpush
