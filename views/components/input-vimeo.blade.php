<div class="input-vimeo form-group-{{ $id }} input-vimeo-{{ $id }} mb-3">
    <div class="form-group">
        <label for="{{ $id }}">{{ $label }}</label>
        <input id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" type="text"
            class="form-control" />
        <small class="text-muted">{{ __('Ao colar uma url do Vimeo, é extraído a código do vídeo.') }}</small>

        <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>
    </div>

    <div class="input-vimeo-iframe">
        <iframe width="640" height="360" frameborder="0" allowfullscreen title="Vimeo"></iframe>
    </div>
</div>

@push('after-scripts')
    <script>
        var classInputVimeo = ".input-vimeo-{{ $id }}";
        var idInput = "#{{ $id }}";

        function getVimeoId(url) {
            var m = url.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
            return m ? m[2] || m[1] : null;
        }

        $(idInput).on('change keyup blur', function() {
            var val = $(this).val();

            var iframe = $(classInputVimeo).find(".input-vimeo-iframe iframe");

            var videoId = val.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
            videoId = videoId ? videoId[2] || videoId[1] : val;

            var embed = 'https://player.vimeo.com/video/' + videoId;
            if (iframe.attr('src') != embed && videoId) {
                $(classInputVimeo).find(".input-vimeo-iframe iframe").attr("src", embed);
            } else if (!videoId) {
                $(classInputVimeo).find(".input-vimeo-iframe iframe").attr("src", null);
            }

            if (val != videoId && videoId) {
                $(this).val(videoId);
            }
        }).change();
    </script>
@endpush
