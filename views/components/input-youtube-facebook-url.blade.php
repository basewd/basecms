<div class="input-yt-fb form-group-{{ $id }} input-yt-fb-{{ $id }} mb-3">
    <div class="form-group">
        <label for="{{ $id }}">{{ $label }}</label>
        <input id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" type="text"
            class="form-control" />
        <small class="text-muted">{{ $helpText }}</small>

        <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>
    </div>

    <div class="input-yt-fb-iframe">
        <iframe width="640" height="480" frameborder="0" allowfullscreen title="Video"></iframe>
    </div>
</div>

@push('after-scripts')
    <script>
        var classInputYtfb = ".input-yt-fb-{{ $id }}";
        var idInput = "#{{ $id }}";

        $(idInput).on('change keyup blur', function() {
            var val = $(this).val();
            var iframe = $(classInputYtfb).find(".input-yt-fb-iframe iframe");
            var video = getVideoDetails(val);
            if (video) {
                if (iframe.attr('src') != video.embed && video) {
                    $(classInputYtfb).find(".input-yt-fb-iframe iframe").attr("src", video.embed);
                    iframe.removeAttr("hidden");
                } else if (!video.videoId) {
                    $(classInputYtfb).find(".input-yt-fb-iframe iframe").attr("src", null);
                }
                if (video && val != video.videoId) {
                    $(this).val(video.videoId);
                }
            } else if (!iframe.attr('src')) {
                iframe.attr('hidden', 'hidden');
            }

        }).change();

        function getVideoDetails(url) {
            let youtubeMatch = url.match(
                /(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)/i
            );
            let facebookMatch = url.match(
                /(?:(?:https?:\/\/(?:www\.)?facebook\.com\/(?:[A-Za-z\d]+\/)?(?:video[s]?(?:\.php)?(?:\/embed)?|watch)\/?(?:\?v(?:ideo_id)?=)?)(\d+))/
            );
            let videoDetails;

            if (youtubeMatch) {
                const ytid = youtubeMatch ? youtubeMatch[7] : url;
                videoDetails = {
                    videoId: ytid,
                    embed: 'https://www.youtube.com/embed/' + ytid
                }
            } else if (facebookMatch) {

                const fbid = facebookMatch ? facebookMatch[1] : url;
                videoDetails = {
                    videoId: fbid,
                    embed: 'https://www.facebook.com/video/embed?video_id=' + fbid
                }

            }
            return videoDetails;
        }
    </script>
@endpush
