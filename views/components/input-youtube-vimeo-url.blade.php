<div class="input-yt-vm form-group-{{ $id }} input-yt-vm-{{ $id }} mb-3">
    <div class="form-group">
        <label for="{{ $id }}">{{ $label }}</label>
        <input id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" type="text"
            class="form-control" />
        <small class="text-muted">{{ $helpText }}</small>

        <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>
    </div>

    <div class="input-yt-vm-iframe">
        <iframe width="640" height="360" frameborder="0" allowfullscreen title="Video"></iframe>
    </div>
</div>

@push('after-scripts')
    <script>
        function getEmbedByUrl(url) {
            let youtubeMatch = url.match(
                /^.*((youtu.be\/|youtube.com.br\/)|(v\/)|(\/u\/\w\/)|(embed\/|shorts\/)|(watch\?))\??v?=?([^#&?]*).*/);
            let vimeoMatch = url.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
            let videoId;

            if (youtubeMatch) {
                videoId = youtubeMatch ? youtubeMatch[7] || youtubeMatch[1] : url;
                return 'https://www.youtube.com/embed/' + videoId;
            } else if (vimeoMatch) {
                videoId = vimeoMatch ? vimeoMatch[2] || vimeoMatch[1] : url;
                return 'https://player.vimeo.com/video/' + videoId;
            }

        }
        var classInputYtVm = ".input-yt-vm-{{ $id }}";
        var idInput = "#{{ $id }}";
        $(idInput).on('change keyup blur', function() {
            var val = $(this).val();
            var iframe = $(classInputYtVm).find(".input-yt-vm-iframe iframe");
            var embed = getEmbedByUrl(val);
            if (embed) {
                if (iframe.attr('src') != embed && embed) {
                    $(classInputYtVm).find(".input-yt-vm-iframe iframe").attr("src", embed);
                } else if (!embed) {
                    $(classInputYtVm).find(".input-yt-vm-iframe iframe").attr("src", null);
                }
                if (val != embed && embed) {
                    $(this).val(embed);
                    iframe.removeAttr('hidden');
                }
            } else {
                iframe.attr('hidden', 'hidden');
            }

        }).change();
    </script>
@endpush
