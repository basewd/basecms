<div class="input-yt form-group-{{ $id }} input-yt-{{ $id }} mb-3">
    <div class="form-group">
        <label for="{{ $id }}">{{ $label }}</label>
        <input id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" type="text"
            class="form-control" />
        <small
            class="text-muted">{{ __('Ao colar uma Url ou Id de vídeo do YouTube, é extraído a código do vídeo.') }}</small>

        <div class='invalid-feedback' data-aire-component='errors' data-aire-for='{{ $id }}'></div>
    </div>

    <div class="input-yt-iframe">
        <iframe width="640" height="360" frameborder="0" allowfullscreen title="Video"></iframe>
    </div>
</div>

@push('after-scripts')
    <script>
        $("#{{ $id }}").on('change keyup blur', function() {
            var classInputYouTube = ".input-yt-{{ $id }}";
            
            var val = $(this).val();
            var iframe = $(classInputYouTube).find(".input-yt-iframe iframe");
            
            var videoId = val.match(
                /^.*((youtu.be\/|youtube.com.br\/)|(v\/)|(\/u\/\w\/)|(embed\/|shorts\/)|(watch\?))\??v?=?([^#&?]*).*/
            );
            videoId = videoId ? videoId[7] || videoId[1] : val;
            var embed = 'https://www.youtube.com/embed/' + videoId;
            if (iframe.attr('src') != embed && videoId) {
                iframe.attr("src", embed);
                iframe.parent().removeClass("d-none");
            } else if (!videoId) {
                iframe.attr("src", null);
                iframe.parent().addClass("d-none");
            }

            if (val != videoId && videoId) {
                $(this).val(videoId);
            }
        }).change();
    </script>
@endpush
