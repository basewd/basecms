<div>
    <input id="radio-{{ $id }}" type='radio' {{ $value ? 'checked' : '' }} />
</div>

<script>
    $("#radio-{{ $id }}").change(function() {
        var url = "{{ $url }}";
        var field = "{{ $field }}";
        var checked = $(this).is(":checked");

        $.post(url, {
            field: field,
            checked: checked,
            _token: $('meta[name=csrf-token]').attr('content')
        }, function(v) {
            $("table").DataTable().ajax.reload(null, false);
        });
    });
</script>
