{{-- <ul> --}}
<li class="c-sidebar-nav-item">
    {{-- <ul> --}}
<li class="c-sidebar-nav-dropdown {{ Route::is($activeRoute) ? 'c-show' : '' }}">
    <x-utils.link href="{{ @$urlIndex ? $urlIndex : $urlCreate }}" icon="c-sidebar-nav-icon {{ $icon }}"
        iconLabel="{{ $iconLabel }}" class="c-sidebar-nav-dropdown-toggle" text="{{ $titlePlural }}" />

    <ul class="c-sidebar-nav-dropdown-items">
        @if (@$urlIndex)
            <li class="c-sidebar-nav-item">
                <x-utils.link href="{{ $urlIndex }}" class="c-sidebar-nav-link"
                    icon="c-sidebar-nav-icon c-icon cil-columns" text="{{ __('Lista') }} - {{ $title }}" />
            </li>
        @endif

        @if (@$urlCreate)
            <li class="c-sidebar-nav-item">
                <x-utils.link href="{{ $urlCreate }}" class="c-sidebar-nav-link"
                    icon="c-sidebar-nav-icon c-icon cil-plus" text="{{ __('Cadastro') }} - {{ $title }}" />
            </li>
        @endif

        @if (@$urlEdit && Route::is($urlEdit))
            <li class="c-sidebar-nav-item">
                <x-utils.link href="#" class="c-sidebar-nav-link" icon="c-sidebar-nav-icon c-icon cil-plus"
                    text="{{ __('Edição') }} - {{ $title }}" active="c-active" />
            </li>
        @endif

        @yield('items')
    </ul>
</li>
{{-- </ul> --}}
</li>
{{-- </ul> --}}
