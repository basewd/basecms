{{-- <ul> --}}
<li class="c-sidebar-nav-item">
    <x-utils.link :active="activeClass(Route::is($activeRoute), 'c-active')" class="c-sidebar-nav-link" text="{{ $title }}" href="{{ $url }}"
        icon="c-sidebar-nav-icon c-icon {{ $icon }}" iconLabel="{{ $iconLabel }}" />
</li>
{{-- </ul> --}}
