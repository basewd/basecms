<div class="py-4">
    <button class="btn btn-danger btn-sm btn-multiple-select-table" data-action="{{ $action }}"
        data-table-selector="{{ $tableSelector }}" data-lang-text="{{ @$langText }}"
        data-lang-confirm="{{ @$langConfirm }}" data-lang-cancel="{{ @$langCancel }}">
        {{ $label }}
    </button>
</div>

@push('after-scripts')
    <script src="{{ asset('base/js/base-table-multiple-select.js') }}"></script>
@endpush
