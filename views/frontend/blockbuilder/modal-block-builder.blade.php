@if (request()->blockbuilder || request()->is('admin/*') || request()->is('livewire/*'))
    @php
        $pageEdit = route('admin.pages.edit', ['page' => '@ID@', 'isModal' => 1]);
        $blockEdit = route('admin.blocks.edit', ['block' => '@ID@', 'isModal' => 1]);
        if (@$keys) {
            $blockEdit = route('admin.blocks.editGroupKeys', ['block' => '@ID@', 'keys' => @$keys, 'isModal' => 1]);
        }
        
        $blockCreate = route('admin.blocks.selectComponent', ['isModal' => 1, 'parentBlockId' => '@PARENT_BLOCK_ID@']);
        if (@$keys) {
            $blockCreate = route('admin.blocks.selectComponentGroupKeys', ['isModal' => 1, 'parentBlockId' => '@PARENT_BLOCK_ID@', 'keys' => @$keys]);
        }
    @endphp

    <div tabindex="-1" aria-hidden="true" class="modal-block-builder hidden">
        <div class="modal-backdrop-bb" onclick="window.modalBlockBuilderClose(true)"></div>
        <div class="modal-content">
            <iframe id="iframe-block-builder" class="w-full" noborder data-page-src="{{ $pageEdit }}"
                data-edit-src="{{ $blockEdit }}" data-create-src="{{ $blockCreate }}" title="Blocks">
                <p>Your browser does not support iframes.</p>
            </iframe>
        </div>
    </div>

    @push('after-scripts')
        <script src="{{ asset('base/blockbuilder/base-blockbuilder.js') }}"></script>
        <script src="{{ asset('base/blockbuilder/base-iframe-modal.js') }}"></script>

        <script>
            setInterval(function() {
                autosizeIframeModal('#iframe-block-builder');
            }, 60);
        </script>
    @endpush
@endif
