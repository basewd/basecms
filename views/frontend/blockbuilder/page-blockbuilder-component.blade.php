@extends('frontend.layouts.app')

{{-- 
 1. Retorna o componente passado por parâmetro, apenas para validação
 2. Retorna componentes em modo "draft"
--}}

@php
    $isBlockBuilder = isset(request()->blockbuilder) || request()->is('admin/*');
@endphp

@section('app-content')
    <div id="blockcomponent" style="{{ @$style }}">
        @if ($isBlockBuilder && $showEdit)
            <div class="blockbuilder-page-edit" style="right:42px; top:1px;">
                <div class="btn-blockbuilder btn-blockbuilder-show-hidden" onclick="window.toggleBlockBuilderEdit()"></div>
            </div>
        @endif

        {{ $block ? \BaseCms\BaseBlockBuilder\BlockBuilder::renderBlock($block, 0, [], null, $isBlockBuilder)->render() : null }}
    </div>
@endsection
