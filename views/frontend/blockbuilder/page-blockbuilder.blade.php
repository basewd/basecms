@extends('frontend.layouts.page')

@if (@$page->fl_no_indexable)
    @section('metaRobots')
        <meta name="robots" content="noindex, nofollow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    @endsection
@endif

@php
    $isBlockBuilder = isset(request()->blockbuilder) || request()->is('admin/*');
@endphp

@section('content')
    <x-frontend.seo.metatags :meta_title="$page->meta_title" :meta_description="$page->meta_description" :meta_image="$page->meta_image" :meta_all="$page->meta_all" />

    @if ($isBlockBuilder)
        <div class="blockbuilder-page-edit">
            <div class="btn-blockbuilder btn-blockbuilder-show-hidden" onclick="window.toggleBlockBuilderEdit()"></div>
            <div class="btn-blockbuilder" onclick="window.modalBlockBuilderPage(event,{{ $page->id }})">✎</div>
            <div class="btn-blockbuilder" onclick="window.modalBlockBuilder(event,null,{{ $block->id }})">+</div>
        </div>
    @endif

    @php
        $renderBlock = $block ? \BaseCms\BaseBlockBuilder\BlockBuilder::renderBlock($block, 0, [], null, $isBlockBuilder) : null;
    @endphp

    {{ $renderBlock ? $renderBlock->render() : null }}
@endsection
